﻿// JScript File
var dragRevert = true;
var Dragid, Dropid;
var d1, d2, d3, d4, d5;
var d1Title, d2Title, d3Title, d4Title, d5Title;
var nDrop = 0;

$(document).ready(function(){

	InitDrag();
});

    function InitDrag() {

        $(".ui-draggable").draggable({ containment: '.ui_slides', stack: ".drag", revert: function () { if (dragRevert == true) { return true; } else { dragRevert = true; } } });

        if (/iphone|ipod|ipad|android/i.test(navigator.userAgent)) {
            init();
        }
      
    }
    
function init(){
	var a=0;
	$(".drag").each(function(){
		var b=$(this).attr("id");
		var c=document.getElementById(b);
		a++;
		c.addEventListener("touchstart",touchHandler,true);
		c.addEventListener("touchmove",function(e){e.preventDefault();},true);
		c.addEventListener("touchmove",touchHandler,true);
		c.addEventListener("touchend",touchHandler,true);
		c.addEventListener("touchcancel",touchHandler,true);
	});
}

function touchHandler(a){
	var b=a.changedTouches,c=b[0],d="";
	switch(a.type){
		case"touchstart":
			d="mousedown";
			break;
		case"touchmove":
			d="mousemove";
			break;
		case"touchend":
			d="mouseup";
			break;
		default:
			return
	}
	var e=document.createEvent("MouseEvent");
	e.initMouseEvent(d,true,true,window,1,c.screenX,c.screenY,c.clientX,c.clientY,false,false,false,false,0,null);
	c.target.dispatchEvent(e);
}


$('.ui_create').on('click', function () {
    $(".drop").each(function (ej) {
        ej = ej + 1;
		t1 = $('#d'+ej+' li').html();

		switch (ej){
		case 1:
			d1Title = stripTitle(t1);
			d1 = $(this).find('img').attr('id');
			break;
		case 2:
			d2Title = stripTitle(t1);
			d2 = $(this).find('img').attr('id');
			break;
		case 3:
			d3Title = stripTitle(t1);
			d3 = $(this).find('img').attr('id');
			break;
		case 4:
			d4Title = stripTitle(t1);
			d4 = $(this).find('img').attr('id');
			break;
		case 5:
			d5Title = stripTitle(t1);
			d5 = $(this).find('img').attr('id');
			break;
		}
	});
	
});

function stripTitle(n){
	t1 = n.search("</strong>");
	return n.substring(8,t1);
}

$('.drop').on('drop', function () {
    $(this).droppable('disable').css('opacity','1');
	nDrop++;												 // count until all are filled
	if (nDrop == 5){
		$('#createButton').removeClass("ui-state-disabled"); // all filled
	}
});

$('#createButton').on("click",function(e){
	if ($(this).hasClass('ui-state-disabled')) {			//if disabled SHUT DOWN clicking
		e.preventDefault();
		e.stopImmediatePropagation();
	}
});


$('.ui_reset').on('click', function () {
	//Must clear to null for the popup code to work properly
    d1 = null;
	d2 = null;
	d3 = null;
	d4 = null;
	d5 = null;
	
	d1Title = null;
	d2Title = null;
	d3Title = null;
	d4Title = null;
	d5Title = null;
	
    $('.drop').droppable('enable');
	nDrop = 0;
	$('#createButton').addClass("ui-state-disabled");

});
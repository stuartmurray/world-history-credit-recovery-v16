﻿var count = 0;
var currentSlide = 0;
$(document).ready(function () {
     preload(['interactives/08_00_01/08_00_01a/imgs/08_00_01_a.jpg',
        'interactives/08_00_01/08_00_01a/imgs/08_00_01_b.jpg',
        'interactives/08_00_01/08_00_01a/imgs/08_00_01_c.jpg',
        'interactives/08_00_01/08_00_01a/imgs/08_00_01_d.jpg', ]);
		
     countmessage();
     $('#imgPlay').live('click', function () {
         $('.playerOver').css('display', 'none');
         $('#mep_0').find('.mejs-play').find('button').trigger('click');
		currentSlide = 1;
     });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/08_00_01/08_00_01a/audio/08_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        $('<img/>')[0].src = this;
    });
}

function showBkgnd() {
    if (currentSlide < 6) {
        $('.ui_interactive').css('background', 'url("interactives/08_00_01/08_00_01a/imgs/08_00_01_a.jpg") no-repeat');
        $('.copytext').html("&copy; 2012 Panoramic Images/Image Quest");
        $('.ui_interactive').attr("title", "Photo of Red Square. Three large castle-like buildings are on the edges of the area, but the middle is completely empty and outlined with trees. Hundreds of people are walking around. ");
    }
    else if (currentSlide >= 6 && currentSlide < 10) {
        $('.ui_interactive').css('background', 'url("interactives/08_00_01/08_00_01a/imgs/08_00_01_b.jpg") no-repeat');
        $('.copytext').html("&copy; 2012 The Associated Press");
        $('.ui_interactive').attr("title", "Photo of Times Square. Large billboards and screens are on every visible building from the bottom to the top. The streets are filled with people and vehicles. ");
    } else if (currentSlide >= 10 && currentSlide < 14) {
        $('.ui_interactive').css('background', 'url("interactives/08_00_01/08_00_01a/imgs/08_00_01_c.jpg") no-repeat');
        $('.copytext').html("&copy; 2012 The Associated Press");
        $('.ui_interactive').attr("title", "Photo of a woman walking down the street in Russia. A castle-like building is in the background and a woman is leaning on a wall on the left of the building and bending over at the waist. Other people are walking in the background as well. ");
    } else {
        $('.ui_interactive').css('background', 'url("interactives/08_00_01/08_00_01a/imgs/08_00_01_d.jpg") no-repeat');
        $('.copytext').html("&copy; 2012 The Associated Press");
        $('.ui_interactive').attr("title", "Photo of a sky view of the World Trade Center being crashed into on September 11, 2001. The Empire State Building is in the front of the picture, and the two towers are smoking behind it. ");
    }
};

function countmessage() {
	showBkgnd();
	switch (currentSlide){
	case 0:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! It's good to see you again. Soo-jin is here too.");
		break;
	case 1:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! It's good to see you again. Soo-jin is here too.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! This is Red Square in Moscow. What do you think?");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("It's really a magnificent public place. From this angle you can see how incredibly large the square is. Can you think of anything like it in the United States?");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("We have many large parks and public green areas. But unlike Moscow, the centers of our important cities are cluttered with tall buildings packed with office spaces and businesses.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("That's right, Soo-jin. And this difference isn't a coincidence. While Red Square pre-dates the Russian Revolution, the concept of a city centered around a public space dedicated to the glorification of the people was a physical manifestation of the communist ideology.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("The same is true for the United States. Think of New York. In many respects, it's the corporate center of the country and of the world. But it has very little open public space outside of Central Park. Even Times Square is surrounded by the modern glitz of advertising.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 7:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("The differences between these two squares stand as testament to the ideological differences that fueled the Cold War for years.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 8:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("The United States saw itself as the champion of the free world. It touted its powerful economy as a natural byproduct of free and fair elections, individual liberty, and a restrained federal government.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 9:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("The Soviet Union presented itself as the defender of the downtrodden, the champion of those held down by the capitalist structures.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 10:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("But when the Cold War ended with the Soviet Union's collapse, the once great champion of the world was left to fend for itself.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 11:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("The United States, however, struggled to find a new role. It could no longer compare itself approvingly against the corruption of the Soviet Union.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 12:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("For a while it seemed like all previous struggles were over.The world had moved from feudalism to autocracy to revolution to the exaltation of liberal democracy.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 13:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("The United States found itself the undisputed leader of the world. For the first time in history, the levers of justice were controlled by a nation that, at least rhetorically, seemed to believe in democracy for all nations and all people.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 14:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Suddenly, on one bright fall morning, the United States and the world discovered that history was not at an end.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 15:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("That's right, Soo-jin. In fact, the Cold War had come back to haunt the United States right at the moment it thought it had escaped from it.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 16:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Osama bin Laden, once supported materially by the United States, had been an integral part of America's victory over the Soviets in Afghanistan. But as the United States looked toward a bright future where liberal democracy and free-market capitalism ruled the day, Osama bin Laden decided to remind the world he was still around.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 17:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("As you read the lessons in this module, pay attention to how ideology has shaped the post-war world. Be sure to look at how the end of the war helped to shape those ideologies.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 18:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("So, now that Ali and I have gotten you excited about the post-war world and the last act, let's go and take a closer look.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
    }

}
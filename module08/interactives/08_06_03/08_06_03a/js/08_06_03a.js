﻿var count = 0;
$(document).ready(function () {

    $('.customPopUp').hide();
    $(".common").bind('click', function () {
        if ($(this).css('opacity') == 1 || count == 1) {
            return;
        }
        count = 1;
        $('.Entire').css('opacity', '0.4');
        var temp = $(this).attr('id');
        var temp1 = temp.substring(6, 5);
        console.log(temp1);
        var temp1 = parseInt(temp1) + 1;
        $('#block' + temp1).css('opacity', '0').css('cursor', 'pointer'); 
        if ($(this).attr('id') == "block1") {
            $('.customPopUp .cont').html("<div class='popImage'><img src='interactives/08_06_03/08_06_03a/images/08_06_03a_2.jpg' alt='Image of a cotton plant in the field.' title='Image of a cotton plant in the field.'></div>" + "<div class='copy'> Public Domain</div>" + "<div class='hd'>Step 1. Raw Material</div>" + "<div class='t_one'>Most T-shirts are 100 percent cotton. The life of our T-shirt begins in the fields of Texas. This cotton is ready for harvest, which will be completed by harvesting machines. One acre of Texas-grown cotton can be used to make 1,200 T-shirts.</div>");
        }
        else if ($(this).attr('id') == "block2") {
            $('.customPopUp .cont').html("<div class='popImage'><img src='interactives/08_06_03/08_06_03a/images/08_06_03a_3.jpg' alt='Picture of three ships at sea, yellow sky in the background.' title='Picture of three ships at sea, yellow sky in the background.'></div>" + "<div class='copy'> Public Domain</div>" + "<div class='hd'>Step 2. Transport</div>" + "<div class='t_one'>The cotton is transported from Texas to Malaysia on large ocean-crossing cargo ships. Once it arrives in Malaysia, the cotton is spun into yarn and dyed a variety of colors. The colored yarn is then knit into cloth, wrapped onto bolts, and sent to Hong Kong.</div>");
        } else if ($(this).attr('id') == "block3") {
            $('.customPopUp .cont').html("<div class='popImage'><img src='interactives/08_06_03/08_06_03a/images/08_06_03a_4.jpg' alt='Diagram of the construction of a T-shirt, with parts labeled in the Magyar language.' title='Diagram of the construction of a T-shirt, with parts labeled in the Magyar language.'></div>" + "<div class='copy'> Public Domain</div>" + "<div class='hd'>Step 3. Design</div>" + "<div class='t_one'>Once the bolts of cloth arrive in Hong Kong, they are cut into pieces, following a design that originated in Hungary. (The language shown here is Magyar, a Hungarian language.) Textile cutting is low-tech, with saws and hand scissors, not the high-tech lasers found in the U.S. The cut pieces are then sent to a factory in China.</div>");
        } else if ($(this).attr('id') == "block4") {
            $('.customPopUp .cont').html("<div class='popImage'><img src='interactives/08_06_03/08_06_03a/images/08_06_03a_5.jpg' alt='Chinese street scene with two large buildings on either side of the street. Loading trucks are parked on the street.' title='Chinese street scene with two large buildings on either side of the street. Loading trucks are parked on the street.'></div>" + "<div class='copy'> Public Domain</div>" + "<div class='hd'>Step 4. Production</div>" + "<div class='t_one'>The T-shirt pieces then arrive at this Chinese textile factory near Guangzhou, China. Here, the pieces are sewn by workers, usually women, who are seated in front of small sewing machines. Each worker completes only one part of the shirt: the sleeve, collar, or hem. When the finished T-shirts pass quality inspection, they are sent in boxes to a Chinese export company.</div>");
        } else if ($(this).attr('id') == "block5") {
            $('.customPopUp .cont').html("<div class='popImage'><img src='interactives/08_06_03/08_06_03a/images/08_06_03a_6.jpg' alt='Eight T-shirts of different colors hanging on clothes hangers on a rack, shown from above. ' title='Eight T-shirts of different colors hanging on clothes hangers on a rack, shown from above. '></div>" + "<div class='copy'> Public Domain</div>" + "<div class='hd'>Step 5. Retail Sale</div>" + "<div class='t_one'>Politics plays a large part in who can make T-shirts, how many they can make, and who can buy the wholesale products. To reduce the frequency of &quot;sweat shops&quot; and improve working conditions, every country can only produce a certain number of a given textile product. National leaders and the WTO haggle over quotas and regulations. Our T-shirt that was assembled in China is shipped back to the United States for retail sale. The retailer (paying wholesale prices) bought 12 T-shirts for $13. You (paying retail prices) buy one of those shirts for $12.99.</div>");
        } else if ($(this).attr('id') == "block6") {
            $('.customPopUp .cont').html("<div class='popImage'><img src='interactives/08_06_03/08_06_03a/images/08_06_03a_7.jpg' alt='Roll of gray foam rolled up, sitting on a white table with a white curtain in the background.' title='Roll of gray foam rolled up, sitting on a white table with a white curtain in the background.'></div>" + "<div class='copy'> Public Domain</div>" + "<div class='hd'>Step 6. Recycled</div>" + "<div class='t_one'>Once you're tired of your T-shirt and drop it into a donation bin at a thrift shop, a new journey begins. While some T-shirts are resold and worn locally, others continue a more exotic life. If your world-travelling T-shirt is in good shape, it may be stacked into a bale of similar colored shirts and sent to Africa for resale. Or it may be cut into 15-inch squares and sold to a rag manufacturer. If the T-shirt is low quality, the &quot;shoddy&quot; will be sold for one to two cents per pound and re-spun for mattress pads, carpet pads, insulation, or low-grade yarn for new, cheap T-shirts!</div>");
        }
        $('.customPopUp').show();
        $('.close1').unbind('click').bind('click', function () {
            $('.customPopUp').hide();
            count = 0;
            $('.Entire').css('opacity', '');

        });
    });
});
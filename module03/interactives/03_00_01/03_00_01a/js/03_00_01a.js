﻿var count = 1;
var currentSlide = 0;
$(document).ready(function () {
    preload(['interactives/03_00_01/03_00_01a/imgs/03_00_01_a.jpg',
        'interactives/03_00_01/03_00_01a/imgs/03_00_01_b.jpg',
        'interactives/03_00_01/03_00_01a/imgs/03_00_01_c.jpg' ]);
    countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click'); //start audio player mep_0
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden" ); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/03_00_01/03_00_01a/audio/03_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        $('<img/>')[0].src = this;
    });
}

function showBkgnd() {
    if (count < 5) {
        $('.ui_interactive').css('background', 'url("interactives/03_00_01/03_00_01a/imgs/03_00_01_a.jpg") no-repeat');
        $('.common').attr("title", "History time travelers Ali and Soo-Jin");
    }
    else if (count >= 5 && count < 8) {
        $('.ui_interactive').css('background', 'url("interactives/03_00_01/03_00_01a/imgs/03_00_01_b.jpg") no-repeat');
        $('.common').attr("title", "History time travelers Ali and Soo-Jin");
    } else {
        $('.ui_interactive').css('background', 'url("interactives/03_00_01/03_00_01a/imgs/03_00_01_c.jpg") no-repeat');
        $('.common').attr("title", "History time travelers Ali and Soo-Jin");
    }
};

function countmessage() {
	showBkgnd();
	switch (currentSlide){
	case 0:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It´s good to see you again. Ali is here too.");
		break;
	case 1:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It´s good to see you again. Ali is here too.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! Welcome to Africa! What do you think?");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Have you ever heard the saying, &quot;I've been from here to Timbuktu&quot;? If you have, you know that the person speaking hasn't really been to Africa. In reality, that person is using the name Timbuktu to mean somewhere far, far away.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("That´s right. But as you can see, we´re actually in Timbuktu. And, as you will learn in this module, the association of a place far away with the name Timbuktu is especially significant. You see, Timbuktu seems so far away not because it is geographically far (it´s actually closer than Italy). Rather, it seems far away because it is so unfamiliar.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("You might be asking yourself, why is it so unfamiliar? Well, the reasons for this are really quite complicated. But much of it has to do with the fact that we haven´t preserved the history of Africa in the same way we have for Europe.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("It´s really quite unfortunate, Soo-jin. The historical record in Africa has mostly been lost because, until relatively recently, people haven't thought it very important to preserve the history of civilizations in Africa.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 7:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Unfortunately, some of the problems are due to racism. You see, many Europeans just didn´t value the history of Africa. In some cases, the ideas of African racial inferiority were so strong that the many accomplishments of ancient Africa were just never recognized.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 8:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("But things are changing now. Historians are now excited about Africa again. And we´re learning new things about Africa all the time.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 9:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That´s right, Ali. We're also learning about different methods of historical preservation like oral histories. From studying African history, we learn that some cultures in Africa have preserved their own history through telling epic stories orally.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 10:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("This type of historical preservation may seem strange to you. But studying it can reveal much about the history of Africa in much the same way that written documents do. It's through these oral histories that we know about some of the great warrior heroes of Ancient Africa.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 11:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("There are also many artifacts and physical remnants of Ancient African civilizations. These artifacts can be anything from broken pieces of pottery buried under the sands of West Africa to the mysterious ruins of Great Zimbabwe.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 12:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("So, now that Soo-jin and I have gotten you excited about learning more about Africa, let´s go and take a closer look.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
    }
}
$(document).ready(function (e) {
    var dropCntAbraham = 0;
    var dropCntSin = 0;
    var correctCounter = 0;
    var dropCounter = 0;
    dropCounter = $('.droppable').length;

    $('.droppable').bind('drop', function (event, ui) {
        if ($(this).attr('data-label') == $(ui.draggable).attr('data-label')) {
            correctCounter++;
            if ($.trim($(ui.draggable).text()) == "irrigation") {
                dropCntAbraham++;
                $(this).find('li').each(function () {
                    if ($(this).text() == $.trim($(ui.draggable).text())) {
                        $(this).remove();
                        dropCntAbraham--;
                        correctCounter--;
                    }
                });
                if (dropCntAbraham < 3) {
                    setTimeout(function () {
                        $('.draggable').each(function () {
                            if ($.trim($(this).text()) == "irrigation") {
                                $(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
                                $(this).attr('aria-disabled', false);
                                $(this).removeAttr('style');
                                $(this).css('display', 'inline-block');
                                $(this).draggable('enable');
                            }
                        });
                    }, 500);
                }
            }
        }
        else if ($(this).attr('data-label2') == $(ui.draggable).attr('data-label') || $(this).attr('data-label3') == $(ui.draggable).attr('data-label')) {
			correctCounter++;
            if ($.trim($(ui.draggable).text()) == "astronomy") {
                dropCntSin++;
                $(this).find('li').each(function () {
                    if ($(this).text() == $.trim($(ui.draggable).text())) {
                        $(this).remove();
                        dropCntSin--;
                        correctCounter--;
                    }
                });

                $(this).find('ul').append('<li>' + $.trim($(ui.draggable).text()) + '</li>');
                $(this).addClass('ui-state-highlight');
                $(ui.draggable).draggable('disable').css('display', 'none');
                setTimeout(function () {
                    $('.droppable').each(function (index, element) {
                        $(this).removeClass('ui-state-highlight');
                    });
                }, 750);
                if (dropCntSin < 2) {
                    setTimeout(function () {
                        $('.draggable').each(function () {
                            if ($.trim($(this).text()) == "astronomy") {
                                $(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
                                $(this).attr('aria-disabled', false);
                                $(this).removeAttr('style');
                                $(this).css('display', 'inline-block');
                                $(this).draggable('enable');
                            }
                        });
                    }, 500);
                }
            }
            else {
                $(this).find('ul').append('<li>' + $.trim($(ui.draggable).text()) + '</li>');
                $(this).addClass('ui-state-highlight');
                $(ui.draggable).draggable('disable').css('display', 'none');
                setTimeout(function () {
                    $('.droppable').each(function (index, element) {
                        $(this).removeClass('ui-state-highlight');
                    });
                }, 750);
            }
        }
		
        if (correctCounter >= 12) {
            $('.ui-dialog').show().css({ 'margin-left': '140px', 'margin-top': '102px' });
        }
        else {
            $('.ui-dialog').hide();
        }
    });
	
	$('.ui_reset').bind('click', function() {
		correctCounter = 0;
	});
});
﻿var count = 0;
$(document).ready(function () {

    $('.customPopUp').hide();
    $('.overlay').hide();
    $('.hiddentool').hide();
   // $('.overlay').hide();
    //  $(function () {

    $(".common").bind('click', function () {


        if ($(this).css('opacity') <= 0.4 || count == 1) {
            return;
        }
        count = 1;
        var temp = $(this).attr('id');
        var temp1 = temp.substring(3, 4 + 1)
        var temp1 = parseInt(temp1) + 1;
        $('#Img' + temp1).css('opacity', '1').css('cursor', 'pointer'); ;
        $('#Img0' + temp1).css('opacity', '1');
        $('#Img01').css('opacity', '1');

        if ($(this).attr('id') == "Img1") {
            $('#Img1').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_meroe1h.png" alt="" title="" />');
            $('.customPopUp').css("height", "315px");
            $('.customPopUp .leftimg').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_Meroe.jpg" alt="Pyramids at Meroe" title="Pyramids at Meroe"/><br/>   &copy; Creative Commons Attribution 3.0 Unported');
            $('.customPopUp .rightxt').html('<strong>Meroe:</strong><br/><ul><li>Ancient city settled on Nile</li><li>Kush broke free of Egyptian rule, c.1000 BCE, and moved capital to Meroe, c. 590 BCE</li><li>Dependent on farming and iron-making</li><li>Thrived through trade</li><li>Expanded trade to include Greek, Roman, Arab, and Indian merchants</li><li>Flourished for several hundred years</li><li>Buried kings and queens in pyramids, like the Egyptians</li><li>4th century CE, Axum conquered Meroe</li></ul>').css({ "left": "329px", "width": "254px" });
        } else if ($(this).attr('id') == "Img2") {
            $('#Img2').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_funj1h.png" />');
            $('.customPopUp').css("height", "153px");
            $('.customPopUp .leftimg').html('');
            $('.customPopUp .rightxt').html('<strong>Funj:</strong><br/><ul><li>Ruled area now known as Sudan</li><li>Relied heavily on trade</li><li>Converted to Islam in 1500s CE</li><li>Plagued by internal political conflict</li><li>Taken over by Egypt in the 19th century CE </li></ul>').css({ "left": "17px", "width": "564px" });
        } else if ($(this).attr('id') == "Img3") {
            $('#Img3').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_axum1h.png" />');
            $('.customPopUp').css("height", "400px");
            $('.customPopUp .leftimg').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_Axum.jpg" alt="Image of obelisk" title="Image of obelisk" /><br/>   &copy; 2015 Associated Press');
            $('.customPopUp .rightxt').html('<strong>Axum:</strong><br/><ul><li>Established 1st century CE</li><li>Fueled by trade, became the wealthiest, most influential market city on the Ethiopian coast</li><li>Extended power by conquering neighboring lands</li><li>Utilized trade routed throughout Africa, the Mediterranean, and Asia</li><li>Traded ivory, gold, glass, agricultural, and metal goods</li><li>Christianity brought in during the 4th century CE by missionaries</li><li>320 CE, King Ezan becomes first known Christian king in Africa</li><li>Persian dominance in Arabia cut off Axum&#39;s power, resulting in its decline in the 7th century CE </li></ul>').css({ "left": "316px", "width": "267px" });
        } else if ($(this).attr('id') == "Img4") {
            $('#Img4').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_ethiopia1h.png" />');
            $('.customPopUp').css("height", "315px");
            $('.customPopUp .leftimg').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_Ethiopia.jpg" alt="A  Zagwe king" title="A  Zagwe king" /><br/>   Public Domain');
            $('.customPopUp .rightxt').html('<strong>Ethiopia:</strong><br/><ul><li>Agew people emerged following the fall of Axum</li><li>United much of the region and ruled modern-day Ethiopia, 12th and 13th centuries CE</li><li>Rebellion resulting in the Solomonic Dynasty would bring Ethiopia into modern age</li><li>Solomonic rulers claimed descent from the Biblical King Solomon</li><li>Dependent upon coffee trade</li><li>Threats from Muslim and Portuguese peoples were largely resisted </li></ul>').css({ "left": "296px", "width": "280px" });
        } else if ($(this).attr('id') == "Img5") {
            $('#Img5').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_oromo1h.png" />');
            $('.customPopUp').css("height", "154px");
            $('.customPopUp .leftimg').html('');
            $('.customPopUp .rightxt').html('<strong>Oromo:</strong><br/><ul><li>Primarily herders began raiding neighboring lands looking for more resources beginning in 13th century CE</li><li>Pushed north and west into more fertile land</li><li>Known as fierce warriors, they won a great deal of territory by the 17th century CE</li><li>Dependent on coffee trade to generate wealth </li></ul>').css({ "left": "18px", "width": "564px" });
        } else if ($(this).attr('id') == "Img6") {
            $('#Img6').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_great-zimbabwe1h.png" />');
            $('.customPopUp').css("height", "300px");
            $('.customPopUp .leftimg').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_Zimbabwe.jpg" alt="Ruins in Zimbabwe" title="Ruins in Zimbabwe" /><br/>   &copy; 2012 Jupiterimages Corporation');
            $('.customPopUp .rightxt').html('<strong>Great Zimbabwe:</strong><br/><ul><li>Bantu migration resulted in growth of many cities in Central and South Africa, such as Great Zimbabwe  </li><li>Prospered through the trade of gold, copper, and iron</li><li>Cattle herding and agriculture also contributed to the power</li><li>Stone architecture and pottery became symbols between the 11th and 15th centuries CE</li><li>Declined and largely abandoned in the late 15th century CE </li></ul>').css({ "left": "328px", "width": "264px" });
        } else if ($(this).attr('id') == "Img7") {
            $('#Img7').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_mwenamutapa1h.png" />');
            $('.customPopUp').css("height", "153px");
            $('.customPopUp .leftimg').html('');
            $('.customPopUp .rightxt').html('<strong>Mwenamutapa:</strong><br/><ul><li>Rose around the time as the fall of Great Zimbabwe</li><li>Shona people acquired power in the 14th century CE and ruled for three centuries</li><li>Legend holds that a prince from Great Zimbabwe came in search of salt resources and flourished from the find of it along with copper</li><li>Portuguese invasion occurred in the 17th century CE </li></ul>').css({ "left": "18px", "width": "564px" });
        } else if ($(this).attr('id') == "Img8") {
            $('#Img8').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_kongo1h.png" />');
            $('.customPopUp').css("height", "214px");
            $('.customPopUp .leftimg').html('');
            $('.customPopUp .rightxt').html('<strong>Kongo:</strong><br/><ul><li>Thanks to ancient Bantu migrations, farming and mining established a strong people in the region</li><li>Gold, copper, iron, ceramics, patterned textiles, salt, and dried river fish resulted in prosperous trade</li><li>In the late 14th century CE, King Lukeni Iua Nimi united several territories</li><li>Ruled the region into the 17th century CE</li><li>The Portuguese brought Christianity, resulting in broken ties with the local rulers and trade relationships </li></ul>').css({ "left": "18px", "width": "564px" });
            $('#chkTool').bind('click', function () {
                $('.hiddentool').show();
            });
            $('.close2').bind('click', function () {
                $('.hiddentool').hide();
                if ($('#mep_0 .mejs-button').hasClass('mejs-pause')){
                    $('#mep_0 button').trigger('click');
                }
            });
            counter2 = 1;
        } else if ($(this).attr('id') == "Img9") {
            $('#Img9').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_lunda1h.png" />');
            $('.customPopUp').css("height", "345px");
            $('.customPopUp .leftimg').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_Lunda%20and%20Luba.jpg"  alt="Lunda houses" title="Lunda houses" /><br/>   Public Domain');
            $('.customPopUp .rightxt').html('<strong>Lunda and Luba:</strong><br/><ul><li>Although a mix of African traditions had influence on the region, Bantu was predominate</li><li>Agriculture, fishing, mining, and local crafts contributed to its success</li><li>Small kingdoms were formed and grew stronger through metalworking and the salt trade</li><li>Salt was so important it became currency (money)</li><li>These kingdoms traded with not only African neighbors, but also with Arab Portuguese and Indian merchants</li><li>Both kingdoms lasted well into the 18th century CE </li></ul>').css({ "left": "310px", "width": "278px" });
        } else if ($(this).attr('id') == "Img10") {
            $('#Img10').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_swahili1h.png" />');
            $('.customPopUp').css("height", "180px");
            $('.customPopUp .leftimg').html('');
            $('.customPopUp .rightxt').html('<strong>Swahili:</strong><br/><ul><li>"Swahili" means "of the coast"</li><li>Arab and other traders had been coming to East Africa for centuries</li><li>Port cities had been established along the Indian Ocean</li><li>Descended from the Bantu, interaction with Arabs resulted in the new language of Swahili</li><li>Interaction with the Arab world resulted in a strong Muslim base</li><li>Portuguese conquered the region in 1498 CE </li></ul>').css({ "left": "18px", "width": "564px" });
        } else if ($(this).attr('id') == "Img11") {
            $('#Img11').html('<img src="interactives/03_02_03/03_02_03a/img/03_02_03_bunyoro1h.png" />');
            $('.customPopUp').css("height", "140px");
            $('.customPopUp .leftimg').html('');
            $('.customPopUp .rightxt').html('<strong>Bunyoro-Kitara:</strong><br/><ul><li>Kingdom arose from descendants of the Bantu tribes who made their home around the African Great Lakes</li><li>Area known as modern-day Rwanda, Kenya, and Uganda</li><li>Predominately cattle herders, but salt trade fueled prosperity </li></ul>').css({ "left": "18px", "width": "564px" });
        }
        $('.customPopUp').show();
        $('.overlay').show();
        $('.close1').unbind('click').bind('click', function () {
            $('.customPopUp').hide();
            $('.overlay').hide();
            count = 0;
            $('.hiddentool').hide();

            if ($('#mep_0 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_0 button').trigger('click');
            }
        });
        //counter = 1;
    });
});
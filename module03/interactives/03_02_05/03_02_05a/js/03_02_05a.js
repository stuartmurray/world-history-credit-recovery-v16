﻿$(document).ready(function () {
    $('.divclass').css('display', 'none');
    $(".droppable").bind("drop", function (event, ui) {
        $('.divclass').css('display', 'block');
    });

    $('.ui-icon-closethick').unbind('click').bind('click', function () {
        $('.divclass').css('display', 'none');
    });
    $('.ui-dialog-buttonset').unbind('click').bind('click', function () {
        $('.divclass').css('display', 'none');
    });
});
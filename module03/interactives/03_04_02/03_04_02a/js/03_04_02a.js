﻿$(document).ready(function () {
    $('.customPopDiv').hide();
  //  alert();
    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
    });

    $('.area').unbind('click').bind('click', function () {
        $('.customPopDiv').show();
    //    alert();
        var getId = $(this).attr('id');
        if (getId == 'area1') {
            $('.customPopDiv .copyrights').html('&copy; 2012 The Associated Press').css("left", "31px"); 
            $('.customPopDiv .head1').html('Temple at Tikal').css("left", "83px");
            $('.customPopDiv .popimg').html('<img src="interactives/03_04_02/03_04_02a/img/03_04_02_01.jpg" alt="The Mayan city of Tikal is shown in a photograph. There are many stone temples and buildings surrounded by dense forest. " title="The Mayan city of Tikal is shown in a photograph. There are many stone temples and buildings surrounded by dense forest. " />');
            $('.customPopDiv .content1').html(' The Mayan civilization flourished in Mesoamerica between 250 and 900 CE. Mayan accomplishments include a written language, sculpture, and architectural monuments, including stone pyramids like the one seen here at the site of the ancient city Tikal.');
        } else if (getId == 'area2') {
            $('.customPopDiv .copyrights').html('&copy; Creative Commons Attribution 2.0 Generic license').css("left", "-22px");
            $('.customPopDiv .popimg').html('<img src="interactives/03_04_02/03_04_02a/img/03_04_02_02.jpg" alt="The Incan city of Machu Picchu is shown in a photograph. The city is shown built high up on a mountain with other mountains surround the city. There are many stone buildings." title="The Incan city of Machu Picchu is shown in a photograph. The city is shown built high up on a mountain with other mountains surround the city. There are many stone buildings."  />');
            $('.customPopDiv .head1').html("Macchu Picchu").css("left", "83px"); ;
            $('.customPopDiv .content1').html('The Inca civilization flourished between the 12th and 15th centuries CE. Although its capital was Cuzco, the most famous Incan city might be Machu Picchu, shown here. Incan accomplishments include building suspension bridges and the impressive city of Machu Picchu nestled among mountains.');

        } else if (getId == 'area3') {
            $('.customPopDiv .copyrights').html('Public Domain').css("left", "62px");
            $('.customPopDiv .popimg').html('<img src="interactives/03_04_02/03_04_02a/img/03_04_02_03.jpg" alt="The Aztec city of Tenochtitlan is shown through an artist&apos;s rendering. The city is shown in the center of a lake completely surrounded by water. Boats and canoes approach various parts of the city, while observers from land ride on horseback." title="The Aztec city of Tenochtitlan is shown through an artist&apos;s rendering. The city is shown in the center of a lake completely surrounded by water. Boats and canoes approach various parts of the city, while observers from land ride on horseback."  />');
            $('.customPopDiv .head1').html('Artist Rendering of Tenochtitlan').css("left","32px");
            $('.customPopDiv .content1').html('The Aztecs were the last of the great pre-Columbian civilizations, thriving between 1325 and 1519 CE. They were a rich culture. Although they built several cities-including Tenochtitlan, shown here-the Aztecs were also great farmers and are known for their advanced agricultural techniques. The Aztec empire ended when Spanish conquistadors arrived in Mesoamerica.');
        } 
    });

});
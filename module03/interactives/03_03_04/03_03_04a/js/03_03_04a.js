﻿var count = 0;
$(document).ready(function () {

    $('.customPopUp').hide();


    $(".common").on('click', function () {
        if ($(this).css('opacity') <= 0.4 || count == 1) {
            return;
        }
        count = 1;
        var temp = $(this).attr('id');

        if ($(this).attr('id') == "Img1") {
            $('.img_left1').html('<img src="interactives/03_03_04/03_03_04a/img/03_03_04a_tab_1_1.jpg" alt="Map showing the Andean region in modern Peru." title="Map showing the Andean region in modern Peru."/> <span class="labelTxt">Chavin</span> <span class="labelTxt1">Nazca</span> <span class="labelTxt2">Moche</span>');
            $('.customPopUp').css("height", "345px");
            $('.customPopUp .head').html('<strong>The Chav&iacute;n</strong');
            $('.customPopUp .rightimg').html('<img src="interactives/03_03_04/03_03_04a/img/03_03_04a_i1.jpg" alt="Chavin de Huantar is located at the side of a mountain. The ruins are located on a &apos;step,&apos; a flat surface built into the mountain." title="Chavin de Huantar is located at the side of a mountain. The ruins are located on a &apos;step,&apos; a flat surface built into the mountain."/><br/>   Public Domain');
            $('.customPopUp .imgbottomtxt').html('Chav&iacute;n de Huantar is the major archeological site associated with the ChavÍn civilization. As the first of the Andean civilizations, the ChavÍn influence can be seen in the cultures that followed.').css({ "top": "176px", "left": "292px","width":"284px" });
            $('.customPopUp .leftxt').html('The Chav&iacute;n culture thrived in the area of modern day Peru for over a thousand years between 900-200 BCE. While the culture dominated much of the coast of central Peru, the influence of this culture was felt much further north and south along the coast.<br/><br/>Much of what remains of the Chav&iacute;n culture is preserved at ChavÍn de Huantar. Now merely an archeological site, the stone ruins built into the mountain demonstrate the high level of organization this culture once had.');
        } else if ($(this).attr('id') == "Img2") {
            $('.img_left1').html(' <img src="interactives/03_03_04/03_03_04a/img/03_03_04a_tab_6.jpg" alt="Map showing the Andean region in modern Peru." title="Map showing the Andean region in modern Peru."/> <span class="labelTxt">Chavin</span> <span class="labelTxt1">Nazca</span> <span class="labelTxt2">Moche</span>');
            $('.customPopUp').css("height", "434px");
            $('.customPopUp .rightimg').html('<img src="interactives/03_03_04/03_03_04a/img/03_03_04a_i1_e.jpg" alt="An aerial view of a Nazca line in the shape of a monkey with a curved tail." title="An aerial view of a Nazca line in the shape of a monkey with a curved tail."/><br/> Public Domain');
            $('.customPopUp .imgbottomtxt').html('The Nazca civilization is known for its art, particularly the Nazca lines. This photograph, taken from the air, shows Nazca lines in the shape of a monkey. It is about 360 feet long. No one knows why the Nazca lines were made.').css({ "top": "350px", "left": "292px","width":"284px" });
            $('.customPopUp .head').html('<strong>Nazca</strong');
            $('.customPopUp .leftxt').html('The Nazca were also from what is now modern day Peru. However, this culture did not emerge until after the ChavÍn had mostly disappeared. The Nazca dominated a smaller area of southern coastal Peru between 200 BCE to 600 CE. The Nazca are most famous for the large-scale geological art they left behind. Often referred to as the Nazca lines, these works of art were sometimes many hundreds of feet long and wide. The size of these works had led some to speculate about their possible intentions, giving rise to fantastic stories about alien visitors to the Nazca. However, the exact use for the Nazca lines is still a matter of debate.');
        } else if ($(this).attr('id') == "Img3") {
            $('.img_left1').html('<img src="interactives/03_03_04/03_03_04a/img/03_03_04a_tab_5.jpg" alt="Map showing the Andean region in modern Peru." title="Map showing the Andean region in modern Peru."/> <span class="labelTxt">Chavin</span> <span class="labelTxt1">Nazca</span> <span class="labelTxt2">Moche</span>');
            $('.customPopUp').css("height", "440px");
            $('.customPopUp .rightimg').html('<img src="interactives/03_03_04/03_03_04a/img/03_03_04a_i1_f.jpg" alt="Archeologists stand amidst the ruins of the Temple of the Moon at Moche Valley. The Temple appears to have been made up of many rooms." title="Archeologists stand amidst the ruins of the Temple of the Moon at Moche Valley. The Temple appears to have been made up of many rooms."/><br/>  &copy; 2012 The Associated Press');
            $('.customPopUp .leftxt').html('A contemporary culture of the Nazca was the Moche. The Moche thrived in northern coastal Peru near the border with Ecuador between 400 BCE to 800 CE. Like the ChavÍn culture, much of what is known about the Moche comes from archeology. Of the many remaining ruins, the ruins at Moche Valley are some of the best known. The site is the home of the Moche Temple of the Moon and the Temple of the Sun. The area surrounding the temples appears to have been the site of a large Moche city.');
            $('.customPopUp .head').html('<strong>Moche</strong');
            $('.customPopUp .imgbottomtxt').html('The ruins at Moche Valley are the best-known site of the Moche civilization. The site is the home of the Moche Temple of the Moon and the Temple of the Sun, shown being excavated here. The area surrounding the temples appears to have been the site of a large Moche city.').css({ "top": "365px", "left": "31px", "width": "502px" });
        }


        $('.customPopUp').show();

        $('.close1').off('click').on('click', function () {
            $('.customPopUp').hide();
           
            var temp1 = temp.substring(3, 4 + 1)
            var temp1 = parseInt(temp1) + 1;
            $('#Img' + temp1).css('opacity', '1').css('cursor', 'pointer'); ;
            $('#Img0' + temp1).css('opacity', '1');
            $('#Img01').css('opacity', '1');
           
            count = 0;
        });



    });

});
﻿$(document).ready(function () {
    $('.customPopDiv').hide();


    $('.clickedArea').unbind('click').bind('click', function () {
        $('.customPopDiv').show();
        var getId = $(this).attr('id');
        if (getId == 'area1' || getId == 'area11') {
            $('.customPopDiv .head1').html('Kings and Nobility');
            $('.customPopDiv .content1').html('At the top were the king and his family, who ran the country, followed by a lower level of nobles. They were comprised of the local nobility, who held lesser positions in the government.');
        } else if (getId == 'area2' || getId == 'area22') {
            $('.customPopDiv .head1').html('Freemen');
            $('.customPopDiv .content1').html('Below the nobility were the freemen, the citizens of the empire. The freemen could be Muslim clerics, craftspeople, griots, and artists.');
        } else if (getId == 'area3' || getId == 'area33') {

            $('.customPopDiv .head1').html('Slaves and War Captives');
            $('.customPopDiv .content1').html('Slavery in Songhai was different than the institution that developed later in the Americas. In Songhai, a slave might have a very high position in society or a very low one. As the Songhai armies captured more land, entire towns might be considered slaves, but they remained on their land and farmed it because that was how they could best serve the kingdom.');
        }
   });




});


﻿var count = 0;

$(document).ready(function () {
    $('.customPopUp').hide();
    
    $('.num').unbind('click').bind('click', function () {
        if (count != 0) {
            return;
        }
        count++;
        if ($(this).attr('id') == 'num1') {
            $('.customImg').html(' <img src="interactives/07_04_01/07_04_01a/imgs/07_04_01a_map2.png" alt="" title="" />');
            $('.customPopUp .cont').html("<div class='hd'>Allies before the attack on Pearl Harbor, including colonies and occupied countries:</div>Before the attack on Pearl Harbor, the Allies consisted of Canada, Australia, Russia, China, France, Norway, Poland, Greece, Yugoslavia, India and most of the continent of Africa. Not all of these countries were sovereign; however, a number of them were colonies.");
        } else if ($(this).attr('id') == 'num2') {
            $('.customImg').html(' <img src="interactives/07_04_01/07_04_01a/imgs/07_04_01a_map3.png" alt="" title="" />');
            $('.customPopUp .cont').html("<div class='hd'>Allied countries that entered the war after the Japanese attack on Pearl Harbor:</div>After Pearl Harbor, a number of other countries entered the war on the side of the Allies. These countries included the United States, most of South America, Mexico, Central America and the Caribbean. The countries of Turkey, Persia and Saudi Arabia also joined.");
        } else if ($(this).attr('id') == 'num3') {
            $('.customImg').html(' <img src="interactives/07_04_01/07_04_01a/imgs/07_04_01a_map5.png" alt="" title="" />');
            $('.customPopUp .cont').html("<div class='hd'>Axis Powers and their colonies:</div>The Axis Powers and their colonies included Germany, Austria, Italy, Finland, Czechoslovakia, Romania, Albania, Libya, Manchuria, Japan, part of Indo-China, Ethiopia, Somalia, and Eritrea.");
        } else if ($(this).attr('id') == 'num4') {
            $('.customImg').html(' <img src="interactives/07_04_01/07_04_01a/imgs/07_04_01a_map4.png" alt="" title="" />');
            $('.customPopUp .cont').html("<div class='hd'>Neutral countries during WWII:</div>During WWII there were a number of neutral countries as well. These countries included Spain, Portugal, the Western Sahara, Angola, Afghanistan, Tibet, Sweden, and Mozambique.");
        }
        $('.blurdiv').css('z-index', '2');

        $('.customPopUp').show();
        $('.close1').unbind('click').bind('click', function () {
            count = 0;
            $('.customPopUp').hide();
            $('.customImg').html(' <img src="interactives/07_04_01/07_04_01a/imgs/07_04_01a_map.png" alt="" title="" />');
            $('.blurdiv').css('z-index', '-1');
        });
    });

    preload(['interactives/07_04_01/07_04_01a/imgs/07_04_01a_map.png',
        'interactives/07_04_01/07_04_01a/imgs/07_04_01a_map2.png',
        'interactives/07_04_01/07_04_01a/imgs/07_04_01a_map3.png',
        'interactives/07_04_01/07_04_01a/imgs/07_04_01a_map4.png',
        'interactives/07_04_01/07_04_01a/imgs/07_04_01a_map5.png'
        
 ]);

    function preload(arrayOfImages) {
        $(arrayOfImages).each(function () {
            $('<img/>')[0].src = this;
        });
    }

});
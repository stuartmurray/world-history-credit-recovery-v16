﻿$(document).ready(function () {
    $('.customPopDiv').hide();
    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
    });

    $('.area').unbind('click').bind('click', function () {

        $('.customPopDiv').show();
        $('.area').each(function () {
            $(this).removeClass('active');
        });

        $(this).addClass('active');
        var getId = $(this).attr('id');
        if (getId == 'area1') {
            $('.customPopDiv .head1').html('1882: Germany, Austria-Hungary, and Italy form the Triple Alliance.');
            $('.customPopDiv .img1').html('<img src="interactives/07_01_01/07_01_01a/imgs/07_01_01a_img_01.jpg" alt="" /> ');
            $('.customPopDiv .content1').html('The countries of Germany, Austria-Hungary, and Italy agree to defend the others if any are attacked. This type of treaty creates a series of triggers for full-scale war in Europe.').css("width", "170px").css("margin-left", "390px");
            $('.customPopDiv .copyright1').html('Public Domain <a class="pop" href="interactives/07_01_01/07_01_01a/pop_a/07_01_01a_txta.htm" title="Text Version" ><i class="icon-share"></i></a>').css("top", "312px").css("left", "7px");
        }

        else if (getId == 'area2') {
            $('.customPopDiv .head1').html("1888: Wilhelm II becomes Kaiser (emperor) of Germany.");
            $('.customPopDiv .img1').html('<img src="interactives/07_01_01/07_01_01a/imgs/07_01_01a_img_02.jpg" alt="" />');
            $('.customPopDiv .content1').html('When Wilhelm II becomes Kaiser, he immediately forces Chancellor Otto von Bismarck to resign. He then begins a series of diplomatic failures that would ultimately strain relations with other European countries.').css("width", "275px").css("margin-left", "267px");
            $('.customPopDiv .copyright1').html('Public Domain <a class="pop" href="interactives/07_01_01/07_01_01a/pop_a/07_01_01a_txtb.htm" title="Text Version" ><i class="icon-share"></i></a>').css("top", "312px").css("left", "-50px");

        }
        else if (getId == 'area3') {
            $('.customPopDiv .head1').html("1890: Wilhelm II allows Germany's treaty with Russia to lapse.");
            $('.customPopDiv .img1').html('');
            $('.customPopDiv .content1').html('The treaty had stated that each country would remain neutral if the other one became involved in a war with a third nation.').css("width", "275px").css("margin-left", "13px").css("width", "529px");
            $('.customPopDiv .copyright1').html('');
        }
        else if (getId == 'area4') {
            $('.customPopDiv .head1').html("1894: Russia and France sign the Dual Entente.");
            $('.customPopDiv .img1').html('');
            $('.customPopDiv .content1').html('Under this treaty, each country agrees to defend the other should it be attacked by Germany. Both countries agree to mobilize their militaries if any member of the Triple Alliance mobilizes its forces.').css("width", "275px").css("margin-left", "13px").css("width", "529px");
            $('.customPopDiv .copyright1').html('');
        }
        else if (getId == 'area5') {
            $('.customPopDiv .head1').html("1904: France and Great Britain sign the Entente Cordiale.");
            $('.customPopDiv .img1').html('<img src="interactives/07_01_01/07_01_01a/imgs/07_01_01a_img_03.jpg" alt="" />');
            $('.customPopDiv .content1').html('The agreement settled a number of issues and reduced tensions between the two countries.').css("width", "275px").css("margin-left", "267px");
            $('.customPopDiv .copyright1').html('Public Domain <a class="pop" href="interactives/07_01_01/07_01_01a/pop_a/07_01_01a_txtc.htm" title="Text Version"><i class="icon-share"></i></a>').css("top", "312px").css("left", "-49px");
        }
        else if (getId == 'area6') {
            $('.customPopDiv .head1').html('1911: France and Germany clash over Morocco.');
            $('.customPopDiv .img1').html('<img src="interactives/07_01_01/07_01_01a/imgs/07_01_01a_img_04.jpg" alt="" /> ');
            $('.customPopDiv .content1').html('Germany threatens war. Britain and Italy back France, and Germany backs down.').css("width", "170px").css("margin-left", "390px");
            $('.customPopDiv .copyright1').html('Public Domain <a class="pop" href="interactives/07_01_01/07_01_01a/pop_a/07_01_01a_txtd.htm" title="Text Version" ><i class="icon-share"></i></a>').css("top", "312px").css("left", "18px");
        }

        else if (getId == 'area7') {
            $('.customPopDiv .head1').html("1912: Serbia, Greece, Bulgaria, and Montenegro form the Balkan League.").css("width","511px");
            $('.customPopDiv .img1').html('<img src="interactives/07_01_01/07_01_01a/imgs/07_01_01a_img_05.jpg" alt="" />');
            $('.customPopDiv .content1').html('The league&#39;s combined military forces drive the Ottoman Empire out of Europe.').css("width", "275px").css("margin-left", "267px");
            $('.customPopDiv .copyright1').html('Public Domain <a class="pop" href="interactives/07_01_01/07_01_01a/pop_a/07_01_01a_txte.htm" title="Text Version" ><i class="icon-share"></i></a>').css("top", "312px").css("left", "-40px");
        }
        else if (getId == 'area8') {
            $('.customPopDiv .head1').html("1914: Gavrilo Princip assassinates Archduke Franz Ferdinand.").css("width", "511px");
            $('.customPopDiv .img1').html('<img src="interactives/07_01_01/07_01_01a/imgs/07_01_01a_img_06.jpg" alt="" />');
            $('.customPopDiv .content1').html('The Archduke is the heir to the throne of Austria-Hungary. This assassination triggers a series of reactions and overreactions that culminate with the start of the Great War.').css("width", "275px").css("margin-left", "267px");
            $('.customPopDiv .copyright1').html('Public Domain <a class="pop" href="interactives/07_01_01/07_01_01a/pop_a/07_01_01a_txtf.htm" title="Text Version"><i class="icon-share"></i></a>').css("top", "312px").css("left", "-41px");
        }
    });
    $('.area#area1').trigger('click');
});
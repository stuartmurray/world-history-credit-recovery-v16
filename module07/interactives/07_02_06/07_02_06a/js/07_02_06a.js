﻿var count = 0;
var currentSlide = 1;
$(document).ready(function () {

    $(".messageBox").hide();
	countmessage();
    $('#imgPlay').on('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click'); //start audio player mep_0
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        $(".messageBox").hide();
        currentSlide = 1;
		count = 1;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/07_02_06/07_02_06a/audio/07_02_06_0"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
		console.log("currentSlide = " + currentSlide);
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}



function countmessage() {
	switch (currentSlide) {
	case 1:
        $(".calloutman1").hide();
        $(".calloutman2").hide();
        $(".calloutman3").hide();
        $(".calloutman4").hide();
        $(".calloutman3_1").hide();
        $(".calloutman4_1").hide();
        $(".messageBox").hide();
		break;
	case 2:
        $(".messageBox").show();
        $(".calloutman1").show();
        $(".calloutman2").hide();
        $(".calloutman3").hide();
        $(".calloutman4").hide();
        $(".calloutman3_1").hide();
        $(".calloutman4_1").hide();
        $('.messageBox > span').html("It is of primary importance to both me and Britain as a whole for the British Empire to be maintained. The best way to do this is to secure a peace that would return Europe to the diplomatic balance that has existed essentially since the Conference of Vienna.");
		break;
	case 3:
        $(".calloutman1").hide();
        $(".calloutman2").show();
        $(".calloutman3").hide();
        $(".calloutman4").hide();
        $(".calloutman3_1").hide();
        $(".calloutman4_1").hide();
        $('.messageBox > span').html("My biggest goal is to gain territory from these peace agreements. The Italian people feel that the Trention, the Tyrol, Brenner, Trieste, and most of the Dalmatian Coast are rightfully part of Italy.");
		break;
	case 4:
        $(".calloutman1").show();
        $(".calloutman2").hide();
        $(".calloutman3").show();
        $(".calloutman4").show();
        $(".calloutman3_1").hide();
        $(".calloutman4_1").hide();
        $('.messageBox > span').html("I don't see how we can agree to those demands.");
		break;
	case 5:
        $(".calloutman1").hide();
        $(".calloutman2").show();
        $(".calloutman3").hide();
        $(".calloutman4").hide();
        $(".calloutman3_1").hide();
        $(".calloutman4_1").hide();
        $('.messageBox > span').html("If the demands of the Italian people will not be met, then I'm afraid I will no longer be able to take part in this conference.");
		break;
	case 6:
        $(".calloutman1").hide();
        $(".calloutman2").hide();
        $(".calloutman3").hide();
        $(".calloutman4").hide();
        $(".calloutman3_1").hide();
        $(".calloutman4_1").show();
        $('.messageBox > span').html("I'm sorry to see Vittorio Orlando leave, but I do not intend to have his departure keep us from going to an agreement about how to keep the peace. I'm hoping that we can reach a settlement that will ensure an enduring peace. Approving the League of Nations is the best way to keep the United States engaged, and adopting the Fourteen Points is guaranteed to keep Europe free of conflict.");
		break;
	case 7:
         $(".calloutman1").hide();
        $(".calloutman2").hide();
        $(".calloutman3").hide();
        $(".calloutman4").hide();
        $(".calloutman3_1").show();
        $(".calloutman4_1").hide();
        $('.messageBox > span').html("President Wilson, I agree with you that an enduring peace in Europe is the number one goal. However, I disagree with you about how to achieve this goal. The best way to ensure peace is to make sure Germany is so economically and militarily weak that it can no longer threaten the peace in Europe. I need assurances from you both that you will support me in my aim to hold Germany accountable for its actions. We need to seek reparations from Germany. I think doing so might help to keep the country weak for years to come.");
    }

}
﻿var count = 0;
var currentSlide = 0;
$(document).ready(function () {
    preload(['interactives/07_00_01/07_00_01a/imgs/07_00_01_a.jpg',
        'interactives/07_00_01/07_00_01a/imgs/07_00_01_b.jpg',
        'interactives/07_00_01/07_00_01a/imgs/07_00_01_c.jpg',
        'interactives/07_00_01/07_00_01a/imgs/07_00_01_d.jpg', ]);
    countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click');
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/07_00_01/07_00_01a/audio/07_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        $('<img/>')[0].src = this;
    });
}

function showBkgnd() {
    if (currentSlide < 5) {
        $('.ui_interactive').css('background', 'url("interactives/07_00_01/07_00_01a/imgs/07_00_01_a.jpg") no-repeat');
        $('.copytext').html("&copy; Alinari Archives/Image Quest 2012");
        $('.ui_interactive').attr("title", "This is a color image of a Sarajevo. The image shows several terraces of houses in the background. It also shows a mosque minaret. There are a number of women wearing burqas in the foreground. ");
    }
    else if (currentSlide >= 5 && currentSlide < 9) {
        $('.ui_interactive').css('background', 'url("interactives/07_00_01/07_00_01a/imgs/07_00_01_b.jpg") no-repeat');
        $('.copytext').html("&copy; Getty Images/Image Quest 2012");
        $('.ui_interactive').attr("title", "This is a black and white photograph of a street scene in Sarajevo. It shows an elderly woman sitting on the ground with her chickens with a man wearing a hat next to her. It also shows a younger woman and her daughter shopping.");
    } else if (currentSlide >= 9 && currentSlide < 13) {
        $('.ui_interactive').css('background', 'url("interactives/07_00_01/07_00_01a/imgs/07_00_01_c.jpg") no-repeat');
        $('.copytext').html("Public Domain");
        $('.ui_interactive').attr("title", "This is a color photograph of the Latin Bridge in Sarajevo. This is a brick and stone bridge spanning a shallow river. On one side is a line of apartments and storefronts.");
    } else {
        $('.ui_interactive').css('background', 'url("interactives/07_00_01/07_00_01a/imgs/07_00_01_d.jpg") no-repeat');
        $('.copytext').html("&copy;  Public Domain");
        $('.ui_interactive').attr("title", "This is an image of a bombed out library. The roof of the library has crumbled in and the rubble is piled up on the floor. A man with a large cello is sitting on the rubble and playing.");
    }
};

function countmessage() {
	showBkgnd();
	switch (currentSlide){
	case 0:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It's good to see you again. Ali is here, too.");
		break;
	case 1:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It's good to see you again. Ali is here too.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! We're in Sarajevo right now. As you can see, this is an interesting place with a complex ethnic and religious makeup.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("You're right, Ali. Bosnia and Herzegovina, where the city of Sarajevo is located, is in the Balkan Peninsula. This is a place that has changed hands many times over the last two thousand years. It has been part of the Roman Empire, the Byzantine Empire, and the Ottoman Empire.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("And as you've already learned, these different empires mean different religions: Catholic, Orthodox, and Muslim. This has left a population in Sarajevo with mixed loyalties.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("But this situation wasn't unusual in Europe. Consider the region of Bohemia, now a part of the modern nation of the Czech Republic. Bohemia, in the 19th century, was one of many ethnic regions of the Empire of Austria-Hungary. Czechs now have their own country; but until relatively recently, there seemed no inconsistency with an empire ruled by German and Hungarian speakers holding territory occupied by Czechs.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("And Czechs were by no means the only ethnic group in Austria-Hungary. The empire included Poles, Slovaks, Ukrainians, Italians, Slovenes, Croats, Serbians, and Romanians.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 7:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("As you can see, that's a lot of different ethnic groups living in one empire. These days we would see an organization like this as fundamentally flawed. How, you might ask, could all of these different groups exist in one place? Didn't they have their own national aspirations?");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 8:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("These are great questions, Soo-jin. And it's true that these groups did have national aspirations. But the important point here is that the empire wasn't organized to benefit the different national groups. Rather, it included vast amounts of territory in order to provide power, wealth, and authority to the empire. The fact that this territory was populated by different groups was largely inconsequential.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 9:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("But this would all begin to change toward the end of the 19th century. At that time, the Ottoman Empire was losing control of many areas in the Balkans, including Sarajevo.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 10:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("Austria recognized that the weakness of the Ottoman Empire could spell disaster for them. To control the situation, Austria annexed Bosnia and Herzegovina.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 11:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That's right, Ali. But this didn't work out very well for them. In fact, it all fell apart on the bridge you see here.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 12:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("This is the bridge where the Archduke Franz Ferdinand and his wife were assassinated by a radical Serbian nationalist.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 13:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("This assassination set in motion a chain reaction that would result in World War I.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 14:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("That's right, Soo-jin. And the war would change the way Europeans understood and dealt with nationalism on the continent.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 15:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("After the war, Europe was carved up along national lines. The result was the creation of entirely new countries, the re-creation of former countries, and the loss of territory by existing countries.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 16:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Europeans may have thought this would solve the problem, but it actually made things worse. As you read the lessons in this module, pay attention to situations that show how struggles over national identity contribute to large global problems.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 17:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("So now that Ali and I have gotten you excited about the effects of nationalism that occurred over the first part of the 20th century, let's go and take a closer look.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
    }
}
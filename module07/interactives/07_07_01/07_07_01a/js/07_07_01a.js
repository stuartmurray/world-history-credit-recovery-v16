﻿var count = 1;

$(document).ready(function () {
    $('.customPopUp').hide();
    $('#num5').css('opacity', '0.4').css('cursor', 'default');
    $('.num').unbind('click').bind('click', function () {
        $('#num5').css('opacity', '1').css('cursor', 'pointer');
        $('.num').each(function () {
            if ($(this).attr('id') != 'num5') {
                $(this).css('cursor', 'pointer').css('border', '1px solid #000');
            }
        });
        $(this).css('cursor', 'default').css('border','2px solid rgb(97, 64, 100)');


        count = 0;
        if ($(this).attr('id') == 'num1') {
            $('.customImg').html(' <img src="interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_01.png" alt="" title="" />');
            $('.sub_title').html('Allied Military');

        } else if ($(this).attr('id') == 'num2') {
            $('.customImg').html(' <img src="interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_03.png" alt="" title="" />');
            $('.sub_title').html('Allied Civilians');


        } else if ($(this).attr('id') == 'num3') {
            $('.customImg').html(' <img src="interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_02.png" alt="" title="" />');
            $('.sub_title').html('Axis Military');


        } else if ($(this).attr('id') == 'num4') {
            $('.customImg').html(' <img src="interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_04.png" alt="" title="" />');
            $('.sub_title').html('Axis Civilians');
        }

    });
    $('#num5').unbind('click').bind('click', function () {
        if (count == 1) {
            return;
        }
        $('#num5').css('opacity', '0.4').css('cursor', 'default');
        count = 1;
        $('.customImg').html(' <img src="interactives/07_07_01/07_07_01a/imgs/07_07_01a_map.png" alt="" title="" />');
        $('.sub_title').html(' ');
        //$('#num1').css('cursor', 'pointer');
       // $('#num2').css('cursor', 'pointer');
       // $('#num3').css('cursor', 'pointer');
       // $('#num4').css('cursor', 'pointer');
        $('.num').each(function () {
            if ($(this).attr('id') != 'num5') {
                $(this).css('cursor', 'pointer').css('border', '1px solid #000');
            }
        });
    });


    preload(['interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_01.png',
        'interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_01.png',
        'interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_02.png',
        'interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_03.png',
        'interactives/07_07_01/07_07_01a/imgs/07_07_01a_map_04.png'

 ]);

    function preload(arrayOfImages) {
        $(arrayOfImages).each(function () {
            $('<img/>')[0].src = this;
        });
    }

});
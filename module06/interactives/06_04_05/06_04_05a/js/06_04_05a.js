﻿var count = 0;
$(document).ready(function () {
    $('.customPopDiv').hide();

    $('.close1').unbind('click').bind('click', function () {
        count = 0;
        $('.clickarea').css('cursor', 'pointer');
        $('.customPopDiv').hide();
    });

    $('map .area').unbind('click').bind('click', function () {
        if (!count == 0) {
            return;
        }
        count++;
        $('.customPopDiv').show();
        $('.clickarea').css('cursor', 'default');
        var getId = $(this).attr('id');
        if (getId == 'area1') {
            $('.customPopDiv .copyrights').html(' Public Domain');
            $('.customPopDiv .imginpop').html('<img src="interactives/06_04_05/06_04_05a/img/06_04_05a_img_1.jpg" alt="Black and white photo of Rudyard Kipling standing in his library smoking a pipe." title="Black and white photo of Rudyard Kipling standing in his library smoking a pipe." />')
            $('.customPopDiv .head1').html('Propaganda in Literature');
            $('.customPopDiv .content1').html('<span class="ii">The process whereby European nations sought colonies in order to bring good and civilization perpetuated the process of colonization. It also reinforced the idea of white racial superiority. This notion is best summarized in the English Poet Rudyard Kipling&apos;s poem, "The White Man&apos;s Burden."</span><br/><br/> Take up the White Man&apos;s burden&#8211;Send forth the best ye breed&#8211;Go, bind your sons to exile To serve your captives&#39; need;To wait, in heavy harness,On fluttered folk and wild&#8211;Your new-caught sullen peoples,Half devil and half child.');
        } else if (getId == 'area2') {
            $('.customPopDiv .copyrights').html('&copy; 2012 The Granger Collection/Image Quest');
            $('.customPopDiv .imginpop').html('<img src="interactives/06_04_05/06_04_05a/img/06_04_05a_img_2.jpg" alt="Political cartoon showing Santa giving a gift of school books to a boy with a note attached that reads UNCLE SAM TO THE FILIPINO." title="Political cartoon showing Santa giving a gift of school books to a boy with a note attached that reads UNCLE SAM TO THE FILIPINO."/>')
            $('.customPopDiv .head1').html("Propaganda in Political Cartoons");
            $('.customPopDiv .content1').html('<i>The press in the United States used the same idea of bringing civilization to the uncivilized to support its occupation of the Philippines. As is the case in the cartoon, it was common for cartoonists to depict the colonized group as a child in need of instruction.</i>');
            $('.pop_illuminated').show();
        } else if (getId == 'area3') {
            $('.customPopDiv .copyrights').html('&copy; 2012 Bridgeman Art Library/Image Quest');
            $('.customPopDiv .imginpop').html('<img src="interactives/06_04_05/06_04_05a/img/06_04_05a_img_4.jpg" alt="Advertisement poster for a French exposition. Africans are drawn in traditional gowns and holding pots on their heads." title="Advertisement poster for a French exposition. Africans are drawn in traditional gowns and holding pots on their heads."/>')
            $('.customPopDiv .head1').html('Propaganda for Cultural Events');
            $('.customPopDiv .content1').html('<i>Colonial nations like Britain, Spain, and France celebrated their colonial success by hosting elaborate exhibitions. These exhibitions presented their colonial operations as a great success. They also reinforced the notions of white superiority and the civilizing mission of colonialism.</i>');
        } else if (getId == 'area4') {
            $('.customPopDiv .copyrights').html('&copy; 2012 akg-images/Image Quest');
            $('.customPopDiv .imginpop').html('<img src="interactives/06_04_05/06_04_05a/img/06_04_05a_img_3.jpg" alt="Advertisement for Pears&apos; Soap. One picture shows a black boy in the bathtub being given soap by a white girl, and the second picture shows that using the soap has caused the boy&apos;s body to now be white." title="Advertisement for Pears&apos; Soap. One picture shows a black boy in the bathtub being given soap by a white girl, and the second picture shows that using the soap has caused the boy&apos;s body to now be white." />')
            $('.customPopDiv .head1').html('Propaganda in Advertisements');
            $('.customPopDiv .content1').html('<i>Look at this advertisement for soap. What messages does this advertisement send about whiteness, cleanliness, and civilization? How might this idea have encouraged people to see colonialism as something beneficial to all&quest;<i>');
        }

    });

});
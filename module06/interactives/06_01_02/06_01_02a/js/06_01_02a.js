﻿var count = 0;

$(document).ready(function () {
    $('.customPopDiv').hide();
    $('.hiddentool').hide();
    $('.labels span').unbind('click').bind('click', function () {
        if (count == 1) {
            return;
                   }
        count = 1;
        $('.mapOverlay').css('z-index', '10');
        
        $('.labels span').css('cursor', 'default');
        $(this).css('color', '#861200');
        var getId = $(this).attr('id');

        if (getId == 'popup_1') {
            
            $('.customPopDiv .head1').html('<strong>Mexico: </strong> Mexico declared independence from Spain several times, starting in 1810. The Republic of Mexico was finally established in 1821. It was much larger than modern Mexico. The nations of Central America broke away from Mexico in 1823. Territory was lost to the United States in 1836, 1845, 1848, and 1853. France took control of Mexico for a short time from 1864 to 1867.');

        } else if (getId == 'popup_2') {

            $('.customPopDiv .head1').html("<strong>Colombia: </strong> In 1800, Colombia was part of a Spanish colony called New Granada. In 1813, Colombia declared independence from Spain but did not achieve it until 1819. In 1821, Venezuela joined Colombia to form the Republic of Greater Colombia. In 1822, Ecuador joined, reuniting New Granada under the flag of Greater Colombia. In 1830, both Venezuela and Ecuador broke free of Greater Colombia. In 1903, Panama broke away, leaving the modern country of Colombia.");

        } else if (getId == 'popup_3') {

            $('.customPopDiv .head1').html("<strong>Venezuela: </strong> In 1800, Venezuela was part of a Spanish colony called New Granada. In 1821, Venezuela achieved independence from Spain and rejoined Colombia to become the Republic of Greater Colombia. In 1822, Ecuador joined, reuniting New Granada under the flag of Greater Colombia. In 1830, Venezuela declared its independence from Greater Colombia.");

        } else if (getId == 'popup_4') {

            $('.customPopDiv .head1').html("<strong>Guyana:</strong> Guyana was originally a Dutch colony. Great Britain took control in 1796. In 1831, the colony was renamed British Guiana. Guyana achieved independence in 1966 and became a republic in 1970.");

        } else if (getId == 'popup_5') {

            $('.customPopDiv .head1').html("<strong>Suriname:</strong> Suriname was a Dutch colony. The Netherlands gave Suriname self-rule in 1954. Independence followed in 1975.");

        }
        else if (getId == 'popup_6') {

            $('.customPopDiv .head1').html('<strong>French Guiana:</strong> For years, control of what is today French Guiana shifted between France, the Netherlands, Great Britain, and Portugal. France kept control after 1817. In 1948, French Guiana became an overseas department of France. This means it was part of France and its people are French citizens.');

        } else if (getId == 'popup_7') {

            $('.customPopDiv .head1').html('<strong>Ecuador:</strong> Ecuador was part of a Spanish colony called New Granada. In 1822, Ecuador achieved independence from Spain, and rejoined the rest of the former New Granada, which had become Greater Colombia. In 1830, Ecuador declared independence from Greater Colombia.');

        } else if (getId == 'popup_8') {

            $('.customPopDiv .head1').html('<strong>Peru:</strong> Peru was part of a Spanish colony called Peru. Bolivia declared independence in 1809. Chile broke away in 1810. That left modern-day Peru, which declared independence from Spain in 1821. Its war for independence continued through 1824. Spain did not recognize Peru&#39;s independence until 1879.');

        } else if (getId == 'popup_9') {

            $('.customPopDiv .head1').html('<strong>Bolivia:</strong> Most of Bolivia was part of a Spanish colony called Peru. The rest was part of another Spanish colony, La Plata. Bolivia declared independence in 1809. After 16 years of fighting, the republic of Bolivia was established in 1825.');

        } else if (getId == 'popup_10') {

            $('.customPopDiv .head1').html('<strong>Brazil:</strong> Brazil declared independence from Portugal in 1822. It was a monarchy. The emperor was overthrown in 1889, and Brazil became a republic.');

        } else if (getId == 'popup_11') {

            $('.customPopDiv .head1').html('<strong>Chile:</strong> Chile was part of a Spanish colony called Peru. In 1810, a <span id="chkTool" >junta</span> took control. It declared Chile a self-governing republic within the Kingdom of Spain, loyal to the deposed King Ferdinand. In 1818, the junta was overthrown. Chile became an independent republic.');
            $('#chkTool').bind('click', function () {
                $('.hiddentool').show();
            });
            $('.close2').bind('click', function () {
                $('.hiddentool').hide();

                if ($('#mep_0 .mejs-button').hasClass('mejs-pause')) {
                    $('#mep_0 button').trigger('click');
                }
            });
            counter2 = 1;
        } 

        
        else if (getId == 'popup_12') {

            $('.customPopDiv .head1').html('<strong>Paraguay:</strong> Paraguay was part of La Plata, a Spanish colony. It gained independence from Spain in 1811.');

        } else if (getId == 'popup_13') {

            $('.customPopDiv .head1').html('<strong>Argentina:</strong> Argentina was part of La Plata, a Spanish colony. Bolivia broke away in 1809. Uruguay and Paraguay declared independence in 1811. The remaining part of the La Plata was in modern-day Argentina. It declared independence from Spain in 1816. Border wars and internal revolts continued for many years. The Republic of Argentina wasn&#39;t formed until 1853. It did not attain its modern borders until 1881.');

        } else if (getId == 'popup_14') {

            $('.customPopDiv .head1').html('<strong>Uruguay: </strong>Uruguay was part of La Plata, a Spanish colony. It gained independence from Spain in 1811. In 1821, Portugal made the area part of Brazil. Uruguay declared independence from Brazil in 1825. After three years of fighting, independence was achieved in 1828.');

        }


        $('.customPopDiv').show();
    });



    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
        $('.hiddentool').hide();
        count = 0;
        if ($('#mep_0 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_0 button').trigger('click');
        }
        $('.mapOverlay').css('z-index', '0');
        $('.labels span').css('cursor', 'pointer');
    });


});


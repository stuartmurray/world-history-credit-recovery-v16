﻿var count = 0;
var currentSlide = 0;
$(document).ready(function () {
    preload(['interactives/06_00_01/06_00_01a/imgs/06_00_01_a.jpg',
        'interactives/06_00_01/06_00_01a/imgs/06_00_01_b.jpg',
        'interactives/06_00_01/06_00_01a/imgs/06_00_01_c.jpg',
        'interactives/06_00_01/06_00_01a/imgs/06_00_01_d.jpg' , ]);
    countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click');
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/06_00_01/06_00_01a/audio/06_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}            

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        $('<img/>')[0].src = this;
    });
}

function showBkgnd() {
    if (currentSlide < 5) {
        $('.ui_interactive').css('background', 'url("interactives/06_00_01/06_00_01a/imgs/06_00_01_a.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    }
    else if (currentSlide >= 5 && currentSlide < 9) {
        $('.ui_interactive').css('background', 'url("interactives/06_00_01/06_00_01a/imgs/06_00_01_b.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    } else if (currentSlide >= 9 && currentSlide < 13) {
        $('.ui_interactive').css('background', 'url("interactives/06_00_01/06_00_01a/imgs/06_00_01_c.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    } else {
        $('.ui_interactive').css('background', 'url("interactives/06_00_01/06_00_01a/imgs/06_00_01_d.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    }
};

function countmessage() {
	showBkgnd();
	switch (currentSlide){
	case 0:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It´s good to see you again. Ali is here too.");
		break;
	case 1:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It´s good to see you again. Ali is here too.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! Have you ever seen a beach so beautiful? Look at the blue water, lush forest, and long rolling hills in the background.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("You´re right, Ali. It sure looks fantastic. There aren´t any parking lots or highways interrupting the forests. There aren´t even any tall beachfront condominiums blocking the view.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("It´s hard to believe from this beautiful scene. But for the vast majority of those who live here, Haiti is a desperate place.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Wow! Look at the garbage here! There is so much that it´s even being used to create a road through the crumbling town. That´s unbelievable.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("It´s difficult to imagine that the beach and this settlement can be in the same place. But this scene is a reality for many people in Haiti.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 7:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That´s right, Ali. Haiti has a very small population of wealthy people. But the vast majority of Haitians live on the edge of total ruin their entire lives. A small injury at work or anything that disrupts their income even for a few days can mean going without food or even water.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 8:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("But this situation didn´t emerge out of thin air. There is a direct correlation between the unbelievable level of inequality and desperate poverty in Haiti and the long history of exploitation, colonization, and post-colonial disorder in Haiti.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 9:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Haiti was actually a very profitable colony for France. But it was profitable mainly because the economy of the colony was linked to the forcible and brutal enslavement of nearly half a million Africans.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 10:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("As you might imagine, this situation wasn´t very stable. And in the late 18th century, the slaves revolted and ultimately gained independence for the island.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 11:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Despite Haiti's years of suffering as a slave colony and the long and bloody battle for independence from France, in 1825 France actually demanded they be compensated with 150 million francs ($21 billion in today´s money) for the loss of the colony.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 12:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("And this was just the beginning of the many problems Haiti would face as it tried to chart its own course as a free and independent post-colonial nation.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 13:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("In this module, you will be learning about the many changes that occurred during the 19th century and some modern changes.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 14:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("That´s right Soo-jin. We´ll discuss the way older colonies in the Caribbean and Latin America loosened and escaped from their colonial shackles.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 15:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("But we´ll also learn about a newer form of colonialism that emerged in the 19th century and resulted in the colonization of Africa, India, and China.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 16:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("We´ll also see how those in Europe responded to these changes. And finally, we´ll see how nationalism plays out in some key areas of the world today.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 17:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("So now that Ali and I have gotten you excited about the many changes that occurred in the 19th century, let´s go and take a closer look.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
    }
}
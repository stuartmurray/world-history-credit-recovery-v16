﻿$(document).ready(function () {
        $('.panel').each(function () {
            $(this).find('.card').each(function (e) {
                if (e >= 3) {
                    $(this).remove();
              
                }
            });
        });
        $('.hiddentool').hide();
        $(".aud").bind('click', function () {
            $('.hiddentool').show();
            $('.close2').bind('click', function () {
                $('.hiddentool').stop();
                if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
                    $('#mep_1 button').trigger('click');
                }
            });
        });

        $('.carousel-prev').bind('click', function () {
            $('.hiddentool').hide();

            if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {

                $('#mep_1 button').trigger('click');
            }
        });

        $('.carousel-next').bind('click', function () {
            $('.hiddentool').hide();

            if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_1 button').trigger('click');

                $('.hiddentool').hide();
            }
        });
     
});
﻿var count = 1;
var currentSlide = 0;
$(document).ready(function () {
    countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click'); //start audio player mep_0
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden" ); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/04_00_01/04_00_01a/audio/04_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function countmessage() {
	switch (currentSlide){
		case 0:
    	    $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        	$('.messageBox > span').html("Hi, students! It´s good to see you again. Ali is here too.");
			break;
		case 1:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("Hi, students! It´s good to see you again. Ali is here too.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 2:
			$('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
	        $('.messageBox > span').html("Hi, students! Welcome to the beautiful city of Venice! I wish you were here. You can smell the salt air and feel the warm Adriatic breeze coming off the sea.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 3:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("It might be difficult to imagine now from this relatively sleepy-looking city, but Venice was once the center of a powerful trading state.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 4:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
	        $('.messageBox > span').html("That´s right. Venice´s influence stretched down to the coast of modern day Slovenia and Croatia. More than this, the wealth it brought in from distant trade allowed it to exert influence all over Europe and beyond.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 5:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("Does that seem hard to believe? This is understandable. It´s very hard for us in the 21st century to understand how this little island city could have acquired so much importance.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 6:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
	        $('.messageBox > span').html("The source of Venice´s wealth has to do with its strategic geographic position as an island city at the crossroads of Europe, Asia, and Africa.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 7:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("That´s right, Ali. As an island city it was not only easy to defend, but it was especially well situated to exploit the booming trade with Asia that began in the centuries following the Crusades.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 8:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
	        $('.messageBox > span').html("In fact, Venice actually funded the fourth Crusade, helped sack Constantinople and the Byzantine Empire, and captured territory once controlled by the Empire.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 9:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("And what do you think it did with all this wealth and power? Well, it did quite a lot. It built beautiful squares and buildings like those you see here.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 10:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
    	    $('.messageBox > span').html("Right. And its wealthy merchants and financiers commissioned incredible works of art that even today bring in tourists from all over the world.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 11:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("That´s right. All of this from a small island city in the sea.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 12:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
	        $('.messageBox > span').html("So, you might be asking, how did Venice lose all of this power and influence? Well, the long answer is that it happened gradually over time. But the short answer is that the world changed around it.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 13:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("Venice´s power came from its strategic place in the Mediterranean. By controlling the trade in spices and other goods coming into Europe from Asia, it was able to reap unbelievable wealth. But all that wealth dried up once its control on these trade routes was broken.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 14:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
    	    $('.messageBox > span').html("That´s right. Once the states of Portugal and Spain decided that they wanted to benefit from this trade, they sent out explorers to chart new routes to the East.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 15:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("But they did far more than just find new routes to the East. These explorations changed the course of European history and the world. The places and riches these explorers found made the Spanish Kingdom of Castile the most powerful kingdom in Europe, with possessions all over the world and a great Armada to protect it.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 16:
	        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
    	    $('.messageBox > span').html("Venice, however, lost its influence. With the flow of Asian goods no longer flowing through the Mediterranean, Venice was just not that important anymore.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
		case 17:
	        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
    	    $('.messageBox > span').html("So, now that Ali and I have gotten you excited to learn more about Venice, the Renaissance, and the Age of Exploration, let´s go take a closer look.");
	        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
			break;
    } 
}
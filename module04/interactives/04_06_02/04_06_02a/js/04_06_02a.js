﻿var count = 0;

$(document).ready(function () {
    $('.customPopDiv').hide();
    $('.main_clk div').unbind('click').bind('click', function () {
        if (count == 1) {
            return;
        }
        count = 1;
        $('.main_clk div').css('cursor', 'default');
       
        var getId = $(this).attr('id');
        if (getId == 'A') {

            $('.customPopDiv .head1').html('Explorers for Portugal');
            $('.customPopDiv .content1').html("The Portuguese sent several explorers to Africa and Asia. These explorers left from Portugal and went southward along the coast of Africa. Some of these routes made several stops along the western coast of sub-Saharan Africa. Others went all the way to the cape before turning back. One route made the trip all the way around the cape across the Indian Ocean to India before returning to Portugal along the same route.");

        } else if (getId == 'B') {

            $('.customPopDiv .head1').html('Explorers for Spain');
            $('.customPopDiv .content1').html("Spain sent explorers southwest from Spain to the Caribbean and Central America. They also sailed southwest around the southern tip of South America, westward through the Pacific and Indian oceans, and finally around the southern tip of Africa on their way back to Europe.");

        } else if (getId == 'C') {

            $('.customPopDiv .head1').html('Explorers for England');
            $('.customPopDiv .content1').html("England sent explorers to the northeastern coast of North America from Southern England. These explorers explored the areas of modern day Nova Scotia and New England. These explorers made their way back across the North Atlantic to England.");

        } else if (getId == 'D') {

            $('.customPopDiv .head1').html('Explorers for France');
            $('.customPopDiv .content1').html("France sent explorers to the northeastern coast of North America and into the interior north of the Great Lakes. They traveled from Northern France to the coast of North America across the North Atlantic.");

        } else if (getId == 'D') {

            $('.customPopDiv .head1').html('Explorers for France');
            $('.customPopDiv .content1').html("France sent explorers to the northeastern coast of North America and into the interior north of the Great Lakes. They traveled from Northern France to the coast of North America across the North Atlantic.");

        }
        else if (getId == 'E') {

            $('.customPopDiv .head1').html('Explorers for the Netherlands');
            $('.customPopDiv .content1').html("The Netherlands sent explorers from the Netherlands up the coast of Norway and back down across the North Atlantic past Iceland and to the eastern Atlantic coast of the modern-day United States to the Chesapeake Bay.");

        }



        $('.customPopDiv').show();
    });



    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
        count = 0;
        $('.main_clk div').css('cursor', 'pointer');
    });

});
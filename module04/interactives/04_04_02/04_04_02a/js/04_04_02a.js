﻿var count = 0;
var currentSlide = 0;
$(document).ready(function () {


    $('#imgPlay').on('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click'); //start audio player mep_0
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/04_04_02/04_04_02a/audio/04_04_02_Slide"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}
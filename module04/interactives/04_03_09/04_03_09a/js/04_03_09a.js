﻿$(document).ready(function () {
    $('.droppable').bind('drop', function () {
        if($(this).find('li').length > 0) {
            $(this).droppable('disable').css('opacity', '1');
        }
    });
    $('.ui_reset').bind('click', function () {
        $('.droppable').each(function () {
            $(this).droppable('enable');
        });
    });
});
﻿var count = 0;
var count2 = 0;

$(document).ready(function () {
    $('.nextpop1').css('cursor', 'default');
    $('.nextpop2').css('cursor', 'default');

    $('.customPopDiv').hide();
    $('.popb1').hide();
    $('.popb2').hide();

    $('.close1').unbind('click').bind('click', function () {


        $('.popb1').hide();
        $('.popb2').hide();
        $('.nextpop1').css('cursor', 'default');
        $('.nextpop2').css('cursor', 'default');
        $('.nextpop' + count2).css('cursor', 'pointer');
        $('.area').css('cursor', 'pointer');
        count2 = 0;

    });

    $('.area').unbind('click').bind('click', function () {
        if (!count2 == 0) {
            return;
        }

        $('.nextpop1').css('cursor', 'default');
        $('.nextpop2').css('cursor', 'default');

        var getId = $(this).attr('id');
        if (getId == 'area1') {
            count = 1;
            $('.nextpop1').css('cursor', 'pointer');
            $('.customPopDiv .popimg').html('<img src="interactives/04_07_03/04_07_03a/img/04_07_03_red.png" /> ').css({ "top": "88px", "left": "33px" });
        } else if (getId == 'area2') {
            count = 2
            $('.nextpop2').css('cursor', 'pointer');
            $('.customPopDiv .popimg').html('<img src="interactives/04_07_03/04_07_03a/img/04_07_03_green.png" />').css({ "top": "102px", "left": "34px" });
        }

        $('.customPopDiv').show();

    });

    $('.nextpop2').unbind('click').bind('click', function () {
        if (count == 0 || count2!=0) {
            return;
        } else if (count2 == 0 && count != 2) {
            return;
        }
        count2 = 2;
        $('.nextpop1').css('cursor', 'default');
        $('.nextpop2').css('cursor', 'default');
        $('.area').css('cursor', 'default');

        $('.popb2').show();
        var getId = $(this).attr('id');

        if (getId == 'area21') {
            $('.popb2').css('height', '32px');
            $('.popb2 .head1').html('Europe');
            $('.popb2 .content1').html('Europe shipped manufactured goods to Africa');
        }
        else if (getId == 'area22') {
            $('.popb2').css('height', '32px');
            $('.popb2 .head1').html('West Africa');
            $('.popb2 .content1').html('West Africa traded manufactured goods for slaves.');
        }
        else if (getId == 'area23') {
            $('.popb2').css('height', '55px');
            $('.popb2 .head1').html('Caribbean');
            $('.popb2 .content1').html('Colonies in the Caribbean traded slaves and used profit to buy coffee, tobacco, and sugar, which was shipped back to Europe and sold.');
        }
    });

    $('.nextpop1').unbind('click').bind('click', function () {
        if (count == 0 || count2 != 0) {
            return;
        } else if (count2 == 0 && count != 1) {
            return;
        }

        count2 = 1;
        $('.nextpop1').css('cursor', 'default');
        $('.nextpop2').css('cursor', 'default');
        $('.area').css('cursor', 'default');

        $('.popb1').show();
        var getId = $(this).attr('id');
        if (getId == 'area11') {
            $('.popb1').css('height', '32px');
            $('.popb1 .head1').html('West Africa');
            $('.popb1 .content1').html('West Africa traded rum and other goods for slaves.');
        }
        else if (getId == 'area12') {
            $('.popb1').css('height', '32px');
            $('.popb1 .head1').html('New England');
            $('.popb1 .content1').html('New England shipped rum and other goods to Africa.');
        }
        else if (getId == 'area13') {
            $('.popb1').css('height', '47px');
            $('.popb1 .head1').html('Caribbean');
            $('.popb1 .content1').html('Colonies in the Caribbean traded slaves and used profit to buy molasses and sugar, which was sent back to New England and sold.');
        }
    });
});
// FLVS Framework Functions Library
// v1.4.3
//
// AUTHORS:
//		Kennon Bickhart || kbickhart@flvs.net
// 
// Last Modified: 2/5/12
// =====================================

function activateAssets() {

//Assign lightbox
//=====================================
$('a.lightbox').each(function() {
	$(this).lightBox({fixedNavigation:true});
	$(this).append($(document.createElement("div")).addClass("magGlass"));
});

//Interactive Embedding
//=====================================
$('.interactive').each(function(index){

	var params           = $(this).attr("data-src").split(':');
	var textVersion      = $(this).html();
	var printTextVersion = $(document.createElement("button")).addClass("printText").text(lang.Print);
	var isSWF            = params[0].search(".swf");
	var showTextVersion;

	// Check if the notext param is defined. If not, then define it.		
	if ( typeof params[3] === "undefined" ) { showTextVersion = -1; }
	else { showTextVersion = params[3].search("notext"); }

	if ( isSWF != -1 && !touchSupport ) {
		
		if ( showTextVersion != -1 ) {
			$(this).removeClass("interactive").addClass("activeInteractive").empty().append($(document.createElement("div")).addClass("swfObject"));
		} else {
			$(this).removeClass("interactive").addClass("activeInteractive").empty().append($(document.createElement("button")).addClass("toggleSwitch").text(lang.TextVersion)).append($(document.createElement("div")).addClass("swfObject")).append($(document.createElement("div")).addClass("textVersion").addClass("notVisible").append(textVersion).append(printTextVersion));
		}
		
		$(this).children("div.swfObject").flash({swf: params[0],width: params[1], height: params[2], flashvars: params[3]});
	} else if ( params[0].search(".htm") != -1 || params[0].search(".html") != -1 ) {
		var iframe = $(document.createElement("iframe"));
		
		if ( showTextVersion != -1 ) {
			$(this).removeClass("interactive").addClass("activeInteractive").empty().append($(iframe).attr("src", params[0]).attr("scrolling", "no").attr("width", params[1]).attr("height", params[2]));
		} else {
			$(this).removeClass("interactive").addClass("activeInteractive").empty().append($(document.createElement("button")).addClass("toggleSwitch").text(lang.TextVersion)).append($(iframe).attr("src", params[0]).attr("scrolling", "no").attr("width", params[1]).attr("height", params[2])).append($(document.createElement("div")).addClass("textVersion").addClass("notVisible").append(textVersion).append(printTextVersion));
		}
		
		$(iframe).load(function() {
			$(iframe).after($(document.createElement("div")).addClass("iframePrint").append($(this).contents().find("body").html()));
		});
	}

});


//Audio Player
//=====================================
$('.jQPlayer').each(function(intIndex) {
	var href  = $(this).attr("href"),
		ogg = href.substring(0, href.length - 3) + "ogg",
		title = $(this).text();

	$(this).after('<audio controls><source src="'+href+'" /><source src="'+ogg+'" /><span class="flash"></span></audio>');
	$(this).next('audio').find('span.flash').flash({swf: path+"audio.swf?mp3String="+href,width:"253", height: "30", flashvars: "mp3String="+href});
	$(this).remove();
});


//Older Single Player
//Audio Pill Player
//=====================================
$('.jQPlayer_mini').each(function(intIndex) {
	$(this).before($(document.createElement("span")).attr("title", $(this).text()).addClass("jQPlayer_mini"));
	$(this).siblings(".jQPlayer_mini").flash({swf: "../global/swf/jQPlayer.swf?mp3String="+$(this).attr("href"),width:"20", height: "20", flashvars: "mp3String="+$(this).attr("href")});
	$(this).remove();
});

	
//Tabs
//=====================================
$('.tabs').each(function(index) {
	$(this).find("div.tab:gt(0)").addClass("notVisible");
	$(this).find("h3:lt(1)").addClass("tabOn");
	$(this).find("h3:gt(0)").addClass("tabOff");
});

$('.tabs h3').each(function(index) {
	var index = $(this).parent().find("h3").index( $(this) );
	var offset = 0;

	if ( index > 0 ) {
		offset = parseFloat($(this).parent().find("h3:eq("+ (index-1) +")").css("left").replace(/px/, "")) + parseFloat($(this).parent().find("h3:eq("+ (index-1) +")").outerWidth(true)) + (1);
	}
	
	$(this).css("left",offset+"px");
});

$(".tabs h3").click(function(){
	var index = $(this).parent().find("h3").index($(this));
	$(this).parents(".tabs").find("h3").removeClass("tabOn").addClass("tabOff");
	$(this).addClass("tabOn").removeClass("tabOff");
	$(this).parent().find("div.tab").addClass("notVisible");
	$(this).parent().find("div.tab:eq("+ index +")").removeClass("notVisible");
	return false;
});


//Accordian
//=====================================
$('dl.accordian > dt').click(function() {
	$(this).removeClass('on');
	$(this).parent().find('dd').slideUp('normal');

	if ( $(this).next().is(':hidden') === true ) {
		$(this).addClass('on');
		$(this).next().slideDown('normal');
	} 
});

$('dl.accordian > dd').hide();


// Click to reveal
//=====================================
$(".clickReveal dt").click(function() {
	$(this).parent().find("dd").toggleClass("notVisible");
	$(this).toggleClass("open");
}).parent().find("dd").toggleClass("notVisible");


//Tooltip
//=====================================
$('.tooltip').each(function(i) {
	if ( $(this).hasClass('question') ) { $(this).prepend('<div class="icon"></div>'); };

	var text = $(this).find('span').html();
	$(this).attr('title', text);

	$(this).poshytip({
		className: 'tip-twitter',
		showTimeout: 1,
		alignTo: 'target',
		alignX: 'center',
		offsetY: 5,
		allowTipHover: false,
		fade: true,
		slide: false,
		showOn: 'focus'
	});

	$(this).toggle(
		function() { $('.tooltip').poshytip('hide'); $(this).poshytip('show'); },
		function() { $(this).poshytip('hide'); }
	);

	$('.tip-twitter').click( function() { $('.tooltip').poshytip('hide'); } );
});


//Slideshows
//=====================================
$("dl.slideshow").each(function(index) {
	var $this = $(this);
	var children = ($(this).children().length)/2;
	var params = $(this).attr("data-size").split(':');

	// Formatting
	$this.wrap('<div class="slider" style="width: '+params[0]+'px; height: '+params[1]+'px;"></div>');

	$this.removeClass("slideshow");
	$this.css("width", ((children+1)*params[0]) +"px").css("height", (params[1]) +"px");

	$this.find("dd").css("width", params[0] +"px").css("height", (params[1]) +"px");

	// Add Title, if present
	$this.find("dt").each(function(i) {
		//console.log( $(this).text() );

		if ( $(this).text() !== '' ) {
			$this.find("dd:eq("+i+")").prepend($(document.createElement("h4")).text($(this).text()));
		}
	});
	$this.find('dt').remove();
	
	// Add Navigation
	$this.parent().append('<div class="slideshowCrumbs">1 of '+children+'</div>');
	$this.parent().append('<button class="arrows right">Next</button>');
	$this.parent().append('<button class="arrows left">Prev</button>')

	$this.parent().find('button.right').on("click",{direction:"right"},adjustXPos);
	$this.parent().find('button.left').on("click",{direction:"left"},adjustXPos);

	var int = 0;

	function adjustXPos(evt){
		if ( evt.data.direction == "right" ) {
			int = ((int - 1) < -($(this).parent().find("dl").children().length -1))? 0 : int - 1 ;
			$(this).parent().find("dl").animate({left:int*$("div.slider").width()+"px"},1000);
		} else {
			int = ((int + 1) > 0)? -($(this).parent().find("dl").children().length -1) : int + 1 ;
			$(this).parent().find("dl").animate({left:int*$("div.slider").width()+"px"},1000);
		}
		
		$(this).parent().find(".slideshowCrumbs").text(Math.abs(int)+1 + " of "+ children);
	}

});


//Form Submission in New Window
//=====================================
$("form.discoverVideo").submit(function() {
	this.target="_blank";
});


//MultiChoice
//=====================================
$("div.quiz").each(function() {

	var cheatsheet  = new Array();
	var userAnswers = new Array();
	var position    = 0;
	var progress    = $(document.createElement("div")).addClass("progressBar");

	for ( var i = 0; i<$(this).find("dl").length; i++ ) {
		progress.append($(document.createElement("div")).addClass("incOff").width((100/$(this).find("dl").length) + "%"));
	}
	$(this).append(progress);

	$(this).find("dl").each(function() {

		var set = $(this);
		var length = $(set).find("dd").length;

		cheatsheet.push($(set).find("dd:eq(0)").text());

		var newSet = $(document.createElement("dl")).addClass("notVisible");
		newSet.append($(document.createElement("dt")).html($(this).find("dt").html()));

		for ( var i = 0; i<length; i++ ) {
			var ranNumber = ($(set).find("dd").length == 1)? 0 : randomNumber($(set).find("dd").length-1);
			newSet.append($(document.createElement("dd")).html($(set).find("dd:eq("+ranNumber+")").html()));
			$(this).find("dd:eq("+ranNumber+")").remove();
		}
		$(this).replaceWith(newSet);

	});

	$(this).find("div.directions").append($(document.createElement("button")).text("Begin"));
	$(this).find("div.directions button").bind('click', function() {
		$(this).parent().parent().find("dl:eq(0)").removeClass("notVisible");
		$(this).parent().addClass("notVisible");
	});


	$(this).find("dl dd").bind('click', function() {
	$(this).parent().find("dd").removeClass("selected");
	
	userAnswers[$(this).parent().parent().find("dl").index($(this).parent())] = $(this).text();
	
	$(this).addClass("selected");
	$(this).parents("div.quiz").find("dl").addClass("notVisible");

	position = position + 1;

	$(this).parents("div.quiz").find("dl:eq("+(position)+")").removeClass("notVisible");
	$(this).parents("div.quiz").find("div.progressBar div:lt("+position+")").removeClass("incOff").addClass("incOn");

	if ( position == $(this).parents("div.quiz").find("dl").length ) {
		$(this).parent().parent().append(returnResults(cheatsheet, userAnswers));
		var restartBtn = $(document.createElement("button"));

		$(restartBtn).bind('click', function() {
			position = 0;
			$(this).parents("div.quiz").find("table").remove();
			
			$(this).parent().find("dd").removeClass("selected");
			$(this).parents("div.quiz").find("div.progressBar div").addClass("incOff").removeClass("incOn");
			$(this).parents("div.quiz").find("div.directions").removeClass("notVisible");

			$(this).remove();
		});

		$(this).parent().parent().append($(restartBtn).text("Restart"));
	}

	});
});


//Video Player
//=====================================
var videoTest = document.createElement("video");

if ( $.flash.available || videoTest.play ) {
	$('.video').each(function(index) {
		var textVersion = $(this).html();

		if ( !videoTest.play ) {
			var swfPath = ($(this).attr("data-swf"))? $(this).attr("data-swf") : locPath+"swf/video.swf" ;
			
			$(this).empty().append($(document.createElement("button")).addClass("toggleSwitch").text(lang.TextVersion)).append($(document.createElement("div")).addClass("swfObject")).append($(document.createElement("div")).addClass("textVersion").addClass("notVisible").append(textVersion).append($(document.createElement("button")).addClass("printText").text(lang.Print)));
			$(this).children("div.swfObject").flash({swf:swfPath +"?swfId="+index + "&amp;"+"thumb="+ $(this).attr("data-html") + "&amp;"+"vid="+ $(this).attr("data-flash"),width: "100%", height: "100%"});;
			
		} else {
			
			$(this).children("p").addClass("notVisible");
			
			var video = $(document.createElement("video")).attr("controls", "controls").attr("poster", $(this).attr("data-html")+"thumb.jpg").append($(document.createElement("source")).attr("src", $(this).attr("data-html")+"video.mp4").attr("type", "video/mp4")).append($(document.createElement("source")).attr("src", $(this).attr("data-html")+"video.ogv").attr("type", "video/ogg")).append($(document.createElement("source")).attr("src", $(this).attr("data-html")+"video.webm").attr("type", "video/webm"));
			
			//Added by Will Jones
			//to keep the Text Version Option in All Browsers
			$(this).empty().append($(document.createElement("button")).addClass("toggleSwitch").text(lang.TextVersion)).append($(document.createElement("div")).addClass("textVersion").addClass("notVisible").append(textVersion).append($(document.createElement("button")).addClass("printText").text(lang.Print)));
			
			//			
			if ( 'createTouch' in document && $(this).attr("data-width") != "undefined" ) {
				$(video).attr("width", "720");
			}

			video.bind('timeupdate', updateTime);
		}
		$(this).append(video).append($(document.createElement("div")).addClass("vidCaption").hide());

	});
}


/*//Video Player
//=====================================
var videoTest = document.createElement("video");

if ( $.flash.available || videoTest.play ) {
	$('.video').each(function(index) {
		var textVersion = $(this).html();

		if ( !videoTest.play ) {
			var swfPath = ($(this).attr("data-swf"))? $(this).attr("data-swf") : locPath+"swf/video.swf" ;
			
			$(this).empty().append($(document.createElement("button")).addClass("toggleSwitch").text(lang.TextVersion)).append($(document.createElement("div")).addClass("swfObject")).append($(document.createElement("div")).addClass("textVersion").addClass("notVisible").append(textVersion).append($(document.createElement("button")).addClass("printText").text(lang.Print)));
			$(this).children("div.swfObject").flash({swf:swfPath +"?swfId="+index + "&amp;"+"thumb="+ $(this).attr("data-html") + "&amp;"+"vid="+ $(this).attr("data-flash"),width: "100%", height: "100%"});;
			
		} else {
			
			$(this).children("p").addClass("notVisible");
			
			var video = $(document.createElement("video")).attr("controls", "controls").attr("poster", $(this).attr("data-html")+"thumb.jpg").append($(document.createElement("source")).attr("src", $(this).attr("data-html")+"video.mp4").attr("type", "video/mp4")).append($(document.createElement("source")).attr("src", $(this).attr("data-html")+"video.ogv").attr("type", "video/ogg")).append($(document.createElement("source")).attr("src", $(this).attr("data-html")+"video.webm").attr("type", "video/webm"));
			var ccBtn = $(document.createElement("button")).addClass("ccBtn").append($(document.createElement("span")).addClass("notVisible").text("CC")).addClass("notVisible").attr("title", "Closed Captions");
			
			ccBtn.bind("click", function(){$(this).parents(".video").find(".vidCaption").toggle();});
			$(this).append($(ccBtn));
			
			if ( 'createTouch' in document && $(this).attr("data-width") != "undefined" ) {
				$(video).attr("width", "720");
			}

			video.bind('timeupdate', updateTime);

		}
		$(this).append(video).append($(document.createElement("div")).addClass("vidCaption").hide());

	});
}*/


//Cycle
//=====================================
$('.cycle').each(function(i) {
	$(this).attr('id', 'cycle'+i); //Add ID for specificity

	//Loop through images and hide all but the first.
	for ( var i = 0; i < $(this).find('img').length; i++ ) {
		if ( i > 0 ) {	$(this).find('img').eq(i).hide(); }
		else { $(this).find('img').eq(i).addClass('visible'); }
	}

	setTimeout('nextCycleImage()', 7000);	//Set Timer for progressing through images
});

}


//Generic Functions
//=====================================
function updateTime(evt){
	if($(evt.target).parent().width() != $(evt.target).width()){
		$(evt.target).parent().width($(evt.target).width());
		$(evt.target).parent().find(".ccBtn").removeClass("notVisible");
		$(evt.target).parent().find(".script").removeClass("notVisible");
	}
	
	updateCaption($(".video").index($(evt.target).parent()), evt.target.currentTime);
}

function updateCaption(index, time){
	if ( $(".video:eq("+index+")").find("p[data-start='"+Math.floor(time)+"']").length == 1 ) {
		$(".video:eq("+index+")").find(".vidCaption").html($(".video:eq("+index+")").find("p[data-start='"+Math.floor(time)+"']").html());
	} else if ( $(".video:eq("+index+")").find("p[data-end='"+Math.floor(time)+"']").length == 1 ) {
		$(".video:eq("+index+")").find(".vidCaption").empty();
	}
}

function adjustSwfSize(swfId, width, height) {
	$(".swfObject:eq("+swfId+")").css({width:width,height:height}).parent().css("width", width);
	//$(".swfObject:eq("+swfId+")").css({width:width,height:height}).parent().css({width:width,height:height});
	$(".swfObject:eq("+swfId+")").parent().find(".ccBtn").removeClass("notVisible");
	$(".swfObject:eq("+swfId+")").parent().find(".ccBtn").bind("click", function(){$(this).parents(".video").find(".vidCaption").toggle();});
}

function returnResults(cheatsheet, userAnswers) {
	var table = $(document.createElement("table"));
	var th = $(document.createElement("thead")).append($(document.createElement("th")).text("Question")).append($(document.createElement("th")).text("Your Answer")).append($(document.createElement("th")).text("Correct Answer"));
	
	table.append(th);
	
	for ( var i = 0; i < cheatsheet.length; i++ ) {
		var tr = $(document.createElement("tr")).append($(document.createElement("td")).text($("div.quiz dl:eq("+i+") dt").text())).append($(document.createElement("td")).text(userAnswers[i])).append($(document.createElement("td")).text(cheatsheet[i]));
		
		if ( userAnswers[i] == cheatsheet[i] ) {
			tr.addClass("correct");
		} else {
			tr.addClass("incorrect");
		}
		
		table.append(tr);
	}
	return table;
}

function randomNumber(length) {
  var randVal = 0+(Math.random()*(length-0));
  return Math.round(randVal);
}

var touchSupport = 'createTouch' in document;

function nextCycleImage(cycleID) {
	var $imgs = $('.cycle img');

	$imgs.each(function(i) {
		if ( $imgs.eq(i).hasClass('visible') ) {
			$imgs.eq(i).toggleClass('visible').fadeOut('500', function() {
				if ( i === ($imgs.length - 1) ) {
					$imgs.eq(0).toggleClass('visible').fadeIn('500');
				} else {
					$imgs.eq(i+1).toggleClass('visible').fadeIn('500');
				}
				
			});
		}
	});

	var t = setTimeout('nextCycleImage()', 7000);
}


//=====================================
//////////////Live Events/////////////
//=====================================


//Toggle Text Version
//=====================================
$(".toggleSwitch").live('click', toggleVersion);

function toggleVersion(evt){
	$(this).text(($(this).text() == lang.Interactive)? lang.TextVersion : lang.Interactive);
	$(this).parent().children("div.textVersion").toggleClass("notVisible");
	
	if ( $(this).parent().children("div.swfObject").length > 0 ) {
		$(this).parent().children("div.swfObject").toggleClass("notVisible");
	} else if ( $(this).parent().children("iframe").length > 0 ) {
		$(this).parent().children("iframe").toggleClass("notVisible");
	}

	return false;
}

//Pop Up Code
//=====================================
$('.pop').live('click', function() {
	var location = $(this).attr("href");
	var height   = 600;
	var width    = 700;

	if ( $(this).attr("data-height") ) { height = $(this).attr("data-height"); }
	else if ( $(this).attr("rel") ) { height = $(this).attr("rel"); }

	if ( $(this).attr("data-width") ) { width = $(this).attr("data-width"); }

	var externalLink = (location.indexOf("http://") != -1)? "yes" : "no" ;
	var popup = window.open(location,'popUp','toolbar='+externalLink+',location='+externalLink+',directories='+externalLink+', status=yes,menubar='+externalLink+',scrollbars=yes,resizable=yes,width='+width+',height='+height);
	
	return false;
});

//Print Text Version
//=====================================
$(".printText").live("click", printTextVersion);

function printTextVersion(evt){
	$("div#content").addClass("hideInPrint");

	var copiedElement = $(this).parent().clone().html();
	var overlay = $(document.createElement("div")).attr("id", "overlay");
	var printBox = $(document.createElement("div")).attr("id", "printBox").html(copiedElement).append($(document.createElement("button")).addClass("close").attr("title", lang.CloseWindow).append($(document.createElement("span")).text(lang.Close)).bind("click", removeElement));

	$("body").prepend(overlay.append(printBox));
	$("div#printBox").find("button.printText").remove();

	window.print();

	return false;
}

function removeElement(evt){
	$("div#content").removeClass("hideInPrint");
	$(this).parent().parent().remove();
	
	return false;
}


/*

NEW Added by Will Jones

*/

//show scroll background image for top Nav
/*$(window).scroll(function(){ 

//height of scroll
var a = 90;
var pos = $(window).scrollTop();
	if(pos > a) {
	
		$("div#header").addClass('scroll');
		
	} else {
	
		$("div#header").removeClass('scroll');
	}
});*/
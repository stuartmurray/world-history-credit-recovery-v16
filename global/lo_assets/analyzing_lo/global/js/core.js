// FLVS LO Core
// v1
//
// AUTHORS:
//		Will Jones || wjones@flvs.net
// 
// Last Modified: 5/2/12
// =====================================

var root;
var lessonRoot;
var levelRoot;
var lessonCount;


//Pages
var prevPage;
var nextPage;
var endPage;
var previousPage;
var levelTitle;
var pageCount;

var startPos;
var sitePos;//-2
var lastPos;
var copiedContents;
var accessOff = true;
var currentPos;

//Link Locations  (easy access)
//var endPage = '../pages/end.htm';
var flvs_link = '../global/imgs/logo_flvs_blue.png';
var arrowLeft = '../global/imgs/left_arrow.png';
var arrowRight = '../global/imgs/right_arrow.png';

var viewPortMeta = $(document.createElement("meta")).attr("name", "viewport").attr("content", "width=768px,initial-scale=1,minimum-scale=1,maximum-scale=1").attr("user-scalable", "no");
$("html head").append(viewPortMeta);
var appMeta = $(document.createElement("meta")).attr("name", "apple-mobile-web-app-capable").attr("content", "yes");
$("html head").append(appMeta);

$.ajax({
	url: sitemap,
	processData: false,
	dataType: "xml",
	error: function(jqXHR, exception){
		
		//errors
		 if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
		//alert(sitemap);
	},
	beforeSend: function(){
		copiedContents = $("body div#appContent").children().clone(true,true);
		$("body").empty();
		$("body").append($(document.createElement("div")).attr("id","appLoader"));
	}
	,
	success: function(xml) {

		//root of xml - site
		root = $(xml).find("sitemap");
		lessonRoot = $(root).find("lesson");
		levelRoot = $(root).find("level");
		lessonCount = $(root).find("page").length - 1;
		
		var textLogo = $(lessonRoot).attr("title");
		
		//go through each page listed						
		//find the current positon in the Level
		$(root).find("page").each(function(index) {
											
			if($(this).attr("title") == document.title) {
					
				sitePos = startPos = lastPos = index;
				//console.log('attr title: ' + $(this).attr("title"));
			}
				
		}); //end each loop	
				
		//Access pages			
		var currentpage = $(root).find("page:eq("+(sitePos)+")");	
				
		//loop through the current level - find index of current page
		//Check if LO is Showing Levels
		//======================================================================================
		
		if ( typeof(show.lo_levels) === undefined || show.lo_levels === true ) {
												  										
				$(currentpage).parent().find('page').each(function(index) {
					
					if ($(this).attr('title') == document.title) {
					
						sitePos = index;
						//console.log('Page: ' + sitePos);
					}
					
				});
		}
		
		//console.log('New Page: ' + sitePos);
			
		//# of Pages within the Level
		pageCount = $(currentpage).parent().find('page').length-1;		
			
		//titles
		levelTitle = currentpage.parent().attr('title');
		var pageTitle = currentpage.attr('title');
		
		//Check if LO is Showing Levels
		//======================================================================================
			
		if ( typeof(show.lo_levels) === undefined || show.lo_levels === true ) {
				
			//console.log('pos:'+ $(currentpage).parent().next().find("page:eq("+(0)+")").attr('href'));
			//next level page
			var nextLevelPage = $(currentpage).parent().next().find("page:eq("+(0)+")").attr('href');
			//prev level page
			var prevLevelPage = $(currentpage).parent().prev().find("page").eq(-1).attr('href');
			
			//console.log('PrevLevel: ' + prevLevelPage);
				
			//create end page
			endPage = (nextLevelPage == undefined) ? $(root).attr('href') : nextLevelPage;
			previousPage = (prevLevelPage == undefined) ? $(root).attr('href') : prevLevelPage;
			
			//console.log('Pos: ' + sitePos);
									
			//Access Next and Previous Page
			nextPage = (sitePos == pageCount) ? endPage : $(currentpage).parent().find("page:eq("+(sitePos + 1)+")").attr('href');
			prevPage = (sitePos == 0) ? previousPage : $(currentpage).parent().find("page:eq("+(sitePos - 1)+")").attr('href');
			
			//console.log('Prev: ' + prevPage + ' and Next:' + nextPage);
			
			//init the Drop Down Menu
			var menu = new MainMenu({Pages: $(currentpage).parent().find("page"), Last: $(currentpage).parent().find("page").eq(-1)});
			
		} else {
				
			//NO Levels  - Show all Lesson Pages
			
			//Access Next and Previous Pages
			nextPage = (sitePos == lessonCount) ? $(root).attr('href') : $(root).find("page:eq("+(sitePos + 1)+")").attr('href');
			prevPage = (sitePos == 0) ? $(root).attr('href') : $(root).find("page:eq("+(sitePos - 1)+")").attr('href');
			
			//init the Drop Down Menu
			var menu = new MainMenu({Pages: $(root).parent().find("page"), Last: $(currentpage).parent().find("page").eq(-1)});
			
		}
			
		//======================================================================================
		
		/*
			Header elements
		*/
			
		//Nav Buttons
		var nav = new NavButtons();
				
		//Crumbs
		var crumb = new breadCrumbs();			
		var crumbs = $(document.createElement("div")).attr("id", "crumbs").append(crumb);		
		
		
		var logo = $(document.createElement("div")).attr("id", "logo");
		//Page Elements
		var header = $(document.createElement("div")).attr("id", "header").prepend(menu).prepend(nav).prepend(logo);
			
		//first in the content section - title with H2 tag
		var firstTitle = $(document.createElement('h2')).addClass('pageTitle').text(pageTitle);
		var content = $(document.createElement("div")).attr("id", "content").prepend(crumbs).append(firstTitle);	
		
					
		//FOOTER==========
		if (typeof(show.footer) !== undefined && show.footer == true) {
		
			var footerLogo = $(document.createElement("img")).attr('src',flvs_link).attr('width','100').attr('height','34').attr('alt','FLVS Logo');
			var footer_span = $(document.createElement("div")).attr("id", "footer_span").html(footerText).append(footerLogo);
			//Final Footer
			var footer = $(document.createElement("div")).attr("id", "footer").append(footer_span);
			
		}
		
		//footer HR use
		if (typeof(show.footer_hr) !== undefined && show.footer_hr === true) {
			
			//add hr to footer
			var hr =  $(document.createElement("hr"));
			$(footer).prepend(hr);
		}
		//====== End Footer
		
		var wrapper = $(document.createElement("div")).attr("id", "page").append(header);
		
		$("body").empty();
				
		var docTitle = document.title;
		document.title = docTitle + " | FLVS";
		
		$('body').append($(wrapper).append($(content).append(copiedContents)).append(footer));
		activateAssets();

	}
	
	
});


/*

Class Functions


*/

//Drop Down Menu
var MainMenu = function(thepages) {
	
	var firstLinkname = 'Menu';
	
	if ( typeof(show.dropDownMenu) === undefined || show.dropDownMenu === true ) {
	
		var dl = $(document.createElement('dl')).attr('id','menu').addClass('dropdown');
		var dt = $(document.createElement('dt')).append($(document.createElement('a')).on('click',toggleMenu).attr('href', '#').append($(document.createElement('span')).text(firstLinkname)));
		var dd = $(document.createElement('dd'));
		var ul = $(document.createElement('ul')).addClass('notVisible');
		
		$(dl).append(dt).append($(dd).append(ul));
		
		//get each page
		if (thepages.Pages != undefined) {
			
				thepages.Pages.each(function() {
					
					var title = $(this).attr('title'); 
					var href = $(this).attr('href');
					
					//console.log('Last: ' + $(this).attr('last'));
					
					//if last page and current page match - do not show
					if ($(this).attr('href') != thepages.Last.attr('href')) {
						
						var li = $(document.createElement('li')).append($(document.createElement('a')).attr('href',href).text(title));
					
					}
					
					$(ul).append(li);
					
				});
		}
		
		return dl;	
	
	}
	
}



//Function Toggle Menu
function toggleMenu(evt) {
	evt.preventDefault();		
	$(".dropdown dd ul").toggleClass("notVisible");
	
}
//If Outside of Menu is Clicked - Hide Menu
$(document).on('click', function(e) {
	
	if (!$(".dropdown dd ul").hasClass("notVisible")) {
		var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown"))
          $(".dropdown dd ul").addClass("notVisible");	
	}
          			 
});	    
		

//Bread Crumbs
var breadCrumbs = function() {
	
	if ( typeof(show.breadcrumbs) === undefined || show.breadcrumbs === true ) {
	
		var crumbcase = $(document.createElement('span')).attr('id','pageCrumbs');
		//var page = $(root).find("page:eq("+sitePos+")");
		//console.log(lessonCount);
		//lessonCount
		var crumbs = $(crumbcase).html('<strong>' + levelTitle + '</strong>' + " - " + "Page " + (sitePos + 1) + " of " + (pageCount + 1));
		
		return crumbs;	

	}
}

//
//Navigation Buttons (Left - Right)
var NavButtons = function() {
	
	if ( typeof(show.headerNavigation) === undefined || show.headerNavigation === true ) {
		
		//attributes = Id, ImgSrc, Alt
		var ul = $(document.createElement("ul")).attr("id", "nav");	
		
		var navItemPrev = $(document.createElement("li")).attr("id", "prevPage").prepend($(document.createElement("a")).attr("href", prevPage).prepend($(document.createElement("img")).attr('src',arrowLeft).attr('alt','image').attr('width', navButtonSize).attr('height',navButtonSize).attr('border',0)));
		var navItemNext = $(document.createElement("li")).attr("id", "nextPage").prepend($(document.createElement("a")).attr("href", nextPage).prepend($(document.createElement("img")).attr('src',arrowRight).attr('alt','image').attr('width', navButtonSize).attr('height',navButtonSize).attr('border',0)));
		
		ul.append(navItemPrev).append(navItemNext);
			
		return ul;/**/
	}
}
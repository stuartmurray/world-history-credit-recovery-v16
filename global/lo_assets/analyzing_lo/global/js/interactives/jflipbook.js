$('document').ready(function() {
	
	//Setup Book
 	$('#jFlipbook').booklet({
		width: 700,
    	height: 400,
    	pagePadding: 20,
    	hash: true,
    	manual: false,
    	closed: true,
    	autoCenter: true,
    	covers: true
  	});

  	if ( !$(' #jFlipbook ').hasClass('cover-childrens') ) {
  		$(' #jFlipbook div.b-page-cover ').eq(0).append('<h4 class="instructions">Open this flipbook by hovering over the page ends and selecting with your mouse, or navigate with your keyboard arrows.</h4>');
  	}

	$('.beginning-btn').click(function(e) {
		e.preventDefault();
		
		$('#jFlipbook').booklet(1);
	});


	//Questions
	//=====================
	$('.questions').each(function(i) {
		var $this = $(this).find('.question');

		var correct   = [],
			incorrect = [];

		//Hide
		$this.each(function(i) {
			$this.eq(i).find('.correct').hide();
			correct.push($this.eq(i).find('.correct').text());

			$this.eq(i).find('.incorrect').hide();
			incorrect.push($this.eq(i).find('.incorrect').text());

			if ( i > 0 ) { $this.eq(i).hide(); }
		});

		$this.find('input[type=radio]').click(function () {
			var indexArr = $(this).attr('name').split('-');
			var index = indexArr[1];

			//console.log(index);

			$this.find('input[type=radio]').change(function() {
				$this.find('.alert').remove(); //Remove alert box
			});

			//Add Button and Click Event if Button isn't in DOM
			if ( !$this.eq(index).find('.button').length ) {
				$this.eq(index).append('<div><a class="button">Submit</a></div>');

				$('.button').on('click', function() {
				var action = $(this).text();

				switch(action) {
					case "Submit":
						if ( !$this.eq(index).find('.alert').length ) { $this.eq(index).prepend('<div class="alert"></div>') } //If Alert isn't in the DOM, add it

							$this.find('input[type=radio]').each(function(i) {

							if ( $this.find('input[type=radio]').eq(i).attr('checked') === 'checked' && $this.find('input[type=radio]').eq(i).val() === 'true' ) {
								$this.find('.alert').addClass('pass').removeClass('fail').html(correct[index]);
								
								if ( index >= ($this.length - 1) && $this.find('.button').text('Next') ) { $this.find('.button').text('Restart'); }
								else { $this.find('.button').text('Next'); }

								return false;
							} else {
								$this.find('.alert').addClass('fail').removeClass('pass').html(incorrect[index]);
							}
						});

						break;

					case "Next":
						$this.eq(index).slideUp(); //Hide current question
						$this.eq(index+1).slideDown(); //Show next question

						$this.find('input[type=radio]').each(function() { $(this).removeAttr('checked'); }); //Unchceck all radio buttons
						$this.find('.alert').remove(); //Remove alert box
						$this.eq(index).find('.button').parent().remove(); //Remove button
						
						break;

					case "Restart":
						$this.eq(index).slideUp(); //Hide current question
						$this.eq(0).slideDown(); //Show first question

						$this.find('input[type=radio]').each(function() { $(this).removeAttr('checked'); }); //Unchceck all radio buttons
						$this.find('.alert').remove(); //Remove alert box
						$this.eq(index).find('.button').parent().remove(); //Remove button

						break;
				}

				//Alert Close Button
				$('.alert').on('click', function() {
					$(this).stop().fadeOut(500, function() {
						$(this).remove();
					})
				});
			});
			}
		});
	});
});
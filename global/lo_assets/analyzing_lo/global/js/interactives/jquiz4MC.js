$(document).ready(function() {
  //SETUP Variables
  var container = ".jQuiz4MC",
    directions,
    endfeedback,
    questions = [],
    sub_questions = [],
    selections = {
        a : [],
        b : [],
        c : [],
        d : []
    },
    feedback = {
        a : [],
        b : [],
        c : [],
        d : []
    };


//Use background image, if defined
var bg = $( container ).attr('data-bg');

if ( typeof( bg ) !== "undefined" ) {
	$( container ).css('background', 'transparent url('+ bg +') no-repeat center center');
}

$(container).each(function() {

    $.ajax({
        url: $(this).attr('data-ref'),
        dataType: 'xml',
        success: function(data) {
            
            //SETUP VARS FOR QUICKER jquiz2_init
            directions = $(data).find('directions').text();
            endfeedback = $(data).find('endfeedback').text();
            
            $(data).find('question').each(function(index) {
                questions.push($(this).attr('string'));
                
                if ($(this).attr('substring').length) {
                    sub_questions.push($(this).attr('substring'));
                }
                selections.a.push($(this).find('choice').eq(0).attr('string'));
                selections.b.push($(this).find('choice').eq(1).attr('string'));
                selections.c.push($(this).find('choice').eq(2).attr('string'));
                selections.d.push($(this).find('choice').eq(3).attr('string'));
                feedback.a.push($(this).find('feedback').eq(0).text());
                feedback.b.push($(this).find('feedback').eq(1).text());
                feedback.c.push($(this).find('feedback').eq(2).text());
                feedback.d.push($(this).find('feedback').eq(3).text());
            });
            
            // START THE INTERACTIVE
            init(directions, endfeedback, questions, sub_questions, selections, feedback);
        }
    });

});
    
  function init(directions, endfeedback, questions, sub_questions, selections, afeedback) {

    tries_num = 0;
    correct_num = 0;

    // SHOW INSTRUCTIONS
    showOverlay("Instructions", directions, "Go!", container);

    // ADD QUESTIONS and ANSWERS
    $(container).append('<div class="question"><span class="bracket"></span><span class="text">&nbsp;</span></div>');

    for (i = 0; i < questions.length; i++) {
        $(container).append('<div id="item-'+i+'" class="item"></div>');
      if (!sub_questions.length) {
        $(container + ' .item').eq(i).append('<h1>'+questions[i]+'</h1><ol class="answer"><li id="a" class="selection"><div class="letter">A.</div><div class="text">'+selections.a[i]+'</div></li><li id="b" class="selection"><div class="letter">B.</div><div class="text">'+selections.b[i]+'</div></li><li id="c" class="selection"><div class="letter">C.</div><div class="text">'+selections.c[i]+'</div></li><li id="d" class="selection"><div class="letter">D.</div><div class="text">'+selections.d[i]+'</div></li></ol>');
      } else {
        $(container + ' .item').eq(i).append('<h1>'+questions[i]+'</h1><h2>'+sub_questions[i]+'</h2><ol class="answer"><li id="a" class="selection"><div class="letter">A.</div><div class="text">'+selections.a[i]+'</div></li><li id="b" class="selection"><div class="letter">B.</div><div class="text">'+selections.b[i]+'</div></li><li id="c" class="selection"><div class="letter">C.</div><div class="text">'+selections.c[i]+'</div></li><li id="d" class="selection"><div class="letter">D.</div><div class="text">'+selections.d[i]+'</div></li></ol>');
      }
        
        
        //HIDE THE UPCOMING QUESTIONS
      if (i === 0) { $(container + ' .item').addClass('active'); }
      if (i > 0) { $(container + ' .item').addClass('inactive'); }
    };

    // ADD THE NAVIGATION and FEEDBACK
    $(container).append('<div class="navigation">1 of '+questions.length+'</div><div class="item endfeedback inactive"></div>');

    // ANSWER CLICK FUNCTION
    $(container + ' .selection').click(function() {
        // VARS FOR THE CURRENT QUESTION ID
        var item = $(this).parent().parent().attr('id'),
            itemArray = item.split('-'),
            id = itemArray[1];
            letter = $(this).attr('id');

            //console.log(feedback);

        if (letter === 'a') { showOverlay("", feedback.a[id], "Close", container); }
        else if (letter === 'b') { showOverlay("", feedback.b[id], "Close", container); }
        else if (letter === 'c') { showOverlay("", feedback.c[id], "Close", container); }
        else if (letter === 'd') { showOverlay("", feedback.d[id], "Close", container); }

        nextQues(id);
    });
  }

  function nextQues(currQues) {
    tries_num = 0;

    // SETUP VARS FOR MOVING THROUGH QUESTIONS and UPDATING NAV
    var nextQues = Number(currQues) + 1,
        nextQuesNav = nextQues + 1;
  
    if (nextQuesNav > questions.length) {
        // APPEND THE TOTAL CORRECT
        $(container + ' .question span.text').text('You have finished!');
        $(container + ' .endfeedback').append('<h3>'+endfeedback+'</h3>');

        // REMOVE NAVIGATION NUMBERS
        $(container + ' .navigation').html('').remove();
        
        // SHOW RESTART BUTTON
        $(container).append('<button>Restart</button>');
        $(container + ' button').hide().fadeIn('slow');
        
        // RESTART CLICK FUNCTION
        $(container + ' button').click(function() {
            for (i = 0; i < questions.length; i++) { $(container + ' #item-'+i).remove(); };
            // REMOVE ALL QUESTIONS

            $(this).fadeOut('slow', function() { $(this).remove(); }); // REMOVE RESTART BUTTON
            $(container + ' .question').fadeOut('slow', function() { $(this).remove();  }); //REMOVE STATEMENT
            $(container + ' .endfeedback').fadeOut('slow', function() {
                // REMOVE FEEDBACK
                $(this).remove();
                
                // RESTART Interactive
                init(directions, endfeedback, questions, sub_questions, selections, feedback);
            });
        });

    } else {
        $(container + ' .navigation').html(nextQuesNav + ' of ' + questions.length); // UPDATE NAVIGATION
    }
    
    // HIDE CURRENT QUESTION then SHOW THE NEXT
    $(container + ' .item').eq(currQues).switchClass('active', 'inactive', 500);
    $(container + ' .item').eq(nextQues).switchClass('inactive', 'active', 500);
  };
});
jQuery(function($) {			
		
	$('.sp_container').each(function(index) {	
	// adding a close button to the table of contents. this is styled in the css. not a link, do not want screenreaders to pick it up.
		$(this).children('.sp_table_contents_container').append('<div class="sp_table_contents"><div class="sp_toc_close">Close</div></div>');
	//do not want the toc open when slideshow is first viewed. toc is not accessed by screenreaders. 			
		$(this).children('.sp_table_contents_container').hide();
	
	//by saying "each", each .sp_container will loop through each slide and run the code that follows.	
		$(this).children('.sp_slide').each(function(index) {
	//displays how many div's with .sp_slide there are.		
			var sp_slideLength = $(this).parents('.sp_container').children('.sp_slide').length;	
	//grabbing the h4 title text so we can add it to the table of contents list. 	
			var sp_slideTitle = $(this).children('.sp_padding').children('h4.sp_slide_title').html();
	//Adding the slide title (that we just made a var for) and the slide it is on.	(title 1 of 4) for instance.			
			$(this).parent('.sp_container').children('.sp_table_contents_container').children('.sp_table_contents').append("<p>" + sp_slideTitle + " <span class='sp_small'>" + (index + 1) + " of " + sp_slideLength + "</span></p>");
	//Adding a slide counter container in the page title header. We are adding it in the header so a screen reader will read "Title 1 of 4" to let students know where they are in the interactive in each slides title.		
			$(this).children('.sp_padding').children('h4.sp_slide_title').append("<span class='sp_slide_counter'>" + (index + 1) + " of " + sp_slideLength + "</span>");
		});
		
	//wrapping all the slides in a container div. need to do this so we can find out how wide the entire list of div slides are so we can "move" the container by each slides width.			
		$(this).children('.sp_slide').wrapAll('<div class="sp_content"></div>');
	//adding the table of contents button and next and back buttons (but div's because we do not want screenreaders reading them) on the slideshow. styled in the css.	
		$(this).children('.sp_content').after("<div class='sp_toc_open'></div><div class='sp_next'></div><div class='sp_back'></div>");
	//mostly this is not needed, but if anyone wanted to add anything that should be stripped out if there is js... they can add a class of .noscript and js will remove it.	
		$(this).find('.noscript').remove();
	//hiding the back button when the slideshow first opens.	
		$(this).children('.sp_back').hide();
	//getting the width of the sp_container which is in the css. everything uses this number.	
		var sp_css_container_width = $(this).width();
		
		$(this).children('.sp_content').children('.sp_slide').css({ 
			//sets the width of each slide to the width of the container
			'width' : sp_css_container_width});
			//we multiply the width of the container by the number of slides and that is how we determine how wide our content wrapper needs to be.
		$(this).children('.sp_content').css({
			//say the container in the css is 700 and there are 4 slides (which above are set now to 700) the content wrapper around the slides will now be 700px x 4 in width or 2800px.
			'width' : sp_css_container_width * $(this).find('.sp_slide').length});
	});
	
	
	// start table of contents
	 $(this).find('.sp_table_contents').children('p').bind('click', sp_moveToSlide);
		function sp_moveToSlide(e) {
			//when someone selects a item in the table of contents (each TOC header is wrapped in a p tag) the toc will fade out.
			$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').fadeOut();
			//we just need to find out how many slides there are. we can not use a variable set in another function. it would have to be a global variable in order to pick that information up. because there can be more than one slideshow on the page, we do not want this to be a global variable.
			var sp_track_slideLength = $(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_content').children('.sp_slide').length;
			//the index is the ordered number the <p> is in. so... if there are 5 <p> and you select the 3rd one, that 3rd ones index is 3.
			if ($(this).index() == sp_track_slideLength) {
			//basically, if you are at the last screen (so if you select the last indexed <p> that equals the last slide, then the next/back fades in or out.	
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_next').fadeOut();
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_back').fadeIn();
			};

			if ($(this).index() != sp_track_slideLength) {
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_next').show();
			};			
						
			if ($(this).index() == 1) {
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_back').fadeOut();
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_next').fadeIn();
			};
			
			if ($(this).index() != 1) {
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_back').fadeIn();
			};	
										
			// the items below animate the slideshow to the appropriate slide when the toc item is selected
			var sp_slideWidth = $(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_content').children('.sp_slide').width();
			var sp_contentWidth = $(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_content').width();
			var sp_contentPosition = $(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_content').position();
			var sp_checkPosition = sp_contentPosition.left;		
			
			var sp_slideLength = $(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parent('.sp_container').children('.sp_content').children('.sp_slide').length;
			if ( (sp_checkPosition) >= (sp_checkPosition - sp_slideWidth * $(this).index()) ) {
				var sp_left_indent = sp_slideWidth * (-1) * ($(this).index() - 1);
				
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parents('.sp_container').children('.sp_content').animate({'left' : (sp_left_indent)}, 500, function () { 
					sp_forms();			
				});
				
			} else if ( (sp_checkPosition) <= (sp_checkPosition - sp_slideWidth * $(this).index()) ) {
				var sp_right_indent = sp_slideWidth * $(this).index();
				$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').parents('.sp_container').children('.sp_content').animate({'left' : (sp_right_indent)}, 500, function () {				
					sp_forms();  
				});					
			}	
						
		};
		// end toc
		
		 $(this).find('.sp_table_contents_container').children('.sp_table_contents').children('.sp_toc_close').bind('click', sp_toc_close);
			function sp_toc_close(e) {
			 	$(this).parent('.sp_table_contents').parent('.sp_table_contents_container').fadeOut();
		 }
		 
		 $(this).find('.sp_toc_open').bind('click', sp_toc_open);
			function sp_toc_open(e) {
			 	$(this).parent('.sp_container').children('.sp_table_contents_container').fadeIn();
		 }		 
									 
	 $(this).find('.sp_next').bind('click', sp_moveNext);
		function sp_moveNext(e) {					
			var sp_slideWidth = $(this).parent('.sp_container').children('.sp_content').children('.sp_slide').width();			
			var sp_contentWidth = $(this).parent('.sp_container').children('.sp_content').width();
			var sp_contentPosition = $(this).parent('.sp_container').children('.sp_content').position();
			var sp_checkPosition = sp_contentPosition.left;			
			var sp_left_indent = sp_checkPosition - sp_slideWidth;	
			$(this).unbind();
			$(this).parents('.sp_container').children('.sp_back').fadeIn();
			$(this).parents('.sp_container').children('.sp_content').animate({'left' : (sp_left_indent)}, 500, function () { 
				$(this).parent('.sp_container').children('.sp_next').bind('click', sp_moveNext);
				sp_forms(); 						
			});
			
			var sp_slideNum = sp_left_indent/sp_slideWidth;
			var sp_slideNum = Math.abs(sp_slideNum);
			var sp_slideNum = Math.round(sp_slideNum);
					
			if ((sp_slideNum + 1) == ($(this).parents('.sp_container').children('.sp_content').children('.sp_slide').length)) {
				$(this).fadeOut();				
			} else if ((sp_slideNum + 1) != ($(this).parents('.sp_container').children('.sp_content').children('.sp_slide').length)) {
				$(this).fadeIn();
			}	
			
		};
		// end next
		
	$(this).find('.sp_back').bind('click', sp_moveBack); 
		function sp_moveBack(e) {
			
			var sp_slideWidth = $(this).parent('.sp_container').children('.sp_content').children('.sp_slide').width();
			var sp_contentWidth = $(this).parent('.sp_container').children('.sp_content').width();
			var sp_contentPosition = $(this).parent('.sp_container').children('.sp_content').position();
			var sp_checkPosition = sp_contentPosition.left;			
			var sp_right_indent = sp_checkPosition + sp_slideWidth;
			
			$(this).unbind();
			$(this).parents('.sp_container').children('.sp_next').fadeIn();
			
			$(this).parents('.sp_container').children('.sp_content').animate({'left' : (sp_right_indent)}, 500, function () {				
				$(this).parent('.sp_container').children('.sp_back').bind('click', sp_moveBack);
				sp_forms();  
			});
		
			var sp_slideNum = sp_right_indent/sp_slideWidth;
			var sp_slideNum = Math.abs(sp_slideNum);
			var sp_slideNum = Math.round(sp_slideNum);
			
			if ((sp_slideNum+1) == 1) {
				$(this).parent('.sp_container').children('.sp_content').animate({"left":"0px"});
				$(this).fadeOut();
			} else if ((sp_slideNum+1) != 1) {
				$(this).fadeIn();	
			}
			
		};
		// end back	
});

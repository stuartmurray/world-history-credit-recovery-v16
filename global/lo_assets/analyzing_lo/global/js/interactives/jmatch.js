$(document).ready(function() {

var container = ".jMatch"
   ,directions
   ,innerDirections
   ,count = 0
   ,correct = 0
   ,total
   ,words = []
   ,ids = []
   ,definitions = []
   ,answers = []
;

//Use background image, if defined
var bg = $( container ).attr('data-bg');

if ( typeof( bg ) !== "undefined" ) {
	$( container ).css('background', 'transparent url('+ bg +') no-repeat center center');
}

$(container).each(function() {

	$.ajax({
		dataType: 'xml',
		url: $(this).attr('data-ref'),
		success: function(data) {

      directions = $(data).find('settings').attr('directions');
      innerDirections = $(data).find('settings').attr('innerDirections');
      total = $(data).find('word').size();
      lTitle = $(data).find('title').attr('left');
      rTitle = $(data).find('title').attr('right');
      
			
			$(data).find('word').each(function(index){
				words[index] = $(this).text();
				ids[index] = $(this).attr('id');
			});
			$(data).find('definition').each(function(index){
				definitions[index] = $(this).text();
			});
			
      //Show Instructions
      showOverlay("Instructions", directions, "Go!", container);

      //Start the Interactive
			init(innerDirections, container, count, correct, total, words, ids, definitions);
		}
	}); //END AJAX REQUEST
	
	function init(innerDirections, container, count, correct, total, words, ids, definitions) {

		$(container).append('<div class="t3565"></div>');
		$('.t3565').append('<div class="innerInstructions"><h2>'+innerDirections+'</h2></div>');
		$('.t3565').append('<h1 id="title1">'+lTitle+'</h1><h1 id="title2">'+rTitle+'</h1>');
		$('.t3565').append('<div class="t3565Left"></div><div class="t3565Right"></div>');
        $('.t3565Left').append('<div class="words"></div>');
		$('.t3565Right').append('<div class="definitions"></div>');
		
		for (i=0; i<words.length; i++) {
			$('.words').append('<div class="word" id='+ids[i]+'>'+words[i]+'</div>');
			$('.definitions').append('<div class="definition" id='+ids[i]+'>'+definitions[i]+'</div>');
		}

		$('.definitions').randomize('.definition');
		
		$('.definitions, .words').jScrollPane({
			showArrows: true,
			verticalGutter: 10
		});
		
		$('.word, .definition').click(function() {
			$(this).addClass('selected');
			count++;
			if (count == 2) {
				$(container).find('.selected').each(function(index) {
					answers[index] = $(this).attr('id');
				});
				if (answers[0] == answers[1]) {
					$(container).find('.selected').slideUp('slow', function() {
						$(this).remove()
					});
					count = 0;
					correct ++;
					if (correct == total) {
			            showOverlay("Great job!", "If you would like to try this again, please select the Restart button.", "Restart", container);

						$(container + ' .go-btn').click(function() {
							$('.t3565').fadeOut('slow', function() {
								$(this).remove();
								init(innerDirections, container, count, correct, total, words, ids, definitions);
							});
						});
					}
				}
				else {
          showOverlay("Try Again.", "That selection was incorrect. Please try again.", "Close", container);

					$(container).find('.selected').removeClass('selected');
					count = 0;
				}
			}
		});	 // END CLICK FUNCTION
	};
	
});
	
//RANDOMIZE Function
(function($) {

$.fn.randomize = function(childElem) {
  return this.each(function() {
      var $this = $(this);
      var elems = $this.children(childElem);
      
      elems.sort(function() { return (Math.round(Math.random())-0.5); });  

      $this.empty();  

      for(var i=0; i < elems.length; i++)
        $this.append(elems[i]);      

  });    
}
})(jQuery);
	
});
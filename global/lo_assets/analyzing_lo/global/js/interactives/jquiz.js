$(document).ready(function() {
	$('.alert').slideDown();

	$('.button').click(function() {
		for ( i = 0; i < $('.question label').length; i++ ) {
			if ( $('.question label').eq(i).find('input[type=radio]').attr('checked') == 'checked' ) {
				$(this).parent().parent().slideUp();
				$(this).parent().parent().parent().find('.feedback').slideDown();
				$('.alert').hide();
				break;
			}
		}
	});
});
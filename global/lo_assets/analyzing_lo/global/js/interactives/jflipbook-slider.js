$(document).ready(function() {

var container = ".jFlipbook-Slider";

//Use background image, if defined
var bg = $( container ).attr('data-bg');

if ( typeof( bg ) !== "undefined" ) {
	$( container ).css('background', 'transparent url('+ bg +') no-repeat center center');
}


//Hide Future Pages
for ( i = 0; i < $(container + ' .page').length; i++ ) {
	if ( i > 0 ) {
		$(container + ' .page').eq(i).hide();
	}
}

//Click event for page transitions
$( container + ' .page' ).click(function() {

	var $this = $( this );
	var curr  = ($this.index() + 1);
	var total = $( container + ' .page' ).length;

	//Hide current page and show the next
	$this.hide('slide', {direction: 'left'}, 500, function() {
		$this.next('.page').show('slide', {direction: 'right'}, 500);

		//Update navigation numbers
		$(container + ' .navigation').text((curr + 1) +' of ' + total);

		//If the current page is the last page, then go back to the beginning
		if ( curr === total ) {
			$(container + ' .page').eq(0).toggle('drop', 500);
			$(container + ' .navigation').text('1 of ' + total);
		}
	});
});

});
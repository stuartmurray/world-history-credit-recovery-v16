// FLVS LO Core
// v1
//
// AUTHORS:
//		Will Jones || wjones@flvs.net
// 
// Last Modified: 3/16/12
// =====================================

var root;
var lessonRoot;
var startPos;
var sitePos;
var lastPos;
var copiedContents;
var accessOff = true;
var currentPos;

var viewPortMeta = $(document.createElement("meta")).attr("name", "viewport").attr("content", "width=768px,initial-scale=1,minimum-scale=1,maximum-scale=1").attr("user-scalable", "no");
$("html head").append(viewPortMeta);
var appMeta = $(document.createElement("meta")).attr("name", "apple-mobile-web-app-capable").attr("content", "yes");
$("html head").append(appMeta);

$.ajax({
	url: sitemap,
	processData: false,
	dataType: "xml",
	error: function(jqXHR, exception){
		
		 if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
			
		//alert(sitemap);
	},
	beforeSend: function(){
		copiedContents = $("body div#appContent").children().clone(true,true);
		/*copiedContents2 = $("body div#appContent").children().clone(true, true);*/
		$("body").empty();
		$("body").append($(document.createElement("div")).attr("id","appLoader"));
	}
	,
	success: function(xml) {

		//root of xml - site
		root = $(xml).find("sitemap");
		lessonRoot = $(root).find("lesson");
		
		var textLogo = $(lessonRoot).attr("title");
		
		console.log('test' + textLogo);
				
		//go through each page listed
		$(root).find("page").each(function(index) {
			
			if($(this).attr("title") == document.title) {
				sitePos = startPos = lastPos = index;
			}
			
		}); //end each loop
		
			
		
		//var lessonCount = $(root).find("page:eq("+(sitePos)+")").parent().length-1;
		//var lessonEnd = $(root).find("page:eq("+(sitePos)+")").attr("href") == $(root).find("page:eq("+(sitePos)+")").parent().find("page:eq("+(lessonCount)+")").attr("href");
				
		//var page = $(root).find("page:eq("+(sitePos)+")");
		// Nav Buttons
		var nav = new NavButtons();
		
		//Page Elements
		var logo = $(document.createElement("div")).attr("id", "logo");
		var header = $(document.createElement("div")).attr("id", "header").prepend(header).prepend(logo);
		var crumbs = $(document.createElement("div")).attr("id", "crumbs");
		
		var footer = $(document.createElement("div")).attr("id", "footer");
		var footer_span = $(document.createElement("div")).attr("id", "footer_span");
		
		
		//alert('Got the wile!!!');
		var content = $(document.createElement("div")).attr("id", "content");
				
		var wrapper = $(document.createElement("div")).attr("id", "page");
		
		$("body").empty();
		
		
		
		var docTitle = document.title;
		document.title = docTitle + " | FLVS";
		
		
		$('body').append($(wrapper).append($(content).append(copiedContents).append(footer).append(footer_span)));
		activateAssets();

	}
});


//Navigation Buttons
var NavButtons = function() {
	
	//attributes = Id, ImgSrc, Alt
	/**/var ul = $(document.createElement("ul")).attr("id", "nav");
	var li = $(document.createElement("li"));
	var a = $(document.createElement("a"));
	var img = $(document.createElement("img"));
	
	
	var navItemPrev = $(li).attr("id", "prevPage").append($(a)).attr("href", 'prev.htm').append($(img).attr('src',"global/imgs/left_arrow.png").attr('alt','image').attr('width', '25').attr('height','25'));
	var navItemNext = $(li).attr("id", "nextPage").append($(a)).attr("href", 'next.htm').append($(img).attr('src',"global/imgs/right_arrow.png").attr('alt','image').attr('width', '25').attr('height','25'));
	
	$(ul).append(navItemPrev).append(navItemNext);
}


//Create Crumbs
/*var Crumbs = function(page) {
	var courseCrumbs = $(document.createElement("div")).attr("id", "crumbs");
	var courseCrumbsSpan = $(document.createElement("span")).attr("id", "courseCrumbs");
	courseCrumbsSpan.append($(document.createElement("a")).attr("href", $(root).find("page:eq("+sitePos+")").find("page:eq(0)").attr("href")).text($(root).find("page:eq("+sitePos+")").attr("title")));
	courseCrumbsSpan.append($(document.createElement("a")).attr("href", $(root).find("page:eq("+sitePos+")").find("page:eq(0)").attr("href")).text($(root).find("page:eq("+sitePos+")").attr("title")));
	courseCrumbsSpan.append($(document.createElement("a")).attr("href", $(root).find("page:eq("+sitePos+")").find("page:eq(0)").attr("href")).text($(root).find("page:eq("+sitePos+")").attr("title")));
	//courseCrumbs.append(courseCrumbsSpan);
	var pageCrumbs = $(document.createElement("span")).attr("id", "pageCrumbs").text("Page " + ($(page).index()+1) + " of " + $(page).parent("unit").find("page").length);
	return $(courseCrumbs).append(pageCrumbs);

}*/

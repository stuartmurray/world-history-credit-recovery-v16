$(document).ready(function() {

var container = '.jFlip'
  , front = []
  , back = [];

$(container).each(function() {

//Use background image, if defined
var bg = $( container ).attr('data-bg');

if ( typeof( bg ) !== "undefined" ) {
	$( container ).css('background', 'transparent url('+ bg +') no-repeat center center');
}

  $.ajax({
    url: $(this).attr('data-ref'),
    dataType: 'xml',
    complete: function(data) {
    	$('.front').bind("click",function(){
    		var elem = $(this);

    		// data('flipped') is a flag we set when we flip the element:

    		if(elem.data('flipped'))
    		{
    			// If the element has already been flipped, use the revertFlip method
    			// defined by the plug-in to revert to the default state automatically:

    			elem.revertFlip();

    			// Unsetting the flag:
    			elem.data('flipped',false)
    		}
    		else
    		{
    			elem.flip({
    				direction:'lr',
    				speed: 350,
    				color: '#f9f9f9',
    				onBefore: function(){
    					// Insert the contents of the .sponsorData div (hidden
    					// from view with display:none) into the clicked
    					// .sponsorFlip div before the flipping animation starts:

    					elem.html(elem.siblings('.back').html());
    				}
    			});

    			// Setting the flag:
    			elem.data('flipped',true);
    		}
    	});
    },
    success: function(data) {
      //SETUP VARS FOR QUICKER INIT
			directions = $(data).find('directions').text();
			
			$(data).find('flipcard').each(function(){
			  front.push($(this).find('front').text());
			  back.push($(this).find('back').text());
			});
			
			//APPEND FLIP CARDS
			for (i=0; i<front.length; i++) {
			  $(container).append('<div class="flipcard" id="flipcard-'+i+'"><div class="front"><div class="content">'+front[i]+'</div></div><div class="back">'+back[i]+'</div></div>');
			}

			//APPEND Instructions Button
			$(container).append('<div class="btn-i"></div>');
			
			showOverlay("Instructions", directions, "Go!", container);
			
            var cards_per_row = Number($(data).find('cards_per_row').text());

            $(container + ' .go-btn').click(function() {
		    
    		    if (front.length > cards_per_row) {
    			    $(container).css({
                        'overflow-y': 'scroll',
                        'overflow-x': 'hidden'
      		        });
    		    }

			});

            var width = 77/cards_per_row + '%';
            var margin = 100/(cards_per_row*10) + '%';
            $(container + ' .flipcard').css({
                'width': width,
                'margin': '10% 0 10% ' + margin
            });

            //console.log(cards_per_row);
			
			$(container + ' .btn-i').click(function() {
			  showOverlay("Instructions", directions, "Go!", container);
			});
    },
    error: function(data) {
      console.log(data);
    }
  });
});	

});
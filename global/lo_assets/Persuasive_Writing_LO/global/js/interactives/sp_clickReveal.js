$(document).ready(function() {
		// everthing happens when you select an image.
	 $('.sp_img_container').children('.sp_clickReveal').children('img').bind('click', sp_showText);
		function sp_showText(e) {	
		//this removes any class of "sp_selected" from the sp_clickReveal. basically this performs a reset.
	  		$(this).parent('.sp_clickReveal').parent('.sp_img_container').children('.sp_clickReveal').children('.sp_selected').removeClass('sp_selected').addClass('sp_clickReveal_text');
			//we need all the font across the board to be back at 0... once we add the class of sp_selected to the item someone just selected, we will up the font-size again.
			$(this).parent('.sp_clickReveal').parent('.sp_img_container').children('.sp_clickReveal').children('.sp_clickReveal_text').css({'fontSize' : 0});	
			
			// uncomment lines below to remove green border around other images when user selects a new image. right now, the border stays so they know they selected them all.
			/*
			$(this).parent('.sp_clickReveal').parent('.sp_img_container').children('.sp_clickReveal').children('img').css({
				'zIndex' : 1,
				'border' : "" 
			});
			*/									
			
			//adding the sp_selected class to the image just selected so we can only mess with that one and its associated content
	  		$(this).parent('.sp_clickReveal').children('.sp_clickReveal_text').removeClass('sp_clickReveal_text').addClass('sp_selected');
			//we need to check the position and do some math here because we need the items associated text to move left pending the images position so that it centers under all images.
			var sp_img_Position = $(this).parent('.sp_clickReveal').position();
			var sp_check_img_position = sp_img_Position.left;
			//we need it to move left a negative number... so if an image is left: 200 right now, we will need our text to move -200 so we multiply by -1
			var sp_check_img_position = sp_check_img_position * (-1);			
			var sp_img_container_width = $(this).parent('.sp_clickReveal').parent('.sp_img_container').width();
			var sp_entire_container_width = $(this).parent('.sp_clickReveal').parent('.sp_img_container').parent('.sp_horz_clickReveal').width();
			var sp_clickReveal_font = $(this).parent('.sp_clickReveal').children('.sp_selected').css("font-size");
			
			//we only want certain code to run once... so when we select an image in this interactive, it will run the "if" statement below. our interactive (since the if code has not run yet) will not have the class "sp_interactive_used" yet so this if will run.
      		if ($(this).parent('.sp_clickReveal').parent('.sp_img_container').parent('.sp_horz_clickReveal').attr('class') !== 'sp_horz_clickReveal sp_interactive_used' ) {  
	  		//adding the class "sp_interactive_used" so when someone selects another image, this if statement will not longer run.
			$(this).parent('.sp_clickReveal').parent('.sp_img_container').parent('.sp_horz_clickReveal').removeClass('.sp_horz_clickReveal').addClass('sp_horz_clickReveal sp_interactive_used');
			$(this).parent('.sp_clickReveal').children('.sp_selected').animate({'top' : (200)}, 500, function () {});	
				
      		} else {
      		}		
	  		
			$(this).parent('.sp_clickReveal').children('.sp_selected').css({
				'width' : sp_img_container_width,
				'fontSize' : 15,			
				'left' : sp_check_img_position - 20,
				'top' : 200
				});
			// just adding some green when someone selects the image so they can track they selected them all. code commented out at the top of this js file will remove the green when they select another image. just comment that back out if you need that functionality.
			$(this).parent('.sp_clickReveal').children('img').css({
				'zIndex' : 2,
				'borderBottom' : "solid 2px green",
				'borderTop' : "solid 2px green"
				});				
		};

});
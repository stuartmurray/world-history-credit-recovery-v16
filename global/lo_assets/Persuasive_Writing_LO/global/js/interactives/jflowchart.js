$(document).ready(function () {

	var container = '.jFlowchart'; //container class of the flowchart

	//Use background image, if defined
	var bg = $( container ).attr('data-bg');

	if ( typeof( bg ) !== "undefined" ) {
		$( container ).css('background', 'transparent url('+ bg +') no-repeat center center');
	}

	$(container).each(function() {
		$('.node').click(function() {

			//Hide any showing nodes
			$(container).each(function() {
				$(this).find('.active').switchClass('active', 'inactive');
			});
			
			$(this).find('.inactive').switchClass('inactive', 'active'); //Show inner content
			
			//Show bottom branches
			if ($(this).attr('data-rel') === 'hideR' & $('.' + $(this).attr('data-rel')).hasClass('inactive')) {
				$('.' + $(this).attr('data-rel')).switchClass('inactive', 'active');
			} else if ($(this).attr('data-rel') === 'hideL' & $('.' + $(this).attr('data-rel')).hasClass('inactive')) {
				$('.' + $(this).attr('data-rel')).switchClass('inactive', 'active');
			}
		});

		//Click event for bottom branches
		$('.inner-node').click(function() {
			//Hide any showing inner nodes
			$(container).each(function() {
				$(this).find('.inner-node').find('.active').switchClass('active', 'inactive');
			});

			if ($(this).find('.inactive').length) {
				$(this).find('.inactive').switchClass('inactive', 'active');
			} else {
				$(this).find('.active').switchClass('active', 'inactive');
			}
		});

		showOverlay('Instructions', 'Select each area of this map to view an example of the section.', 'Go!', container); //Show instructions
	});

});

//Instructions Overlay
function showOverlay(title, directions, btnname, container) {
	var overlay = "overlay",
		$overlay = "." + overlay;
	
	$(container).append('<div class="'+overlay+'"><div class="instructions"><div id="title">'+title+'</div><div id="text">'+directions+'</div><div class="go-btn">'+btnname+'</div></div></div>');
	$(container + $overlay).hide().fadeIn(750);
	
	$(container + ' .go-btn').click(function() {
		$(this).parent().parent().fadeOut('slow', function() {
			$(this).remove();
		});
	});
};
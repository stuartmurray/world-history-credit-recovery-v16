var container = ".jQuiz3"
  , main_directions
  , popup_directions
  , correct
  , incorrect
  , tries
  , tries_num = 0
  , correct_num = 0
  , complete
  , questions = []
  , images = []
  , citations = []
  , captions = []
  , selections = {
      a : []
    , b : []
    , c : []
    , d : []
  }
  , answers = {
      a : []
    , b : []
    , c : []
    , d : []
  }
  , currQuestion = 0;


//Use background image, if defined
var bg = $( container ).attr('data-bg');

if ( typeof( bg ) !== "undefined" ) {
	$( container ).css('background', 'transparent url('+ bg +') no-repeat center center');
}

$(document).ready(function() {
  $.ajax({
    url: $(container).attr('data-ref'),
    dataType: 'xml',

    success: function(data) {
      main_directions = $(data).find('directions').text();
      popup_directions = $(data).find('popup_directions').text();
      correct = $(data).find('correct').attr('text');
      incorrect = $(data).find('incorrect').attr('text');
      tries = $(data).find('tries').attr('text');
      complete = $(data).find('complete').attr('text');
      //intro_image = $(data).find('intro_image').attr('url');
      
      for(i = 0; i < $(data).find('question').length; i++) {
        questions.push($(data).find('question').eq(i).attr('text'));
        images.push($(data).find('question').eq(i).find('image').attr('url'));
        citations.push($(data).find('question').eq(i).find('citation').text());
        captions.push($(data).find('question').eq(i).find('caption').text());
        selections.a.push($(data).find('question').eq(i).find('selections').find('selection').eq(0).attr('text'));
        selections.b.push($(data).find('question').eq(i).find('selections').find('selection').eq(1).attr('text'));
        selections.c.push($(data).find('question').eq(i).find('selections').find('selection').eq(2).attr('text'));
        selections.d.push($(data).find('question').eq(i).find('selections').find('selection').eq(3).attr('text'));
        answers.a.push($(data).find('question').eq(i).find('selections').find('selection').eq(0).attr('correct'));
        answers.b.push($(data).find('question').eq(i).find('selections').find('selection').eq(1).attr('correct'));
        answers.c.push($(data).find('question').eq(i).find('selections').find('selection').eq(2).attr('correct'));
        answers.d.push($(data).find('question').eq(i).find('selections').find('selection').eq(3).attr('correct'));
      }
      
      init(main_directions, popup_directions, correct, incorrect, tries, complete, questions, images, citations, captions, selections, answers);
    },

    error: function(data) {
      console.log("ERROR!");
      console.log(data);
    }
  });
});


function init(main_directions, popup_directions, correct, incorrect, tries, complete, questions, images, citations, captions, selections, answers) {
  showOverlay('Instructions', main_directions, 'Go!', 'instructions');
  
  //console.log();
  
  $(container).append('<div class="btn-i"><div class="popup-directions">'+popup_directions+'</div></div>');
  
  for (i = 0; i < questions.length; i++) {
    if ( !images.length || typeof images[i] === "undefined" ) {
    	$(container).append('<div id="question-'+i+'" class="question"><div class="question-text">'+questions[i]+'</div><ul class="answers"><li id="a" class="'+answers.a[i]+'">'+selections.a[i]+'</li><li id="b" class="'+answers.b[i]+'">'+selections.b[i]+'</li><li id="c" class="'+answers.c[i]+'">'+selections.c[i]+'</li><li id="d" class="'+answers.d[i]+'">'+selections.d[i]+'</li></ul><div class="caption" style="font-size: 20px; line-height: 24px; top: 25px;">'+captions[i]+'</div></div>');
    } else {
    	$(container).append('<div id="question-'+i+'" class="question"><div class="question-image"><img src="'+images[i]+'" /></div><div class="question-text">'+questions[i]+'</div><ul class="answers"><li id="a" class="'+answers.a[i]+'">'+selections.a[i]+'</li><li id="b" class="'+answers.b[i]+'">'+selections.b[i]+'</li><li id="c" class="'+answers.c[i]+'">'+selections.c[i]+'</li><li id="d" class="'+answers.d[i]+'">'+selections.d[i]+'</li></ul><div class="citation">'+citations[i]+'</div><div class="caption">'+captions[i]+'</div></div>');
    }
    
    
    $('#question-'+i).hide();
  }
  
  $('ul.answers li').click(function(e) {
    if ($(this).hasClass('true')) {
      showFeedback('true');
    }
    else if ($(this).hasClass('false')) {
      showFeedback('false');
    }
  });
  
  $('.btn-i').click(function() {
    $('.popup-directions').toggle('drop', 500);
  });
}

function nextQuestion(id) {
  var prevQuestion = id - 1;
  tries_num = 0;

  if (id > 0) {
    if ($.browser.msie && $.browser.version < 8) {
      $('#question-'+prevQuestion).fadeOut(500);
    }
    else {
      $('#question-'+prevQuestion).toggle('drop', 500);
    }
  }

  if (id >= questions.length) {
    var complete_txt = complete + correct_num + ' of ' + $('.question').length + ' correct.<br /><br />Select &quot;Restart&quot; to try again.';
    showOverlay('Congratulations!', complete_txt, 'Restart', 'restart')
  }
  else {
    if ($.browser.msie && $.browser.version < 8) {
      $('#question-'+id).fadeIn(1000);
    }
    else {
      $('#question-'+id).toggle('drop', 1000);
    }
    
    $('.nav').html((id+1) + ' of ' + $('.question').length);    
  }
}

function showFeedback(answer) {
  if (answer === 'true') {
    correct_num++;
    tries_num = 0;
    $(container).append('<div class="feedback">'+correct+'</div>');
    $('.feedback').hide();
    $('.feedback').fadeIn(250, function() {
      $(this).delay(3000).toggle('drop', 250, function() {
        $(this).remove();
        
        currQuestion++;
        nextQuestion(currQuestion);
      });
    });
  }
  else if (answer === 'false') {
    if (tries_num > 0) {
      var tries_txt = tries + '<br /><br /><strong>' + $('.answers li.true').eq(currQuestion).text() + '</strong>';

      showOverlay('Note', tries_txt, 'Next', 'tries');
    }
    else {
      tries_num++;
      $(container).append('<div class="feedback">'+incorrect+'</div>');
      $('.feedback').hide();
      $('.feedback').fadeIn(500, function() {
        $(this).delay(3000).toggle('explode', 500, function() {
          $(this).remove();
        });
      });      
    }
  }
}

function showNav() {
  $(container).append('<div class="nav">'+(currQuestion+1)+' of '+$('.question').length+'</div>');
  $('.nav').hide();
  $('.nav').fadeIn('slow');
}

function resetVars() {
  tries_num = 0
  , correct_num = 0
  , currQuestion = 0;
}

function showOverlay(title, text, btnname, type) {
	var overlay = "overlay",
		$overlay = "." + overlay;

	$(container).append('<div class="'+overlay+'"><div class="instructions"><div id="title">'+title+'</div><div id="text">'+text+'</div><div class="go-btn">'+btnname+'</div></div>');
	$($overlay).hide();
	$($overlay).fadeIn('slow');
	
	if (type === 'instructions') {
  	$('.go-btn').click(function() {
  		$(this).parent().parent().fadeOut('slow', function() {
  			$(this).remove();
    		  nextQuestion(currQuestion);
          showNav();
  		});
  	});
	}
	else if (type === 'tries') {
  	$('.go-btn').click(function() {
  		$(this).parent().parent().fadeOut('slow', function() {
  			$(this).remove();

        currQuestion++;
        nextQuestion(currQuestion);
  		});
  	});
	}
	else if (type === 'restart') {
	  $('.go-btn').click(function() {
      for (i = 0; i < questions.length; i++) {
        $('#question-'+i).remove();
      }

      $('.btn-i').remove();
      $('.nav').remove();

  		$(this).parent().parent().fadeOut('slow', function() {
  			$(this).remove();
  		});
  		
  		resetVars();
  		
      init(main_directions, popup_directions, correct, incorrect, tries, complete, questions, images, citations, captions, selections, answers);
  	});
	}
};
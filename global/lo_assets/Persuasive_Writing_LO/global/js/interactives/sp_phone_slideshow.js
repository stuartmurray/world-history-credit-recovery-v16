$(document).ready(function() {
	
	
	$('.sp_horz_phone').each(function(index) {
		
	//Accordian
	$('dl.accordian > dt').click(function() {
		$(this).removeClass('on');
		$(this).parent().find('dd').slideUp('normal');
		if($(this).next().is(':hidden') == true) {
		$(this).addClass('on');
		$(this).next().slideDown('normal');
		} 
	});
	$('dl.accordian > dd').hide();
	// end accordian
		
		$(this).children('.sp_slide').each(function(index) {
			var sp_slideLength = $(this).parents('.sp_horz_phone').children('.sp_slide').length;	
			$(this).children('.sp_padding').children('h4').children('.sp_slide_counter').replaceWith("<span class='sp_slide_counter'>" + (index + 1) + " of " + sp_slideLength + "</span>");
		});
				
		$(this).children('.sp_slide').wrapAll('<div class="sp_content"></div>');		
		
		$(this).children('.sp_content').after("<div class='sp_mobile_next'></div><div class='sp_mobile_back'></div>");
		$(this).find('.noscript').remove();
		$(this).children('.sp_mobile_back').hide();
		var sp_css_container_width = $(this).width();
		
		$(this).children('.sp_content').children('.sp_slide').css({ 
			'width' : sp_css_container_width});
		
		$(this).children('.sp_content').css({
			'width' : sp_css_container_width * $(this).find('.sp_slide').length});
	});
									 
	 $(this).find('.sp_mobile_next').bind('click', moveNext);
		function moveNext(e) {					
			var sp_slideWidth = $(this).parent('.sp_horz_phone').children('.sp_content').children('.sp_slide').width();			
			var sp_contentWidth = $(this).parent('.sp_horz_phone').children('.sp_content').width();
			var sp_contentPosition = $(this).parent('.sp_horz_phone').children('.sp_content').position();
			var sp_checkPosition = sp_contentPosition.left;			
			var sp_left_indent = sp_checkPosition - sp_slideWidth;
					
			$(this).unbind();
			$(this).parents('.sp_horz_phone').children('.sp_mobile_back').fadeIn();
			$(this).parents('.sp_horz_phone').children('.sp_content').animate({'left' : (sp_left_indent)}, 500, function () { 
				$(this).parent('.sp_horz_phone').children('.sp_mobile_next').bind('click', moveNext);
				sp_forms(); 						
			});
			
			var sp_slideNum = sp_left_indent/sp_slideWidth;
			var sp_slideNum = Math.abs(sp_slideNum);
			var sp_slideNum = Math.round(sp_slideNum);
			
			if ((sp_slideNum + 1) == ($(this).parents('.sp_horz_phone').children('.sp_content').children('.sp_slide').length)) {
				$(this).fadeOut();				
			} else if ((sp_slideNum + 1) != ($(this).parents('.sp_horz_phone').children('.sp_content').children('.sp_slide').length)) {
				$(this).fadeIn();
			}	
		};
		// end next
		
	$(this).find('.sp_mobile_back').bind('click', moveBack); 
		function moveBack(e) {
			
			var sp_slideWidth = $(this).parent('.sp_horz_phone').children('.sp_content').children('.sp_slide').width();
			var sp_contentWidth = $(this).parent('.sp_horz_phone').children('.sp_content').width();
			var sp_contentPosition = $(this).parent('.sp_horz_phone').children('.sp_content').position();
			var sp_checkPosition = sp_contentPosition.left;			
			var sp_right_indent = sp_checkPosition + sp_slideWidth;
			
			$(this).unbind();
			$(this).parents('.sp_horz_phone').children('.sp_mobile_next').fadeIn();
			
			$(this).parents('.sp_horz_phone').children('.sp_content').animate({'left' : (sp_right_indent)}, 500, function () {				
				$(this).parent('.sp_horz_phone').children('.sp_mobile_back').bind('click', moveBack);
				sp_forms();  
			});
				
			var sp_slideNum = sp_right_indent/sp_slideWidth;
			var sp_slideNum = Math.abs(sp_slideNum);
			var sp_slideNum = Math.round(sp_slideNum);						

			if ((sp_slideNum+1) == 1) {
				$(this).parent('.sp_horz_phone').children('.sp_content').animate({"left":"0px"});
				$(this).fadeOut();
			} else if ((sp_slideNum+1) != 1) {
				$(this).fadeIn();	
			}
		};
		// end back		

});
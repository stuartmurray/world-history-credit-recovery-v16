//Tooltips
//=========================
function activateTooltips( item ) {

    $(item).each(function (i) {

        if ( $(this).hasClass('question') ) { $(this).prepend('<div class="icon"></div>'); }

        $(item + ': span').each(function (l) {
            $(this).addClass(($(this).parent().attr("title") === "View")? "hidden" : "notVisible").css("z-index", "2000");
        });

        $(this).bind("click", function() {
            var index = $(item).index($(this));
            
            $(item + ':lt('+index+')').css("z-index", "2000");
            $(item + ':gt('+index+')').css("z-index", "2000");
            $(this).css("z-index", "3000");

            $(item + ':lt('+index+') span').each(function (l) {
                $(this).addClass(($(this).parent().attr("title") === "View")? "hidden" : "notVisible").css("z-index", "2000");
            });
            $(item + ':gt('+index+') span').each(function (l) {
                $(this).addClass(($(this).parent().attr("title") === "View")? "hidden" : "notVisible").css("z-index", "2000");
            });

            $(this).find("span").toggleClass(($(this).attr("title") === "View")? "hidden" : "notVisible").css("z-index", "8000");

            if ( $(this).parent().parent().hasClass('b-wrap-left') || $(this).parent().parent().parent().hasClass('b-wrap-left') ) {
                if ( ( $(this).find("span").offset().left ) > ( $("#jFlipbook").width() / 2 ) ) {
                    $(this).find("span").addClass("tipLeft");
                }
            } else {
                if ( ( $(this).find("span").offset().left - 16 ) > ( $("#jFlipbook").width() ) ) { $(this).find("span").addClass("tipRight"); }
            }
            
            if ( ( $(this).find("span").offset().top + 100 ) > ( $("#jFlipbook").height() ) ) { $(this).find("span").addClass("tipBottom"); }
            
            return false;
        });
    });
}
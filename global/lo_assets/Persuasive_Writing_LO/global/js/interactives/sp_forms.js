var sp_input_container = new Array();
var sp_answer_index;
var sp_input_container;

$(document).ready(function() {			
	
	function sp_setup_forms() {	
		//next function for screenreader users
		//for each question on the page, we will perform the code in this function
		$('.sp_slide').children('.sp_padding').children('.sp_question_text').each(function(index) {
			//snagging the question text and containing it in the variable sp_questionText
			var sp_questionText = $(this).parent('.sp_padding').children('.sp_question_text').html();
			//clearning the html text completely
			$(this).parent('.sp_padding').children('.sp_question_text').html("");
			//adding a named anchor to inside the question text before the question then readding the question.
			//this anchor is going to be a link back for students on screenreaders who want to have a question repeated. not for visual students.
			$(this).parent('.sp_padding').children('.sp_question_text').append("<a name='sp_q_id" + index + "'></a>" + sp_questionText);
		});	
		
		//next function for accessibility as well
		//each question has a print answer (one that will show if the html page were printed on paper)
		$('.sp_slide').children('.sp_padding').children('.sp_print_answer').each(function(index) {
			//we are going to get the name attribute of the anchor we just assigned at the top, we are going to reference that to link back to it
			var sp_a_id = $(this).parent('.sp_padding').children('.sp_question_text').children('a').attr('name');
			//we want to have a link after the print answer code allowing students with a screenreader to go to the named anchor associated with the question.
			$(this).after("<a href='#" + sp_a_id + "' title='Select this link to go back to the question' class='sp_accessible'>Go back to question</a>");
		});
		
		//we want to add some feedback for the students.
		$('.sp_question').children('form').after("<div class='sp_q_feedback'></div>");
		//in each form...
		$('.sp_question').children('form').each(function(index) {
		//we want to wrap all spans with labels (this allows us to select the radio button OR text next to it)
		$(this).children('span').wrap('<label />');
		//grab the information in the input's value attribute and push it to the array. this is how someone adds answer feedback in the html
		var sp_input_answers = new Array();
		for (i=0; i< $(this).children('label').children('span').children('input').length; i++) {
			//we need each inputs value attribute... 
			sp_input_answers.push($(this).children('label').children('span').children('input')[i].value);
		}
		sp_input_container.push(sp_input_answers);
		//we want to remove the value attribute now that we stored its contents for each question answer option so a screenreader does not read the answers
		$(this).children('label').children('span').children('input').removeAttr('value');		
		//we need the input boxes to have an id that matches the label for attribute so you can select the radio and text
		$(this).children('label').children('span').children('input').each(function() {
			i = i+1;
			$(this).attr('id', 'r' + index + i);
		});
		//adding the for attribute that matches each input id. this just makes a more intuitive and easier to select quiz.
		$(this).children('label').each(function (i) {
			var sp_label_id = $(this).children('span').children('input').attr('id');
			$(this).attr('for', sp_label_id);
		});	
				
		});
	};
	sp_setup_forms();
	sp_forms();
});

function sp_forms() {
	//mostly we are resetting questions here
	$('.sp_question').children('form').each(function(index) {
		//we need to clear the question feedback each time
		$(this).parent('.sp_question').children('.sp_q_feedback').html('');
		//unselects any radio buttons selected
		$(this).children('label').children('span').children('input').attr('checked', false);
		//clear the input values
		$(this).children('label').children('span').children('input').val('');
	});
	
	$('.sp_question').children('form').children('label').children('span').children('input').click(function() {	
		//when an input answer option is selected, we want to uncheck all radio buttons
		$(this).parents('form').children('label').children('span').children('input').attr('checked', false);
		//then check the one selected
		$(this).attr('checked', true);
		//setting a variable to contain the index number of the question in relation to the others
		var sp_question_index = $(this).parents('form').parent('.sp_question').index('.sp_question');
		//need to find the index of the input options in relation to the others
		var sp_answer_index = $(this).parents('form').children('label').children('span').children('input').index(this);	
		//to give feedback... we are going to display the information that was placed in that input value attribute that we pushed to an array			
		$(this).parents('form').parent('.sp_question').children('.sp_q_feedback').html(sp_input_container[sp_question_index][sp_answer_index]);		
	});
};




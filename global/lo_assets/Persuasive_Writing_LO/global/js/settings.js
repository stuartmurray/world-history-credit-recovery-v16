//=========================================================
// URLs
//=========================================================

//School Wide Path
var path = "http://c225587.r87.cf1.rackcdn.com/";

//Local Path
var locPath = "../global/";

//Sitemap
var sitemap = locPath + "xml/sitemap.xml";

//Copyright
//var copyrightLocation = locPath+"docs/copyright.htm";
var footerText = 'Unless otherwise noted, &copy; 2012';

//AJAX Spinner Image
var spinnerLocation = locPath+"imgs/icon-library/spinner.gif";

//===========================================================
// Default Visibility Settings
//===========================================================

var show = {
	lo_levels : true,
	breadcrumbs : true,
	dropDownMenu: true,
	headerNavigation : true,
	footer : true,
	footer_hr : true,
	mathJax: true
};

//===========================================================
// Attributes
//===========================================================

//Square for width and height
var navButtonSize = 35;
//Dropdown Menu - basic, slide, fade
//var menuIn = 'fade';

//===========================================================
// Content Loader - DO NOT TOUCH, UNLESS ABSOLUTELY NECESSARY
//===========================================================

//Hide Flickr of Static Content
var initCSS = document.createElement('style');
initCSS.type = 'text/css';
initCSS.media = 'screen';


//Load jQuery for faster access
document.write('<script type="text/javascript" src="'+locPath+'js/jquery-1.7.1.min.js"></script>');

if ( initCSS.styleSheet ) {
	initCSS.styleSheet.cssText = "#appContent{display:none;}";
} else {
	initCSS.appendChild(document.createTextNode("html,body{height:100%;}body{background-repeat:no-repeat;background-position:center center;background-image:url('"+spinnerLocation+"');}#appContent{display:none;}"));
}

//
document.getElementsByTagName("head")[0].appendChild(initCSS);

var file = new Array();

file.push(path + "print.css");
file.push(locPath+"css/theme.css");
file.push(locPath+"css/lib.css");/**/
file.push(locPath + "js/jquery-1.7.1.min.js");
//file.push(locPath+"js/jquery-ui-1.8.20.min.js");
//file.push(locPath+"js/interactives/jquery_ui/custom_plugins/jquery.ui.touch-punch.js");
//file.push(path+"jquery-ui-1.8.20.min.js");
file.push(locPath + "js/lang.js");

if ( show.mathJax === true ) {
	file.push("http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML");
}

/**/file.push(path+"jquery.poshytip.min.js");
file.push(path+"jquery.lightbox-0.5.js");
file.push(path+"jquery.swfobject.1-1-1.min.js");
file.push(locPath + "js/flvsLib-1.4.3.js");
file.push(locPath+"js/core.js");
//jQuery UI
file.push(locPath+"js/interactives/jquery_ui/custom_plugins/ui_importer.js");



var loadFiles = function ( fileNo ) {

	var linkedFile = ( file[fileNo].search(".js") != -1 ) ? document.createElement("script") : document.createElement( "link" );
	linkedFile.setAttribute( "type", ( file[fileNo].search(".js") != -1 ) ? "text/javascript" : "text/css" );

	if ( file[fileNo].search(".js") != -1 ) {
		linkedFile.setAttribute( "src", file[fileNo] );
	} else {
		linkedFile.setAttribute( "media", "all" );
		linkedFile.setAttribute( "href", file[fileNo] );
		linkedFile.setAttribute( "rel", "stylesheet" );
	}

	if ( file[fileNo].search(".js") != -1 ) {

		if ( linkedFile.readyState ) {
			linkedFile.onreadystatechange = function() {
				if ( linkedFile.readyState == "loaded" || linkedFile.readyState == "complete" ) {
				
					linkedFile.onreadystatechange = null;
					
					if ( fileNo != file.length-1 ) { loadFiles(fileNo+1);}
					if ( fileNo == ( file.length-1 ) ) { document.getElementsByTagName("style")[0].removeNode(true); }
				}
			};
		} else {
			linkedFile.onload = function() {
				if ( fileNo != file.length-1 ) { loadFiles(fileNo+1); }
				if ( fileNo == ( file.length-1 ) ) { document.getElementsByTagName("head")[0].removeChild( document.getElementsByTagName("style")[0] ); }
			};
		}
	} else {
		if( fileNo != file.length-1 ) { loadFiles(fileNo+1); }
	}

	document.getElementsByTagName("head")[0].appendChild( linkedFile );

}

loadFiles(0);

//
//
//Will Jones | wjones@flvs.net
//
//
var root; // This is for the XML to store data

$(document).ready(function(){

	// Setup Defaults
	//$('body').addClass('bg1');
	
	$.ajax({
		url: "global/xml/sitemap.xml",
		processData: false,
		dataType: "xml",
		beforeSend: function(){},
		success: function(xml){
			
			root = $(xml).find("sitemap");
		
			//var lessonTitle = $(document.createElement("h1")).append($(this).attr("title").text());
			// Determine at which lesson number to start the lo
			//Lesson Title
			var lessonTitle = $(document.createElement("h1")).append($(root).find('lesson').attr("title"));
			var ul = $(document.createElement("ul")).addClass('levels');
			
			//get each level
			$(root).find("level").each(function(index){
				
				var levelTitle = $(this).attr('title');
				
						//get page link value (Will Jones Dec 2011)
						var pageLink = $(this).find('page:eq(0)').attr("href"); //wj
						var li = $(document.createElement("li")).append($(document.createElement("a")).attr("href", 'lo_template/'+ pageLink).text(levelTitle))
						
						$(ul).append(li);
			});
					
			
			$('#wrapper').prepend($(document.createElement('div')).attr('id','container').append(lessonTitle).append(ul));
			//console.log();
			
		},
		error: function(request,error) {
				alert(error);
		}
	});



});




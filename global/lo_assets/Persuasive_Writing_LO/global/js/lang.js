var lang = {

Prev:"Previous Page",
PrevTitle:"Go to the Previous Page",
Next:"Next Page",
NextTitle:"Go to the Next Page",

AccessibleToggle:"Accede la versión de texto",
AccessibleToggleTitle:"Accede la versión de texto",
IncreaseFont:"Increase Font Size",
IncreaseFontTitle:"Increase Font Size",
DecreaseFont:"Decrease Font Size",
DecreaseFontTitle:"Decrease Font Size",
Access:"Toggle Accessible Versions",

Search:"Search",

Close:"Close",
CloseWindow:"Close Window",
TextVersion:"Text Version",
Interactive:"Interactive",

Print:"Print",
PrintPage:"Print Page",

Copy:"Unless otherwise noted"


};

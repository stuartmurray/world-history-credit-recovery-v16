﻿package 

{

import flash.external.ExternalInterface;

import flash.display.Sprite;

import flash.display.MovieClip;

import flash.events.NetStatusEvent;

import flash.events.AsyncErrorEvent;

import flash.events.Event;

import flash.events.MouseEvent;

import flash.net.URLLoader;

import flash.net.URLRequest;

import flash.display.Loader;

import flash.utils.*;

import fl.transitions.Tween;

import fl.transitions.TweenEvent;

import fl.transitions.easing.*;

import flash.text.TextField;

import flash.text.TextFieldAutoSize;

import flash.text.TextFormat;

import flash.events.IOErrorEvent;

import flash.text.TextFieldAutoSize;

import flash.media.Sound;

import flash.media.SoundChannel;

import flash.media.SoundMixer;

import flash.utils.Dictionary;

import flash.media.Video;



import flash.utils.getQualifiedClassName;

import flash.utils.getDefinitionByName;

import flash.net.NetConnection;

import flash.net.NetStream;

import flash.display.StageAlign;

import flash.display.StageScaleMode;



public class video extends Sprite

{

private var mediaConnection:NetConnection = new NetConnection();

private var mediaStream:NetStream;

private var mediaSrc:String;

private var thumbSrc:String;

private var vidPlaying:Boolean;

private var splash:Sprite = new Sprite();

private var externalMedia:Video = new Video();

private var swfId;

private var pBtn:pauseBtn = new pauseBtn();

private var sBtn:stopBtn = new stopBtn();

private var progressBar:Sprite = new Sprite();

private var loadBar:Sprite = new Sprite();

private var videoLength;

private var timeStampSp:Sprite = new Sprite();

private var timeStamp:TextField = new TextField();

private var vControls:volumeControls = new volumeControls();

private var img:Sprite = new Sprite();

private var scrubberGutter:Sprite = new Sprite();

var videoWindow:MovieClip = new MovieClip();



public function video():void

{

this.addEventListener(Event.ADDED_TO_STAGE, init);



}

public function init(evt:Event):void

{

pBtn.x = pBtn.width;

stage.scaleMode = StageScaleMode.NO_SCALE;

stage.align = StageAlign.TOP_LEFT;



var client:Object = new Object();

client.onMetaData = mData;

client.onCuePoint = cPoint;

mediaConnection.connect(null);

mediaStream = new NetStream(mediaConnection);

mediaStream.client = client;



mediaConnection.addEventListener(NetStatusEvent.NET_STATUS, mediaStatus, false, 0, true);

mediaStream.addEventListener(NetStatusEvent.NET_STATUS, mediaStatus, false, 0, true);

mediaConnection.addEventListener(AsyncErrorEvent.ASYNC_ERROR, mediaError, false, 0, true);

mediaStream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, mediaError, false, 0, true);

var checkInterval = setInterval(checkStatus, 100);



//mediaStream.playheadPercentage;

var folder = (this.root.loaderInfo.parameters.vid != null) ? this.root.loaderInfo.parameters.vid : "../../module01/01_01_01/";

mediaSrc = folder+"video.flv";

swfId = (this.root.loaderInfo.parameters.swfId != null) ? this.root.loaderInfo.parameters.swfId : 0;

thumbSrc = (this.root.loaderInfo.parameters.thumb != null) ? this.root.loaderInfo.parameters.thumb+"thumb.jpg" : "../../module01/01_01_01/thumb.jpg";



var loader:Loader = new Loader();

//myLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressStatus);

loader.contentLoaderInfo.addEventListener(Event.COMPLETE, displayThumb);

loader.load(new URLRequest(thumbSrc));



externalMedia.attachNetStream(mediaStream);

//mediaStream.play(mediaSrc);

}

public function mediaError(evt:AsyncErrorEvent):void

{



}



public function checkStatus():void

{

ExternalInterface.call("updateCaption", swfId, Math.floor(mediaStream.time));

loadBar.width = Math.floor((mediaStream.bytesLoaded / mediaStream.bytesTotal) * (Math.floor(controlBar.width - pBtn.width - sBtn.width - 40 - timeStamp.width)));

progressBar.width = Math.floor((mediaStream.time / videoLength) * (Math.floor(controlBar.width - pBtn.width - sBtn.width - 40 - timeStamp.width)));



timeStamp.text = getTimeStamp(mediaStream.time) + " / " + getTimeStamp(videoLength);

}

public function getTimeStamp(seconds){

var seconds = seconds;

var hours = Math.floor(seconds/3600);

seconds = seconds%3600;

var minutes = Math.floor(seconds/60);

seconds = Math.floor(seconds%60);

if (seconds.toString().length < 2) seconds = "0"+seconds;

if (minutes.toString().length < 2) minutes ="0"+minutes;

return (hours+":"+minutes+":"+seconds);

}

public function mediaStatus(evt:NetStatusEvent):void

{

if (evt.info.code == "NetStream.Play.Start")

{

vidPlaying = true;

}



}

public function mData(info:Object):void

{

videoLength = info.duration;

videoWindow.addChild(externalMedia);

this.addChild(videoWindow);

ExternalInterface.call("adjustSwfSize", swfId, info.width,(info.height+controlBar.height));

//ExternalInterface.addCallback("appendControls", addAssets);

controlBar.width = externalMedia.width = info.width;

controlBar.y = externalMedia.height = info.height;

sBtn.y = info.height;

pBtn.y = info.height;

sBtn.buttonMode = pBtn.buttonMode = videoWindow.buttonMode = true;

videoWindow.addEventListener(MouseEvent.MOUSE_DOWN, pauseVideo);

pBtn.addEventListener(MouseEvent.MOUSE_DOWN, pauseVideo);

sBtn.addEventListener(MouseEvent.MOUSE_DOWN, stopVideo);

this.addChild(pBtn);

this.addChild(sBtn);





vControls.x = controlBar.width - vControls.width;

vControls.y = controlBar.y + (controlBar.height/2) - (vControls.height/2);

//this.addChild(vControls);



timeStamp.text = "0:00:00/0:00:00";

timeStamp.autoSize =  TextFieldAutoSize.LEFT ;

timeStamp.x = info.width - timeStamp.width - 20;

timeStamp.y = controlBar.y + (controlBar.height/2) - (timeStamp.height/2);

this.addChild(timeStamp);



scrubberGutter.graphics.beginFill(0xCCCCCC);

scrubberGutter.graphics.drawRect(0,0,100,5);

scrubberGutter.graphics.endFill();

scrubberGutter.x = pBtn.x + pBtn.width;

scrubberGutter.width = Math.floor(info.width - (pBtn.width + sBtn.width + 40) - (timeStamp.width));

scrubberGutter.y = controlBar.y + (controlBar.height/2) - (scrubberGutter.height/2);

this.addChild(scrubberGutter);



loadBar.graphics.beginFill(0x666666);

loadBar.graphics.drawRect(0,0,100,5);

loadBar.graphics.endFill();

loadBar.x = pBtn.x + pBtn.width;

loadBar.width = 300;

loadBar.y = controlBar.y + (controlBar.height/2) - (loadBar.height/2);

this.addChild(loadBar);



progressBar.graphics.beginFill(0x003366);

progressBar.graphics.drawRect(0,0,100,5);

progressBar.graphics.endFill();

progressBar.x = pBtn.x + pBtn.width;

progressBar.width = 100;

progressBar.y = controlBar.y + (controlBar.height/2) - (progressBar.height/2);

this.addChild(progressBar);









}

public function addAssets():void

{



}

public function cPoint(info:Object):void

{

trace(info.paramaters.text);

}

public function pauseVideo(info:Object):void

{

mediaStream.togglePause();

if (vidPlaying)

{

pBtn.gotoAndStop(2);

vidPlaying = false;

}

else

{

pBtn.gotoAndStop(1);

vidPlaying = true;

}

}

public function stopVideo(info:Object):void

{

mediaStream.close();

pBtn.gotoAndStop(2);

pBtn.visible = sBtn.visible = controlBar.visible = loadBar.visible = progressBar.visible = scrubberGutter.visible = videoWindow.visible = false;

vidPlaying = false;

splash.visible = true;

splash.x = stage.stageWidth/2 - splash.width/2;

splash.y = stage.stageHeight/2 - splash.height/2;

splash.parent.setChildIndex(splash, this.numChildren - 1);



}

public function playVideo(evt:MouseEvent):void

{

	pBtn.visible = sBtn.visible = controlBar.visible = loadBar.visible = progressBar.visible = scrubberGutter.visible = videoWindow.visible = true;



pBtn.gotoAndStop(1);

splash.visible = false;

if (vidPlaying)

{

mediaStream.resume();

}

else

{

mediaStream.play(mediaSrc);

}

vidPlaying = true;

}



public function displayThumb(evt:Event):void

{

img.addChild(evt.target.content);

img.alpha = .3;



splash.addChild(img);



var pBtn = new playBtn();

pBtn.x = img.width / 2;

pBtn.y = img.height / 2;

splash.addChild(pBtn);





this.addChild(splash);

splash.x = stage.stageWidth/2 - splash.width/2;

splash.y = stage.stageHeight/2 - splash.height/2;



splash.buttonMode = true;

splash.addEventListener(MouseEvent.MOUSE_DOWN, playVideo);





}

}

}
//===========================================================
// Learning Objects Settings/ Content Loader - DO NOT TOUCH, UNLESS ABSOLUTELY NECESSARY
//===========================================================

// ----------------------------------------------------------------------
// -- LEARNING OBJECT SETTINGS:
// ----------------------------------------------------------------------
// -- NOTE: Do not edit this file unless you are a WDS team member.
// -- AUTHOR: Will Jones - WDS
// ======================================================================
//Load external libs/files here
var skin = 'blitzer';
var lo_csslibs = ['../global/css/global.css','http://fonts.googleapis.com/css?family=Sonsie+One','../global/css/template.css','../global/css/'+skin+'/ui_interactive_skin_'+skin+'.css'];//-- example: '../global/js/jslib.js','../global/js/jslib2.js'
var lo_jslibs = ['../global/js/global.js']; //-- example: '../global/js/jslib.js','../global/js/jslib2.js'

// IMPORTANT MUST HAVES
//===========================================================

//Size of width and height (left and right nav buttons)
var navButtonSize = 35;
var islearningobject = true; // IMPORTANT Loads LO
var lo_show = {};
// location to course global root from LO folder
var loroot = '../../../';

//theme
lo_show['use_templates_for_design'] = true; //use template (add css template in lo_csslibs)
lo_show['uiSkin'] = 'local'; //local ui skin, add css folder link to lo_csslibs array above
lo_show['lo_levels'] = false;
lo_show['lo_breadcrumbs'] = true;
lo_show['lo_dropDownMenu'] = true;
lo_show['lo_headerNavigation'] = true;
lo_show['lo_footer'] = true;
lo_show['flvs_logo_type'] = ''; // color, white, black
				
//local images
var flvs_link = '../global/imgs/logo_flvs_blue.png';
var arrowLeft = '../global/imgs/left_arrow.png';
var arrowRight = '../global/imgs/right_arrow.png';
var loMenuHome = '../global/imgs/home.png';

//==============================================================
// JavaScript Document
$(document).ready(function(){
	
	// Load Sitemap and Run createIndex() once complete.
	loadSitemap(createIndex);

});

/*

	XML Sitemap has been converted into an JSON object (dot notation)
	FLVS / Sitemap / Lesson / Level / Page

*/

//Run This function immediately after loading XML sitemap
function createIndex(){
		
	//FLVS Sitemap / Lesson / Level / Page
	//console.log(FLVS.Sitemap); //View console of Sitemap
	var LO_title = FLVS.Sitemap.title;
	//count levels
	var levelCount = FLVS.Sitemap.lesson[0].level.length;
	var hmlink = FLVS.Sitemap.lesson[0].level[0].page[0].href;
	//hmlink = hmlink.replace("../",""); //replace ../ in href links...
	
	//put title
	$('.lo_title').append(LO_title);
	
	//Menu of Levels
	//List the Menu links as you see fit (ul or divs)
	for(var i=0; i<levelCount; i++){
		
		// Find the first link in the module
		var levelLink = FLVS.Sitemap.lesson[0].level[i].page[0].href;
		levelLink = levelLink.replace("../","");
		if (FLVS.Sitemap.lesson[0].level[i] != 'undefined') {
			//title
			var levelTitle = FLVS.Sitemap.lesson[0].level[i].title;
			// Create Link and add it to the page
			var lv = '<li><a class="green" href="'+levelLink+'">'+levelTitle+'</a></li>';
			$('._levels').append(lv);	
		}
	} //end for loop
	
}

//Additional Function
function createMenu(){
}
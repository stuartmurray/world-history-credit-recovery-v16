document.addEventListener('DOMContentLoaded', function(){ new iScroll('musical-inst', {desktopCompatibility:true}); }, false);
var finished = false;
$(document).ready(function(){
	if(navigator.platform == "iPad" || navigator.platform == "iPhone") {
		dragiPad("div.drag-item");
		dropiPad("div.light-blue-box");
	}
	else {
		dragWeb("div.drag-item");
		dropWeb("div.light-blue-box");
	}
	
	
	$("div.int-play").bind("click", function(){
		$(this).hide();
		PlayAudio("read");	
	});
	
	$("div.popup").find("img.close").click(function(){
		$(this).parents("div.popup").hide();	
		document.getElementById(0).pause();
		$("div#overlay").hide();
		
		if(finished) {
			$("input#submit").attr("disabled", "true");	
		}
	});
	
	var s = document.getElementById(0);
	$(s).bind("ended", function(){
		if(play) {
			$(this).attr("src","media/drag.mp3");
			document.getElementById(0).play();
			play = false;
		}	
		else 
		$("#overlay").hide();
	});
	
	$("div#playpause").bind("click", function(){
		if($(this).hasClass("play")) {
			document.getElementById(0).pause();
			$(this).removeClass("play").addClass("stop");	
		}
		else {
			$(document.getElementById(0)).attr("src", "media/musical.mp3");
			document.getElementById(0).play();
			$(this).removeClass("stop").addClass("play");
		}
	});
});

var play = true;
function PlayAudio(st) {
	var s = document.getElementById(0);
	$(s).attr("src","media/"+st+".mp3");
	document.getElementById(0).play();
}

function dragWeb(sFilter){
	$(sFilter).draggable({revert:true});
    $(sFilter).bind("dragstart", function(event, ui) {
        //$(this).css("z-index","98");
    });
}

function dropiPad(sFilter){
	$(sFilter).each(function(){
		webkit_drop.add(this, {
			onDrop : function(dragged,dropped){
        		DoDrop(dragged,$(webkit_drop.dropped));
			}
	    });
	});
}

function dragWebDisable(sFilter){
	$(sFilter).draggable("disable");
}

function dropWeb(sFilter){
	$(sFilter).droppable({ 
        drop: function(event, ui){
			DoDrop(ui.draggable,$(this));
		} 
    });
}


function dragiPad(sFilter){
	$(sFilter).each(function(){
	new webkit_draggable(this,{scroll:false, revert: true });
        $(this).bind("touchstart", function(event, ui) {
			   // $(this).css("z-index","98");
		});
	});
}

function dragiPadDisable(){
	webkit_draggable.destroy();
}

function DoDrop(draggable,droppable){
	if($(droppable).children("div").length == 0) {
		$(draggable).removeClass("pos-abs");
		$(droppable).append(draggable);
	}
	$("input#submit").removeAttr("disabled");
}

var tryagain = 1;
function Check() {
	if(finished) return;
	var try_again = false;
	$("div.flow-chart").find("div.light-blue-box").each(function(){
		//if(right) {
			var right = false;
			var compareId = $(this).attr("id").split("_");
			if($(this).find("div.blue-box").length > 0) {
				var compare = $(this).find("div.blue-box").attr("id").split("-");
				if(compareId.length == 2 && compareId.length == compare.length) {
					if(parseInt(compareId[1]) == parseInt(compare[1])) { right = true; }
					else right = false;	
				}
				else if(compareId.length == 3 && compareId.length == compare.length) {
					if(compareId[1] == compare[1]) {
						right = true;
					}
					else right = false;	
				} 
				else right = false;	
			} else right = false;	
			
			if(!right) {
				$("div#drop-items").append($(this).find("div.blue-box"));
				$("div#drop-items").find("div.blue-box").addClass("pos-abs").css({top:"0px",left:"0px"});
				try_again = true;
			}
		//}
	});
	$("input#submit").attr("disabled","disabled");
	if(try_again) {
		if(tryagain == 1) {
			tryagain++;	
			Popup(1);
			PlayAudio(1);
		}
		else {
			Popup(2);
			ResetBlocks();
			ShowAnswer();
			PlayAudio(2);
			finished = true;
		}
	}
	else {
		Popup(3);
		PlayAudio(3);
		finished= true;
	}
	
}

function ResetBlocks() {
	$("div#drop-items").append($("div.blue-box").not(".pos-abs"));
	$("div#drop-items").find("div.blue-box").addClass("pos-abs").css({top:"0px",left:"0px"});
}

function ShowAnswer() {
	$("div.flow-chart").find("div.light-blue-box").each(function(){
		var id1 = $(this).attr("id").split("_");
		
		if(id1.length == 2) {
			$(this).append($("div#drop-items").find("div#music-"+id1[1]));
		}
		else {
			var len = $("div#drop-items").find("div[id^=music-"+id1[1]+"]").length;
			$(this).append($("div#drop-items").find("div#music-"+id1[1]+"-"+len));
		}
	});
}

function Popup(st) {
	var $popup = $("div.popup");	
	
	$popup.show();
	$popup.find("div.tryagain,div.showanswer,div.correct").hide();
	$popup.find("div.tr"+st).show();
}


﻿// JScript File

$(document).ready(function(){
    //InitDragDrop();
    $(".PlayerStart").click(function(){
       $(this).hide();
       $(".MediaLibrary").find("audio").get(0).addEventListener("ended", onQuesAudioEnd, false);
       $(".MediaLibrary").find("audio").get(0).play();
    });
});	

var isDragDropTried;

function onQuesAudioEnd(){
    $(".Player").hide();
    InitDragDrop();
    isDragDropTried=false;
}


// ############# drag drop  start ##############



function InitDragDrop(){

   if ((navigator.platform == 'iPhone') || (navigator.platform == 'iPod') || (navigator.platform == 'iPad')) {
        dragiPad(".Drag");
        dropiPad(".Drop");
    }
    else{
        dragWeb(".Drag");
        dropWeb(".Drop");
    }
    
}

function dragWeb(sFilter){
    $(sFilter).draggable({revert:true});
    $(sFilter).bind("dragstart", function(event, ui) {
        $(this).css("z-index","2");
        $(this).parent().css("overflow","visible");
    });
}

function dropWeb(sFilter){
    $(sFilter).droppable({ 
        drop: function(event, ui){
		    if ($.trim($(this).text())==""){
	            DoDrop(ui.draggable,$(this));
	        }
        } 
    });
}

function dragiPad(sFilter){
    $(sFilter).each(function(){
	     new webkit_draggable(this,{scroll:false, revert: true });
        $(this).bind("touchstart", function(event, ui) {
		        $(this).css("z-index","2");
		        $(this).parent().css("overflow","visible");
	    });
    });
}

function dropiPad(sFilter){
    $(sFilter).each(function(){
        webkit_drop.add(this, {onDrop : function(dragged,dropped){
               if ($.trim($(webkit_drop.dropped).text())==""){
                    DoDrop(dragged,$(webkit_drop.dropped));
                }
		    }
	    });
			
    });
}

function DoDrop(draggable,droppable){
    $(droppable).append(draggable); 
    $(draggable).css("top","-2px");
    $(draggable).css("left","0px");       
    $(draggable).css("z-index","auto"); //because in drag start z-index is 1001
    $(draggable).animate( {top:"0px"}, 300, "linear", function(){
        $(this).css("left","0px");
        $(this).css("top","0px");
    });
    CheckDragDrop();
}


function CheckDragDrop(){
    if ($(".AnswerContainer div.Answer").length == 0) {
        $(".submitBtn").removeAttr("disabled");
        $(".submitBtn").click(function(){
            var isCorrect, sGroup;
            
            isCorrect = true;
            
            $(".UserAnswer").each(function(){
                sGroup = $(this).attr("id").substring(0,6);
                if ($(this).find(".Answer").length>0){
                    if ($(this).find(".Answer").attr("id").indexOf(sGroup)==-1){
                        isCorrect = false;
                    }
                    sGroup="";
                }
            });

            
            if(isCorrect){
                $(".correct").show();
                $(".correct").find("audio").get(0).play();
                $(".correct").find(".close").click(function(){
                    $(this).closest(".correct").hide();
                     $(".Interaction").hide();
                     $(".LastStep").show();
                     $(".LastStep").find("audio").get(0).play();
                });
            }
            else{
                if(isDragDropTried){
                    $(".incorrect").show();
                    $(".incorrect").find("audio").get(0).play();
                    $(".ShowAnswer").show();
                    $(".incorrect").find(".close").click(function(){
                        $(this).closest(".incorrect").hide();
                        $(".Interaction").hide();
                        $(".LastStep").show();
                        $(".LastStep").find("audio").get(0).play();
                    });
                }
                else{
                    $(".tryagain").show();
                    $(".tryagain").find("audio").get(0).play();
                    $(".tryagain").find(".close").click(function(){
                        isDragDropTried=true;
                        $(this).closest(".tryagain").hide();
                        $(".UserAnswer").each(function(){
                            var oSwap;
                            sGroup = $(this).attr("id").substring(0,6);
                            
                            if ($(this).find(".Answer").length>0){
                                if ($(this).find(".Answer").attr("id").indexOf(sGroup)==-1){
                                    oSwap = $(this).find(".Answer");
                                    $(".AnswerContainer").each(function(){
                                        if($(this).find(".Answer").length==0){ 
                                            $(this).append(oSwap);
                                            $(this).css("overflow","hidden");
                                        }
                                    });
                                    
                                }
                                sGroup="";
                            }
                        });
                    });
                }
            }
            $(".submitBtn").attr("disabled","true");
        });
    }
}

// ############# drag drop end ##############

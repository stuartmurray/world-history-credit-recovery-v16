var deviceType;
function setiScroll(id){
    eval("setiScroll"+deviceType+"('"+id+"')");
}

function dragByClass(dragContainer, enableDragStart,sPageContainerID){
    //var tmpFunc = new Function(codeToRun);
    eval("dragByClass"+deviceType+"('"+dragContainer+"',"+enableDragStart+",'" + sPageContainerID +"')");
}

function dropByClass(dropContainer,buttonId,disableMultiDrop,sPageContainerID){
    eval("dropByClass"+deviceType+"('"+dropContainer+"','"+buttonId+"',"+disableMultiDrop+",'"+ sPageContainerID +"')");
}

function dropByClass1(dropContainer,buttonId,disableMultiDrop){
    eval("dropByClass1"+deviceType+"('"+dropContainer+"','"+buttonId+"',"+disableMultiDrop+")");
}

function dropSwapByClass(dropContainer,buttonId){
    eval("dropSwapByClass"+deviceType+"('"+dropContainer+"','"+buttonId+"')");
}

function dropByClassInFolder(dropContainer){
    eval("dropByClassInFolder"+deviceType+"('"+dropContainer+"')");
}

function dropByClassHideDrop(dropContainer,QuesCnt){
    eval("dropByClassHideDrop"+deviceType+"('"+dropContainer+"',"+QuesCnt+")");
}

function dropByClassHideDropIfCorrect(dropContainer,QuesCnt){
    eval("dropByClassHideDropIfCorrect"+deviceType+"('"+dropContainer+"',"+QuesCnt+")");
}

function dragByClassScrumble(dragContainer, enableDragStart){
    eval("dragByClassScrumble"+deviceType+"('"+dragContainer+"',"+enableDragStart+")");
}

function dropByClassScrumble(dropContainer,buttonId,disableMultiDrop){
    eval("dropByClassScrumble"+deviceType+"('"+dropContainer+"','"+buttonId+"',"+disableMultiDrop+")");
}


var myScroll;
function setiScrollTouch(id){
	myScroll = new iScroll(id);
	myScroll.refresh();
}


function dragByClassTouch(dragContainer, enableDragStart,sPageContainerID){
    //enableDragStart -- To display the drag item on the top
	$(sPageContainerID).find('.' + dragContainer).each(function(){
			if($(sPageContainerID).find(".CheckMyDropTable2").length>0){ // this special case to show next answer
                new webkit_draggable(this,{containment:sPageContainerID,scroll:false, revert:true,seconddrag:false});    
            }else{
			    new webkit_draggable(this,{containment:sPageContainerID,scroll:false, revert:true});
			}
			if (enableDragStart!=undefined) enableDragStart=false;
            $(this).bind("touchstart", function(event, ui) {
				    if(enableDragStart) $(this).css("z-index","1001");
				    GBLpreventWipe=true;
			});
	});
}

function RedragByTouch(obj, enableDragStart,sPageContainerID){
    //enableDragStart -- To display the drag item on the top
	//$(sPageContainerID).find('.' + dragContainer).each(function(){
			if($(sPageContainerID).find(".CheckMyDropTable2").length>0){ // this special case to show next answer
                new webkit_draggable(obj,{containment:sPageContainerID,scroll:false, revert:true,seconddrag:false});    
            }else{
			    new webkit_draggable(obj,{containment:sPageContainerID,scroll:false, revert:true});
			}
			if (enableDragStart!=undefined) enableDragStart=false;
            $(obj).bind("touchstart", function(event, ui) {
				    if(enableDragStart) $(this).css("z-index","1001");
				    GBLpreventWipe=true;
			});
	//});
}


function dragByClassOthers(dragContainer, enableDragStart,sPageContainerID){
	$(sPageContainerID).find('.' + dragContainer).draggable({containment:sPageContainerID,scroll:false, revert:true});
	if (enableDragStart==undefined) enableDragStart=false;
	if(enableDragStart==true){
	    $(sPageContainerID).find('.' + dragContainer).bind("dragstart", function(event, ui) {
	        $(this).css("z-index","1001");
        });
    }
}



function dropByClassTouch(dropContainer,buttonId,bEnableMultiDrop,sPageContainerID){
    //var sPageContainerID = $('.' + dropContainer).parents(".PageContainer").attr("id");
	$('.' + dropContainer).each(function(){
			webkit_drop.add(this, {onDrop : function(dragged,dropped){
					GBLpreventWipe=false;
					if(bEnableMultiDrop == undefined) bEnableMultiDrop=true;
		            if(!bEnableMultiDrop){ 
		                if ($.trim($(webkit_drop.dropped).text())==""){
		                    DoDrop(dragged,$(webkit_drop.dropped));
		                }
		                else{
		                     RedragByTouch(dragged,true,sPageContainerID);
		                }
		            }
		            else{
                        DoDrop(dragged,$(webkit_drop.dropped));
                         
		            }
					isAllDragAnswered(sPageContainerID);
		            if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false); 
				}
			});
			
	});
}

function dropByClassOthers(dropContainer,buttonId,bEnableMultiDrop,sPageContainerID){
   // var sPageContainerID = $('.' + dropContainer).parents(".PageContainer").attr("id");
	$(sPageContainerID).find('.' + dropContainer).droppable({ 
        drop: function(event, ui){
			if(bEnableMultiDrop == undefined) bEnableMultiDrop=true;
			if(!bEnableMultiDrop){ 
			    if ($.trim($(this).text())==""){
			        DoDrop(ui.draggable,$(this));
			    }
			}
			else{
                DoDrop(ui.draggable,$(this));
                 
			}
			isAllDragAnswered(sPageContainerID);
			if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false); 
        } 
    });
   
}

function isAllDragAnswered(sPagid){
    if($(sPagid).find(".CheckDragBoxPlaceholder .CheckDragBox").length==0){
        $(sPagid).find(".slider:visible").find('.CheckMyWorkDummy').hide();   
        $(sPagid).find(".slider:visible").find(".CheckMyWork").show();
    }
    if($(sPagid).find(".CheckDragBoxPlaceholder2 .CheckDragBox2").length==0){
       $(sPagid).find(".SubmitButtonDisable").hide();
       $(sPagid).find(".Popup").hide();
       $(sPagid).find(".TryAgainButton").hide();
       $(sPagid).find(".SubmitButton").show();
    }
}

function dragByClassScrumbleTouch(dragContainer, enableDragStart){
    //enableDragStart -- To display the drag item on the top
    var sPageContainerID = $('.' + dragContainer).parents(".PageContainer").attr("id");
	$("#" + sPageContainerID).find('.' + dragContainer).each(function(){
			new webkit_draggable(this,{containment:"#" +sPageContainerID,scroll:false, revert:true});
			if (enableDragStart!=undefined) enableDragStart=false;
            $(this).bind("touchstart", function(event, ui) {
				    if(enableDragStart) $(this).css("z-index","1001");
				    GBLpreventWipe=true;
			});
	});
}


function dragByClassScrumbleOthers(dragContainer, enableDragStart){
    var sPageContainerID = $('.' + dragContainer).closest(".PageContainer").attr("id");
	$('.' + dragContainer).draggable({containment:"#" +sPageContainerID,scroll:false, revert:true});
	if (enableDragStart==undefined) enableDragStart=false;
	if(enableDragStart==true){
	    $('.' + dragContainer).bind("dragstart", function(event, ui) {
	        $(this).css("z-index","1001");
        });
    }
}


function dropByClassScrumbleTouch(dropContainer,buttonId,disableMultiDrop){
	$('.' + dropContainer).each(function(){
			webkit_drop.add(this, {onDrop : function(dragged,dropped){
					GBLpreventWipe=false;
					$(dragged).css("top","-10px");
                    $(dragged).css("left","0px");
                    if(disableMultiDrop == undefined) disableMultiDrop=true;
			        if(!disableMultiDrop || $.trim($(webkit_drop.dropped).text())==""){
				        $(webkit_drop.dropped).append(dragged);   
				        $(dragged).animate( {top:"0px"}, 500, "linear", function(){
                            $(this).css("top","0px");
                            $(this).css("left","0px");
                        });  
                        if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false);  
			        }
				}
			});
			
	})
}

function dropByClassScrumbleOthers(dropContainer,buttonId,disableMultiDrop){
	$('.' + dropContainer).droppable({ 
        drop: function(event, ui){
            ui.draggable.css("top","-10px");
            ui.draggable.css("left","0px");            
			if(disableMultiDrop == undefined) disableMultiDrop=true;
			if(!disableMultiDrop || $.trim($(this).text())==""){
				$(this).append(ui.draggable);
				ui.draggable.css("z-index","auto");   
				ui.draggable.animate( {top:"0px"}, 300, "linear", function(){
                    $(this).css("top","0px");
                    $(this).css("left","0px");
                });
                if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false);  
			}
        } 
    });
}

function dropSwapByClassTouch(dropContainer,buttonId){
	$('.' + dropContainer).each(function(){
		webkit_drop.add(this, {onDrop : function(dragged,dropped){
		        GBLpreventWipe=false;
				var swapDiv=$(webkit_drop.dropped).children(0);
				$(dragged).css("top","-10px");
			    $(dragged).css("left","0px");
				$(webkit_drop.dropped).html(dragged);
				$(dragged).animate( {top:"0px"}, 300, "linear", function(){
                    $(this).css("top","0px");
                    $(this).css("left","0px");
                });	
				$('.' + dropContainer).each(function(){
					if ($(this).html() == ""){
						new webkit_draggable($(swapDiv),{revert:true});	
						$(swapDiv).bind("touchstart", function(event, ui) {
				            $(this).css("z-index","1001");
				            GBLpreventWipe=true;
			            });				  
						$(this).html(swapDiv); 
						$(swapDiv).css("top","-10px");
					    $(swapDiv).animate( {top:"0px"}, 300, "linear", function(){
                            $(this).css("top","0px");
                            $(this).css("left","0px");
                        });	 
                        $('#' + buttonId).attr("disabled",false); 
					}
				});
			}
		});
			
	});
}

function dropSwapByClassOthers(dropContainer,buttonId){
	$('.' + dropContainer).droppable({ 
		drop: function(event, ui){
			var swapHtml=$(this).children(0);
			ui.draggable.css("top","-10px");
			ui.draggable.css("left","0px");
            $(this).html(ui.draggable);
            ui.draggable.animate( {top:"0px"}, 300, "linear", function(){
                $(this).css("top","0px");
                $(this).css("left","0px");
            });		
			$('.' + dropContainer).each(function(){
				if ($(this).text() == ""){
					$(swapHtml).draggable({revert:'invalid'});
					$(swapHtml).bind("dragstart", function(event, ui) {
	                    $(this).css("z-index","1001");
                    });
					$(this).html($(swapHtml));
					$(swapHtml).css("top","-10px");
					$(swapHtml).animate( {top:"0px"}, 300, "linear", function(){
                        $(this).css("top","0px");
                        $(this).css("left","0px");
                    });	
				}
				if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false); 
			});
		} 
	});
}


/* need to check, below methods are unused - start*/

function dropByClass1Touch(dropContainer,buttonId,disableMultiDrop){
    $('.' + dropContainer).each(function(){
			webkit_drop.add(this, {onDrop : function(dragged,dropped){
			        GBLpreventWipe=false;
					$(dragged).attr("style","position:absolute; top:-10px; left:0px;z-index:993;");
					 if(disableMultiDrop == undefined) disableMultiDrop=true;
					 if(!disableMultiDrop || $.trim($(webkit_drop.dropped).text())==""){
					    $(webkit_drop.dropped).append(dragged);
				        $(dragged).animate( {top:"0px"}, 500, "linear", function(){
                            $(this).attr("style","position:absolute; top:0px; left:0px;z-index:993;");
                        });
                        if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false);  
					 }
					
				}
			});
			
	})
}


function dropByClassHideDropTouch(dropContainer,QuesCnt){
    $('.' + dropContainer).each(function(){
		webkit_drop.add(this, {onDrop : function(dragged,dropped){
		    var iSeletedIndex;
		    GBLpreventWipe=false;
			$(dragged).attr("style","position:absolute; top:0px; left:0px;");
			$(webkit_drop.dropped).prepend(dragged);
			$(dragged).animate( {width:"1%", height:"1%"}, 500, "linear", function(){$(this).hide();} );
			
            iSeletedIndex= $(dragged).attr("id").substring(4,$(dragged).attr("id").length);
            $("#Ques" + iSeletedIndex).parent(0).attr("class","fl QuesBoxBorderDisable");
            $("#Ques" + iSeletedIndex).attr("class","QuesBoxDisable");
            $(webkit_drop.dropped).children(".TestMe").html("Master Card </br> " + ($(webkit_drop.dropped).children(0).length -1) + " out of " + QuesCnt);
            if ($(".TestMeBorder").children("div [class!=TestMe]").length>0) $("#btnTestMe").attr("disabled",false);
		    }
		});
			
	});
}

function dropByClassInFolderTouch(dropContainer){
    $('.' + dropContainer).each(function(){
		webkit_drop.add(this, {onDrop : function(dragged,dropped){
		    GBLpreventWipe=false;
		    $(dragged).attr("style","position:absolute; top:-100px; left:10px;z-index:991;");
            $(webkit_drop.dropped).prepend(dragged);  
            $(dragged).animate( {top:"-40px"}, 500, "linear", function(){
                var zIndex=900;
                zIndex = zIndex + $(this).parent(0).children("div").length;
                $(this).attr("style","position:absolute; top:-40px; left:10px;z-index:" + zIndex + ";");
                //$(this).draggable({ revert: true });
				 $("#btnSubmit").attr("disabled",false);
            });
		}
		});
	});
}

function dropByClassHideDropIfCorrectTouch(dropContainer,buttonId,disableMultiDrop){
	$('.' + dropContainer).each(function(){
			webkit_drop.add(this, {onDrop : function(dragged,dropped){
					GBLpreventWipe=false;
					//$(dragged).attr("style","position:relative; top:0px; left:0px;");
					//** $(dragged).css("top","-10px");
                    //**$(dragged).css("left","0px");
                    $(dragged).attr("style","position:absolute");
                    
                    iAryIndex=$(webkit_drop.dropped).attr('id').replace("QuesDiv","") -1;
                    if(disableMultiDrop == undefined) disableMultiDrop=true;
			        if(!disableMultiDrop || $.trim($(webkit_drop.dropped).text())==""){
                    if (GBLCorrectAnswerIDAry.length > iAryIndex){
                        if (GBLCorrectAnswerIDAry[iAryIndex].toString() == $.trim($(dragged).text()) ){
                            setDivContent("ds2pFeedbackText","You are correct");
                            //$(this).prepend(ui.draggable); 
                            $(webkit_drop.dropped).prepend(dragged);
                            $(dragged).html("<img src='images/ds2p_tickimg.png' />")
                            
                            $(dragged).animate( {opacity: "0.2"}, 1000, "linear", function(){} );            
                            $(dragged).animate( {width:"1%", height:"1%"}, 500, "linear", function(){$(dragged).hide();} );            
                                                
                            if ($("#ds2pDragContainer > div").length==0){
                                ShowModalPopUpHeigthWidth(" <br /><b>&nbsp;&nbsp;You have successfully Completed.</b>", "Result",100, 300)
                            }
                            
                        }
                        else{
                            $(dragged).attr("style","position:relative");
                            //$(dragged).css("position","relative");$(dragged).css("left","0px"); $(dragged).css("top","0px");                   
                            setDivContent("ds2pFeedbackText","You are wrong");
                            return false;
                        }
                    }
                    else{
                        $(dragged).attr("style","position:relative");
                        //$(dragged).css("position","relative");$(dragged).css("left","0px"); $(dragged).css("top","0px");                   
                        setDivContent("ds2pFeedbackText","You are wrong");
                        return false;
                    }
                    }
        
			        
				}
			});
			
	})
}

function setiScrollOthers(id){
	//Empty Function this is only for the Touch device
}


function DoDrop(draggable,droppable){
    //alert('DoDrop');
    if($(draggable).closest(".PageContainer").find(".CheckMyDropTable2").length>0){ // this special case to show next answer
        var iCurrent = $(draggable).closest(".CheckMyDropable").find(".Answer").index(draggable);
        $(draggable).closest(".CheckMyDropable").find(".Answer:eq(" + (iCurrent+1) + ")").parent().show();
    }
    $(droppable).append(draggable);
    $(draggable).css("top","-10px");
    $(draggable).css("left","0px");       
    $(draggable).css("z-index","auto"); //because in drag start z-index is 1001  
    $(draggable).animate( {top:"0px"}, 300, "linear", function(){
        $(this).css("top","0px");
        $(this).css("left","0px");
        //$(this).destroy();
        //webkit_draggable.destroy($(this));
        //alert(webkit_draggable);
        //webkit_draggable.destroy();
        //alert($(GBL_Drag).length);
    });
    //$(draggable).get(0).removeEventListener("touchstart", $(draggable).get(0).touchstart,false);
    //$(draggable).get(0).removeEventListener("touchstart", $(GBL_Drag).ts,false)
    /*$(draggable).get(0).addEventListener("touchstart", function(e){
    //alert('start');
    //return false;
    //e.preventDefault();
    $(this).trigger('touchend');
    },false);*/
    //$(draggable).get(0).destroy();
    $(draggable).removeClass('Drag');
    
}



function dropByClass1Others(dropContainer,buttonId,disableMultiDrop){
	$('.' + dropContainer).droppable({ 
        drop: function(event, ui){
            ui.draggable.attr("style","position:absolute; top:-10px; left:0px;z-index:993;");            
            if(disableMultiDrop == undefined) disableMultiDrop=true;
            if(!disableMultiDrop || $.trim($(this).text())==""){
			    $(this).append(ui.draggable);
			    ui.draggable.css("z-index","auto"); 
			    ui.draggable.animate( {top:"0px"}, 500, "linear", function(){
                    $(this).attr("style","position:absolute; top:0px; left:0px;z-index:992;");
                });
                if (buttonId!='undefined') $('#' + buttonId).attr("disabled",false); 
            }        
        } 
    });
}


function dropByClassHideDropOthers(dropContainer,QuesCnt){
	$('.' + dropContainer).droppable({ 
        drop: function(event, ui){
            ui.draggable.attr("style","position:absolute; top:5px; left:5px;")
            $(this).prepend(ui.draggable);  
            ui.draggable.animate( {width:"1%", height:"1%"}, 500, "linear", function(){$(this).hide();} );
            if (QuesCnt!=undefined) {
                // below code particularly for graphic organizer
                var iSeletedIndex;
                iSeletedIndex= ui.draggable.attr("id").substring(4,ui.draggable.attr("id").length);
                $("#Ques" + iSeletedIndex).parent(0).attr("class","fl QuesBoxBorderDisable");
                $("#Ques" + iSeletedIndex).attr("class","QuesBoxDisable");
                $(this).children(".TestMe").html("Master Card </br> " + ($(this).children(0).length -1) + " out of " + QuesCnt);
                if ($(".TestMeBorder").children("div [class!=TestMe]").length>0) $("#btnTestMe").attr("disabled",false);
            }
        } 
    });
}

function dropByClassInFolderOthers(dropContainer){
	$('.' + dropContainer).droppable({ 
        drop: function(event, ui){
            
            ui.draggable.attr("style","position:absolute; top:-100px; left:10px;z-index:991;")
            $(this).prepend(ui.draggable);  
            ui.draggable.animate( {top:"-40px"}, 500, "linear", function(){
                    var zIndex=900;
                    zIndex = zIndex + $(this).parent(0).children("div").length;
                    $(this).attr("style","position:absolute; top:-40px; left:10px;z-index:" + zIndex + ";");
                    //$(this).draggable({ revert: true });
					 $("#btnSubmit").attr("disabled",false);
                });
        } 
    });
}

function dropByClassHideDropOthers(dropContainer,QuesCnt){
	$('.' + dropContainer).droppable({ 
        drop: function(event, ui){
            ui.draggable.attr("style","position:absolute; top:5px; left:5px;")
            $(this).prepend(ui.draggable);  
            ui.draggable.animate( {width:"1%", height:"1%"}, 500, "linear", function(){$(this).hide();} );
            if (QuesCnt!=undefined) {
                // below code particularly for graphic organizer
                var iSeletedIndex;
                iSeletedIndex= ui.draggable.attr("id").substring(4,ui.draggable.attr("id").length);
                $("#Ques" + iSeletedIndex).parent(0).attr("class","fl QuesBoxBorderDisable");
                $("#Ques" + iSeletedIndex).attr("class","QuesBoxDisable");
                $(this).children(".TestMe").html("Master Card </br> " + ($(this).children(0).length -1) + " out of " + QuesCnt);
                if ($(".TestMeBorder").children("div [class!=TestMe]").length>0) $("#btnTestMe").attr("disabled",false);
            }
        } 
    });
}


function dropByClassHideDropIfCorrectOthers(dropContainer,QuesCnt){
	$('.' + dropContainer).droppable({ 
        drop: function(event, ui){
            $(ui.draggable).attr("style","position:absolute");
            //$(ui.draggable).css("position","absolute");
            //ui.draggable.css("position","absolute");
             
            iAryIndex=$(this).attr('id').replace("QuesDiv","") -1;
            //alert("$(this).attr('id'):" + $(this).attr('id') + "\n\n $(ui.draggable).attr('id'):"+ $(ui.draggable).attr("id")+ "\n\n iAryIndex:"+iAryIndex);
            if (GBLCorrectAnswerIDAry.length > iAryIndex){
                //alert("GBLCorrectAnswerIDAry[iAryIndex] :" +GBLCorrectAnswerIDAry[iAryIndex].toString() + "\n $(ui.draggable).text():" + $(ui.draggable).text());
                if (GBLCorrectAnswerIDAry[iAryIndex].toString() == $.trim($(ui.draggable).text()) ){
                    setDivContent("ds2pFeedbackText","You are correct");
                    $(this).prepend(ui.draggable); 
                    $(ui.draggable).html("<img src='images/ds2p_tickimg.png' />")
                    
                    $(ui.draggable).animate( {opacity: "0.2"}, 1000, "linear", function(){} );            
                    $(ui.draggable).animate( {width:"1%", height:"1%"}, 500, "linear", function(){$(this).hide();} );            
                                        
                    if ($("#ds2pDragContainer > div").length==0){
                        ShowModalPopUpHeigthWidth(" <br /><b>&nbsp;&nbsp;You have successfully Completed.</b>", "Result",100, 300)
                    }
                    
                }
                else{
                    $(ui.draggable).attr("style","position:relative");
                    //$(ui.draggable).css("position","relative");$(ui.draggable).css("left","0px"); $(ui.draggable).css("top","0px");                   
                    setDivContent("ds2pFeedbackText","You are wrong");
                    return false;
                }
            }
            else{
                    $(ui.draggable).attr("style","position:relative");
                    //$(ui.draggable).css("position","relative");$(ui.draggable).css("left","0px");$(ui.draggable).css("top","0px");
                    setDivContent("ds2pFeedbackText","You are wrong");
                    return false;
            }
            
        } 
    });
}

/* need to check, above methods are unused - end*/

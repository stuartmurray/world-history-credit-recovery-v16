var currObject='';
var currPage='';
var GBL_PageID='';
$(document).ready(function()
{
    $('#Image7').click(function(){
        ShowVirtualBookBag(); //skinutility.js
       
    });
    $('#Image5').click(function(){
        ShowResource(); //skinutility.js
       
    });
    DetectDevice();
    if(isTouchDevice){
        // text version for ipad
        $(".PageContainer").each(function(){
            if ($(this).find(".ipad").find(".textversion").length==0){
                $(this).find(".ipad").append($(this).find(".web").find(".textversion").closest(".AlignCenter").clone());
            }
        });
        $('.web').remove();
        $('.ipad').show();
        RenderCompatibilityTag();
    } else{
        $('.ipad').html('');
        $('.ipad').remove();
        $('.web').show();
    }
    
    
 	readCouseMap();
 	
 	$(".slideshow").each(function(){
 	    var sPageID;
 	    sPageID = "#" + $(this).closest(".PageContainer").attr("id");
 	    $(this).find("img:first").addClass("active");
 	    IntializeSlideShow(sPageID);
 	});
 	$(".SequenceGroup").each(function(){
 	    //$(this).find(".SequenceStart").show();
 	    //$(this).find(".SequenceRestart").hide();
 	    if($(this).find(".SequenceStart").find(".imgStart , .imgStartSmall, .imgStartMed").length>0){
 	        $(this).find(".SequenceStart").find(".imgStart , .imgStartSmall, .imgStartMed").removeClass("hide");
 	        $(this).find(".SequenceStart").find(".text , .textSmall, .textMed").removeClass("hide");
 	        $(this).find(".SequenceStart img").css({"opacity":"0.3","position":"absolute"});
 	    }
 	    if($(this).find(".SequenceRestart").find(".imgRestart, .imgRestartMed").length>0){
 	        $(this).find(".SequenceRestart").find(".imgRestart, .imgRestartMed").removeClass("hide");
 	        $(this).find(".SequenceRestart").find(".text, .textMed").removeClass("hide");
 	        $(this).find(".SequenceRestart img").css({"opacity":"0.3","position":"absolute"});
 	    }
 	    var sPageID = $(this).closest(".PageContainer").attr("id");
 	    $(this).find("video:not([onended])").attr("onended","SequenceFinish('#"+ sPageID +"')");
 	    $(this).find(".Sequence:first").show();
 	    
 	    if($(this).find(".Sequence").length==1){
 	       SingleSequenceBind($(this).find(".Sequence:first"))
 	    }
 	});
 	$(".PageContainer").each(function(){
 	    var sPageID = $(this).attr("id").substring(5,$(this).attr("id").length);
 	    var bookID;
 	    GBL_PageID=sPageID;
 	    GBLShowValidationStatusImg=1;
 	    GBLPopupDivID="divLightBox";
 	    
 	    bookID=replaceAll($('#divTitle').html(),' ','_');
 	    if(isBookBagAdded(bookID)){
 	        $('#page_' + sPageID).find('.bagIcon').css('opacity','0.4').css('cursor','default');
 	        $('#page_' + sPageID).find('.bagIcon').css('filter','alpha(opacity=40)');
 	        $('#page_' + sPageID).find('.bagIcon').unbind('click');
 	        $('#page_' + sPageID).find('.bagIcon').attr('onclick','');
 	    }
 	    PostRender(sPageID);
        PostAction(sPageID); 
 	});
 	$(".AudioPlayerControl").each(function(){
 	    $(this).find("audio").attr("onended","AudioPlayerEnded(this);");
 	    $(this).find(".play").show();
 	    
 	    $(this).find(".play").click(function(){
 	        $(this).hide();
 	        $(this).closest(".AudioPlayerControl").find(".pause").show();
 	        $(this).closest(".AudioPlayerControl").find("audio").get(0).play();
 	    });
 	    $(this).find(".pause").click(function(){
 	        $(this).hide();
 	        $(this).closest(".AudioPlayerControl").find(".play").show();
 	        $(this).closest(".AudioPlayerControl").find("audio").get(0).pause();
 	    });
 	    $(this).find(".replay").click(function(){
 	        $(this).hide();
 	        $(this).closest(".AudioPlayerControl").find("audio").get(0).load();
 	        $(this).closest(".AudioPlayerControl").find("audio").get(0).play();
 	        $(this).closest(".AudioPlayerControl").find(".pause").show();
 	    });
 	});
 	$('.play').live('click',function(){
 	    //alert('play');
 	    $(this).removeClass('play').addClass('pause');
 	    GBL_CurrMedia.pause();
 	});
 	$('.mute').live('click',function(){
 	    $(this).removeClass('mute').addClass('unmute');
 	    GBL_CurrMedia.volume=0;
 	    
 	});
 	$('.pause').live('click',function(){
 	    //alert('pause');
 	    $(this).removeClass('pause').addClass('play');
 	    GBL_CurrMedia.play();
 	});
 	$('.unmute').live('click',function(){
 	    $(this).removeClass('unmute').addClass('mute');
 	    GBL_CurrMedia.volume=1;
 	});
 	//$('#audio1').get(0).play();
 	$('.playicon').click(function(){
 	    //RenderAudioTag();
 	    //InitAudio();
 	    GBL_CurrMedia.play();
 	    
 	    /*$('audio').eq(0).bind('ended',function(){
                    //alert('audio ended');
                    $(this).closest('.sliderParent').find('.audiooverlay').hide();
        });
        $('audio').eq(0).get(0).play();*/
 	    //alert('audio playing');
 	    $('.playicon').hide();
 	    //$('.audiooverlay').hide();
 	});
 	InitToolTip();
});
var iAudioCount=0;
var iAudioLoaded=0;
function InitAudio(){
    //$("#page_"+PageID)
    iAudioCount=$('#divFeedback_' + GBL_PageID).find('audio').length;
    //alert(iAudioCount);
    var oAudio=$('#correctresponse_330_1').find('audio').get(0);
    oAudio.load();
    $(oAudio).attr('oncanplay','CheckAudioProgress(this);');
    /*$('#divFeedback_' + GBL_PageID).find('audio').each(function(){
        var oAudio=$(this).get(0);
        oAudio.load();
        $(oAudio).attr('oncanplay','CheckAudioProgress(this);');
    });*/
}
function CheckAudioProgress(obj){
    iAudioLoaded += 1;
    //alert(iAudioLoaded);
    if(iAudioLoaded==iAudioCount){
        alert('All audio loaded');
    }
}


/*function readCouseMap(){
    $.ajax({
        async: false,
        type: "GET",
        url: "../../../coursemap.xml",
        dataType: (jQuery.browser.msie) ? 'text' : 'xml',
        success: function(data){
            parseCourseMap(ConvertToSupportableFormat(data));
        }
      });
}

function parseCourseMap(xml){
    buildTOC(xml);
    getCurrentObject();
    bindPageMeta(xml);
    //$('.learnObj').find('a').eq(5).addClass('topicListSelected');
    $('.learnObj').find('a').eq($(xml).find('item').index($(xml).find("content[src='" + currPage + "']").parent())).addClass('topicListSelected');
}

function getCurrentObject(){
    var sURL=location.href.toLowerCase();
    var aURL;
    aURL=sURL.split('/');
    
    currObject=aURL[aURL.length-4] + '/' + aURL[aURL.length-3] + '/' + aURL[aURL.length-2];
    currPage=aURL[aURL.length-4] + '/' + aURL[aURL.length-3] + '/' + aURL[aURL.length-2] + '/' + aURL[aURL.length-1]
}*/

/*function bindPageMeta(xml){
    var subTitle;
    var parentNode;
    var totalPageNumber;
    var currPageNumber;
    var totalItemPageNumber;
    var currItemPageNumber;
    var currNode;
    
    parentNode=$(xml).find("content[src='" + currPage + "']").parent();
    subTitle=$(parentNode).attr('title');
    $('#divTitle').html(subTitle);
    
    totalItemPageNumber=$(parentNode).find('content').length;
    currItemPageNumber=$(parentNode).find("content").index($(parentNode).find("content[src='" + currPage + "']")) + 1;
    $('#tbPage').html(currItemPageNumber + ' of ' + totalItemPageNumber);
    
    totalPageNumber=$(xml).find('content').length;
    currPageNumber=$(xml).find("content").index($(xml).find("content[src='" + currPage + "']")) + 1;
    if(currPageNumber==1){
        $('#iPreviousDisable').show();
        $('#iPrevious').hide();
    } else{
        $('#iPreviousDisable').hide();
        $('#iPrevious').show();
        $('#iPrevious').attr('href','../../../'+$(xml).find("content").eq(currPageNumber-2).attr('src'));
    }
    if(currPageNumber==totalPageNumber){
        $('#iNextDisable').show();
        $('#iNext').hide();
    } else{
        $('#iNextDisable').hide();
        $('#iNext').show();
        $('#iNext').attr('href','../../../' + $(xml).find("content").eq(currPageNumber).attr('src'));
    }
}*/

function ShowFirstPopup(obj){
    //alert('ShowFirstPopup');
    /*$(obj).closest(".SecondPopup").hide();
    $(obj).closest(".SecondPopup").parent().find(".FirstPopup").show();*/
    $(obj).closest(".infoPopUp").find(".SecondPopup").hide();
    $(obj).closest(".infoPopUp").find(".SecondPopup").parent().find(".FirstPopup").show();
    
    $('#popup_link').html($(obj).closest(".infoPopUp").find('.FirstPopup').find('.SubpageLink').outerHTML());    
    $('#popup_link').find('.SubpageLink').removeClass('hide');
}

function ShowSecondPopup(obj){
    //alert('ShowSecondPopup'); popupContent
    /*$(obj).closest(".FirstPopup").hide();
    $(obj).closest(".FirstPopup").parent().find(".SecondPopup").show();*/
    $(obj).closest(".infoPopUp").find('.FirstPopup').hide();
    $(obj).closest(".infoPopUp").find('.FirstPopup').parent().find(".SecondPopup").show();
    
    $('#popup_link').html($(obj).closest(".infoPopUp").find('.SecondPopup').find('.SubpageLink').outerHTML());    
    $('#popup_link').find('.SubpageLink').removeClass('hide');
    
    SetPopupScroll();
}


function ShowPreviousPopup(sID){
    //GBL_ParentPopup=null;
    ShowSubPageContent($('#' + sID),'Text Version');
} 

function ShowPopupNextChild(obj){
    $(obj).hide();
    $(obj).closest(".infoPopUp").find(".PopupChild:eq(0)").hide();
    $(obj).closest(".infoPopUp").find(".PopupChild:eq(1)").show();
    $(obj).closest(".infoPopUp").find(".PopupNavPrevious").hide();
    $('#popup_link').html($(obj).closest(".infoPopUp").find(".PopupNavPrevious").outerHTML());
    $('#popup_link').find('.PopupNavPrevious').show();
    /*if(isTouchDevice){ 
        var myScroll;
        var sID='popup_scroll';
        myScroll = new iScroll(sID);
    }*/
    SetPopupScroll();
}

function ShowPopupPreviousChild(obj){
    $(obj).hide();
    $(obj).closest(".infoPopUp").find(".PopupChild:eq(0)").show();
    $(obj).closest(".infoPopUp").find(".PopupChild:eq(1)").hide();
    $(obj).closest(".infoPopUp").find(".PopupNavNext").hide();
    $('#popup_link').html($(obj).closest(".infoPopUp").find(".PopupNavNext").outerHTML());
    $('#popup_link').find('.PopupNavNext').show();
    
    /*if(isTouchDevice){ 
        var myScroll;
        var sID='popup_scroll';
        myScroll = new iScroll(sID);
    }*/
    SetPopupScroll();
}

function isBookBagAdded(bookBagID){
    var sContent='';
    var bFlag=false;
    if(bookBagID!=''){
        sContent=$.jStorage.get(bookBagID,'');
        if(sContent!='') bFlag=true;
    }
    return bFlag;
}

function PopupPageSelect(obj){
    ShowSubPageContent(obj,'Text Version');
}

function blockCtrlEvent(letter,e){
    var key;
    var isCtrl;
    if(window.event)
    {
        key = window.event.keyCode;     //IE
        if(window.event.ctrlKey)
            isCtrl = true;
        else
            isCtrl = false;
    }
    else
    {
        key = e.which;     //firefox
        if(e.ctrlKey)
            isCtrl = true;
        else
            isCtrl = false;
    }

    if (isCtrl){
    if( letter.toLowerCase() == String.fromCharCode(key).toLowerCase() ){
        var ev = e||window.event; 
        if(ev.preventDefault){ // non-IE browsers
            ev.preventDefault();
        }
        else{  // IE Only        
            ev.returnValue = false;
        }
    }
    }
}

function RenderCompatibilityTag(){
    $('object').each(function(){
        //var sType=$(this).attr('type');
        var sSrc = $(this).attr('data');
        var sType=sSrc.substring(sSrc.lastIndexOf('.')+1,sSrc.length).toLowerCase();
        var oCompatibilityNode;
        var sCompatibilityTag=$(this).outerHTML();

        //sCompatibilityTag=replaceAll(sCompatibilityTag,'style="display:none"','');
        sCompatibilityTag=$(sCompatibilityTag).removeAttr('style','').outerHTML();
        switch(sType){
            case 'mp3':
                //sCompatibilityTag=replaceAll(sCompatibilityTag,'<object','<div style="display:none"><audio');
                sCompatibilityTag=replaceAll(sCompatibilityTag,'<object','<div style="display:none"><audio preload="auto"');
                sCompatibilityTag=replaceAll(sCompatibilityTag,'</object>','</audio></div>');
                break;
            case 'mp4':
            case 'mov':
                var sPoster=$(this).attr('data');
                sPoster=sPoster.substring(0,sPoster.lastIndexOf('.')) + '.jpg';
                //sCompatibilityTag=replaceAll(sCompatibilityTag,'<object','<video controls="controls" preload="metadata" poster="' + sPoster + '"');
                //sCompatibilityTag=replaceAll(sCompatibilityTag,'<object','<span><video style = "background-image: url(' + sPoster + ') ;" controls="controls" preload="none" poster="' + sPoster + '"');
                //sCompatibilityTag=replaceAll(sCompatibilityTag,'<object','<span><video type="video/' + sType + '" style = "background-image: url(' + sPoster + ') ;" controls="controls" preload="none" poster="' + sPoster + '"');
                sCompatibilityTag=replaceAll(sCompatibilityTag,'<object','<span><video type="video/' + sType + '" controls="controls" preload="none" poster="' + sPoster + '"');
                
                sCompatibilityTag=replaceAll(sCompatibilityTag,'</object>','</video></span>');
                //sCompatibilityTag=replaceAll(sCompatibilityTag,'</object>','<img src="' + sPoster + '" /></video></span>');
                break;
        }
        //sCompatibilityTag=replaceAll(sCompatibilityTag,'style="display:none"','');
        sCompatibilityTag=replaceAll(sCompatibilityTag,'data=','src=');
        sCompatibilityTag=replaceAll(sCompatibilityTag,'title=','alt=');
        //alert(sCompatibilityTag);
        $(this).parent().html(replaceAll($(this).parent().html(),$(this).outerHTML(),sCompatibilityTag));
    });
}

function RenderAudioTag(){
    $('audio').each(function(){
        var sCompatibilityTag=$(this).outerHTML();
        
        sCompatibilityTag=replaceAll(sCompatibilityTag,'<audio','<audio preload="auto"');
        $(this).parent().html(replaceAll($(this).parent().html(),$(this).outerHTML(),sCompatibilityTag));
    });
}

var GBLcookieName="O21";
var CONDelimiter1="$#%";
var CONDelimiter2="-#$";
var CONDelimiter3="%^#";
var GBLcookieIndex=-1;
var GBLBookMarkcookieIndex=-1;
var GBLBookMarkcookieName= "BookMark" + GBLcookieName;
var GBLPageNavigationFlag=0;

function showGlossary(){
	hideLeftMenu();
	var content = '';
	//ShowModalPopUp("<h2>" + document.getElementById("divGlossary").innerHTML +"</h2>","Glossary");    
	content = content + '<div id="divGlossary">';
	content = content + '			<div>';
	content = content + '				<div id="divAlphabets" class="mgTop5 pd5 mgLeft10 glossaryBG" style="height:58px;"></div>';
	content = content + '				<div id="divKeyWords" class="fl glossaryBG glossaryText mgTop5 pd10" style=" width:150px;  height:205px; overflow:auto; display:none;">';
	content = content + '					<div class="bottomBorderBlue"><a href="javascript:void(0);" onclick="javascript:GlossaryContent();"></a></div>';
	content = content + '				</div>';
	content = content + '				<div style="width:390px; height:235px; overflow:auto;"><div id="divContent" class="glossaryText mgTop5 pd10"></div></div>';
	content = content + '			</div>';
	content = content + '</div>';
	ShowModalPopUpHeigthWidth(content,"Glossary",340,430);   		    
	parseGlossaryXML();
	setAlphabets();
	setiScroll('divContent')
}

function setAlphabets(){      
    var link,i, divAlphabets;
    divAlphabets=document.getElementById("divAlphabets");
    divAlphabets.innerHTML="";  
    for(i=65;i<=90;i++){
        link = "<a href='#' onclick='DisplayGlossary(\""+getCharacter(i)+"\");' ><div class='fl glossaryAlphabetContainer'>" + getCharacter(i) +"</div></a>";
        divAlphabets.innerHTML= divAlphabets.innerHTML + link + " "
    }
}

function getCharacter(ASCIICode){
    return String.fromCharCode(ASCIICode);
}

function GlossaryContent(){
    alert("GlossaryContent GlossaryContent");
}

function showNotes(){
	hideLeftMenu();
    //ajaxCall("notes.html"); 
	var content = '';
	content = content + '<div style="height:265px;width:385px;overflow:auto;position:relative;">';
    content = content + '<div id="divCreateNotes">';
    content = content + '    <div id="divTextarea">';
    content = content + '        <textarea id="ta" cols="45" rows="12"></textarea>';
    content = content + '    </div>';
    content = content + '    <div class="fr mgRight15">';
    content = content + '        <input id="btnCancel" type="button" value="Cancel" onclick="javascript:showNotes();" />';
    content = content + '        <input id="btnSave" type="button" value="Save" onclick="javascript:btnSave_onclick()" />';
    content = content + '    </div>';
    content = content + '</div>';
    content = content + '<div id="divListNotes">';
    content = content + '</div>';
	content = content + '</div>';
	
	showNotesModal(content);
	$("#lnkAddNotes").show();
	
}

function showNotesModal(res){
    GBLcookieIndex=-1;
    ShowModalPopUpHeigthWidth(res,"Notes",265,385);
    showArraylist();
}

function createCookie(CookieName,value,expiryDays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expiryDays);
    document.cookie=CookieName+'='+escape(value)+ ((expiryDays==null) ? "" : ";expires="+exdate.toUTCString());
}

function getCookie(CookieName)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(CookieName + "=");
  if (c_start!=-1)
    {
    c_start=c_start + CookieName.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}

function ajaxCall(sURL){
	$.ajax({		
		type: "GET",
		url: sURL,
		dataType: "html",
		success: showNotesModal
	  });
}

function printText(elem) { 
    popup = window.open('','popup','toolbar=no,menubar=no,width=400,height=350'); 
    popup.document.open(); 
    popup.document.write("<html><head></head><body onload='print()'>"); 
    popup.document.write(elem); 
    popup.document.write("</body></html>"); 
    popup.document.close(); 
} 


function getCookiesArray() {
    var cookies = { };
    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            cookies[decodeURIComponent(name_value[0])] = name_value[1];
        }
    }
    return cookies;
}

function getListItems(){
    var contents="";
    var cookies = getCookiesArray();
    contents=cookies[GBLcookieName]; 
    if ((contents=="") || (contents==undefined)){
        return "";
    }
    else{
        return unescape(contents);
    }    
}

function showArraylist(){
    showNotesList()    
    var cList,divListItem,listItems;
    var i,j=0;
    cList=""    
    //cList=cList+"<div style='overflow:auto;height:240px;'>";
    cList=cList+"<table width='100%' border='0' cellpadding='5' cellspacing='1' bgcolor='#c9c9c9' ><thead><tr class='trTitle'><td width='30px;'>No.</td><td>Notes</td><td width='70px;'>Action</td></tr></thead><tbody>";
    listItems=getListItems();
    arylistItems=listItems.split(CONDelimiter1);
    for (i=0;i<arylistItems.length;i++){
        j=i+1;
        if (arylistItems[i]!=""){
            var applyClass="";
            if (GBLcookieIndex==i){
                applyClass='trWhiteBg';                
            }
            else{
                applyClass='trWhiteBg';                
            }
            cList=cList+"<tr class='" + applyClass + "'><td ><center>" + j + "</center></td>";
            cList=cList+"<td class='mousePointer' onclick='javascript:doAction(\"edit\"," + i + ",this);'>" + replaceAll(replaceAll(getSubstring(arylistItems[i],160),"<","&lt;"),">","&gt;") + "<select id='select" + i + "' style='width:0px;height:0px;border: solid 0px;' /></td><td>"+ ActionDrp(i) + "</td></tr>" ;
        }
    }
    cList=cList+"</tbody></table>"
    
    $('#divListNotes').html(cList);
    var drpID="select" + GBLcookieIndex ;
    if (document.getElementById(drpID)){
        document.getElementById(drpID).focus();    
    }
    GBLcookieIndex=-1;
	
	setiScroll('divListNotes');
}

function getSubstring(str,len){
    if (str.length>len){
        str=str.substring(0,len)+"...";
    }
    return str;
}

function ActionDrp(cookieIndex){
    var sSelect='<select id="drpAction' + cookieIndex + '" onchange="javascript:doAction(this.value,' + cookieIndex + ',this)">';
    sSelect=sSelect+'<option value="select">Select</option>';
    sSelect=sSelect+'<option value="edit">Edit</option>';
    sSelect=sSelect+'<option value="print">Print</option>';
    sSelect=sSelect+'<option value="delete">Delete</option>';
    sSelect=sSelect+'</select>';
    return sSelect;
}

function doAction(act,cookieIndex,thisobj){
            listItems=getListItems();
            arylistItems=listItems.split(CONDelimiter1);
    switch (act){
    
        case "edit":
            document.getElementById("ta").value=arylistItems[cookieIndex];
            GBLcookieIndex=cookieIndex;
            showAddNotes();
            break;
            
        case "print":
            thisobj.selectedIndex=0;
            printText(arylistItems[cookieIndex]);
            break;
            
        case "delete":
            var cList="";
            arylistItems.splice(cookieIndex,1)
            cList=convertArraytoString(arylistItems);
            createCookie(GBLcookieName,cList,5);
            showNotes();
            break;
            
    }
}

function convertArraytoString(ary){
var str="";
    for (i=0;i<ary.length;i++){                
        if (str==""){
            str=ary[i];
        }
        else{
            str=str+CONDelimiter1+ary[i];
        }
    }
return str;
}

function btnSave_onclick() { 
    var newlistItem="";
    var listItems=getListItems();
    if ($.trim(document.getElementById("ta").value)==""){
        document.getElementById("ta").focus();
        alert("Please enter notes.");
        return false;
    }    
    if (GBLcookieIndex==-1){
        if (listItems==""){
            newlistItem=document.getElementById("ta").value;
            GBLcookieIndex=1;
        }
        else{
            newlistItem=listItems+CONDelimiter1+document.getElementById("ta").value;
            arylistItems=listItems.split(CONDelimiter1);
            GBLcookieIndex=arylistItems.length;
        }              
    }
    else{        
         arylistItems=listItems.split(CONDelimiter1);
         arylistItems[GBLcookieIndex]= document.getElementById("ta").value; 
         newlistItem=convertArraytoString(arylistItems);
    }
    createCookie(GBLcookieName,newlistItem,5);
    showNotes();
}

function showAddNotes(){    
    document.getElementById("divListNotes").style.display="none";
    document.getElementById("divCreateNotes").style.display="block";
    document.getElementById("lnkAddNotes").style.display="none";    
}

function showNotesList(){
    document.getElementById("divListNotes").style.display="block";
    document.getElementById("divCreateNotes").style.display="none";
    document.getElementById("lnkAddNotes").style.display="block";
}

//****************** BOOKMARK SECTION ********** START ***********************************************//
function showBookMark(){
	hideLeftMenu();
	var content="";	
	content = content + '<table width="100%"  cellpadding="3" cellspacing="0" bgcolor="#c9c9c9">';
    content = content + '<tr class="trTitle">';
    content = content + '<td colspan="2">&nbsp;Title&nbsp;';
    content = content + '<input id="txtBookMarkTitle" type="text" maxlength="30" class="textfield">';
    content = content + '<img src="images/btnAdd.jpg" style="cursor:pointer;"  onclick="javascript:saveBookMark();" width="52" height="27" border="0" align="top"></td>';
    content = content + '</tr>';
    content = content + '</table>';
    content = content + '<div id="divBookMarkList"></div>';

    ShowModalPopUpHeigthWidth(content,"Bookmark",265,385);
	
    intBookMark();
	//setiScroll('divBookMarkList');
}

function intBookMark(){
    GBLBookMarkcookieIndex=-1;
    $("#txtBookMarkTitle").val("");  
    $("#divBookMarkAdd").show();   
    showBookMarkArraylist();     
}

function showBookMarkForm(){
    $("#divBookMarkAdd").hide();        
}

function getBookMarkListItems(){
    var contents="";
    var cookies = getCookiesArray();
    contents=cookies[GBLBookMarkcookieName]; 
    if ((contents=="") || (contents==undefined)){
        return "";
    }
    else{
        return unescape(contents);
    }    
}

function saveBookMark(){

    var newlistItem="";
    var listItems=getBookMarkListItems();
    if ($("#txtBookMarkTitle").val()==""){
        $("#txtBookMarkTitle").focus();
        alert("Please enter bookmark title.");
        return false;
    } 
    
    if (GBLBookMarkcookieIndex==-1){
        if (listItems==""){
            newlistItem=$("#txtBookMarkTitle").val()+CONDelimiter2 + currObject  + CONDelimiter3 +  currPageNumber;
            GBLBookMarkcookieIndex=1;
        }
        else{
            newlistItem=listItems+CONDelimiter1+$("#txtBookMarkTitle").val()+CONDelimiter2 + currObject  + CONDelimiter3 +  currPageNumber;
            arylistItems=listItems.split(CONDelimiter1);
            GBLBookMarkcookieIndex=arylistItems.length;
        }              
    }
    /*
    else{        
         arylistItems=listItems.split(CONDelimiter1);
         arylistItems[GBLBookMarkcookieIndex]= $("#txtBookMarkTitle").val(); 
         newlistItem=convertArraytoString(arylistItems);
    }*/
    //alert("GBLBookMarkcookieName" + GBLBookMarkcookieName);
    createCookie(GBLBookMarkcookieName,newlistItem,5);
    alert("Bookmark added.");
    intBookMark();
}

function showBookMarkArraylist(){
    var cList,divListItem,listItems;
    var i,j=0;
    cList="";
    //cList=cList+"<div style='overflow:auto; height:208px;'>";
    cList=cList+"<table width='100%' cellpadding='0' cellspacing='0' bgcolor='#c9c9c9'>";
    listItems=getBookMarkListItems();
    arylistItems=listItems.split(CONDelimiter1);
    for (i=0;i<arylistItems.length;i++){
        j=i+1;
        if (arylistItems[i]!=""){
            var applyClass="";
            var BookMarkListItem=arylistItems[i].split(CONDelimiter2)
            var ObjectPage=BookMarkListItem[1].split(CONDelimiter3)
            cList=cList+"<tr>";
            cList=cList+"<td class='trWhiteBg mousePointer' onclick='javascript:gotoBookMark(\"" + ObjectPage[0] + "\"," + ObjectPage[1] + ");'>&nbsp;" + BookMarkListItem[0] + "<select id='select" + i + "' style='width:0px;height:0px;border: solid 0px;' /></td><td class='trWhiteBg mousePointer' style='width:15px;'><img src='images/icon_delete.png' onclick='deleteBookMark("+ i + ");'/></td></tr>" ;
        }
    }
    cList=cList+"</table>"    
    document.getElementById("divBookMarkList").innerHTML=cList;
	var drpID="select" + GBLBookMarkcookieName ;
    if (document.getElementById(drpID)){
        document.getElementById(drpID).focus();    
    }    
    GBLBookMarkcookieIndex=-1;
	
}

function deleteBookMark(cookieIndex){
    var cList="";
    listItems=getBookMarkListItems();
    arylistItems=listItems.split(CONDelimiter1);            
    arylistItems.splice(cookieIndex,1);
    cList=convertArraytoString(arylistItems);
    createCookie(GBLBookMarkcookieName,cList,5);
    alert("Bookmark deleted.");
    showBookMarkArraylist();
}

function gotoBookMark(objectID,pageID){
    HideLightBox("divLightBox");
	currObject=objectID;
	currPageNumber = pageID;
	GBLPageNavigationFlag=1;
	//alert("currObject:"+currObject+"\n\ncurrPageNumber:"+currPageNumber);
	if (objectID==''){
	    tocPageNumber=undefined;
	    parseObject('');
	}
	else{	    	
        parseObject(currObject);
	}
    GBLPageNavigationFlag=0;   
    intBookMark();
    //$("#divLightBox").hide(); 
	//$("#bookmarkbackGround").hide();
}

//****************** BOOKMARK SECTION ********** END ***********************************************//

function tocAnimation(){
			//alert('tocAnimation');
			$('#divLeftMenu').hide();
			$('#menuOpenClose').removeClass('menuSlidertoggle');
			$('#divTOC').animate({
				
				width: 'toggle'
			  }, 300, function() {
				// Animation complete.
				$('#tocOpenClose').toggleClass('tocslidertoggle');
				if ($('#tocOpenClose').attr("class")=="tocslider tocslidertoggle"){				    
				    $('#iMiddleSlider').attr('src','images/menuSplit2.png');
				}
				else{
				    $('#iMiddleSlider').attr('src','images/menuSplit.png');				    
				}
		  	});

}

function menuAnimation(){
			//alert('menuOpenClose');
			$('#divTOC').hide();
			$('#tocOpenClose').removeClass('tocslidertoggle');			
			$('#divLeftMenu').animate({
				
				width: 'toggle'
			  }, 300, function() {
				// Animation complete.
				$('#menuOpenClose').toggleClass('menuSlidertoggle');
				if ($('#menuOpenClose').attr("class")=="menuSlider menuSlidertoggle"){				    
				    $('#iMiddleSlider').attr('src','images/menuSplit1.png');
				}
				else{
				    $('#iMiddleSlider').attr('src','images/menuSplit.png');				    
				}
		  	});

}

function loadScript(url) { 
      var head = document.getElementsByTagName("head")[0]; 
      var script = document.createElement("script"); 
      script.src = url; 
 
      // Handle Script loading 
      { 
         var done = false; 
 
         // Attach handlers for all browsers 
         script.onload = script.onreadystatechange = function(){ 
            if ( !done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") ) { 
               done = true; 
               //if (callback) 
               //   callback(); 
 
               // Handle memory leak in IE 
               script.onload = script.onreadystatechange = null; 
            } 
         }; 
      } 
 
      head.appendChild(script); 
 
      // We handle everything using the script element injection 
      return undefined; 
 }
 
 function divSlider_click(){
    $('#divSlidePointer').toggleClass('slidertoggle');
    $('#divMenu').animate({
		width: 'toggle'
        }, 300, function() {
				// Animation complete.
    });
}

function replaceAll(txt, replace, with_this) {
  return txt.replace(new RegExp(replace, 'g'),with_this);
}


/*function replaceAll(content, replaceString, replaceWith)
{ var st = content;
  if (replaceString.length == 0)
     return st;
  var idx = st.indexOf(replaceString);
  while (idx >= 0)        
  {  st = st.substring(0,idx) + replaceWith + st.substr(idx+replaceString.length);
     idx = st.indexOf(replaceString);
  }
  return st;
}*/
 
function hideLeftMenu(){
    $("#divLeftMenu").css({display:"none"});
    $('#menuOpenClose').removeClass('menuSlidertoggle');
    $('#iMiddleSlider').attr('src','images/menuSplit.png');	
}

/*function getIndex(aSource,sValue){
    return aSource.grep(aSource,function(elm,index){
        return (elm == sValue);
    });
}*/

function setDivContent(sDivID,sText){
    $("#" + sDivID).html(sText);
}


function getParameterByName(name) 
{ 
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]"); 
  var regexS = "[\\?&]"+name+"=([^&#]*)"; 
  var regex = new RegExp( regexS ); 
  var results = regex.exec( window.location.href ); 
  if( results == null ) 
    return ""; 
  else 
    return decodeURIComponent(results[1].replace(/\+/g, " ")); 
}

function redirectToObject(){
    var sHref,sObjectId;
    var aData
    sHref=location.href;
    aData = sHref.split("/")
    sObjectId=aData[aData.length-2];
    //alert(sObjectId);
    location.href=location.href.replace(sObjectId + "/index.html","index.html?objectid=" + sObjectId); 
}

var isTouchDevice=false;
function DetectDevice(){
	/*if ((navigator.userAgent.indexOf('iPhone') != -1) || (navigator.userAgent.indexOf('iPod') != -1) || (navigator.userAgent.indexOf('iPad') != -1)) {
		//document.location = "http://www.scottrockers.com/iphone.html";
	}*/
	if ((navigator.platform == 'iPhone') || (navigator.platform == 'iPod') || (navigator.platform == 'iPad')) {
		isTouchDevice=true;
		//document.location = "http://www.scottrockers.com/iphone.html";
		//$.getScript("http://dev.jquery.com/view/trunk/plugins/color/jquery.color.js"); 
		//$.getScript("Script/iscroll.js?v3.4.3");
		//$.getScript("Script/webkitdragdrop.js");
		//$.getScript("Script/DeviceTouch.js");
		//alert("Detect Device");
		//loadScript("Script/iscroll.js?v3.4.3");
		//loadScript("Script/webkitdragdrop.js");
		//alert("Load Script");
		//loadScript("Script/DeviceTouch.js");
		//$.getScript("Script/DeviceTouch.js");
		//loadScript("Script/DeviceTouch.js");
		//alert("Detect Device End");

		//test();
		deviceType="Touch";
	} else{
		isTouchDevice=false;
		deviceType="Others";
		/*$.getScript("Script/jquery.ui.core.js"); 
		$.getScript("Script/jquery.ui.widget.js"); 
		$.getScript("Script/jquery.ui.mouse.js"); 
		$.getScript("Script/jquery.ui.draggable.js"); 
		$.getScript("Script/jquery.ui.droppable.js"); 
		$.getScript("Script/DeviceOthers.js"); */
		/*loadScript("Script/jquery.ui.core.js"); 
		loadScript("Script/jquery.ui.widget.js"); 
		loadScript("Script/jquery.ui.mouse.js"); 
		loadScript("Script/jquery.ui.draggable.js"); 
		loadScript("Script/jquery.ui.droppable.js"); 
		loadScript("Script/DeviceOthers.js"); */
	}

}
function Randomize(PageID,ContainerClass){}
function Randomize_blockedforFLVS(PageID,ContainerClass){
    var iRnd, iSwap, iSwapLength;
    var Container=$('#page_'+PageID).find('.'+ContainerClass+':visible');
    iSwapLength = Container.length;
    iRnd = 0;
    iSwap = 0;
    if (iSwapLength >1){
        for(var iChild=0;iChild<iSwapLength;iChild++){
            var tmpContainer1;
            var tmpContainer2;
            //alert(iSwapLength-iChild);
            iRnd = Math.floor(Math.random() * (iSwapLength-iChild-1))+1;
            //if (iRnd != iSwap){
                //alert((iSwapLength-iChild-1) + ' : ' + (iRnd-1));
                /*tmpContainer=$(Container.get(iSwapLength-iChild-1)).outerHTML();
                $(Container.get(iSwapLength-iChild-1)).replaceWith($(Container.get(iRnd-1)).outerHTML());
                $(Container.get(iRnd-1)).replaceWith(tmpContainer);*/
                tmpContainer1=$($('#page_'+PageID).find('.'+ContainerClass+':visible').get(iSwapLength-iChild-1)).clone().outerHTML();
                tmpContainer2=$($('#page_'+PageID).find('.'+ContainerClass+':visible').get(iRnd-1)).clone().outerHTML();
                $($('#page_'+PageID).find('.'+ContainerClass+':visible').get(iSwapLength-iChild-1)).replaceWith(tmpContainer2);
                $($('#page_'+PageID).find('.'+ContainerClass+':visible').get(iRnd-1)).replaceWith(tmpContainer1);
            //}
        }
    }
}

function Randomize_old(HtmlContainer){
    var iRnd, iSwap, iSwapLength;
    
    iSwapLength = $(HtmlContainer).children().length;
    iRnd = 0;
    iSwap = 0;
    if (iSwapLength >1){
    for(var iChild=0;iChild<iSwapLength;iChild++){
        while(iRnd==0 && (iSwapLength-iSwap)>0) iRnd = Math.floor(Math.random() * (iSwapLength-iSwap));
        if (iRnd==0 && iRnd<iSwapLength) iRnd++; 
        iSwap++;
        if (iRnd != iSwap) $(HtmlContainer).children("div:nth-child(" + iRnd + ")").insertBefore($(HtmlContainer).children("div:nth-child(" + iSwap + ")"));
    }
    }
}

function RandomizeCombo(HtmlContainer){
    var iRnd, iSwap, iSwapLength;
    
    iSwapLength = $(HtmlContainer).children().length;
    iRnd = 0;
    iSwap = 2;
    
    while(iRnd<2 || iRnd == iSwap) iRnd = Math.floor(Math.random() * iSwapLength);
    $(HtmlContainer).children("option:nth-child(" + iRnd + ")").insertBefore($(HtmlContainer).children("option:nth-child(" + iSwap + ")"));
}



function CreateCustomTable(row, column, sIDPrefix, iwidth, iheight){
    var myTbl, seq, cellWidth, cellHeight, cellMaxHeight, iCellSpacing;
	myTbl='';
	seq=1;
	iCellSpacing=10;

	cellWidth = parseInt((iwidth - (iCellSpacing * column) - iCellSpacing)/column);
	cellHeight = parseInt((iheight - (iCellSpacing * row) - iCellSpacing)/row);
	cellMaxHeight = cellHeight;
	
	myTbl= myTbl + '<table width="' + iwidth + 'px" border="0" height="' + iheight + 'px" cellspacing="' + iCellSpacing + 'px"><tbody>';
	for(var iCnt=1;iCnt<=row;iCnt++){
		myTbl = myTbl + '<tr>';
		for(var jCnt=1;jCnt<=column;jCnt++){
			myTbl = myTbl + '<td id="' + sIDPrefix + seq + '" style="max-height:' + cellMaxHeight + 'px; width:' + cellWidth + 'px; height:' + cellHeight + 'px;" valign="top">';
			seq = seq +1;
			myTbl = myTbl + '</td>';
		}
		myTbl = myTbl + '</tr>';
	}
	myTbl = myTbl + '</tbody></table>';
	return myTbl;
}

function MergePageLayoutWithTable(row, column, span, sCellIDPrefix){
    //alert(sCellIDPrefix);
     $.each(span.split("#"), function(){
		var iRowMin = 10;
		var iRowMax = 0;
		var iColMin = 10;
		var iColMax = 0;
		var iRowSpan;
		var iColSpan;

		$.each(this.split(","),function(){
			var iVal = this.split(":");
			if (iRowMin > iVal[0]){
				iRowMin = iVal[0];
			}
			if (iRowMax < iVal[0]){
				iRowMax = iVal[0];
			}
			if (iColMin > iVal[1]){
				iColMin = iVal[1];
			}
			if (iColMax < iVal[1]){
				iColMax = iVal[1];
			}
		});
		iRowSpan = iRowMax - iRowMin;
        iColSpan = iColMax - iColMin;
		
		var cell = parseInt(((iRowMin)*column)) + parseInt(iColMin) + 1;
		$("#" + sCellIDPrefix + cell).attr("RowSpan", iRowSpan + 1);
        $("#" + sCellIDPrefix + cell).attr("ColSpan", iColSpan + 1);
		//alert($("#" + sCellIDPrefix + cell).css("width"));
		$("#" + sCellIDPrefix + cell).css("width",(parseInt($("#" + sCellIDPrefix + cell).css("width").replace("px","")) * (iColSpan + 1)));
		$("#" + sCellIDPrefix + cell).css("height",(parseInt($("#" + sCellIDPrefix + cell).css("height").replace("px","")) * (iRowSpan + 1)));
		
		var iRow;
		for(iRow = parseInt(iRowMin) + 1;iRow <= iRowMax; iRow++){
			var cell = parseInt(((iRow)*column)) + parseInt(iColMin) + 1;
            $("#" + sCellIDPrefix + cell).remove();
         }
		 var iCol;
         for(iCol = parseInt(iColMin) + 1; iCol <= iColMax; iCol++){
         	for(iRow = iRowMin; iRow<=iRowMax; iRow++){
				var cell = parseInt(((iRow)*column)) + parseInt(iCol) + 1;
            	$("#" + sCellIDPrefix + cell).remove();
         	}
         }
	});
}

function EditMode(obj){
}


function ConvertToSupportableFormat(data){
    var xml; 
    if ( typeof data == 'string') { 
        xml = new ActiveXObject( 'Microsoft.XMLDOM'); 
        xml.async = false; 
        xml.loadXML( data); 
    } else { 
        xml = data; 
    } 
    return xml;
}

function GetCDATAText(ContentNode){
    var data;
    if($.browser.msie) data=ContentNode[0].text;
    else data=ContentNode[0].textContent;
    return data;
}

jQuery.fn.outerHTML = function() {
    return $('<div>').append( this.eq(0).clone() ).html();
};

function InitToolTip(){
    $('.Tooltip').hide();
    $(".Highlight").click(function(){
        var oPosition, iWidth, sID;    
        oPosition = $(this).position();
        iWidth =  $(this).width()/4;
        sID = $(this).attr("id").substring(8,$(this).attr("id").length);
       
        $(".Tooltip").css("left",oPosition.left + iWidth);
        $(".Tooltip").css("top",oPosition.top + $(this).height()-20);
		 $(".Tooltip").find(".TooltipContent").hide();
        $(".Tooltip").find(".TooltipContent[id$='"+ sID +"']").show();
        $(".Tooltip").show();
    });
    $(".Tooltip").find(".close").click(function(){
        $(this).closest(".Tooltip").hide();
        $(this).closest(".Tooltip").find(".TooltipContent").hide();
    });

}
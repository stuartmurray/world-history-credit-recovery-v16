var aMedia;
var src;
function generateLibrary(){
	aMedia = new Array;
	if (currObject != ""){
		content = $(courseMap).find('content');
		$(content).each(function(){
			  src = $(this).attr("src").replace("/LOT.swf","");
			  src = src.substring(0,src.lastIndexOf('/'));
			  getObjectLibrary(src);
		});
	} else{
		getObjectLibrary('');
	}
}

function getObjectLibrary(sSrc){
	sSrc = sSrc;
	if (src != '') src = src + '/';
	$.ajax({
		async: false,
		type: 'GET',
		url: src + 'LO.xml',
		dataType: 'xml',
		success: function(xml){
			var media = $(xml).find('Media');
			//alert(media);
			media.each(function(){
								//alert(src);
				aMedia[aMedia.length]=src + $(this).attr('src');
			});
		}
	});
}

function getLibrary(libType){
	if (aMedia == undefined) generateLibrary();
	var aLibraryItems = new Array;
	
	switch(libType){
		case "Video":
            aLibraryItems = $.grep(aMedia, function(elm,index){
                var ext = elm.split('.').pop().toLowerCase();						
                return (ext == 'mov' );
            });
			break;
			
		case "Image":
    		aLibraryItems = $.grep(aMedia, function(elm,index){
                var ext = elm.split('.').pop().toLowerCase();						
                return ( (ext == 'png' ) || (ext == 'jpg' )  || (ext == 'jpeg' ) || (ext == 'gif' ) );
            });
			break;
			
		case "Audio":
    		aLibraryItems = $.grep(aMedia, function(elm,index){
                var ext = elm.split('.').pop().toLowerCase();						
                return ( (ext == 'aiff' ) || (ext == 'mp3' ) );
            });
			break;
		
	}
    
	return aLibraryItems;
}

function showVideoLibrary(){
	hideLeftMenu();
	aryLibrary=getLibrary('Video');
    var Content = '<div class="Library"><div id="divLibScroll">';
    $.each(aryLibrary,function(key,value){        
       sValue = value.replace("\\","/");
	   //alert(sValue);
	   var sfilename=getFileName(sValue);// aryFileName[(aryFileName.length-1)];
        if (sfilename.length>15){
        sfilename=sfilename.substring(0,15)+".."
        }
        Content = Content + '<div class="fl LibraryItemPanel" onclick="playVideo(\'' + sValue + '\');"><img src="images/video.png" alt="video" class="LibraryItemIcon"/><div class="clr"></div> ' +  sfilename + ' </div>';
    });
    if (aryLibrary.length==0){
        Content = Content + '<b><br /><br /><br /><br /><br />Library is empty.</b>';
    }
    Content = Content + '<div class="clr"></div>';
	Content = Content + '</div></div>';
    ShowModalPopUp("divLibraryBox", Content, "Video Library",280, 550);
	
	setiScroll('divLibScroll');
}

function playVideo(fileName){
    //alert(fileName);
	var wd=480;ht=250;
    var sfilename=getFileName(fileName);
    var Content = '<div style="text-align:center;padding-top:5px;"><video width="' + wd + 'px" height="' + ht + 'px" controls src="' + fileName + '" type="video/mp4"></video><br />' + sfilename + '</div>';		    
    ShowModalPopUp("divVideoBox", Content, "Video",280, 550)
}

function showAudioLibrary(){
	hideLeftMenu();
	aryLibrary=getLibrary('Audio');
    var Content = '<div class="Library"><div id="divLibScroll">';
    $.each(aryLibrary,function(key,value){
        var aryFileName=(value.replace("\\","/")).split("/");
        var sfilename=aryFileName[(aryFileName.length-1)];
        if (sfilename.length>15){
        sfilename=sfilename.substring(0,15)+".."
        }
        Content = Content + '<div class="fl LibraryItemPanel" onclick="playAudio(\'' + value + '\');"><img src="images/video.png" alt="video" class="LibraryItemIcon"/><div class="clr"></div> ' +  sfilename+ ' </div>';
    });
    if (aryLibrary.length==0){
        Content = Content + '<b><br /><br /><br /><br /><br />Library is empty.</b>';
    }
    Content = Content + '<div class="clr"></div>';
	Content = Content + '</div></div>';
    ShowModalPopUp("divLibraryBox", Content, "Audio Library",280, 550);
	
	setiScroll('divLibScroll');
}

function playAudio(fileName){  
    var wd=480;ht=250;
    var sfilename=getFileName(fileName);
    var Content = '<div style="text-align:center;padding-top:5px;"><audio width="' + wd + 'px" height="' + ht + 'px" controls src="' + fileName + '"></audio><br />' + sfilename + '</div>';		    
    ShowModalPopUp("divAudioBox", Content, "Audio",280, 550)
}


function showImageLibrary(){
	hideLeftMenu();
	aryLibrary=getLibrary('Image');
    var Content = '<div class="Library"><div id="divLibScroll">';
    $.each(aryLibrary,function(key,value){
        var sFileFullPath=value.replace("\\","/");
        var aryFileName=(sFileFullPath).split("/");
        var sfilename=aryFileName[(aryFileName.length-1)];
        if (sfilename.length>15){
        sfilename=sfilename.substring(0,15)+".."
        }
        Content = Content + '<div class="fl LibraryItemPanel" onclick="showImage(\'' + sFileFullPath + '\');"><img src="' + value + '" alt="image" class="LibraryItemIcon"/><div class="clr"></div> ' +  sfilename + ' </div>';
    });
    if (aryLibrary.length==0){
        Content = Content + '<b><br /><br /><br /><br /><br />Library is empty.</b>';
    }
    Content = Content + '<div class="clr"></div>';
	Content = Content + '</div></div>';
    ShowModalPopUp("divLibraryBox", Content, "Image Library",280, 550);
	
	setiScroll('divLibScroll');
}

function showImage(fileName){
    var wd=480;ht=250;
    var sfilename=getFileName(fileName);
    var Content = '<div style="text-align:center;padding-top:5px;vertical-align:bottom;"><img style=\'width="' + wd + 'px" height="' + ht + 'px"\' src="' + fileName + '"/><br />' + sfilename + '</div>';		    
    ShowModalPopUp("divImageBox", Content, "Image",280, 550)
}

function getFileName(value){
    var aryFileName=(value.replace("\\","/")).split("/");
    return aryFileName[(aryFileName.length-1)];    
}
var curr_lb_div;
var is_modal = true;
var GBL_PausedVideo=null;
function ShowLightBox(lb_div, isModal)
{
    //$('.bodyContentStrip').hide();
    //$('#' + lb_div + 'fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer 
    ////$('#' + lb_div + 'fade').fadeIn(); //Fade in the fade layer 
    $('#' + lb_div + 'fade').show(); //Fade in the fade layer 
    //document.getElementById(lb_div + 'single1').style.display='block';
    $('#' + lb_div + 'single1').css('display','block');
    //document.getElementById(lb_div +'fade').style.display='block';
    curr_lb_div = lb_div;
    if (isModal)
        is_modal = true;
    else is_modal = false;
    //alert($('.whiteBox').css('height'));
    if(isTouchDevice){
        /*if($('#popup_scroll').height()>$('.whiteBox').height()){
              //var myScroll;
              //var sID='popup_scroll';
              //myScroll = new iScroll(sID,{momentum:false,desktopCompatibility:true});
              //myScroll = new iScroll(sID);
              setiScroll('popup_scroll');
                //alert(lb_div + '_scroll');
                //alert($('#' + lb_div + '_scroll').html());
                //alert(myScroll);
            //alert($('#popup_scroll').attr('style'));
            //$('#popup_scroll').attr('style','');
            //$('#popup_scroll').bind('touchend',function(){
            //    $('#popup_scroll').attr('style','');
            //})
        }*/
        SetPopupScroll();
        GBL_PausedVideo=null;
        $('video').each(function(){
            if(!$(this).get(0).paused){
                $(this).get(0).pause();
                GBL_PausedVideo=$(this).get(0);
            }
        });
        $('.PageContainer').hide();
    }
    //alert($('#popup_scroll').find('.SubpageLink:visible').outerHTML());
    if($('#popup_scroll').find('.SubpageLink:visible').length>0)
        $('#popup_link').html($('#popup_scroll').find('.SubpageLink:visible').outerHTML());
    if($('#popup_scroll').find('.PopupNavNext').length>0)
        $('#popup_link').html($('#popup_scroll').find('.PopupNavNext').outerHTML());    
    //$('#popup_link').find('.SubpageLink').removeClass('hide');
    //$('#popup_scroll').find('.SubpageLink').remove();
   $('#popup_scroll').find('.SubpageLink:visible').addClass('hide');
   $('#popup_scroll').find('.PopupNavNext').hide();
}

function SetPopupScroll(){
    if(isTouchDevice){ 
        //var myScroll;
        //var sID='popup_scroll';
        //myScroll = new iScroll(sID);
        //alert($('#popup_scroll').height() + ' - ' + $('.whiteBox').height());
        if(myScroll!=undefined){
            myScroll.destroy();
        }
        if($('#popup_scroll').height()>$('.whiteBox').height()){
            //$('#popup_scroll').css('overflow','auto');
            //setiScroll('popup_scroll');
             var sID='popup_scroll';
             myScroll = new iScroll(sID,{momentum:false,desktopCompatibility:true});
        } else{
            $('#popup_scroll').attr('style','');
        }
        //alert($('#popup_scroll').attr('style'));
    }
}
function HideLightBox(lb_div)
{
    //$('.bodyContentStrip').show();
    if (document.getElementById(lb_div))
    {
         document.getElementById(lb_div + 'single1').style.display='none';
         //document.getElementById(lb_div + 'fade').style.display='none';
         /*$('#'+lb_div + 'fade').fadeOut(function() {
			$(this).remove();  
	     });*/
	     $('#'+lb_div + 'fade').remove();
         curr_lb_div = '';
    }
    $('.PageContainer').show();
    /*$('video').each(function(){
        if($(this).get(0).paused){
            //$(this).get(0).load();
            $(this).get(0).play();
        }
    });*/
    if(GBL_PausedVideo!=null){
        GBL_PausedVideo.play();
    }
}

var PageContent;

function ShowModalPopUpHeigthWidth(Content, Title,Heigth, Width){
    ShowModalPopUp("divLightBox", Content, Title,Heigth, Width)
}

var GBL_ModalPopUpID;
var GBL_TopMove;
function MovePopup()
{
    var ht = window.pageYOffset + GBL_TopMove;
    //alert(ht.toString());
    $('#'+GBL_ModalPopUpID).css('position','relative');
    $('#'+GBL_ModalPopUpID + 'single1').css('top',ht.toString() + 'px');
}


function ShowModalPopUp(divID, Content, Title,Heigth, Width){
    //alert('ShowModalPopUp');
    if(document.getElementById(divID)){
        PageContent=document.getElementById(divID).innerHTML;
    }

    var maskHeight = $(document).height();
    var maskWidth = $(window).width();

  var sDiv='<div id="' + divID + 'fade" class="LB-black-overlay" style="width:' + maskWidth + 'px;height:'+ maskHeight + 'px;" onclick="if (!is_modal) HideLightBox(\'' + divID + '\'); return false;"></div>';
  var topMove,leftMove;
    //alert('Ht:'+$(window).height() + ' Wd:' + $(window).width());
    //alert(($(window).height()+195));
    topMove=($(window).height()/2) -((Heigth+102)/2);
    leftMove=($(window).width()/2) -((Width+40)/2);
    GBL_ModalPopUpID=divID;
    GBL_TopMove=topMove;
    


   //alert(window.pageYOffset);
    
   //var myElement = document.createElement('div');
   //alert(myElement.screenX);
    //topMove=0;
    //leftMove=0;
    //alert('Ht:'+topMove + ' Wd:' + leftMove);
    /*sDiv +=  '<div id="' + divID + 'single1" class="HelpContent" style="top:' + topMove + 'px;left:' + leftMove + 'px;"  >';
    sDiv +=  '<div class="popUp">';
    
        sDiv +=  '<div class="popUpTitle"><div class="popUpTitleRight">';
	    sDiv +=  '<div class="fl popUpTitleText">' + Title + '</div>';
	    sDiv +=  '<div class="fr popUpNav"><span id="lnkAddNotes" style="display:none;" class="mousePointer"><img onclick="showAddNotes();" src="images/notes-bookmark.png" border="0" style="margin-bottom:8px;"><img src="images/spliter.jpg" hspace="10px;"></span><span id="ahrefClose'+ divID +'" onclick="HideLightBox(\'' + divID + '\'); return false;"  class="mousePointer"><img src="images/close.png" border="0" style="padding-bottom:8px;" hspace="5px;"></span></div>';
	    sDiv +=  '</div></div>';
    	
	sDiv +=  '<div class="popUpBg"><div class="popUpBgRight">';
	sDiv +=  '<div class="popUpGrBg">';

    sDiv +=  '	<div class="whiteBox" style="overflow:auto; height:' + Heigth + 'px; width:'+ Width +'px;">' + Content + '</div>';

    sDiv +=  '</div>';
    sDiv +=  '</div>';
    sDiv +=  '</div>';
        
    sDiv +=  '<div class="popUpFooter"><div class="popUpFooterRight"></div></div>';
    sDiv +=  '</div>';
    sDiv +=  '</div>';*/
    Content = replaceAll(Content,'class="controlParent"','');
    Content = replaceAll(Content,'class=controlParent','');
    var sOuterWidth=Width + 48;
    sDiv +=  '<div id="' + divID + 'single1" class="HelpContent" style="top:' + topMove + 'px;left:' + leftMove + 'px;width:' + sOuterWidth + 'px"  >';
    //sDiv +=  '<div id="' + divID + 'single1" class="HelpContent" style="position:screen-fixed;"  >';
    sDiv +=  '<div class="infoPopUp">';
   sDiv +=  '<div class="infoPopUpTitle">';
     sDiv +=  '<div class="divLeft">';
    //sDiv +=  '<div class="titleImage">[<a href="javascript:window.print()" class="textVersionLink" alt="Print" title="Print">print page</a>]</div>';
    sDiv +=  '<div class="titleImage">[<a href="javascript:void(0)" onclick="print_div(\'whiteBox\');" class="textVersionLink" alt="Print" title="Print">print page</a>]</div>';
    //sDiv +=  '<div class="divLeft titleImage">[<a href="javascript:void(0)" onclick="print_div(\'whiteBox\');" class="textVersionLink">print page</a>]</div>';
    sDiv +=  '</div>';
    sDiv +=  '<div class="divRight popUpNav">';
    sDiv +=  '<div>';
    sDiv +=  '<div><div class="close" onclick="HideLightBox(\'' + divID + '\'); return false;" alt="Close" title="Close">&nbsp;</div></div>';
    //sDiv +=  '<div><a href="javascript:void(0)" class="close" onclick="HideLightBox(\'' + divID + '\'); return false;" alt="Close" title="Close">X</a></div>';
    sDiv +=  '</div>';
    sDiv +=  '</div>';
    sDiv +=  '</div>';
    sDiv +=  '<div class="contentLeftCurve">';
    sDiv +=  '<div class="contentRightCurve bodyText">';
    //sDiv +=  '<p class="text">The first time you login, you will be asked to create a set of security questions. Using these questions, you will be able to reset/change your password anytime and anywhere if you ever forget your password or it expires. Once you have logged in, click on the &quot;Blackboard&quot; link under &quot;Campus Tools&quot;, at the top left of your browser window. You will be redirected to Blackboard, Arcadia's Course Management System. Here, you can access your course and participate in course activities.</p>
    //sDiv +=  '	<div id="' + divID + '_scroll"  style="overflow:auto;height:' + Heigth + 'px; width:'+ Width +'px;">' + Content + '</div>'; -webkit-transform: translate3d(0,0,0);style="-webkit-transform: perspective(1000);"
    sDiv +=  '	<div class="whiteBox tcontent" style="overflow:auto;height:' + Heigth + 'px; width:'+ Width +'px;padding-right:18px;"><div id="popup_scroll"><div class="tcontent" style="background-color:#FFFFFF;">' + Content + '</div></div></div>';
    sDiv +=  '<div id="popup_link"></div>';
    //sDiv +=  '	<div id="' + divID + '_scroll"  style="overflow:auto;height:400px; width:600px;">' + Content + '</div>';
    //sDiv +=  '<br /><br />';   
    sDiv +=  '<p class="text" align="center" style="height:21px;"><a href="javascript:void(0)" class="textVersionLink" onclick="HideLightBox(\'' + divID + '\'); return false;" alt="Close" title="Close">Close this window</a></p>';
    sDiv +=  '</div>';
    sDiv +=  '</div>';
    sDiv +=  '</div></div>';

    if(document.getElementById(divID)==undefined){   
        var myElement = document.createElement('div');
        myElement.setAttribute('id',divID);
        document.getElementsByTagName("body")[0].appendChild(myElement);
    }
    //alert("Content:"+Content);
    document.getElementById(divID).innerHTML=sDiv;
    ShowLightBox(divID ,true);
    //alert($('#' + divID + '_scroll').html());
    if(isTouchDevice){
        //var myScroll;
        //myScroll = new iScroll(divID + '_scroll');
    
        //MovePopup();
        //window.onscroll=MovePopup;
    }
    
    //return false;
}

function print_div(Container){
    //$('.print_div').html($('.' + Container).html());
    if($('.print_div').length==0){
        $('body').append('<div class="print_div"></div>');
    }
    $('.print_div').html('<div class="whiteBox">'+$('.' + Container).html()+'</div>');
    //alert($('.print_div').html());
    window.print();
}

function showPreviousContent(){
    document.getElementById("divLightBox").innerHTML=PageContent;    
    ShowLightBox('single1');    
    return false;
}


function ShowDefaultModalPopUp(Content, Heigth, Width){
var divID='divLightBox';
  var sDiv='<div id="' + divID + 'fade" class="LB-black-overlay" onclick="if (!is_modal) HideLightBox(\'' + divID + '\'); return false;"></div>';
  var topMove,leftMove;

    topMove=($(window).height() -Heigth)/2 +  $(window).scrollTop();
    leftMove=($(window).width() -Width)/2;
        
    sDiv +=  '<div id="' + divID + 'single1" class="HelpContent" style="top:' + topMove + 'px;left:' + leftMove + 'px;position:absolute;"  >';
    sDiv +=  '	<div style="overflow:auto; height:' + Heigth + 'px; width:'+ Width +'px;">' + Content + '</div>'; /**/
    sDiv +=  '</div>';

    if(document.getElementById(divID)==undefined){   
        var myElement = document.createElement('div');
        myElement.setAttribute('id',divID);
        document.getElementsByTagName("body")[0].appendChild(myElement);
    }
    document.getElementById(divID).innerHTML=sDiv;
    ShowLightBox( divID ,true);
    return false;
}


function ShowCheckMyWorkModalPopUp(Content, PageID, Heigth, Width){
$("#page_"+PageID).find('.fc_LigthBox').remove();
$("#page_"+PageID).find('.fc_LigthBoxOverlay').remove();

  var sDiv='<div class="fc_LigthBoxOverlay LB-black-overlay_CheckMyWork" onclick="if (!is_modal) HideCheckMyWorkModalPopUp(\'' + PageID + '\'); return false;"></div>';
  var topMove,leftMove;

    topMove=($(window).height() -Heigth)/2;
    leftMove=($(window).width() -Width)/2;
    
    //sDiv +=  '<div class="fc_LigthBox" style="bottom:' + '0' + 'px;right:' + '40' + 'px;position:absolute;z-index:2;"  >';
    sDiv +=  '<div class="fc_LigthBox" style="top:40%;left:30%;position:absolute;z-index:10;"  >';
    //sDiv +=  '	<div style="overflow:auto; height:' + Heigth + 'px; width:'+ Width +'px;">' + Content + '</div>'; /**/
    sDiv +=  '	<div style="overflow:auto; width:'+ Width +'px;">' + Content + '</div>'; 
    sDiv +=  '</div>';
    
    $("#page_"+PageID).find('.slider:visible').find('.intContent, .intContentDrag').removeClass('posRelative').addClass('posRelative');
    $("#page_"+PageID).find('.slider:visible').find('.intContent, .intContentDrag').append(sDiv);  
    
    return false;
}


function HideCheckMyWorkModalPopUp(PageID){
    $("#page_"+PageID).find('.fc_LigthBox').remove();
    $("#page_"+PageID).find('.fc_LigthBoxOverlay').remove();
    $("#page_" + PageID).find('.feedbackContainer').hide();
    $("#page_" + PageID).find('.feedbackoverlay').hide();
}

function ShowInBuildFeedback(feedbackStatus,Content, PageID,LevelID){
    var FeedbackContainer = $("#page_" + PageID).find('.feedbackContainer');
    FeedbackContainer.find('.feedbackcontent').html(Content);
    FeedbackContainer.show();
    //alert(FeedbackContainer.find('.feedbackcontent').html());
    $("#page_" + PageID).find('.feedbackoverlay').show();
    /*if (FeedbackContainer.find('.feedbackcontent').find("audio").length>0){
            //FeedbackContainer.find('.feedbackcontent').find("audio").removeAttr('id');
            FeedbackContainer.find('.feedbackcontent').find("audio").get(0).play();
            
            GBL_CurrMedia=FeedbackContainer.find('.feedbackcontent').find("audio").get(0);
            
    }*/
        
    FeedbackContainer.find('.feedbackclose').unbind('click');
    FeedbackContainer.find('.feedbackclose').click(function(){
        HideInBuildFeedback(feedbackStatus,PageID,LevelID)
    });
    
}

function HideInBuildFeedback(feedbackStatus,PageID,LevelID){
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    $("#page_" + PageID).find('.feedbackContainer').hide();
    $("#page_" + PageID).find('.feedbackContainer').find('.feedbackcontent').html('');
    GBL_CurrMedia.pause();
    
    if(feedbackStatus==0){
        $("#page_" + PageID).find('.feedbackoverlay').hide();
        switch(type){
            case "wordselection":
                tryAgainWordSelection(PageID,LevelID);    
                break;
            case "multiplechoice":
                //tryAgainDefault(PageID);    
                DefineTryAgain(PageID);
                break;
        }
    }
    /*if(feedbackStatus!=1){
        $("#page_" + PageID).find('.feedbackoverlay').hide();
    }*/
    /*if($('.NextQuestion:visible').length>0){
        //$("#page_" + PageID).find('.feedbackoverlay').hide();
    }*/

    /*if(feedbackStatus==-1){//For Next Question
        $("#page_" + PageID).find('.feedbackoverlay').hide();
    } else if(feedbackStatus==1){//For Next Question
        $("#page_" + PageID).find('.feedbackoverlay').hide();
    }*/
}

//var GBL_ParentPopup;
function ShowSubPageContent(thisObject, Title){
    //alert('ShowSubPageContent');
    
    var sPrevContent=''
    var content='';
   
    //alert(content);
    if($(thisObject).closest("div").find(".popupContent").find(".popupContent").length>0){
        var sID=$(thisObject).closest('.PageContainer').attr('id').replace('page','textversion');
        $(thisObject).attr('id',sID);
        $(thisObject).closest("div").find(".popupContent").find(".popupContent").each(function(){
            if($(this).find('.SubpageLink').length==0){
                sPrevContent='<div class="SubpageLink" onclick="ShowPreviousPopup(\'' + sID  + '\')" title="Previous">[Previous]</div>';
                $(this).append(sPrevContent);
             }
            
        });
    }
     content=$(thisObject).closest("div").find(".popupContent").html();
    /*if(GBL_ParentPopup==null){
        GBL_ParentPopup=thisObject;
    } else{
        var sID=$(GBL_ParentPopup).closest('.PageContainer').attr('id').replace('page','textversion');
        $(GBL_ParentPopup).attr('id',sID);
        
        sPrevContent='<div class="SubpageLink" onclick="ShowPreviousPopup(\'' + sID  + '\')" title="Previous">[Previous]</div>';
    }*/
    
    ShowModalPopUp("divLightBox", content, Title,450,700);
    
    
}

function HideCheckMyWorkModalPopUpMSGBox(PageID){
    $("#page_"+PageID).find('.fc_LigthBox').remove();
}


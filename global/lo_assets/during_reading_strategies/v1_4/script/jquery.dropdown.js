$(document).ready(function(){

	$("ul.dropdown li").dropdown();

});

$.fn.dropdown = function() {
	$(this).hover(function(){
		$(this).addClass("hover");
		$('> .dir',this).addClass("open");
		$('ul:first',this).css('visibility', 'visible');
        if ($(this).parents("ul.dropdown").attr("id").toLowerCase().indexOf("lesson")==-1){
		    $("ul.dropdown").hide();
            $(this).parents("ul.dropdown").show();
        }
        $(this).click(function(){
            $('ul:first',this).css('visibility', 'hidden');
        });
        
	},function(){
		$(this).removeClass("hover");
		$('.open',this).removeClass("open");
		$('ul:first',this).css('visibility', 'hidden');
		$("ul.dropdown").show();
	});

}
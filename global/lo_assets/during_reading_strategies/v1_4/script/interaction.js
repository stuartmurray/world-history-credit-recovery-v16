﻿
/* Depended: Object.js */
// GBLInteractionType and GBLInteractionSubType intialized in renderInstruction method

var GBLDelimiter="$%#";
var GBLDelimiter1="@@";
var GBLAnswerAry,GBLChoiceAry,GBLCorrectAnswerIDAry;
var GBLInteractionType,GBLInteractionSubType,GBLIsCorrect,GBLCorrectResponse,GBLInCorrectResponse;
var submitButton = "<input id='btnSubmit' disabled='true' type='button' value='Submit' onclick='javascript:validator();'></input>";
var sMissingSpellIndexs="";
var GBL_Template_Multi='';
var GBL_Template_Distractor='';

function RenderInteraction(contentNode,sPageContainer){
     var multiTemplate='';
     var multiLevelID=0;
     var distractorTemplate='';
     var controlIndex=-1;
     var divTempMulti=document.createElement('div');
     var sMultiAction='';
     $(template_divXML).find('[multiple="true"]').each(function(){
        multiLevelID=$(this).attr('startinglevel');
        if($(template_divXML).find("[multiple='true']").attr('action') != undefined){
            if($(template_divXML).find("[multiple='true']").attr('action').toLowerCase()=='innerhtml'){
                
                $(template_divXML).find("[multiple='true']").find('.child').each(function(){
                    var oChild=$(this).clone(true);
                    $(oChild).removeClass('child');
                    $(divTempMulti).append($(oChild));
                });
                sMultiAction = $("[multiple='true']").attr('action');
                GBL_Template_Multi=divTempMulti;
            }
        } else{
            $(divTempMulti).html($(this).outerHTML());
            $(divTempMulti).children(0).removeAttr("multiple").removeAttr("startinglevel").removeAttr("defaultcount").removeAttr("level");
            GBL_Template_Multi=$(divTempMulti).html();
        }
        
        multiTemplate=$(divTempMulti).html();
        //alert(multiTemplate);
        //multiLevelID=$(this).attr('startinglevel');
        //multiTemplate = $('<div>').append($(multiTemplate).removeAttr("multiple")).html();
        //multiTemplate = $('<div>').append($(multiTemplate).removeAttr("startinglevel")).html();
        //multiTemplate = $('<div>').append($(multiTemplate).removeAttr("defaultcount")).html();
        //$(multiTemplate).removeAttr("level");
        
        /*$(template_divXML).find('.AnswerContainer').each(function(){
            if($(this).get(0).nodeName.toLowerCase()=='td'){
                var divTemp=document.createElement('div');
                $(divTemp).html(multiTemplate);
                $(divTemp).find("td[class!='AnswerContainer']").html('');
                answerTemplate=$(divTemp).html();
            } else{
                answerTemplate=$(multiTemplate).html($(this).outerHTML()).outerHTML();
            }
        });*/
        if($(template_divXML).find('.distractor').length>0){
            $(template_divXML).find('.distractor').each(function(){
                var divTempDist=document.createElement('div');
                $(divTempDist).html($(this).outerHTML());
                if($(divTempDist).children(0).attr("multiple")!=undefined){ // for distractor and Answer in the same container
                    $(divTempDist).children(0).removeAttr("multiple").removeAttr("startinglevel").removeAttr("defaultcount").removeAttr("level");
                    distractorTemplate=$(divTempDist).html();
                    //alert(distractorTemplate);
                } else{ // for distractor in separate container
                    distractorTemplate=$(this).outerHTML();
                    var divTemp=document.createElement('div');
                    $(divTemp).html(template_divXML);
                    $(divTemp).find('.distractor').remove();
                    template_divXML=$(divTemp).html();
                    //$(this).remove();
                }
                //alert(distractorTemplate);
            });
            /*distractorTemplate = $(template_divXML).find('.distractor').outerHTML();
            var divTemp=document.createElement('div');
            $(divTemp).html(template_divXML);
            $(divTemp).find('.distractor').remove();
            template_divXML=$(divTemp).html();*/
        } else{ // if distractor class doesn't exists
            distractorTemplate=$(this).outerHTML();
        }
        GBL_Template_Distractor=distractorTemplate;
     });
     //alert(multiTemplate);
        /*multiTemplate = $('<div>').append($(multiTemplate).removeAttr("multiple")).html();
        multiTemplate = $('<div>').append($(multiTemplate).removeAttr("startinglevel")).html();
        multiTemplate = $('<div>').append($(multiTemplate).removeAttr("defaultcount")).html();
        $(multiTemplate).removeAttr("level");*/
        //alert("multiTemplate:" + multiTemplate);

    var choiceList='';
    var answerList='';
    var userAnswerList='';
    var oFeedback = document.createElement('div');

    $(contentNode).find('Header').each(function(index){           
            $(this).children(0).each(function(){
                var tagName= $(this).get(0).nodeName.toLowerCase();
                //alert("tagName:"+tagName);
                switch (tagName){
                    case "instruction":
                        GBL_levelID = 0;
                        GBL_GroupID = 1;
                        GBL_itemID = 0;   
                        GBL_fieldID = 1;
                        break;
                        
                    case "question":
                        GBL_levelID = 0;
                        GBL_GroupID = 1;
                        GBL_itemID = 1;  
                        GBL_fieldID = 1;
                        break;                                     
                        
                }   
                $(this).children(0).each(function(){              
                    parseField($(this),sPageContainer);
                });      
            });
    });    
    //alert($(contentNode).find('Level').length);
    $(contentNode).find('Level').each(function(index){
        if($(this).attr('ID')!='undefined'){
            GBL_levelID = $(this).attr('ID');
        } else{
            GBL_levelID = GBL_levelID + 1;
        }
        
        if(multiLevelID!=0 && parseInt(GBL_levelID) > parseInt(multiLevelID)){
            template_divXML=ReprocessMarkup(this,template_divXML);
            /*var Temp;
            //if(!isDynamicDistractor){
            //alert($(this).children(0).get(0).nodeName.toLowerCase());
            if($(this).children().length==1 && ($(this).children(0).get(0).nodeName.toLowerCase()=='distractor')){
                if(distractorTemplate.indexOf('level0')!=-1){
                    Temp = replaceAll(distractorTemplate, "level0", "level" + GBL_levelID);
                } else{
                    Temp = replaceAll(distractorTemplate, "level" + multiLevelID, "level" + GBL_levelID);    
                }
            } else{
                //alert("level" + multiLevelID, " - level" + GBL_levelID);
                Temp = replaceAll(multiTemplate, "level" + multiLevelID, "level" + GBL_levelID);
                //alert(Temp);
            }
            //} else{
                
            //}
            var divMulti=document.createElement('div');
            $(divMulti).html(template_divXML);
            if(sMultiAction=='innerhtml'){
                $(divMulti).find("[multiple='true']").find('.child').each(function(index){
                    //$(this).parent().append($(oDynamicItem).children('div:eq(' + index + ')'));
                    //$(this).parent().append($(oDynamicItem).find('div:nth-child(0)'));
                    $(this).parent().append($(divTempMulti).children().get(0));
                    
                });
            } else{
                $(divMulti).find('[multiple="true"]').parent().append(Temp);   //$(divMulti).find('[multiple="true"]').parent(0).parent(0).append(Temp);
            }
            template_divXML=$(divMulti).html();
            alert(template_divXML);
            //);*/
        }
        
        $(this).find('Group').each(function(index){
        
            GBL_GroupID= $(this).attr('ID');
            var GroupFeedbackDiv='';
            //GBL_levelID = index + 1;
            //alert( "GBL_levelID :"+  GBL_levelID + "\n\n multiLevelID :" + multiLevelID );
            

            GBL_fieldID = 1;        
            GBL_itemID = 1;
            var FieldNode;            
            $(this).children(0).each(function(){
                GBL_isOthers = false;
                FieldNode=$(this).children(0);
                var tagName= $(this).get(0).nodeName.toLowerCase();
                var isCorrect=false;
                switch (tagName){
                    case "question":
                        GBL_itemID = 1;  
                        break;
                        
                    case "answer":
                        GBL_itemID = 2;  
                        isCorrect = true; 
                        break;
                        
                    case "hint":
                        GBL_isOthers =true;
                        isCorrect = false;
                        break;
                        
                    case "distractor":
                        GBL_itemID = 2;   
                        isCorrect = false; 
                        break;
                                        
                    default:
                        //GBL_itemID = -1;
                        GBL_itemID = 1; 
                        FieldNode=$(this);
                        break;
                }
                
                if( (GBL_levelID == 1) && ($(contentNode).children(0).attr('Type')=='MultipleChoice')  &&  ( $(contentNode).children(0).attr('SubType')=='Type3') ){
                    answerList = String('');
                    return true;
                }
                if (GBL_itemID == 2 ){             
                    controlIndex += 1;
                    
                    if (answerList == ''){
                        answerList = String(controlIndex);
                    }
                    else{
                        answerList = answerList + GBLDelimiter1 + String(controlIndex);                    
                    }   
        
                    if ( isCorrect ){
                        answerList = answerList + GBLDelimiter + String(controlIndex) ;
                    }
                    else{
                        answerList = answerList + GBLDelimiter + String(-1);
                    }
                    //template_divXML = PreRender($(contentNode).children(0).attr('Type'),  $(contentNode).children(0).attr('SubType'), template_divXML,controlIndex);
                }
                FieldNode.each(function(index){
                    if(GBL_itemID == 2){
                        if($(this).attr('Src')!= undefined ){                
                            if (choiceList == ''){
                                choiceList = $(this).attr('Src');
                            }
                            else{
                                choiceList = choiceList + GBLDelimiter +$(this).attr('Src');
                                //userAnswerList = userAnswerList + GBLDelimiter + '';
                            }  
                        }
                        else{
                            if (choiceList == ''){
                                choiceList = getNodeText($(this));
                            }
                            else{
                                choiceList = choiceList + GBLDelimiter + getNodeText($(this));
                                //userAnswerList = userAnswerList + GBLDelimiter + '';
                            }  
                        }
                    }
                    //alert(GBL_levelID + ' : ' + GBL_itemID + ' : ' + GBL_fieldID);
                    if ( $(contentNode).children(0).attr('Type') != undefined ){
                        template_divXML = PreRender($(contentNode).children(0).attr('Type'),  $(contentNode).children(0).attr('SubType'), template_divXML,controlIndex);
                    }
                    parseField($(this),sPageContainer);                 
                    
                });              
                                
                switch (tagName){
                    case "question":
                    case "hint":
                        GBL_fieldID = 1;
                        break;
                }
            
            });
           
        }); //group end
        var sFeedbackHTML=RenderFeedback(formPageID() + '_' + GBL_levelID,this);
        var oTempCheckMyWork = document.createElement("div");
        $(oTempCheckMyWork).html(template_divXML);
        
        $(oTempCheckMyWork).find(".CheckMyWork:not([id])").attr('id','CheckMyWork_' + formPageID() + '_' + GBL_levelID).append(sFeedbackHTML);
        template_divXML = $(oTempCheckMyWork).html(); 
        
    }); //level end
    
    
    var sHTML=template_divXML;
    if($(contentNode).children(0).get(0).nodeName.toLowerCase()=='interaction'){
        var FeedbackDiv  = '';
        $(contentNode).find('ItemFooter').each(function(index){
            FeedbackDiv=RenderFeedback(formPageID(),this);
        });
        
        var validationDiv  = '<div id="divValidation_' + formPageID() + '" style="display:none;">';
            validationDiv += '  <div id="divValidation_InteractionType_' + formPageID() + '">' + $(contentNode).children(0).attr("Type") + '</div>';
            validationDiv += '  <div id="divValidation_InteractionSubType_' + formPageID() + '">' + $(contentNode).children(0).attr("SubType") + '</div>';
            validationDiv += '  <div id="divValidation_ChoiceList_' + formPageID() + '">' + choiceList + '</div>';
            validationDiv += '  <div id="divValidation_AnswerList_' + formPageID() + '">' + answerList + '</div>';
            validationDiv += '  <div id="divValidation_UserAnswerList_' + formPageID() + '">' + userAnswerList + '</div>';
            validationDiv += '  <div id="divValidation_MaxCount_' + formPageID() + '">' + $(contentNode).parent().attr("MaxAttempt") + '</div>';
            validationDiv += '  <div id="divValidation_Counter_' + formPageID() + '">' + 0 + '</div>';
            
            validationDiv += '</div>';
        sHTML = sHTML + formSubmitButton() + validationDiv + FeedbackDiv; 

    }
    //$("#divXML").html($("#divXML").html() + formContainerDiv(template_divXML + formSubmitButton() + validationDiv + FeedbackDiv) );  
    
    //For removing the Extra template data based on web/ipad
    var oTrimTemp = document.createElement("div");
    $(oTrimTemp).html(sHTML);
    if (Package.Device.isWeb){
       $(oTrimTemp).find("[device='iPad']").remove();
    };
    if (Package.Device.isTouch){
         $(oTrimTemp).find("[device='Web']").remove();
    }
    sHTML = $(oTrimTemp).html();
    
    //Loading the content into HTML page
    $("#" +sPageContainer).html(sHTML); 
   
    PostRender(formPageID());
    PostAction(formPageID());  
    
    
   
}

function ReprocessMarkup(objLevel,sHTML){
    var Temp;
    //if(!isDynamicDistractor){
    //alert($(this).children(0).get(0).nodeName.toLowerCase());
    var divMulti=document.createElement('div');
    $(divMulti).html(sHTML);
    
    var multiLevelID=$(divMulti).find("[multiple='true']").attr('startinglevel');
    if($(objLevel).children().children().length==1 && ($(objLevel).children().children().get(0).nodeName.toLowerCase()=='distractor')){
        if($(GBL_Template_Distractor).html().indexOf('level0')!=-1){
            Temp = replaceAll(GBL_Template_Distractor, "level0", "level" + GBL_levelID);
        } else{
            Temp = replaceAll(GBL_Template_Distractor, "level" + multiLevelID, "level" + GBL_levelID);    
        }
    } else{
        //alert("level" + multiLevelID, " - level" + GBL_levelID);
        Temp = replaceAll($(GBL_Template_Multi).outerHTML(), "level" + multiLevelID, "level" + GBL_levelID);
        //Temp = replaceAll($(GBL_Template_Multi).html(), "level" + multiLevelID, "level" + GBL_levelID);
        //alert(Temp);
    }
    //} else{
        
    //}
   
    if($(divMulti).find("[multiple='true']").attr('action')!=undefined){
        if($(divMulti).find("[multiple='true']").attr('action')=='innerhtml'){
            var divTemp=document.createElement('div');
            //alert(Temp);
            $(divTemp).html(Temp);
            $(divMulti).find("[multiple='true']").find('.child').each(function(index){
               
                //$(this).parent().append($(oDynamicItem).children('div:eq(' + index + ')'));
                //$(this).parent().append($(oDynamicItem).find('div:nth-child(0)'));
                $(this).parent().append($(divTemp).children().children().get(0));
                
            });
        }
    } else{
        $(divMulti).find('[multiple="true"]').parent().append(Temp);   //$(divMulti).find('[multiple="true"]').parent(0).parent(0).append(Temp);
    }
    return $(divMulti).html();    
    //alert(template_divXML);
}

function RenderFeedback(feedbackID,feedbackObj){
    var FeedbackDiv  = '<div id="divFeedback_' + feedbackID + '" class="hide">'; //class="hide" +feedbackID
    $(feedbackObj).find('FeedBack').children(0).each(function(){
        var tagName= $(this).get(0).nodeName.toLowerCase();            
        
        switch (tagName){
            case "correctresponse":
                FeedbackDiv += '<div id="correctresponse_' + feedbackID + '">' + getNodeText($(this)) + '</div>';
                break;
                
            case "incorrectresponse":
                FeedbackDiv += '<div id="incorrectresponse_' + feedbackID + '">' + getNodeText($(this)) + '</div>';
                break;
                
            case "onsubmitresponse":
                FeedbackDiv += '<div id="onsubmitresponse_' + feedbackID + '">' + getNodeText($(this)) + '</div>';
                break;         
        }
    });
    FeedbackDiv += '</div>';    
    
    return FeedbackDiv;
}

function formPageID(){
    return pg.attr("ID");
}

function formContainerDiv(divContents){
    var divContainer = '<div id="page_' + formPageID() + '" class="PageContainer">' + divContents + '</div>';    
    return divContainer;
}

function formSubmitButton(){
    var submitButton = '<input id="btnSubmit_' + formPageID() + '" disabled="true" type="button" value="Submit" onclick="javascript:validator(' + formPageID() + ');"></input>';
    return submitButton ;
}

function getNodeText(thisObj){
    return $.trim($('<div>').append( GetCDATAText($(thisObj)) ).text());
}

function PreRender(type, subType, sXML,iIndex){
var PageID=formPageID();   
var type = type.toLowerCase();
var subType = subType.toLowerCase();
        if(GBL_itemID == 2){
        switch ( type ){
        case "multiplechoice":
            switch ( subType ){
                case "type1":
                    var delimiter = GenerateDelimiter('content');
	                var txtContent = '<div class="validator">'  + '<div class="fl"><input type="radio" value="' + iIndex + '" name="MultiChoice_Type1" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
                    
                case "type2":
                    var delimiter = GenerateDelimiter('filename');
	                var txtContent = '<div class="validator">'  + '<div class="fl"><input type="radio" value="' + iIndex + '" name="MultiChoice_Type1" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
                    
                case "type3":   
                    if (GBL_levelID>1){
                        var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="fl" border="0" src="images/spacer.gif" />';
    
                        var delimiter = GenerateDelimiter('content');
	                    var txtContent = '<div class="validator">' + imgState + '<div class="fl"><input type="radio" value="' + iIndex + '" name="MultiChoice_Type3_' + GBL_levelID + '" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                        sXML = sXML.replace(delimiter,txtContent);
                    }
                    break;
                case "checkmywork":
                    var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="fl" border="0" src="images/spacer.gif" />';
    
                    var delimiter = GenerateDelimiter('content');
                    var txtContent = '<div class="validator">' + '<div class="fl hide"><input type="radio" value="' + iIndex + '" name="MultiChoice_CheckMyWork_' + GBL_GroupID + '" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
            }            
            break;
            
        case "multipleselect":
            switch ( subType ){
                case "type1":
                    var delimiter = GenerateDelimiter('content');
	                var txtContent = '<div class="validator">'  + '<div class="fl"><input type="checkbox" value="' + iIndex + '" name="MultipleSelect_Type1" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
                    
                case "type2":
                    //var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="fl" border="0" src="images/spacer.gif" />';
                    var delimiter = GenerateDelimiter('filename');
	                var txtContent = '<div class="validator">'  + '<div class="fl"><input type="checkbox" value="' + iIndex + '" name="MultipleSelect_Type1" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
                    
                case "checkmywork":
                    var delimiter = GenerateDelimiter('content');
	                var txtContent = '<div class="validator">'  + '<div class="fl hide"><input type="checkbox" value="' + iIndex + '" name="MultipleSelect_Type1" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
            }            
            break;
            
        case "trueorfalse":
            switch(subType){
                case "checkmywork":
//                    var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="fl" border="0" src="images/spacer.gif" />';
//    
//                    var delimiter = GenerateDelimiter('content');
//                    var txtContent = '<div class="validator"><div class="fl" name="'+ type + '_' + subType + '_' + GBL_levelID + '" onclick="setUserAnswer(' + PageID + ');" ></div><div class="fl">' + delimiter +'</div></div>';	        
//                    sXML = sXML.replace(delimiter,txtContent);
//                    break;
                    var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="fl" border="0" src="images/spacer.gif" />';
    
                    var delimiter = GenerateDelimiter('content');
                    var txtContent = '<div class="validator">'  + '<div class="fl hide"><input type="radio" value="' + iIndex + '" name="MultiChoice_TrueOrFalse_' + GBL_GroupID + '" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                    sXML = sXML.replace(delimiter,txtContent);
                    break;
            default:
                //var imgState = '<img id="imgState_' + PageID + "_" + iIndex + '" width="16px" height="16px" class="fl" border="0" src="images/spacer.gif" />';
                var delimiter = GenerateDelimiter('content');
                var txtContent = '<div class="validator">'  + '<div class="fl"><input type="radio" value="' + iIndex + '" name="MultiChoice_Type1" onclick="setUserAnswer(' + PageID + ');" /></div><div class="fl">' + delimiter +'</div></div>';	        
                sXML = sXML.replace(delimiter,txtContent);
                break;
           }
           
         default:
            /*var imgState = '<img id="imgState_' + formPageID() + '_' + iIndex + '" width="16px" height="16px" class="MoveTopLeft" border="0" src="images/spacer.gif" />';
            var divMulti=document.createElement('div');
            $(divMulti).html(template_divXML);
            
            $(divMulti).find('.Answer[id=""]').each(function(){
                $(this).attr('id','Answer_' + formPageID() + '_' + String(iIndex));
            });
            $(divMulti).find('.UserAnswer[id=""]').each(function(){
                $(this).attr('id','UserAnswer_' + formPageID() + '_' + String(iIndex));
                $(this).html(imgState + $(this).html());
            });
            sXML=$(divMulti).html();
            //$(template_divXML).find('.Answer').attr('id','Answer' + String(iIndex));*/
            break;
            
    }
    }
    return sXML;        
}



function PostRender(PageID){   
var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());

if (subType== "checkmywork"){
   IntializeSlider();
}

var CurrPage = "#page_"+PageID;
        switch (type){       
            
            case "jigsaw":
                var questionSrc = $(CurrPage).find(".JigsawImgBox img").attr("src");
                var newAnswerList='';
                $(CurrPage).find(".AbsoluteImg").each(function(index){
                    $(this).attr("src",questionSrc);
                    if (newAnswerList == ''){
                        newAnswerList = String(index);
                    }
                    else{
                        newAnswerList = newAnswerList + GBLDelimiter1 + String(index);                    
                    }
                    newAnswerList = newAnswerList + GBLDelimiter + String(index) ; 
                });    
                $('#divValidation_AnswerList_'+PageID).text(newAnswerList);   
                                                           
                break;
                
            case "fillintheblanks":
                var textbox = "<input type='text'  onclick='setUserAnswer(" + PageID + ");'  />";
                 switch ( subType ){                   
                    case "fillintheblanks":                        
                        $(CurrPage).html( replaceAll($(CurrPage).html(),"_____",textbox));
                        $(CurrPage).find('[multiple="true"]').each(function(){
                            $(this).attr("style","display: none");
                        });                        
                        break;
                    
                    case "checkmywork":                        
                        $("#page_"+PageID).find(".slider").find(".formatText").each(function(){
                            $(this).append(textbox);
                        });
                        
                        break;
                }
                break;

            case "wordscrumble":
                var sSpell, sSubmit;  
                var sSpell,sSubmit;

                sMissingSpellIndexs=$.trim($(CurrPage).find('.others').html());
                sSpell = $(CurrPage).find('.wordscrumbleword').html();   
                
                $(CurrPage).find('.wordscrumbleword').parent(0).parent(0).append("<div id='divAnswerArea'></div>")
                $(CurrPage).find("#divAnswerArea").append(renderClearDiv());
                
                var sMissingSpellIndexsArr = new Array();
                var spellArr = new Array(sSpell.length);
                
                sMissingSpellIndexsArr = sMissingSpellIndexs.split(",");                
                sMissingSpellIndexsArr.sort();                
                sMissingSpellIndexs = "," + sMissingSpellIndexs + ",";
                for (var i=0;i<sSpell.length;i++)
                {
                    spellArr[i]=sSpell.charAt(i).toUpperCase();
                    if (sMissingSpellIndexs.indexOf("," + i + ",")==-1){
                        if(sSpell.charAt(i)!= " "){
                            $(CurrPage).find("#divAnswerArea").append(renderDiv("Spell" + i ,"fl nonDropScrumble",sSpell.charAt(i).toUpperCase()));
                        }
                        else{
                            $(CurrPage).find("#divAnswerArea").append(renderDiv("Spell" + i ,"fl pd10"," "));
                        }
                    }
                    else{
                        $(CurrPage).find("#divAnswerArea").append(renderDiv("" ,"fl DropScrumble UserAnswer",""));
                    }
                }
                
                $(CurrPage).find('.wordscrumbleword').parent(0).parent(0).append("<div class='clr' style='height:30px;width:inherit;'>&nbsp;</div>");
                $(CurrPage).find('.wordscrumbleword').parent(0).parent(0).append(renderDiv("","DropScrumbleContainer",renderDiv("innerDivKeys","","")));
                
                var newAnswerList='';
                for (var i=0;i<sMissingSpellIndexsArr.length;i++){        
                    $(CurrPage).find("#innerDivKeys").append(renderDiv("","AnswerContainer",renderDiv("","fl ScrumbleDrag Answer",spellArr[sMissingSpellIndexsArr[i]])));
                    if (newAnswerList == ''){
                        newAnswerList = String(i);
                    }
                    else{
                        newAnswerList = newAnswerList + GBLDelimiter1 + String(i);                    
                    }
                    newAnswerList = newAnswerList + GBLDelimiter + String(i) ; 
                }
                $('#divValidation_AnswerList_'+PageID).text(newAnswerList);   
                break;            
                         
            case "memorygame":
                switch ( subType ){                   
                    case "zingo":
                    
                    $(CurrPage + " .ZingoAnsImgBox").each(function(index){
                        $(CurrPage).find(".ZingoQues").append(renderDiv("QusDiv" + index +"_" +PageID,"fl ZingoQuesImgBox","<img height='120px' width='135px' src='" + $(this).find("img").attr("src") + "'/>"));
                        $(this).attr("id","Zingo"+index + "_" +PageID);
                        $(this).children(0).attr("style","display:none");
                        $(this).children(0).attr("class","AnswerImage ZingoChoice Hidden");
                        $(this).append(renderDiv("","PatternImage ZingoChoice Displaying","<img title='cover' height='120px' width='135px' src='images/Pattern.jpg'/>"));
                    });
              
                    
                    break;
                }
                    
                break;       
            
            case "hangman":
                var keys = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
                var spell;
                var spellIndex;
                var spellWrongClick;
                
                spell=$.trim($(CurrPage).find('.wordscrumbleword').text());
                $(CurrPage).find('.wordscrumbleword').parent(0).prepend("<div id='inner1'></div>")

                var spellArr = new Array(spell.length);
                for (var i=0;i<spell.length;i++)
                {
                    if (spell.charAt(i)!=" "){
                        $(CurrPage).find("#inner1").append(renderDiv("Spell" + i ,"fl HangmanSpell",""));
                    }
                    else{
                        $(CurrPage).find("#inner1").append(renderDiv("Spell" + i ,"fl","&nbsp;&nbsp;&nbsp;&nbsp;"));
                    }
                    spellArr[i]=spell.charAt(i).toUpperCase();
                }
                $(CurrPage).find("#inner1").append(renderDiv("clickCounter","HideOnPreview","0"));
                
                
                $(CurrPage).find("#innerDivKeys div.HangmanKey").click(function () {
                    spellWrongClick=parseInt($(CurrPage).find("#clickCounter").text());
                    if (spellWrongClick<5) {
                        $(this).removeClass("HangmanKey")
                        $(this).addClass("HangmanKeyDisable");
                        
                        spellIndex = spell.toUpperCase().indexOf($.trim($(this).text()));
                        if(spellIndex != -1){
                            for (var i=0;i < spellArr.length;i++){                                
                                if(spellArr[i]==$.trim($(this).text())){
                                    $(CurrPage).find("#Spell" + i).text($(this).text());
                                }
                            }   
                            if ($(CurrPage).find("#inner1 div:empty").length==0){
                                $(CurrPage).find("#innerDivKeys div").unbind("click");
                                showFeedback(1,PageID);
                            }
                        }
                        else{
                                $(CurrPage).find("#HangmanImg" + spellWrongClick).hide();
                                spellWrongClick = spellWrongClick +1;
                                $(CurrPage).find("#clickCounter").html(spellWrongClick);
                                if (spellWrongClick==5){
                                    showFeedback(0,PageID);
                                }
                        }
                    }

                }); 
                
                $(CurrPage).find("#innerDivKeys").append(renderDiv("","fl mgTop25","<input type='button' id='btnShowHint_" + PageID + "' value='Show Hint' />"));
                $(CurrPage).find("#hintContentDiv").hide();
                $(CurrPage).find("#btnShowHint_" + PageID).click(function(){                
                    $(CurrPage).find("#hintContentDiv").show(200);
                    $(this).hide(100);                    
                });
                for (var i = 5; i >= 0  ; i--) {
                    $(CurrPage).find("#innerHangmanImg").append(renderDiv("HangmanImg" + i,"HangmanImg","<img src='images/hangman/" + subType + "_" + i + ".png' />")); 
                }
                
                $('#btnSubmit_' + PageID).hide();
                
                break;    
            
            default:
                IntializeRollover();
                IntializeFlashCard();                  
                break;
            
        }
        
        //var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="MoveTopLeft" border="0" src="images/spacer.gif" />';
                //var divMulti=document.createElement('div');
                //$(divMulti).html(template_divXML);
                
                $(CurrPage).find('.Answer[id=""]').each(function(iIndex){
                    $(this).attr('id','Answer_' + PageID + '_' + String(iIndex));
                });
                $(CurrPage).find('.UserAnswer[id=""]').each(function(iIndex){
                    $(this).attr('id','UserAnswer_' + PageID + '_' + String(iIndex));
                    //var imgState = '<img id="imgState_' + PageID + '_' + iIndex + '" width="16px" height="16px" class="MoveTopLeft" border="0" src="images/spacer.gif" />';
                    //$(this).html(imgState + $(this).html());
                });
                //sXML=$(divMulti).html();
                //$(template_divXML).find('.Answer').attr('id','Answer' + String(iIndex));
         
}



function PostAction(PageID){   
    //alert('PostAction');
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());
    var CurrPage = "#page_"+PageID;
    //alert(type);
    //alert(subType);
    var bEnableMultiDrag = false;
    $('#page_' + PageID).find(".CheckMyWork").unbind('click');
    $('#page_' + PageID).find(".CheckMyWork").click(function(){
       var aID=$(this).closest(".slider").find(".levelid").attr('id').split('_');
       var iLevelID=aID[aID.length - 1];
       var iPageID= aID[aID.length - 2];
       //if(validator(iPageID,iLevelID)){
       if(validator(iPageID,iLevelID)){
            //alert('inside validator');
            //showFeedback(1,$(this).attr("title")); 
            //SliderGo($(this),"next");      
       }
    });
    InitClickRevealExternal();
    InitClickRevealExternalType2();
    
        switch ( type ){  
            case "multiplechoice":
            case "multipleselect":
            case "trueorfalse":
                Randomize(PageID,'UserAnswer');
                /*if(subType=='checkmywork'){
                    
                    $('#page_' + PageID).find('.ChoiceBtn').unbind('click');
                    $('#page_' + PageID).find('.ChoiceBtn').bind('click',function(){                                           
                        if($(this).attr('class').indexOf('ChoiceBtnActive')==-1){
                            $(this).find('input').attr('checked',true);
                        } else{
                            $(this).find('input').attr('checked',false);
                        }
                        $('#page_' + PageID).find('input[checked=true]').closest('.ChoiceBtn').removeClass('ChoiceBtn').addClass('ChoiceBtnActive');
                        $('#page_' + PageID).find('input[checked!=true]').closest('.ChoiceBtnActive').removeClass('ChoiceBtnActive').addClass('ChoiceBtn');
                    });
                    $('#btnSubmit_' + PageID).hide();
                }*/
                if(subType=='checkmywork'){
                    //$('#page_' + PageID).find('video').attr('src',$('#page_' + PageID).find('video').attr('src'));
                    //var mediaID=$('#page_' + PageID).find('video').attr('id');
                   // alert(mediaID);
                    //document.getElementById(mediaID).src=$('#' + mediaID).attr('src');
                    //document.getElementById(mediaID).load();
                    //$('#page_' + PageID).find('video').unbind('ended');
//                    $('#page_' + PageID).find('video').bind('ended',function(){
//                        alert('ended');
//                        var PageID = $(this).closest('.PageContainer').attr('id').split('_')[1];
//                        CheckMyWork_SliderGo(PageID,'next');
//                    });
                    $('#page_' + PageID).find('.ChoiceBtn').unbind('click');
                    $('#page_' + PageID).find('.ChoiceBtn').bind('click',function(){    
                        $(this).closest('.slider').find('.CheckMyWorkDummy').hide();
                        $(this).closest('.slider').find('.CheckMyWork').show();                                       
                        if($(this).attr('class').indexOf('ChoiceBtnActive')==-1){
                            $(this).find('input').attr('checked',true);
                        } else{
                            $(this).find('input').attr('checked',false);
                        }
                        $('#page_' + PageID).find('input[checked=true]').closest('.ChoiceBtn').removeClass('ChoiceBtn').addClass('ChoiceBtnActive');
                        $('#page_' + PageID).find('input[checked=true]').closest('.Btn').removeClass('Btn').addClass('BtnActive');
                        $('#page_' + PageID).find('input[checked!=true]').closest('.ChoiceBtnActive').removeClass('ChoiceBtnActive').addClass('ChoiceBtn');
                        $('#page_' + PageID).find('input[checked!=true]').closest('.BtnActive').removeClass('BtnActive').addClass('Btn');
                    });
                    /*  $('#page_' + PageID).find('.restart').unbind('click');
                        $('#page_' + PageID).find('.restart').bind('click',function(){ 
                            CheckMyWork_SliderGotoFirst(PageID,this);
                        });
                    */
                    $('#btnSubmit_' + PageID).hide();
                    
                    
                    $("#page_"+PageID).find(".slider").find(".PreviousQuestion").unbind("click");
                    $("#page_"+PageID).find(".slider").find(".PreviousQuestion").click(function(){
                        CheckMyWork_SliderGo( PageID ,'previous');
                    });
                    
                } else if(subType=='voiceinteraction'){
                    //$(CurrPage).find(".QuestionSet:first").show();
                   $(".ChoiceBtn").click(function(){
                        $(this).find('input').attr('checked',true);
                        var aID=$(this).closest(".slider").find(".levelid").attr('id').split('_');
                        var iLevelID=aID[aID.length - 1];
                        var iPageID= aID[aID.length - 2];
                        validateAnswerMCVoiceInteraction(iPageID,iLevelID);
                        //$(CurrPage).find(".VoiceInteraction").find(".PopUp").show();    
                    });
                }
                break;
            case "dragdrop":
                if($(CurrPage).find(".Answer").length>0){ 
                    if ($(CurrPage).find(".CheckMyDragDropBox").length>0){ 
                        $(CurrPage).find(".CheckMyDropable").css("overflow","visible");
                        $(CurrPage).find(".CheckDragBoxPlaceholder2").css("width","250px").hide();
                        $(CurrPage).find(".CheckDragBox2").css("width","240px");
                        $(CurrPage).find(".CheckDragBoxPlaceholder2:first").show();
                        $(CurrPage).find(".CheckDragBox2").each(function(){
                            if($(this).attr("class").indexOf("hide")>-1){
                                $(this).remove();
                            }
                        });
                        $(CurrPage).find(".SubmitButton").unbind('click');
                        $(CurrPage).find(".SubmitButton").click(function(){
                           var aID=$(this).closest(".CheckMyDragDropBox").find(".levelid").attr('id').split('_');
                           var iLevelID=aID[aID.length - 1];
                           var iPageID= aID[aID.length - 2];
                           validator(iPageID,iLevelID);
                        });
                        $(CurrPage).find(".TryAgainButton").click(function(){
                            var aID=$(this).closest(".CheckMyDragDropBox").find(".levelid").attr('id').split('_');
                            var iLevelID=aID[aID.length - 1];
                            var iPageID= aID[aID.length - 2];
                            tryAgainDragDrop(iPageID);
                            $(CurrPage).find(".CheckMyDragDropBox").find(".Popup").hide();
                            $(CurrPage).find(".CheckDragBoxPlaceholder2").hide();
                            $(CurrPage).find(".CheckDragBoxPlaceholder2:first").show();
                            $(CurrPage).find(".SubmitButtonDisable").show();
                            $(CurrPage).find(".SubmitButton").hide();
                            $(CurrPage).find(".TryAgainButton").hide();
                        });
                        
                        $(CurrPage).find(".RestartButton").click(function(){
                            var aID=$(this).closest(".CheckMyDragDropBox").find(".levelid").attr('id').split('_');
                            var iLevelID=aID[aID.length - 1];
                            var iPageID= aID[aID.length - 2];
                            tryAgainDragDrop(iPageID);
                            resetValidationCounter(iPageID);
                            $(CurrPage).find(".LongFeedback").hide();
                            $(CurrPage).find(".CheckDragBoxPlaceholder2").hide();
                            $(CurrPage).find(".CheckDragBoxPlaceholder2:first").show();
                            $(CurrPage).find(".SubmitButtonDisable").show();
                            $(CurrPage).find(".SubmitButton").hide();
                            $(CurrPage).find(".TryAgainButton").hide();
                        });
                        
                        var iRow, iCol, sFeedbackCell;
                        iCol = 2;
                        iRow = ($(CurrPage).find(".Answer").index($(CurrPage).find(".Answer:last"))+1) / iCol;
                        $(CurrPage).find(".LongFeedback table tbody").html("");
                        for(var iCnt=1;iCnt<=iRow;iCnt++){
	                        sFeedbackCell =  '<tr>';
	                        for(var jCnt=1;jCnt<=iCol;jCnt++){
		                        sFeedbackCell += '<td>';
		                        sFeedbackCell += '</td>';
	                        }
	                        sFeedbackCell +=  '</tr>';
	                        $(CurrPage).find(".LongFeedback table tbody").append(sFeedbackCell);
                        }
                        $(CurrPage).find('.AnswerContainer').each(function(index){
                            $(this).find(".Answer").attr("title",index);
                            $(CurrPage).find(".LongFeedback table").find("td:eq(" + index + ")").html($(this).find(".Answer").html());
                        });
                        $(CurrPage).find(".CheckMyDropTable2:not([id])").find("th").each(function(index){
                            $(CurrPage).find(".LongFeedback table").find("th:eq(" + index + ")").html($(this).html());
                        });
                    } // CheckMyDragDropBox check end
                    if ($(CurrPage).find(".CheckMyDropTable").length>0){ 
                        $(CurrPage).find('.AnswerContainer').each(function(index){
                            $(this).find(".Answer").attr("title",index);
                            $(CurrPage).find("#Showfeedback").find("td:eq(" + index + ")").html($(this).find(".Answer").html());
                        });
                        $(CurrPage).find(".CheckMyDropTable:not([id])").find("th").each(function(index){
                            $(CurrPage).find("#Showfeedback").find("th:eq(" + index + ")").html($(this).html());
                        });
                    }// CheckMyDropTable check end
                    
                } // answer check end
                break;
            default:
                Randomize(PageID,'Answer');        
                break;
        }
        
        
        switch ( type ){       
            
            case "jigsaw":
                $(CurrPage).find(".JigsawImgMap").each(function(){
                    $(this).attr("title","0");
                    rotate($(this).attr("id"),false);
                });
                $(CurrPage).find(".JigsawImgMap").click(function() {
                    if ($(this).parent().attr("class").indexOf("JigsawImgContainer")!=-1) rotate($(this).attr("id"),true);
                }); 
                //dragByClass("JigsawImgMap");
                //dropByClass("JigsawDropContainer",'btnSubmit_' + PageID,false);  
                //dropByClass("JigsawImgContainer",'btnSubmit_' + PageID,false);  
                             
                break;

            case "wordscrumble":
                
                dragByClassScrumble('ScrumbleDrag',true);
	            dropByClassScrumble('DropScrumble','btnSubmit_' + PageID);
	            dropByClassScrumble('DropScrumbleContainer','btnSubmit_' + PageID,false);
                
                break;            
                       
            case "dragdrop":
                if (subType=="type2") bEnableMultiDrag=true;
                if(subType=='checkmywork'){
                    $('#btnSubmit_' + PageID).hide();
                }
                break;
            case "memorygame":
                switch (subType){                   
                    case "zingo":                
                    
                    $(CurrPage + " .ZingoAnsImgBox").click(function(){                        
                        var iSeletedIndex, iQuestionIndex, sQustionID, sSelectedID;                        
                        iQuestionIndex=-1;
                        iSeletedIndex=-1;
                        var sSelectedIDAry = $(this).attr("id").split("_");
                        sSelectedID = sSelectedIDAry[0];
                        iSeletedIndex = parseInt(sSelectedID.substring(5,sSelectedID.length));                        
                        var sQustionIDAry = $(CurrPage).find(".ZingoQues div:visible").attr("id").split("_");
                        sQustionID = sQustionIDAry[0];
                        iQuestionIndex = parseInt(sQustionID.substring(6,sQustionID.length));
                        
                        if (iQuestionIndex==iSeletedIndex){
                            Flip($(this),PageID,true,iQuestionIndex,subType);
                        }
                        else{
                            Flip($(this),PageID);
                        }
                                                                
                        $(CurrPage).find(".ZingoAnsContainer div:not('#" + $(this).attr("id")+"') div:.PatternImage").each(function(){
                            if ( ($(this).hasClass("Hidden"))  &&  ($(this).find("img").attr("title")!="completed")  ) {
                            Flip($(this).parent(),PageID);
                            }
                        });                        
                        
                     });
                                          
                    $(CurrPage).find(".ZingoQuesImgBox").hide();
                    var divClicktoStart=$(CurrPage).find(".ZingoQues .ZingoQuesImgBox:first-child");
                    divClicktoStart.css("cursor","pointer");    
                    SlideToCenter(divClicktoStart);
                    divClicktoStart.click(function(){
                        $(this).hide();
                        var FirstElement="";
                        $(CurrPage).find(".ZingoQues div").each(function(){
                            if ( (FirstElement=="") && ($(this).attr("id")!="")){
                                FirstElement=$(this);
                            }
                        });
                        SlideToCenter(FirstElement);                                                
                        //SlideToCenter($(CurrPage).find("#QusDiv0"+ "_" +PageID));                                                
                        $(CurrPage+ " div:.PatternImage").each(function(){                            
                            if ( ($(this).hasClass("Hidden"))  &&  ($(this).find("img").attr("title")!="completed")  ) Flip($(this).parent(),PageID);
                        });                        
                        $(CurrPage).find("#btnSubmit_" +PageID).attr("disabled",false);
                    });
                                     
                    break;
                }                    
                break; 
            
            case "text_tabtemplate":
            case "text_tabtemplate_text":
            case "text_verticaltabtemplate":
                $(CurrPage).find(".taborange").each(function(){
                    if ($.trim($(this).text()) != "" ){
                        $(this).show();
                    }                    
                });
                break;
             case "voiceinteraction":
                $(CurrPage).find(".QuestionSet:first").show();
                $(".ChoiceBtn").unbind("click");
                $(".ChoiceBtn").click(function(){
                    VoiceInteractionChoice_click($(this),CurrPage);
                    $(".ChoiceBtn").unbind("click");
                });

                $(CurrPage).find(".VoiceInteraction").find(".PopUp .close").click(function(){
                   $(this).closest(".PopUp").hide();
                    $(CurrPage).find(".VoiceInteraction").find(".NextFrame").click(function(){
                        VoiceInteractionNextFrame_Click(CurrPage);
                        $(CurrPage).find(".VoiceInteraction").find(".NextFrame").unbind("click");
                    });
                });
                break;
                
            
        }
            
        if(subType=='checkmywork'){
            $('#btnSubmit_' + PageID).hide();
        }
            
        if ($(CurrPage).find(".Drag").length>0) dragByClass('Drag',true,CurrPage);
        if ($(CurrPage).find(".Drop").length>0) dropByClass('Drop','btnSubmit_' + PageID,bEnableMultiDrag,CurrPage);
        if ($(CurrPage).find(".DropSwap").length>0) dropSwapByClassOthers('DropSwap','btnSubmit_' + PageID);
          
}

function VoiceInteractionChoice_click(obj,CurrPage){
    $(obj).find('input').attr('checked',true);
    var aID=$(obj).closest(".VoiceInteraction").find(".QuestionSet:visible").find(".levelid").attr('id').split('_');
    var iLevelID=aID[aID.length - 1];
    var iPageID= aID[aID.length - 2];
    validateAnswer1(iPageID,iLevelID);
    $(CurrPage).find(".VoiceInteraction").find(".PopUp").show(); 
}

function VoiceInteractionNextFrame_Click(CurrPage){
    var oNext;

    oNext=$(CurrPage).find(".QuestionSet:visible").next();
    $(".ChoiceBtn").click(function(){
        VoiceInteractionChoice_click($(this),CurrPage);
        $(".ChoiceBtn").unbind("click");
    });
    if($(oNext).html()!=null){
        $(CurrPage).find(".QuestionSet:visible").hide();
        if ($(oNext).find("audio").length>0)
            $(oNext).find("audio").get(0).play();
        $(oNext).show();
    }
    else{ // end of question
        SequenceFinish(CurrPage);
    }

}

function showFeedback(FeedbackStatus,PageID,LevelID){
    var sID= PageID;
    var msgTitle="";
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());
    var feedBackMedia='';
    var oFeedbackMedia;
    //Package.SetVisit(-1,PageID,"interaction");
    
    //alert(FeedbackStatus + "--subType:" + subType);
    
    if(LevelID!=undefined){    
        sID += '_' + LevelID 
    }

    //var divFeedbackText=$('#divFeedbackText_' + sFeedbackID);
    var buttonRow, buttonRow2="";
    var msg, msg2="";
    $("#page_"+PageID).find(".slider:visible").find('.CheckMyWorkDummy').show();   
    $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();
    if (FeedbackStatus==1){ //Correct
        //msg = $('#correctresponse_' + sID).text();        
        msg = $('#correctresponse_' + sID).find('.feedbacktext').html();
        feedBackMedia = $('#correctresponse_' + sID).find('.feedbackmedia').html();
        oFeedbackMedia= $('#correctresponse_' + sID).find('.feedbackmedia');
        msg2=msg;
        buttonRow='<input id="btnClose" type="button" value="&nbsp;&nbsp;OK&nbsp;&nbsp;" onclick="HideLightBox(\'' + GBLPopupDivID + '\'); CheckMyWork_SliderGo(' + PageID + ',\'next\'); return false;" />';
         if(subType.toLowerCase()=='checkmywork' || subType.toLowerCase()=='voiceinteraction'){
            //msgTitle ="Nice Job!";             
            msgTitle ="";             
            //msg2= parseCheckMyDropTable(PageID,sID);         
            msg2 = $('#correctresponse_' + sID).find('.feedbacktext').html();
            $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();
            $("#page_"+PageID).find(".slider:visible").find('.NextQuestionDummy').hide();
            $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").show();  
            
            $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").unbind("click");
            $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").click(function(){                
                //CheckMyWork_NextSlider(PageID);
                //return false;
                if($("#page_" + PageID).find('.feedbackContainer').length!=0){
                    HideInBuildFeedback(-1,PageID,0);
                 }   
                 var oNext = $("#page_"+PageID).find(".slider:visible").next();
                if ($(oNext).html()==null){
                    SequenceFinish("#page_"+PageID);
                }
                else{
                    CheckMyWork_NextSlider(PageID);
                }
                return false;
            });
            
            
        }
        //document.getElementById('media_correctresponse_' + sID).play(); 
        /*if($('#media_correctresponse_' + sID).length>0){      
            $('#media_correctresponse_' + sID).get(0).play();      
        }*/
        //Package.SetInteractionSuccess(PageID);
    }
    else if(FeedbackStatus==2){   
            //msg = "Take a look at the correct answers!";
            msg = "";
            buttonRow='<input id="btnClose" type="button" value="&nbsp;&nbsp;OK&nbsp;&nbsp;" onclick="HideLightBox(\'' + GBLPopupDivID + '\'); CheckMyWork_SliderGo(' + PageID + ',\'next\'); return false;" />';
                  
            //msgTitle = "Take a look at the correct answers!";
            msgTitle = "";
            msg2= parseCheckMyDropTable(PageID,sID);
            feedBackMedia = $('#onsubmitresponse_' + sID).find('.feedbackmedia').html();
            oFeedbackMedia = $('#onsubmitresponse_' + sID).find('.feedbackmedia');
            $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();
            $("#page_"+PageID).find(".slider:visible").find('.NextQuestionDummy').hide();
            $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").show();  
            
            //document.getElementById('media_submitresponse_' + sID).play();
            /*if($('#media_onsubmitresponse_' + sID).length>0){
                $('#media_onsubmitresponse_' + sID).get(0).play();
            }*/
    }
    else{       //Incorrect
        //msg = $('#incorrectresponse_' + sID).text();
        msg = $('#incorrectresponse_' + sID).find('.feedbacktext').html();
        feedBackMedia = $('#incorrectresponse_' + sID).find('.feedbackmedia').html();
        oFeedbackMedia = $('#incorrectresponse_' + sID).find('.feedbackmedia');
        msg2=msg;
        buttonRow='<input id="btnTryagain" type="button" value="&nbsp;Try Again&nbsp;" onclick="DefineTryAgain('+ PageID + '); return false;" />' +
                  '&nbsp;&nbsp;<input id="btnShowAnswer" type="button" value="&nbsp;Show Answer&nbsp;" onclick="DefineShowAnswer(' + PageID + ');  return false;" />';
       
            if(subType.toLowerCase()=='checkmywork'){
                msg2 = $('#incorrectresponse_' + sID).find('.feedbacktext').html();
                //msgTitle="Try Again!";
                msgTitle="";
                //buttonRow2 ='<div class="fr boldText mgTop10"><div class="orangeButton"  onclick="CheckMyWork_TryAgain('+ PageID + ','+ LevelID + '); return false;"><div class="orangeBtnLeft"><div class="orangeBtnRight"><span>Try Again</span></div></div></div></div><div class="clr"></div>';                
               /* buttonRow2 ='<div class="AlignCenterContent boldText mgTop10">';
                buttonRow2 +='   <div class="orangeButton"  onclick="CheckMyWork_TryAgain('+ PageID + ','+ LevelID + '); return false;">';
                buttonRow2 +='       <div class="orangeBtnLeft"><div class="orangeBtnRight"><span>Try Again</span></div></div>';
                buttonRow2 +='   </div>';
                buttonRow2 +='</div>';*/
                buttonRow2=''
                //document.getElementById('media_incorrectresponse_' + sID).play();
                $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();
                $("#page_"+PageID).find(".slider:visible").find('.NextQuestionDummy').hide();
                $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").show(); 
                $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").unbind("click");
                $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").click(function(){
                   //CheckMyWork_SliderGo(PageID,'next');
                    //return false;
                     var oNext = $("#page_"+PageID).find(".slider:visible").next();
                    if ($(oNext).html()==null){
                        SequenceFinish("#page_"+PageID);
                    }
                    else{
                        CheckMyWork_NextSlider(PageID);
                    }
                    return false;
                });
            }
            /*if($('#media_incorrectresponse_' + sID).length>0){
                $('#media_incorrectresponse_' + sID).get(0).play();
            }*/
    }
     
    if(type.toLowerCase()=="dragdrop" && subType.toLowerCase()=='checkmywork'){
        
        if($("#page_"+PageID).find(".CheckMyDropTable").length>0){ // feedback for templateid=145
            $("#page_"+PageID).find(".CheckMyDropTable").hide();
            $("#page_"+PageID).find(".CheckMyDropTable[id='Showfeedback']").show();
        }
        else if($("#page_"+PageID).find(".CheckMyDropTable2").length>0){ // feedback for templateid=149
            if (FeedbackStatus==2){
                $("#page_"+PageID).find(".LongFeedback").show();
            }
            else if(FeedbackStatus==1){
                $("#page_"+PageID).find(".CheckMyDragDropBox").find(".Popup").html(msg);
                $("#page_"+PageID).find(".CheckMyDragDropBox").find(".Popup").show();
            }
            else{
                $("#page_"+PageID).find(".CheckMyDragDropBox").find(".Popup").html(msg);
                $("#page_"+PageID).find(".CheckMyDragDropBox").find(".Popup").show();
                $("#page_"+PageID).find(".CheckMyDragDropBox").find(".TryAgainButton").show();
                $("#page_"+PageID).find(".CheckMyDragDropBox").find(".SubmitButton").hide();
            }   
        }
      
    }
    else if($("#page_" + PageID).find('.feedbackContainer').length!=0){
        var feedbackContent='';
        //feedbackContent=feedBackMedia + msg2;
        feedbackContent=msg2;
        oFeedbackMedia.find('audio').get(0).play();
        GBL_CurrMedia=oFeedbackMedia.find('audio').get(0);
        ShowInBuildFeedback(FeedbackStatus,feedbackContent,PageID,LevelID);
    }
    else if(subType.toLowerCase()=='checkmywork'){
     
         var DivContent ='<div class="interecationMSGBox">';
         DivContent +=' <div><div class="fl"><b>' + msgTitle +'</b></div><div class="fr"><img '
         
         //if (FeedbackStatus == 0){
            DivContent +='class="hide"'
         //}
         DivContent +=' src="Theme/template_image/ico_close.png" onclick="HideCheckMyWorkModalPopUp(\'' + PageID + '\'); return false;" /></div><div class="clr"></div><div><div>' + msg2 + '</div>' + buttonRow2+  '</div></div>';
         DivContent +='</div>';
                
        ShowCheckMyWorkModalPopUp(DivContent,PageID,250,300); 
    } else if (type=='voiceinteraction'){
        $("#page_"+PageID).find(".VoiceInteraction").find(".PopUp .popUpText").html(msg);
    }
    else{    
        var content = '';
        content = content + '<div style="width: 100%;">';
        content = content + '<table style="width:100%;height:70px;vertical-align:middle; ">'
        content = content + '<tr><td><center><b><br/>' + msg + '</b><br/><br/>'
        content = content + buttonRow;
        content = content + '</center><td></tr>';
        content = content + '</table>';
        content = content + '</div>';
        ShowModalPopUpHeigthWidth(content,"Feedback",100,430); 
        $("#ahrefClosedivLightBox").hide();
    }    
	
}

function showFeedback_old(FeedbackStatus,PageID,LevelID){
    var sID= PageID;
    var msgTitle="";
    var type=$('#divValidation_InteractionType_' + PageID).text().toLowerCase();
    var subType=$('#divValidation_InteractionSubType_' + PageID).text().toLowerCase();
    
    //Package.SetVisit(-1,PageID,"interaction");
    
    if(LevelID!=undefined){    
        sID += '_' + LevelID 
    }
    //var divFeedbackText=$('#divFeedbackText_' + sFeedbackID);
    var buttonRow, buttonRow2="";
    var msg, msg2="";
    
    if (FeedbackStatus==1){ //Correct
        msg = $('#correctresponse_' + sID).text();
        buttonRow='<input id="btnClose" type="button" value="&nbsp;&nbsp;OK&nbsp;&nbsp;" onclick="HideLightBox(\'' + GBLPopupDivID + '\'); CheckMyWork_SliderGo(' + PageID + ',\'next\'); return false;" />';
         if(subType.toLowerCase()=='checkmywork'){
            msgTitle ="Nice Job!";             
            msg2= parseCheckMyDropTable(PageID,sID);
            $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();            
        }       
        //Package.SetInteractionSuccess(PageID);
    }
    else if(FeedbackStatus==2){   
            msg = "Take a look at the correct answers!";
            buttonRow='<input id="btnClose" type="button" value="&nbsp;&nbsp;OK&nbsp;&nbsp;" onclick="HideLightBox(\'' + GBLPopupDivID + '\'); CheckMyWork_SliderGo(' + PageID + ',\'next\'); return false;" />';
                  
            msgTitle = "Take a look at the correct answers!";         
            msg2= parseCheckMyDropTable(PageID,sID);
            $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide(); 
            
                   
    }
    else{       //Incorrect
        msg = $('#incorrectresponse_' + sID).text();

        buttonRow='<input id="btnTryagain" type="button" value="&nbsp;Try Again&nbsp;" onclick="DefineTryAgain('+ PageID + '); return false;" />' +
                  '&nbsp;&nbsp;<input id="btnShowAnswer" type="button" value="&nbsp;Show Answer&nbsp;" onclick="DefineShowAnswer(' + PageID + ');  return false;" />';
       
            if(subType.toLowerCase()=='checkmywork'){
                msg2 = $('#incorrectresponse_' + sID).text();
                msgTitle="Try Again!";
                buttonRow2 ='<div class="fr boldText mgTop10"><div class="orangeButton"  onclick="CheckMyWork_TryAgain('+ PageID + ','+ LevelID + '); return false;"><div class="orangeBtnLeft"><div class="orangeBtnRight"><span>Try Again</span></div></div></div></div><div class="clr"></div>';
                
            }
            
        
    }
    
    if(subType.toLowerCase()=='checkmywork'){
     
     var DivContent ='<div class="interecationMSGBox">';
     /*DivContent +=' <div class="intTop">';
     DivContent +='   <div class="intTopLeft"></div>';
     DivContent +='   <div class="intTopRight"></div>';
     DivContent +=' </div>';*/
     DivContent +=' <div class="intContent"><div class="fl hide"><b>' + msgTitle +'</b></div><div class="fr"><img '
     
     if (FeedbackStatus == 0){
        DivContent +='class="hide"'
     }
     DivContent +=' src="theme/template_image/ico_close.png" onclick="HideCheckMyWorkModalPopUp(\'' + PageID + '\'); return false;" /></div><div class="clr"></div><div>' + msg2 + buttonRow2+  '</div></div>';
     DivContent +=' <div class="intTop">';
     DivContent +='   <div class="intTopLeft"></div>';
     DivContent +='   <div class="intTopRight"></div>';
     DivContent +=' </div>';
     DivContent +='</div>';
     
     
     /*
        var DivContent ='	<div class="popup2Outer">';	
        DivContent +='	  <div class="popup2InnerHeading">	';
        DivContent +='	    <div class="fl"><strong>' + msgTitle + '</strong></div>	';
        DivContent +='	    <div class="fr"><img '
        
        if (FeedbackStatus == 0){
            DivContent +='class="hide"'
        }
        
        DivContent +=' src="theme/template_image/ico_close.png" onclick="HideLightBox(\'divLightBox\'); return false;" /></div>	';
        DivContent +='	    </div>	';
        DivContent +='	  <div class="popup2InnerContent">	';
        DivContent += msg2;
        DivContent += buttonRow2;
        DivContent +='	  </div>	';
        DivContent +='	</div>	';
       */
        
        ShowCheckMyWorkModalPopUp(DivContent,PageID,150,400); 
        //ShowDefaultModalPopUp(DivContent,400,600); 
    }
    else{    
        var content = '';
        content = content + '<div style="width: 100%;">';
        content = content + '<table style="width:100%;height:70px;vertical-align:middle; ">'
        content = content + '<tr><td><center><b><br/>' + msg + '</b><br/><br/>'
        content = content + buttonRow;
        content = content + '</center><td></tr>';
        content = content + '</table>';
        content = content + '</div>';

        ShowModalPopUpHeigthWidth(content,"Feedback",100,300); 
        $("#ahrefClosedivLightBox").hide();
    }    
	
}

function DefineTryAgain(PageID,LevelID){
    var InteractionType=$.trim($("#divValidation_InteractionType_" + PageID).text().toLowerCase());
    var InteractionSubType=$.trim($("#divValidation_InteractionSubType_" + PageID).text().toLowerCase());
    
    ClearInteraction(PageID);
    
    switch ( InteractionType ){
        case "hangman":
            tryAgain1(PageID);
            break;  
                  
        case "memorygame":
             switch ( InteractionSubType ){
                case "zingo": 
                    tryAgain3(PageID);
                    break;
            }       
            break;  
        
        case "dragdrop":
            switch ( InteractionSubType ){
                case "checkmywork": 
                    if ($('#divValidation_Counter_'+PageID)!= null){
                        var counter= parseInt($('#divValidation_Counter_'+PageID).text());
                        var maxCounter= parseInt($('#divValidation_MaxCount_'+PageID).text());
                        if ( counter == maxCounter ){
                            $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();
                            $("#page_"+PageID).find(".slider:visible").find(".ShowAnswer").show();
                            $("#page_"+PageID).find(".slider:visible").find(".ShowAnswer").click(function(){
                                $(this).hide();
                                $("#page_"+PageID).find(".slider:visible").find(".Answer").removeClass("opacity30");
                                showAnswerDefault(PageID);    
                                showFeedback(2,PageID,LevelID);
                            });                    
                            return;
                        }
                    }
                    break;
            }       
            break;  
            
            
        default:
            tryAgainDefault(PageID);
            break;
    }  
}

function tryAgainDefault(PageID){
    //alert('tryAgainDefault');
    var Type=$.trim($("#divValidation_InteractionType_" + PageID).text().toLowerCase());
    var SubType=$.trim($("#divValidation_InteractionSubType_" + PageID).text().toLowerCase());
    var CurrPage = '#page_'+PageID;

    if(Type=="dragdrop"){
        $(CurrPage).find('.AnswerContainer').each(function(index){
            $(this).html($(CurrPage).find(".Answer[title='"+index+"']"));
        });
         $(CurrPage).find(".CheckMyDropTable").show();
         $(CurrPage).find(".CheckMyDropTable[id='Showfeedback']").hide();
    }
    else{
         $(CurrPage).find('.AnswerContainer').each(function(index){
            $(this).append($('#Answer_'+PageID+'_'+index));
            $(this).find('.ChoiceBtnActive').removeClass('ChoiceBtnActive').addClass('ChoiceBtn');
        });
    }        
    $(CurrPage).find('#divAnswerArea .ScrumbleDrag ').each(function(index){
            $(CurrPage).find('.DropScrumbleContainer').append($(this));
    });
    
    $(CurrPage).find('input[type="radio"], input[type="checkbox"]').each(function(){
        $(this).attr('checked',false);
    });
    
    $(CurrPage).find('input[type="text"]').each(function(){
        $(this).val('');
    });
    resetStateImages(PageID);
    Randomize(PageID,'Answer');
    //PostAction(PageID);
}

function tryAgainDragDrop(PageID){
    //alert('tryAgainDefault');
    var Type=$.trim($("#divValidation_InteractionType_" + PageID).text().toLowerCase());
    var SubType=$.trim($("#divValidation_InteractionSubType_" + PageID).text().toLowerCase());
    var CurrPage = '#page_'+PageID;

   
    $(CurrPage).find('.AnswerContainer').each(function(index){
        $(this).html($(CurrPage).find(".Answer[title='"+index+"']"));
    });
     dragByClass('Answer',true,CurrPage);
}

function tryAgain1(PageID){
    var CurrPage = '#page_'+PageID;
    
    $(CurrPage).find('.HangmanSpell').each(function(index){
            $(this).html('');
    });    
    
    $(CurrPage).find('.HangmanKeyDisable').each(function(index){
            $(this).removeClass('HangmanKeyDisable');
            $(this).addClass('HangmanKey');
    });
    
    $(CurrPage).find("#clickCounter").html('0');
                                
    PostAction(PageID);
}

function tryAgain3(PageID){
    var CurrPage = '#page_'+PageID; 
    $(CurrPage).find(".ZingoQues .ZingoQuesImgBox").hide();
    var divClicktoStart=$(CurrPage).find(".ZingoQues .ZingoQuesImgBox:first-child");                    
    divClicktoStart.show();
    SlideToCenter(divClicktoStart);
    
    $(CurrPage+ " div:.AnswerImage").each(function(){                            
            $(this).find("img").removeAttr("title");        
            $(this).find("img").show();
    });
    
    $(CurrPage+ " div:.PatternImage").each(function(){                            
        if ($(this).hasClass("Hidden")) {
            Flip($(this).parent(),PageID);
            $(this).find("img").attr("title","cover");
        }
    });
  
}

function DefineShowAnswer(PageID){
    var InteractionType=$.trim($("#divValidation_InteractionType_" + PageID).text().toLowerCase());
    var InteractionSubType=$.trim($("#divValidation_InteractionSubType_" + PageID).text().toLowerCase());
    
    ClearInteraction(PageID);
    
    switch (InteractionType){
        case "multiplechoice":
                switch ( InteractionSubType ){
                    case "type1":
                    case "type2":
                    case "checkmywork":
                        showAnswer4(PageID);
                        break;
                    case "type3":
                        showAnswerDefault(PageID);
                        break
                }            
                break;
                
        case "multipleselect":
                switch ( InteractionSubType ){
                    case "type1":
                    case "type2":
                        showAnswer4(PageID);
                        break;
                }            
                break;
            
        case "trueorfalse":
            showAnswer4(PageID);
            break;
            
        case "fillintheblanks":
            switch ( InteractionSubType ){
                    case "checkmywork":
                        showAnswer5(PageID);
                        break;
                    case "fillintheblanks":
                        showAnswer5(PageID);
                        break
                }      
            break;
                
        case "hangman":
            showAnswer1(PageID);
            break;
            
        case "jigsaw":
            showAnswer2(PageID);
            break;
                          
        case "memorygame":
             switch ( InteractionSubType ){
                case "zingo": 
                    showAnswer3(PageID);
                    break;
            }       
            break;  
            
        default:
            showAnswerDefault(PageID);
            break;
    }  
    
}

function showAnswerDefault(PageID){   
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    for (i=0;i<LevelcorrectAnsAry.length ;i++ ){
        var levelAnswer= LevelcorrectAnsAry[i].split(GBLDelimiter);
         
        $('#page_' + PageID).find("input[name^='MultiChoice_Type3_'][value='"+levelAnswer[0]+"']").each(function(index){   
            if(levelAnswer[1]!=-1){
                $('#imgState_'+PageID+'_'+levelAnswer[0]).attr("src","images/circle-tick.png");
                $(this).attr('checked',true);
            }
         });
         
        
        
    }
     var answer=$('#page_'+PageID).find('.Answer:visible');
     //alert(answer);
     $('#page_'+PageID).find('.AnswerContainer:visible').each(function(index){
            //alert(index);
            $(this).append(answer[index]);
     });         
     $('#page_'+PageID).find('.UserAnswer:visible').each(function(index){
        var userAnswerID=$(this).attr('id');
        var answerID=userAnswerID.replace('User','');
        
        $('#'+userAnswerID).append($('#'+answerID));
     });      
}


function showAnswer1(PageID){  
    var CurrPage = '#page_'+PageID; 
    var correctAnswer = $.trim($(CurrPage).find("#divValidation_ChoiceList_"+PageID).text());
    var LevelcorrectAnsAry=new Array(correctAnswer.length);

    for (i=0;i<correctAnswer.length ;i++ ){
        LevelcorrectAnsAry[i]= correctAnswer.substr(i,1);
        //alert(LevelcorrectAnsAry[i] +"\n\n" +correctAnswer.substr(i,1));
    }
    for (i=0;i<correctAnswer.length ;i++ ){
        $(CurrPage).find("#Spell"+i).html(LevelcorrectAnsAry[i]);
        //alert(LevelcorrectAnsAry[i]);
    }      
}

function showAnswer2(PageID){   
    //alert('ShowAnswer2');
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    for (i=0;i<LevelcorrectAnsAry.length ;i++ ){
         var levelAnswer= LevelcorrectAnsAry[i].split(GBLDelimiter);
         //alert($('#page_'+PageID).find('.UserAnswer').length); 
         var userAnswerID='UserAnswer_' + PageID + '_' + levelAnswer[0];
         var answerID='Answer_' + PageID + '_' + levelAnswer[0];
         $('#'+userAnswerID).append($('#'+answerID));
         rotateToZeroDegree(answerID);
         
         /*$('#page_'+PageID).find('.UserAnswer').each(function(index){
            var userAnswerID=$(this).attr('id');
            var answerID=userAnswerID.replace('User','');
            $('#'+userAnswerID).append($('#'+answerID));
            rotateToZeroDegree(answerID);
            
         });*/
        
    }      
}
  
function showAnswer3(PageID){  
    var CurrPage = '#page_'+PageID; 
    $(CurrPage).find(".ZingoQues .ZingoQuesImgBox").hide();
    var divClicktoStart=$(CurrPage).find(".ZingoQues .ZingoQuesImgBox:first-child");                    
    divClicktoStart.show();
    SlideToCenter(divClicktoStart);
    
    $(CurrPage).find(".ZingoAnsContainer div:.AnswerImage").each(function(){                            
        if ($(this).hasClass("Hidden")) Flip($(this).parent(),PageID);
    });
}

function showAnswer4(PageID){
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());
    
    if (subType.toLowerCase()=='checkmywork'){
	    GBLShowValidationStatusImg =1;
    }
    $('#page_' + PageID).find('.UserAnswer:visible').each(function(index){
            var correctAnswer= -1;
            var userAnswer = -1;
            var InteractionCtrl = $(this).find(".ChoiceBtnActive");       
            var aryIndex=parseInt($(this).attr('id').replace('UserAnswer_'+PageID+'_',''));        
            correctAnswer= LevelcorrectAnsAry[aryIndex].split(GBLDelimiter)[1];  
            var imgID = '#imgState_'+ PageID + '_' + $(this).val(); 
            $(imgID).hide();
            if(correctAnswer == aryIndex){
                //alert(GBLShowValidationStatusImg);
                if (GBLShowValidationStatusImg == 1 ){
	                $(imgID).attr('src','../../../genimage/theme/template_image/circle-tick.png');
	                
	                $(this).find("input:radio").attr('checked',true);
	                $(this).find("input:checkbox").attr('checked',true);
                }
                else{
                    //$(this).find('.controlParent').addClass('ChoiceBtnActive').removeClass('ChoiceBtn');  /* Highlight not in new style */
                    //$(this).find("input:radio").attr('checked',true);  /* Highlight not in new style */
                }
            } else{
                if (GBLShowValidationStatusImg == 1 ){
	                $(imgID).attr('src','../../../genimage/theme/template_image/circle-cross.png');
	                $(this).find("input:radio").attr('checked',false); 
	                $(this).find("input:checkbox").attr('checked',false);
                }
                else{
                    //$(this).find('.controlParent').removeClass('ChoiceBtnActive').addClass('ChoiceBtn');   /* Highlight not in new style */
                    //$(this).find("input:radio").attr('checked',false);     /* Highlight not in new style */
                }
            }            
        });     
      
}


function showAnswer5(PageID){
    var LevelcorrectAnsAry = $("#divValidation_ChoiceList_"+PageID).html().split(GBLDelimiter);
   
    $('#page_' + PageID).find('input[type="text"]').each(function(index){        
        if ($(this).is(':visible')){
            var InteractionCtrl = $(this);
            InteractionCtrl.attr("value", LevelcorrectAnsAry[index]);
        }         
    });
    
}

function validator(PageID,LevelID){
    resetStateImages();
    //alert($("#divValidation_InteractionType_" + PageID).text() +" \n\n " + $("#divValidation_InteractionSubType_" + PageID).text());
    var InteractionType=$.trim($("#divValidation_InteractionType_" + PageID).text().toLowerCase());
    var InteractionSubType=$.trim($("#divValidation_InteractionSubType_" + PageID).text().toLowerCase());
    
    switch (InteractionType){
        case "multiplechoice":
            switch ( InteractionSubType ){
                case "type1": 
                case "type2":
                case "checkmywork":
                    validateAnswer1(PageID,LevelID); 
                    break;   
                    
                case "type3":
                    validateAnswer2(PageID,LevelID);                
                    break; 
                default:
                    validateAnswerDefault(PageID,LevelID);                   
                    
            }            
            break;
            
        case "multipleselect":
            switch ( InteractionSubType ){
                case "type1":
                case "type2":
                    validateAnswer1(PageID,LevelID);                 
                    break;
                    
                case "checkmywork":
                    validateAnswer1(PageID,LevelID);                 
                    break;
                
            }            
            break;
            
        case "trueorfalse":
             switch ( InteractionSubType ){
                case "checkmywork": 
                    validateAnswer1(PageID,LevelID);
                    break;   
                    
                default:
                    validateAnswer1(PageID,LevelID);                   
                    
            }            
            
            break;
        
        case "fillintheblanks":
             switch ( InteractionSubType ){
                case "checkmywork": 
                    validateAnswer3(PageID,LevelID);
                    break;   
                    
                default:
                    validateAnswer3(PageID,LevelID);                   
                    
            }            
            
            break;
            
        case "wordscrumble":
            validateAnswer4(PageID,LevelID);
            break;
        
        case "memorygame":
             switch ( InteractionSubType ){
                case "zingo": 
                    validateAnswer5(PageID,LevelID);
                    break;
            }       
            break;
            
        default:
            validateAnswerDefault(PageID,LevelID);
    }
    
    $("#btnSubmit_" + PageID).attr("disabled",true);
    
}

function parseCheckMyDropTable(PageID,sID){
/*Show hide Next question button */
    $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").unbind("click");
    $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").click(function(){
        
        var oNext = $("#page_"+PageID).find(".slider:visible").next();
        if ($(oNext).html()==null){
            SequenceFinish("#page_"+PageID);
        }
        else{
            CheckMyWork_NextSlider(PageID);
        }
        return false;
    });
        
    var oSliderParent = $('#page_' + PageID).find(".sliderParent");
    iMaxSlider = $(oSliderParent).find(".slider").length;
    iCurIndex = $(oSliderParent).find(".slider").index($(oSliderParent).find(".slider:visible"));

    if (iCurIndex <(iMaxSlider-1)){
        $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").show();                                   
    }   
    else{        
        /*if ($("#page_"+PageID).find(".slider:visible").find(".restart").length > 0){
            $("#page_"+PageID).find(".slider:visible").find(".restart").show();
        }
        else{
            var restartBtn ='<div class="restart orangeButton"  onclick="CheckMyWork_SliderGotoFirst('+ PageID + ',this); return false;"><div class="orangeBtnLeft"><div class="orangeBtnRight"><span>Restart</span></div></div></div>';
            $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").parent(0).append(restartBtn);
        }
        $("#page_"+PageID).find(".slider:visible").find('.CheckMyWorkDummy').hide();   
        $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").hide();*/
    }


/* Form the content for the popup */

//var msg2= "<table><tr><td>"+ $("#page_"+PageID).find(".slider:visible").find(".CheckMyDropTable").parent(0).html() +"</td><td style='width:300px;'>" + $('#correctresponse_' + sID).text() + "</td></tr></table>";        
//var msg2 = $('#correctresponse_' + sID).text();        
//var msg2 = $('#onsubmitresponse_' + sID).html();
var msg2 = $('#onsubmitresponse_' + sID).find('.feedbacktext').html();

    var divMsg2=document.createElement('div');
    $(divMsg2).html(msg2);
    $(divMsg2).find("img").remove();   
    $(divMsg2).find("div").removeClass('CheckDragBox');
    $(divMsg2).find('.CheckMyDropTable').addClass('CheckMyDropTable1');
    $(divMsg2).find("table").removeClass('CheckMyDropTable');
    return $(divMsg2).html();
    
}

function parseCheckMyDropTable_old(PageID,sID){
/*Show hide Next question button */

    $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").unbind("click");
    $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").click(function(){
        CheckMyWork_NextSlider(PageID); 
        return false;
    });
        
    var oSliderParent = $('#page_' + PageID).find(".sliderParent");
    iMaxSlider = $(oSliderParent).find(".slider").length;
    iCurIndex = $(oSliderParent).find(".slider").index($(oSliderParent).find(".slider:visible"));

    if (iCurIndex <(iMaxSlider-1)){
        $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").show();                                   
    }   
    else{        
        if ($("#page_"+PageID).find(".slider:visible").find(".restart").length > 0){
            $("#page_"+PageID).find(".slider:visible").find(".restart").show();
        }
        else{
            var restartBtn ='<div class="restart orangeButton"  onclick="CheckMyWork_SliderGotoFirst('+ PageID + ',this); return false;"><div class="orangeBtnLeft"><div class="orangeBtnRight"><span>Restart</span></div></div></div>';
            $("#page_"+PageID).find(".slider:visible").find(".NextQuestion").parent(0).append(restartBtn);
        }
    }


/* Form the content for the popup */

//var msg2= "<table><tr><td>"+ $("#page_"+PageID).find(".slider:visible").find(".CheckMyDropTable").parent(0).html() +"</td><td style='width:300px;'>" + $('#correctresponse_' + sID).text() + "</td></tr></table>";        
//var msg2 = $('#correctresponse_' + sID).text();        
var msg2 = $('#onsubmitresponse_' + sID).text();

    var divMsg2=document.createElement('div');
    $(divMsg2).html(msg2);
    $(divMsg2).find("img").remove();   
    $(divMsg2).find("div").removeClass('CheckDragBox');
    $(divMsg2).find('.CheckMyDropTable').addClass('CheckMyDropTable1');
    $(divMsg2).find("table").removeClass('CheckMyDropTable');
    return $(divMsg2).html();
    
}

function incrementValidationCounter(PageID){
    if ($('#divValidation_Counter_'+PageID)!= null){
        var counter=0;        
        counter = parseInt($('#divValidation_Counter_'+PageID).text());   
        if ( isNaN(counter) ){ 
            counter=0;
        }
        counter = counter+1;
        $('#divValidation_Counter_'+PageID).text(counter)        
    }
}

function resetValidationCounter(PageID){
    if ($('#divValidation_Counter_'+PageID)!= null){   
        $('#divValidation_Counter_'+PageID).text(0)        
    }
    Randomize(PageID,'Answer');   
    if ($("#page_"+PageID).find(".Drag").length>0) dragByClass('Drag',true); 
}

function CheckMyWork_NextSlider(PageID){
    CheckMyWork_SliderGo( PageID ,'next');
    resetValidationCounter(PageID);
}

function CheckMyWork_TryAgain(PageID,LevelID){
    DefineTryAgain(PageID,LevelID);    
    $("#page_"+PageID).find(".slider:visible").find(".Answer").removeClass("opacity30");
}

function validateAnswerDefault(PageID,LevelID){             
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    var isCorrect=true;
    var imgID='';
    var imgSrc='images/circle-tick.png';
    var iControlIndex=-1;
    var corrAns,userAns;
    
    //alert($('#page_' + PageID).find('.UserAnswer').length);
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());
    
    var counter= parseInt($('#divValidation_Counter_'+PageID).text());
    var maxCounter= parseInt($('#divValidation_MaxCount_'+PageID).text());
    
    if (subType.toLowerCase()=='checkmywork'){
        GBLShowValidationStatusImg =0;
    }
    
    incrementValidationCounter(PageID);
    $('#page_' + PageID).find('.UserAnswer:visible').each(function(index){
        var correctAnswer;
        //iControlIndex += 1;
        iControlIndex = parseInt($(this).attr('id').replace('UserAnswer_' + PageID + '_',''));
        imgID = '#imgState_'+ PageID + '_' + iControlIndex;
        var userAnswer = -1;
                
        if (type=="dragdrop"){
            if(LevelcorrectAnsAry.length<iControlIndex +1){
                correctAnswer = iControlIndex
            }
            else{
                correctAnswer= LevelcorrectAnsAry[iControlIndex].split(GBLDelimiter);
            }
            if($(this).find('.Answer').length != 0)
                userAnswer = $(this).find('.Answer').attr('id').replace('Answer_' + PageID + '_','');
            else
                userAnswer=iControlIndex;
        }
        else{
            correctAnswer= LevelcorrectAnsAry[iControlIndex].split(GBLDelimiter);
            if($(this).find('.Answer').length != 0)
                userAnswer = $(this).find('.Answer').attr('id').replace('Answer_' + PageID + '_','');
        }
        
        if(type=='jigsaw' && $(this).find('.Answer').attr('title')!="0"){
            userAnswer=-1;
        }
        if(subType.toLowerCase()=='checkmywork'){ 
            corrAns=(parseInt(correctAnswer[1])%2);
            userAns=(parseInt(userAnswer)%2);
        } else{
            corrAns=correctAnswer[1];
            userAns=userAnswer;
        }
        if(corrAns==userAns){
            if (GBLShowValidationStatusImg == 1 ){
                $(imgID).attr('src','images/circle-tick.png');
            }
            else{
                $(this).find(".Answer").removeClass('opacity30');
            }
        } 
        else{
            if (GBLShowValidationStatusImg == 1 ){
                $(imgID).attr('src','images/circle-cross.png');
            }
            else{
                $(this).find(".Answer").addClass('opacity30');
            }
            isCorrect=false;
        }
    });
    
    if (isCorrect){showFeedback(1,PageID,LevelID);}     
    else{
            if ( counter == maxCounter ){
                showFeedback(2,PageID,LevelID);    
                return;                
            }
            else{ showFeedback(0,PageID,LevelID);} 
        }
        
         	 
}



function validateAnswer1(PageID,LevelID){
    //alert('validateAnswer1');
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());    
    var isCorrect=true;
    var imgID='';
    if (subType.toLowerCase()=='checkmywork'){
	    GBLShowValidationStatusImg =1;
        incrementValidationCounter(PageID);
    }
    if ($('#page_' + PageID).find('.UserAnswer:visible').find("input[checked=true]").length ==0 ) isCorrect=false;
    
      
    var counter= parseInt($('#divValidation_Counter_'+PageID).text());
    var maxCounter= parseInt($('#divValidation_MaxCount_'+PageID).text());

        $('#page_' + PageID).find('.UserAnswer:visible').each(function(index){
            var correctAnswer= -1;
            var userAnswer = -1;
                  
            var aryIndex=parseInt($(this).attr('id').replace('UserAnswer_'+PageID+'_',''));        
            correctAnswer= LevelcorrectAnsAry[aryIndex].split(GBLDelimiter)[1];         
            
            var InteractionCtrl = $(this).find("input:radio");  // for Multichoice type1 , type 2
            if(InteractionCtrl.length == 0) InteractionCtrl = $(this).find("input:checkbox"); // for Multiselect type1 , type 2
            if (InteractionCtrl.attr("checked")) {
                userAnswer = InteractionCtrl.attr("value");
                imgID = '#imgState_'+ PageID + '_' + InteractionCtrl.attr("value");                
                //alert(correctAnswer + ' - ' + userAnswer + ";" + imgID + ":" + GBLShowValidationStatusImg);
                if(correctAnswer == userAnswer){
                    if (GBLShowValidationStatusImg == 1 ){
                        $(imgID).attr('src','../../../genimage/theme/template_image/correct.png');
                    }
                } else{
                    if (GBLShowValidationStatusImg == 1 ){
                        $(imgID).attr('src','../../../genimage/theme/template_image/incorrect.png');
                    }
                    isCorrect=false;
                }
            }
            else{
                if (correctAnswer !=-1){
                    isCorrect=false;
                }            
            }     
                 
        });
        if (isCorrect)
            {showFeedback(1,PageID,LevelID);}     
        else
            {
             $('#page_' + PageID).find('.UserAnswer:visible').each(function(index){
                    correctAnswer= -1;
                    userAnswer = -1;
                          
                    aryIndex=parseInt($(this).attr('id').replace('UserAnswer_'+PageID+'_',''));        
                    correctAnswer= LevelcorrectAnsAry[aryIndex].split(GBLDelimiter)[1];         
                    
                    InteractionCtrl = $(this).find("input:radio");  // for Multichoice type1 , type 2
                    if(InteractionCtrl.length == 0) InteractionCtrl = $(this).find("input:checkbox"); // for Multiselect type1 , type 2
                    userAnswer = InteractionCtrl.attr("value");
                    imgID = '#imgState_'+ PageID + '_' + InteractionCtrl.attr("value");          
                    if(correctAnswer == userAnswer){
                        if (GBLShowValidationStatusImg == 1 ){
                            $(imgID).attr('src','../../../genimage/theme/template_image/correct.png');
                        }
                    }     
                         
                });  
                if ( counter == maxCounter ){
                    showAnswer4(PageID);
                    showFeedback(2,PageID,LevelID);    
                    return;                
                }
                else{ showFeedback(0,PageID,LevelID);}   
                
            }	
   
            
}

function validateAnswer2(PageID,LevelID){                     
    var ChoiceAry=$("#divValidation_ChoiceList_"+PageID).html().split(GBLDelimiter);
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_"+PageID).html().split(GBLDelimiter1);
    var counter=0;
    var imgID='';
    var imgSrc='images/spacer.gif';
    var i;
    var isCorrect=true;
    for (i=0;i<LevelcorrectAnsAry.length ;i++ ){
        imgID = '#imgState_'+ PageID + '_' + i;
        var levelAnswer= LevelcorrectAnsAry[i].split(GBLDelimiter);
        $('#page_' + PageID).find("input[name^='MultiChoice_Type3_'][value='"+levelAnswer[0]+"']").each(function(index){   //alert($(this).attr("value") +"\n" + levelAnswer[index +1]);
            
            imgSrc='images/circle-tick.png';
            if ($(this).attr('checked') && levelAnswer[1]==-1){
                isCorrect=false;
                imgSrc='images/circle-cross.png';
            }    
            else if($(this).attr('checked')==false && levelAnswer[1]!=-1){
                isCorrect=false;
                imgSrc='images/circle-cross.png';
            }
            if ($(this).attr('checked'))
                $(imgID).attr('src',imgSrc);
         });
        
    }

    if (isCorrect)
        {showFeedback(1,PageID);}     
    else
        {showFeedback(0,PageID);}	  
}


function validateAnswer3(PageID,LevelID){
    var LevelcorrectAnsAry = $("#divValidation_ChoiceList_"+PageID).html().split(GBLDelimiter);
    var subType=$('#divValidation_InteractionSubType_' + PageID).text().toLowerCase();    
    var isCorrect=true;
    var imgID='';
    if (subType.toLowerCase()=='checkmywork'){
        incrementValidationCounter(PageID);
    }
    
    var counter= parseInt($('#divValidation_Counter_'+PageID).text());
    var maxCounter= parseInt($('#divValidation_MaxCount_'+PageID).text());
    $('#page_' + PageID).find('input[type="text"]').each(function(index){
        var correctAnswer= -1;
        var userAnswer = -1;
        
        if ($(this).is(':visible')){
            var InteractionCtrl = $(this);
        
            correctAnswer= LevelcorrectAnsAry[index];
            
            userAnswer = InteractionCtrl.attr("value");

            if(correctAnswer == userAnswer){
            } else{
                isCorrect=false;
            } 
        }  
            
       
    });
    
    
    if (isCorrect)
        {showFeedback(1,PageID,LevelID);}     
    else
        {
            if ( counter == maxCounter ){
                showAnswer5(PageID);
                showFeedback(2,PageID,LevelID);    
                return;                
            }
            else{ showFeedback(0,PageID,LevelID);}   
            
        }
      
}

function validateAnswer4(PageID,LevelID){                     
    var isCorrect=true;
    var CurrPage = "#page_"+PageID;
    var correctAnswer = $(CurrPage).find('#divValidation_ChoiceList_'+PageID).text();   
    var userAnswer = $(CurrPage).find('#divAnswerArea').text();                
    
    if (correctAnswer != userAnswer){
        isCorrect=false;
    }
    
    if (isCorrect)
        {showFeedback(1,PageID);}     
    else
        {showFeedback(0,PageID);}	
}


function validateAnswer5(PageID,LevelID){          
    var isCorrect=true;
    var CurrPage = "#page_"+PageID;
    if ($(CurrPage).find(" .ZingoChoice:visible").length != 0){ // this means user attended all correctly
          isCorrect=false; 
    }
    if (isCorrect)
        {showFeedback(1,PageID);}     
    else
        {showFeedback(0,PageID);}	
}

function setUserAnswer(PageID){
    $("#btnSubmit_" + PageID).attr("disabled",false);
    $("#page_"+PageID).find(".slider:visible").find('.CheckMyWorkDummy').hide();   
    $("#page_"+PageID).find(".slider:visible").find(".CheckMyWork").show();
}

function clearUserAnswerList(PageID)
{    
    $("#divValidation_UserAnswerList_" + PageID).html('');
    $("#divXML_" + PageID).find("input:radio,input:checkbox").each(function(index){
        $(this).attr('checked',false);                            
    });   
}

function resetStateImages(PageID){
    $("[id^='imgState_" + PageID + "']").each(function(){
        $(this).attr("src","../../../genimage/theme/template_image/spacer.gif")
    });
}

function ClearInteraction(PageID){
    HideLightBox(GBLPopupDivID);   
    HideCheckMyWorkModalPopUp(PageID);
    resetStateImages(PageID);
    clearUserAnswerList(PageID);
}

function GetInteractionType(PageID){
    return $.trim($("#divValidation_InteractionType_"+PageID).html());
}

function GetInteractionSubType(PageID){
    return $.trim($("#divValidation_InteractionSubType_"+PageID).html());
}

function alertArray(aDisp){
    var aTemp='';
    for(i=0;i<aDisp.length;i++){
        aTemp=aTemp + aDisp[i] + '\n';
    }
    alert(aTemp);
}

function Flip(obj, PageID, isHide, iCurrentIndex, subType){
    var CurrPage = "#page_"+PageID;
    var iWidth;
    if(isHide==undefined) isHide=false;
    if(iCurrentIndex==undefined) iCurrentIndex=-1;
       
    var flipObj1 = $(obj).find(".Displaying");
    var flipObj2 = $(obj).find(".Hidden");
    
    //alert(flipObj1.outerHTML() + "\n\n" + flipObj2.outerHTML());
            
    iWidth = $(obj).width();
    flipObj1.css("position","absolute");
    flipObj1.css("z-index","1000");
    flipObj1.css("left","0px");
    flipObj2.css("position","absolute");
    flipObj2.css("z-index","999");
    
    flipObj1.show();    
    flipObj1.animate( {left: (iWidth/2) + "px", width:"10px"}, 100, "linear", function(){
        $(this).css("z-index","999");
        $(this).hide();
        flipObj2.css("left", (iWidth/2) + "px");
        flipObj2.css("width","10px");
        flipObj2.show();
        flipObj2.animate( {left:"0px", width: iWidth + "px"}, 100, "linear", function(){
            $(this).css("left","0px");
            $(this).css("width", iWidth + "px");
            $(this).css("z-index","1000");
            flipObj1.css("left","0px");
            flipObj1.css("width", iWidth + "px");
            
            $(obj).find(".ZingoChoice").toggleClass("Displaying Hidden");
            
            if (isHide==true){
                switch (subType){
                    case "pairing":

                        break;
                    case "zingo":
                        $(obj).find("img").each(function(){
                            $(this).attr("title","completed");
                        });
                        $(obj).find(".ZingoChoice:visible").hide(300,function(){
                            $(CurrPage).find("#QusDiv" + iCurrentIndex  + "_" +PageID).hide(100);
                            iCurrentIndex = parseInt(iCurrentIndex) + 1;
                            SlideToCenter($(CurrPage).find("#QusDiv" + iCurrentIndex + "_" +PageID));
                        });
                        //$(obj).unbind("click");
                        break;
                       
                }// end switch
            }//end ishide if  
        });
    });
    
    
}

function SlideToCenter(obj){
    $(obj).show();
    $(obj).animate( {left:"390px"}, 300, "linear", function(){
        $(this).css("left","390px");
    });  
}

function renderClearDiv(){
	var txt='';
	txt = '<div class="clr"></div>';
	return txt;
}

function renderDiv(iID,CssClass,nodeText){
	var txt='';
	var sClass='';
	if(CssClass!=""){
		sClass = "class='" + CssClass + "'";
	}
	txt = "<div " + sClass;
	if(iID!=""){
	    txt = txt + " id='" + iID +"'";
	} 
	txt = txt + " >" + nodeText + "</div>";
	return txt;
}

function rotate(objID,animated){
    var iDegree=0;
    var rotatedDegree=0;
    if (animated==true){
         //When the image is clicked;
         rotatedDegree=90;    
        $("#"+objID).animate({rotate: '+=90deg'});
    }
    else{
         //$("#"+objID).animate({rotate: '+=90deg'},0);
        //switch (objID.substring(6,objID.length)){
        switch (Math.floor(Math.random()*4)+1){
            case 1:
                rotatedDegree=270;
                $("#"+objID).animate({rotate: '+=270deg'},0);
                break;    
            case 2:
                rotatedDegree=90;
                $("#"+objID).animate({rotate: '+=90deg'},0);
                break;    
            case 3:
                rotatedDegree=90;
                $("#"+objID).animate({rotate: '+=90deg'},0);
                break; 
            case 4:
                rotatedDegree=180;
                $("#"+objID).animate({rotate: '+=180deg'},0);
                break;       
        }
        
    }
    
    iDegree=parseInt($("#"+objID).attr('title'));
    
    iDegree=iDegree+rotatedDegree;
    iDegree%=360;
    //alert(objID + ' : ' + rotatedDegree + ' : ' + iDegree);
    $("#"+objID).attr('title', iDegree )
}

function rotateToZeroDegree(objID){
    var iDegree=0;
    var rotatedDegree=0;
     //$("#"+objID).animate({rotate: '+=90deg'},0);
    //switch (objID.substring(6,objID.length)){
    iDegree=parseInt($("#"+objID).attr('title'));
    rotatedDegree=360 - iDegree;
    //alert(objID + ' - ' + iDegree + ' - ' + rotatedDegree);
    $("#"+objID).animate({rotate: '+=' + rotatedDegree + 'deg'},0);
    
    $("#"+objID).attr('title', 0 )
}



/*#### Click reveal start*/

function buttonBarPopup(obj){ 
var img = $(obj).find('.imgBorder').html(); 
    var pageContainer = $(obj).closest('.PageContainer');
    var DivContent= $(pageContainer).find('.HideOnPreview').html();
    var divTemp=document.createElement('div');
    $(divTemp).html(DivContent);    
    $(divTemp).find('.ImgWdHt12').html(img);    
    $(divTemp).find('.popUpNav').append('<div style="cursor:pointer;" onclick="HideLightBox(\'divLightBox\')">[X]</div> ');
    DivContent = $(divTemp).html();    
    var PageID = $(pageContainer).attr('id').replace('page_','');
    ShowDefaultModalPopUp(DivContent, 500, 990);
    //ShowDefaultModalPopUp(DivContent, 500, $(window).width()-20);
    var CurrPage = "#divLightBoxsingle1";
    $(CurrPage).find('.tabheader').each(function(index){
        $(this).unbind('click').bind('click',function(){
            showTabContent(index+1,CurrPage);
        });
    });

} 

/*#### Click reveal end*/


function setActiveBtn(obj){
    $(obj).closest(".Btn").addClass("BtnActive").removeClass("Btn");
}

/* Word Selection */
function validateWordSelection(PageID,LevelID){             
    var correctAns = $.trim($("#divValidation_AnswerList_" + PageID).html());
    var userAns = $.trim($("#divValidation_UserAnswerList_" + PageID).html());
    var isCorrect=true;
    var counter= parseInt($('#divValidation_Counter_'+PageID).text());
    var maxCounter= parseInt($('#divValidation_MaxCount_'+PageID).text());
    var correctAnsArray=correctAns.split(' ');
    var userAnsArray = userAns.split(' ');
    var iCount=0;
    var iTotalCorrect=0;
    var inCorrectArray=[];
    
    $('#page_' + PageID).find(".slider:visible").find('.CheckMyWorkDummy').show();
    $('#page_' + PageID).find(".slider:visible").find('.CheckMyWork').hide();
    
    for(iCount=0;iCount<userAnsArray.length;iCount++){
        if($.inArray(userAnsArray[iCount],correctAnsArray)==-1){
            isCorrect=false;
            inCorrectArray.push(userAnsArray[iCount]);
        }else{
            iTotalCorrect +=1;
        }
    }
    
    if(correctAnsArray.length!=userAnsArray.length){
        isCorrect=false;
    }
    
    if(userAns!=correctAns){
        isCorrect=false;
    }
    
    if (isCorrect){
        showFeedback(1,PageID,LevelID);
        sliderEndWordSelection(PageID,LevelID);
    } else{
        if(counter == maxCounter || iTotalCorrect==0){
            showFeedback(2,PageID,LevelID);
            showAnswerWordSelection(PageID,LevelID);
            sliderEndWordSelection(PageID,LevelID);
        }
        else{ 
            showFeedback(0,PageID,LevelID);
            markIncorrectWordSelection(PageID,LevelID,inCorrectArray);
        }
    }
        
         	 
}

function showAnswerWordSelection(PageID,LevelID){
    var correctAns = $.trim($("#divValidation_AnswerList_" + PageID).html());
    var correctAnsArray=correctAns.split(' ');
    
    $('#page_' + PageID).find(".wordSelection:visible").find('.wordselectionBG').removeClass('wordselectionBG');
    
    $('#page_' + PageID).find(".wordSelection:visible").find('span').each(function(){
        if($.inArray($(this).html(),correctAnsArray)!=-1){
            $(this).addClass('wordselectionBG');
        }
    });
}

function markIncorrectWordSelection(PageID,LevelID,inCorrectArray){
     $('#page_' + PageID).find(".slider:visible").find('.wordselectionBG').each(function(){
        if($.inArray($(this).html(),inCorrectArray)!=-1){
            $(this).removeClass('wordselectionBG').addClass('wordselectionBG2');
        }
    });
}

function tryAgainWordSelection(PageID,LevelID){
    $('#page_' + PageID).find(".slider:visible").find('.wordselectionBG').removeClass('wordselectionBG');
    $('#page_' + PageID).find(".slider:visible").find('.wordselectionBG2').removeClass('wordselectionBG2');
    $("#divValidation_UserAnswerList_" + PageID).html('');
    $('#page_' + PageID).find(".slider:visible").find('.CheckMyWorkDummy').hide();
    $('#page_' + PageID).find(".slider:visible").find('.CheckMyWork').show();
}

var GBL_refreshIntervalId;
function sliderEndWordSelection(PageID,LevelID){
    $('#page_' + PageID).find(".slider:visible").find('.sliderend').show();
    //$('#page_' + PageID).find(".slider:visible").find('.blinkword').fadeOut('slow');
     GBL_refreshIntervalId=setInterval(function(){blinkWordSelection(PageID);}, 101);
//     $('#page_' + PageID).find(".slider:visible").find('.blinkword').fadeOut('slow',function(){
//        $('#page_' + PageID).find(".slider:visible").find('.blinkword').fadeIn('slow',function(){
//           
//        });
//    });
//$('#page_' + PageID).find(".slider:visible").find('.blinkword').get(0).blink();

}

function blinkWordSelection(PageID){
    var blink=$('#page_' + PageID).find(".slider:visible").find('.blinkword');
    //alert(blink);
    if($('#page_' + PageID).find(".slider:visible").length==0){
        clearInterval(GBL_refreshIntervalId);
    }
    if(blink.css('display')=='none'){
        $('#page_' + PageID).find(".slider:visible").find('.blinkword').fadeIn('100');
    } else{
        $('#page_' + PageID).find(".slider:visible").find('.blinkword').fadeOut('100');
    }
    
}
/* Word Selection End */
/* MultipleChoiceVoiceInteraction*/
function validateAnswerMCVoiceInteraction(PageID,LevelID){
    // alert('validateAnswer1');
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    var ChoiceAry=$("#divValidation_ChoiceList_" + PageID).html().split(GBLDelimiter);
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());    
    var isCorrect=true;
    var imgID='';
    if (subType.toLowerCase()=='checkmywork' || subType=='voiceinteraction'){
	    GBLShowValidationStatusImg =1;
        incrementValidationCounter(PageID);
    }
    if ($('#page_' + PageID).find('.UserAnswer:visible').find("input[checked=true]").length ==0 ) isCorrect=false;
    
      
    var counter= parseInt($('#divValidation_Counter_'+PageID).text());
    var maxCounter= parseInt($('#divValidation_MaxCount_'+PageID).text());

        $('#page_' + PageID).find('.UserAnswer:visible').each(function(index){
            var correctAnswer= -1;
            var userAnswer = -1;
                  
            var aryIndex=parseInt($(this).attr('id').replace('UserAnswer_'+PageID+'_',''));        
            correctAnswer= LevelcorrectAnsAry[aryIndex].split(GBLDelimiter)[1];         
            
            var InteractionCtrl = $(this).find("input:radio");  // for Multichoice type1 , type 2
            if(InteractionCtrl.length == 0) InteractionCtrl = $(this).find("input:checkbox"); // for Multiselect type1 , type 2
        
            if (InteractionCtrl.attr("checked")) {
                userAnswer = InteractionCtrl.attr("value");
                imgID = '#imgState_'+ PageID + '_' + InteractionCtrl.attr("value");                
                //alert(correctAnswer + ' - ' + userAnswer);
                //alert(GBLShowValidationStatusImg);
                if(correctAnswer == userAnswer){
                    if (GBLShowValidationStatusImg == 1 ){
                        $(imgID).attr('src','../../../genimage/theme/template_image/correct.png');
                        HighlightSummary(ChoiceAry[aryIndex]);
                    }
                } else{
                    if (GBLShowValidationStatusImg == 1 ){
                        $(imgID).attr('src','../../../genimage/theme/template_image/incorrect.png');
                    }
                    isCorrect=false;
                }
            }
            else{
                if (correctAnswer !=-1){
                    isCorrect=false;
                }            
            }     
                 
        });
        if (isCorrect)
            {showFeedback(1,PageID,LevelID);}     
        else
            {
                if (counter == maxCounter){
                    showAnswerMCVoiceInteraction(PageID);
                    showFeedback(2,PageID,LevelID);    
                    return;                
                }
                else{ showFeedback(0,PageID,LevelID);}   
                
            }	
   
            
}

function showAnswerMCVoiceInteraction(PageID){
    var LevelcorrectAnsAry = $("#divValidation_AnswerList_" + PageID).html().split(GBLDelimiter1);
    var ChoiceAry=$("#divValidation_ChoiceList_" + PageID).html().split(GBLDelimiter);
    var type=$.trim($('#divValidation_InteractionType_' + PageID).text().toLowerCase());
    var subType=$.trim($('#divValidation_InteractionSubType_' + PageID).text().toLowerCase());
    
    if (subType.toLowerCase()=='checkmywork'){
	    GBLShowValidationStatusImg =1;
    }
    $('#page_' + PageID).find('.UserAnswer:visible').each(function(index){
            var correctAnswer= -1;
            var userAnswer = -1;
            var InteractionCtrl = $(this).find(".ChoiceBtnActive");       
            var aryIndex=parseInt($(this).attr('id').replace('UserAnswer_'+PageID+'_',''));        
            correctAnswer= LevelcorrectAnsAry[aryIndex].split(GBLDelimiter)[1];  
            //var imgID = '#imgState_'+ PageID + '_' + $(this).val(); 
            var imgID = '#imgState_'+ PageID + '_' + aryIndex; 
            //$(imgID).hide();
            if(correctAnswer == aryIndex){
                
                if (GBLShowValidationStatusImg == 1 ){
	                //$(imgID).attr('src','../../../genimage/theme/template_image/circle-tick.png');
	                $(imgID).attr('src','../../../genimage/theme/template_image/correct.png');
	                //$(this).find("input:radio").attr('checked',true);
	                //$(this).find("input:checkbox").attr('checked',true);
	                HighlightSummary(ChoiceAry[aryIndex]);
                }
                else{
                    //$(this).find('.controlParent').addClass('ChoiceBtnActive').removeClass('ChoiceBtn');  /* Highlight not in new style */
                    //$(this).find("input:radio").attr('checked',true);  /* Highlight not in new style */
                }
            } else{
                if (GBLShowValidationStatusImg == 1 ){
	                //$(imgID).attr('src','../../../genimage/theme/template_image/circle-cross.png');
	                if($(this).find("input:radio").attr('checked')==true || $(this).find("input:checkbox").attr('checked')==true)
	                    $(imgID).attr('src','../../../genimage/theme/template_image/incorrect.png');
	                //$(this).find("input:radio").attr('checked',false); 
	                //$(this).find("input:checkbox").attr('checked',false);
                }
                else{
                    //$(this).find('.controlParent').removeClass('ChoiceBtnActive').addClass('ChoiceBtn');   /* Highlight not in new style */
                    //$(this).find("input:radio").attr('checked',false);     /* Highlight not in new style */
                }
            }            
        });     
      
}

function HighlightSummary(sChoice){
    if($('.questionsummary:visible').length>0){
        sContent=$('.questionsummary:visible').html();
        sChoice=$.trim(sChoice.replace(/\s+/g,' '));
        sContent=$.trim(sContent.replace(/\s+/g,' '));
        sChoice=sChoice.replace('<br />','');
        //alert(sChoice);
        if(sContent.indexOf(sChoice)!=-1){
            sContent=sContent.replace(sChoice,'<span class="HighlightBG">' + sChoice + '</span>');
        }
        $('.questionsummary:visible').html(sContent);
    }
}
/* MultipleChoiceVoiceInteraction*/
/***************************************************************************

	Author 			:Charles B. Coss� 
	
	Email			:ccosse@gmail.com
					
	Copyright		:(C) 2001-2007 Asymptopia Software.
	
	Package			:Asymptopia Math Crossword Builder
	
	Version			:2.6
	
	License			:Gnu Public License (GPL)
	
	
 ***************************************************************************/
/***************************************************************************
                          config_table_stuff.js

		Description: generates configuration panel

 ***************************************************************************/
var key_table_div;
var puzzle_table_div;

var words;
var used;
var used2;
var unused;
var keyvals;
var MROW=10;
var NCOL=10;
function getMROW(){return(MROW);}
function getNCOL(){return(NCOL);}
function setMN(e){
	/*mrow_area=document.getElementById("mrow_area");
	MROW=mrow_area.value;
	ncol_area=document.getElementById("ncol_area");
	NCOL=ncol_area.value;
	//alert(MROW+","+NCOL);
	*/
	//MROW=7;
	//NCOL=7;
	//recreate_cwp_table();
}
function get_total(){
	total=get_words().length+get_used().length;
	return(total);
}
function used2words(){
	
	while(used.length>0)words.push(used.pop());
	while(used2.length>0)words.push(used2.pop());
	
}
function unused2words(){
	while(unused.length>0)words.push(unused.pop());
}

function add_word(word){
	words.push(word);
}
function get_used(){
	//alert("get_used:returning "+used);
	return(used);
}
function get_words(){
	//alert("get_words:returning "+words);
	return(words);
}
function remove_word_by_index(idx){
	for(var dummy=0;dummy<idx;dummy++){words.push(words.shift());}
	return(words.shift());
}
function add_to_words(word){
	words.push(word);
}
function add_to_used(word){
	used.push(word);
}
function add_to_unused(word){
	unused.push(word);
}
function remove_word_by_value(word){
	for(var dummy=0;dummy<words.length;dummy++){
		current=words.shift();
		//alert("shifted-off "+current+" words="+words);
		if(current==word){
			//alert("removed:"+current+" words="+words);
			return(current);
		}
		else words.push(current);
	}
	for(var dummy=0;dummy<used.length;dummy++){
		current=used.shift();
		//alert("shifted-off "+current+" used="+words);
		if(current==word){
			//alert("removed:"+current+" used="+words);
			return(current);
		}
		else words.push(current);
	}
	return(current);
}
function get_hint_by_key(key){
	var tmp;
	for(idx=0;idx<used.length;idx++){
		k=used[0].split(":",2)[0];
		v=used[0].split(":",2)[1];
		tmp=used.shift()
		if(k==key){
			used2.push(tmp);
			return(v);
		}
		used.push(tmp);
	}
}
function recreate_cwp_table(){
	key_table_div=setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,1);
	puzzle_table_div=setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,2);
}
function setup_config_background(){
	div=document.createElement("div");
	div.style.position="absolute";
	div.style.top="40px";
	div.style.height="540px";
	div.style.width="880px";
	div.style.align="center";
	var winwidth = window.document.width || window.document.body.clientWidth;
	div.style.left=(winwidth-880)/2.;
	div.style.backgroundColor="000070";
	return(div);
}


/*function capability_toggle(e){
	if(navigator.appName=="Microsoft Internet Explorer"){
		x=window.event.srcElement.id;
		targ=document.getElementById(x);
	}
	else{
		x=e.target.id;
		targ=e.target;
	}
	if(x=="addition"){
		addition_words*=-1;
		if(addition_words<0)targ.checked=0;
		else targ.checked=1;
	}
	if(x=="subtraction"){
		subtraction_words*=-1;
		if(subtraction_words<0)targ.checked=0;
		else targ.checked=1;
	}
	if(x=="multiplication"){
		multiplication_words*=-1;
		if(multiplication_words<0)targ.checked=0;
		else targ.checked=1;
		}
	if(x=="algebra"){
		algebra_words*=-1;
		if(algebra_words<0)targ.checked=0;
		else targ.checked=1;
	}
	if(x=="fractions"){
		fraction_words*=-1;
		if(fraction_words<0)targ.checked=0;
		else targ.checked=1;
	}
}*/

//filter in add to ensure proper format=word:hint?
function add_entries(e){
	/*sel_keyword=document.getElementById('select_keywordlist');
	add_keyword_area=document.getElementById('add_keyword_area');
	txt=add_keyword_area.value;
	txt_array=txt.split('\n',1000);
	//alert(txt_array);
	for(var i=txt_array.length-1;i>-1;i--){
		if(navigator.appName=="Micorsoft Internet Explorer")txt_array[i]=txt_array[i].replace(/\s+/g,'');//stupid Microsoft doesn't handle newlines correctly!
		else txt_array[i]=txt_array[i].replace(/\s+/g,' ');
		if(txt_array[i]=='')continue;//This line is for Microsoft too! (without it, you get blanks added!)
		opt=document.createElement("option");
		opt.text=txt_array[i];
		opt.defaultSelected=1;
		opt.selected=0;
		//words.push(opt.text);
		if(agent=="MSIE")sel_keyword.add(opt);
		else sel_keyword.add(opt,sel_keyword.options[0]);
	}*/
	//words = new Array();
	//words[0]="one:first number";
	//words[1]="two:second";
	//words[1]="three:second";
	/*words[0]="flower";
    words[1]="circuit";
    words[2]="power";
    words[3]="commonsense";
    words[4]="catridge";
    words[5]="apple";*/
	//add_keyword_area.value='';
	//add_entries_FromXML()
	/*words[0]="aicc";
    words[1]="soap";
    words[2]="cat";
    words[3]="dog";
    words[4]="catridge";
    words[5]="apple";*/
    //words[0]="aaaa";
    //words[1]="bbbb";
    //words[2]="cccccccccc";
    //words[3]="ddd";
}

function add_entries_FromXML(aryCrosswordAnswers,aryCrosswordQues){
	var iCount;
	words=new Array();
	across_list=new Array();
    down_list=new Array();
    used=new Array();
    used2=new Array();
    unused=new Array();
    keyvals=new Array();

	for (iCount=0;iCount<aryCrosswordAnswers.length; iCount++){
	    words[iCount] = aryCrosswordAnswers[iCount].toLowerCase() + ':' + aryCrosswordQues[iCount]
	}
	
}

function rem_entries(e){
	sel_keyword=document.getElementById('select_keywordlist');
	for(i=0;i<sel_keyword.options.length;i++){
		if(sel_keyword.options[i].selected){
			dummy=remove_word_by_value(sel_keyword.options[i].text);
			sel_keyword.options[i]=null;
			//should remove from words via pop!!
		}
	}
}
function remove_word_by_value(word){
	for(var dummy=0;dummy<words.length;dummy++){
		current=words.shift();
		//alert("shifted-off "+current+" words="+words);
		if(current==word){
			//alert("removed:"+current+" words="+words);
			return(current);
		}
		else words.push(current);
	}
	for(var dummy=0;dummy<used.length;dummy++){
		current=used.shift();
		//alert("shifted-off "+current+" used="+words);
		if(current==word){
			//alert("removed:"+current+" used="+words);
			return(current);
		}
		else words.push(current);
	}
	return(current);
}

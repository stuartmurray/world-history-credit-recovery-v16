/***************************************************************************

	Author 			:Charles B. Coss� 
	
	Email			:ccosse@gmail.com
					
	Copyright		:(C) 2001-2007 Asymptopia Software.
	
	Package			:Asymptopia Math Crossword Builder
	
	Version			:2.6
	
	License			:Gnu Public License (GPL)
	
	
 ***************************************************************************/
/***************************************************************************
                          cwp_table_stuff.js

	Description: generates key,puzzle and hints pages

 ***************************************************************************/
var allow_disconnected=0;
var cells=null;
var rows=null;
var pcells=null;
var prows=null;
var across_list;
var down_list;
var HintDiv=null;
var success_label=document.createTextNode("");
var gbs;

CELLSIZE=30;
DEFAULT_IMAGE="images/blank.jpeg";
DEFAULT_STR="blank";
function update_success_string(){
	num_used=get_used().length;
	total=get_total();
	rval=num_used+"/"+total;
	//if(num_used != total) generate(); //Maran
	return(rval);
}
function do_toggle(e){
	//alert("do_toggle");
	b=document.getElementById("allow_disconnected_button");
	if(allow_disconnected==0){
		allow_disconnected=1;
		b.checked=allow_disconnected;
	}
	else{
		allow_disconnected=0;
		b.checked=allow_disconnected;
	}
}

function setup_cwp_table(m,n,cellsize,default_src,default_str,which_table){
    //alert("inside");
	
	theTable=document.createElement("table");
	theTable.cellPadding="0";
	theTable.cellSpacing="1";
	theTable.border="0";
	theTable.style.backgroundColor="#000000";
	theTable.style.color="#0000AA";
	
	//theTable.align="center";
	
	xrows=new Array();
	xcells=new Array();
	for(midx=0;midx<m;midx++){
		xcells[midx]=new Array();
		xrows[midx]=theTable.insertRow(-1);
		xrows[midx].align="right";
		for(nidx=0;nidx<n;nidx++){
			xcells[midx][nidx]=xrows[midx].insertCell(nidx);
			xcells[midx][nidx].bgColor="#AAAAAA";
			xcells[midx][nidx].style.position="relative";
			xcells[midx][nidx].setAttribute("height",cellsize + 'px');
			xcells[midx][nidx].setAttribute("width",cellsize + 'px');
			xcells[midx][nidx].left=nidx*cellsize;
			xcells[midx][nidx].top=midx*cellsize;
			
			x=document.createElement("img");
			x.src=default_src;
			//document.images[default_str+"_"+midx+"_"+nidx]=x;
			//document.images["blank_"+midx+"_"+nidx]
			
			//if(midx==0 && nidx==0)alert(xcells[midx][nidx]);
			
			xcells[midx][nidx].appendChild(x);
			//if(navigator.appName=="Microsoft Internet Explorer")xcells[midx][nidx].appendChild(x);
			//else xcells[midx][nidx].appendChild(document.images[default_str+"_"+midx+"_"+nidx]);
			
			/*
			*/
			xcells[midx][nidx].str=default_str;
			xcells[midx][nidx].AMHEAD=0;
			xcells[midx][nidx].AMROWHEAD=0;
			xcells[midx][nidx].AMCOLHEAD=0;
			xcells[midx][nidx].AMCOL=0;
			xcells[midx][nidx].AMROW=0;
			xcells[midx][nidx].COL_WORD="";
			xcells[midx][nidx].ROW_WORD="";
			/**/
		}
	}
	//alert("div");
	div=document.createElement("div");
	if(which_table==2){
	    div.style.position="absolute";
	    div.style.top="0px";
	}
	//div.style.position="absolute";
	//div.style.top=TOP+30;
	div.style.top=30;
	
	//var winwidth = window.document.width || window.document.body.clientWidth;
	//div.style.left=(winwidth-CELLSIZE*n)/2.;
	
	div.appendChild(theTable);

	if(which_table==1){cells=xcells;rows=xrows;}
	else{pcells=xcells;prows=xrows;}
	//alert(div);
	return(div);
}
var GBLiRandId=0;
function get_rand_idx(){
    var iId=0;
    //iId=Math.floor(Math.random()*get_words().length);
    while(iId==GBLiRandId && get_words().length>1){
        iId=Math.floor(Math.random()*get_words().length);
    }
    //alert(iId);
    //alert(get_words());
    return iId;
}
function reset_table(m,n){
	for(midx=0;midx<m;midx++){
		for(nidx=0;nidx<n;nidx++){
			cells[midx][nidx].firstChild.src=DEFAULT_IMAGE;
			cells[midx][nidx].str=DEFAULT_STR;
			cells[midx][nidx].AMHEAD=0;
			cells[midx][nidx].AMROWHEAD=0;
			cells[midx][nidx].AMCOLHEAD=0;
			cells[midx][nidx].AMROW=0;
			cells[midx][nidx].AMCOL=0;
			cells[midx][nidx].ROW_WORD=null;
			cells[midx][nidx].COL_WORD=null;
			
			pcells[midx][nidx].firstChild.src=DEFAULT_IMAGE;
			pcells[midx][nidx].str=DEFAULT_STR;
			pcells[midx][nidx].AMHEAD=0;
			pcells[midx][nidx].AMROW=0;
			pcells[midx][nidx].AMCOL=0;
		}
	}
}
function advise1(){
	//update success label for last generation:
	/*gbs.removeChild(success_label);
	success_label=document.createTextNode(update_success_string());
	gbs.appendChild(success_label);
	alert("Bailing Out! There was difficulty fitting your words to the grid");*/
	//alert("advise1");
	NCOL=NCOL + 2;
	//recreate_cwp_table();
	//get_key();
	crossword();
}
function advise2(){
	//update success label for last generation:
	gbs.removeChild(success_label);
	success_label=document.createTextNode(update_success_string());
	gbs.appendChild(success_label);
	alert("You did not press the \"ADD\" button in the ConfigPanel!");
}

var reGenCount;
function crossword(){
    //alert("hi");
    /*try{setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,1)}
	catch(ex){alert(ex);}	
	try{setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,2)}
	catch(ex){alert(ex);}*/
	//var key_table_div;\
	//alert(getNCOL());
	reGenCount=0;
	//if(!key_table_div) key_table_div=setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,1);
	//if(!puzzle_table_div)puzzle_table_div=setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,2);
	key_table_div=setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,1);
	puzzle_table_div=setup_cwp_table(getMROW(),getNCOL(),CELLSIZE,DEFAULT_IMAGE,DEFAULT_STR,2);
	//alert(key_table_div);
	get_key();
	
    add_entries();
    
    used2words();
    //cells[0][9].innerHTML="c";
    //cells[0][9].str="c";
    generate();
    
    generate_puzzle();
}


function regenerate(){
    var sucess_text = update_success_string();
	var check = sucess_text.split("/");
	//rval=num_used+"/"+total;
	//alert(check);
	if(check[0] != check[1]) 
	{
	    //alert("Next Try");
	    /*reGenCount += 1;
	    if(reGenCount==9){
	        reGenCount = 0;
	        MROW=MROW + 2;
	        NCOL=NCOL + 2;
	    
	        recreate_cwp_table();
	        get_key();
	    }*/
	    //alert("Generate called");
	    generate(); //Maran
	}
}

function insertionInBlankArea(){
    mrow=getMROW();
	ncol=getNCOL();
	
	offset=getMROW();
	
	while(1){
		offset--;
		start_with_row=Math.floor(Math.random()*2);
		
		m=Math.floor(Math.random()+Math.random()*offset);
		n=Math.floor(Math.random()+Math.random()*offset);
		if(m<0){advise1();return}
		if(n<0){advise1();return}
		//alert(m);
		rand_idx=get_rand_idx();
		var iCount;
		var iIndex;
		var sTemp=0;
		for(iCount=0;iCount<words.length;iCount++){
		    if(words[iCount].length>sTemp){
		        sTemp=words[iCount].length;
		        iIndex=iCount;
		    }
		}
		//keyval=remove_word_by_index(rand_idx);
		keyval=remove_word_by_index(iIndex);
		//alert("keyval" + keyval);
		//alert(words);
		submission=keyval.split(":",2)[0];
		
		//alert(submission.length);
		
		if(submission.length<1){
			add_to_words(keyval);
			submission=null;
			continue;
		}
		if(start_with_row){
			if(n+submission.length>cells[0].length){
				add_to_words(keyval);
				submission=null;
				continue;
			} else{
			    var iFlag=false;
			    for(var sidx=0;sidx<submission.length;sidx++){
			        if(cells[m][n+sidx].str!="blank"){
			            iFlag=true;
			            //alert("Inside Con")
			            break;   
			        }
			    }
			    if(iFlag){
			         //alert("Excluded")
			        add_to_words(keyval);
				    submission=null;
				    continue;
			    }
			}
		}
		if(!start_with_row){
			if(m+submission.length>rows.length){
				add_to_words(keyval);
				submission=null;
				continue;
			}else{
			    var iFlag=false;
			    for(var sidx=0;sidx<submission.length;sidx++){
			        if(cells[m+sidx][n].str!="blank"){
			            iFlag=true;
			            //alert("Inside Con2")
			            break;   
			        }
			    }
			    if(iFlag){
			         //alert("Excluded2")
			        add_to_words(keyval);
				    submission=null;
				    continue;
			    }
			}
		}
		if(submission)break;
	}
	//alert(add_to_words);
	//alert(submission);
	for(var sidx=0;sidx<submission.length;sidx++){
		if(start_with_row){
			//if(submission.charAt(sidx)=="-"){cells[m][n+sidx].firstChild.src="images/dash.jpeg";}
			//else{
			    //cells[m][n+sidx].firstChild.src="images/"+submission.charAt(sidx)+".jpeg";
			    cells[m][n+sidx].innerHTML=submission.charAt(sidx);
			 //}
			
			//alert(submission.slice(sidx:sidx+1));
			
			cells[m][n+sidx].str=submission.charAt(sidx);
			cells[m][n+sidx].AMROW=1;
			if(sidx==0){
				cells[m][n+sidx].AMHEAD=1;
				cells[m][n+sidx].AMROWHEAD=1;
				cells[m][n+sidx].ROW_WORD=submission;
			}
		}
		else{
			//if(submission.charAt(sidx)=="-"){cells[m+sidx][n].firstChild.src="images/dash.jpeg";}
			//else{
			    //cells[m+sidx][n].firstChild.src="images/"+submission.charAt(sidx)+".jpeg";
			    cells[m+sidx][n].innerHTML=submission.charAt(sidx);
			//}
			cells[m+sidx][n].str=submission.charAt(sidx);
			cells[m+sidx][n].AMCOL=1;
			if(sidx==0){
				cells[m+sidx][n].AMHEAD=1;
				cells[m+sidx][n].AMCOLHEAD=1;
				cells[m+sidx][n].COL_WORD=submission;
			}
		}
	}
	//alert("after For");
	add_to_used(keyval);
	
	insertionInIntersectArea();
}
var GBLUnUsedLength=0;
function insertionInIntersectArea(){
    //alert("insertion in intersect Area");
    stuck=0;
	really_stuck=0;
	
	while(get_words().length>0){
		//alert(stuck+","+really_stuck+","+submission+","+words);
		rand_idx=get_rand_idx();
		keyval=remove_word_by_index(rand_idx);
		submission=keyval.split(":",2)[0];
		rval=localize(submission,really_stuck);
		//alert(submission+" "+rval);
		
		if(rval==1){
			add_to_used(keyval);
			stuck=0;
			really_stuck=0;
		}
		else{
			//add_to_words(keyval);
			add_to_unused(keyval);
			stuck++;
		}
		if(stuck>get_words().length*4){
			if(allow_disconnected==0)break;
			really_stuck++;
			if(really_stuck>get_words().length*2)break;	
		}
	}
		
	if(GBLUnUsedLength==unused.length){
	    GBLUnUsedLength=0;
	    unused2words();
	    regenerate();
	} else{
	    GBLUnUsedLength=unused.length;
	    unused2words();
	    insertionInIntersectArea();
	}
}

function generate(){
    insertionInBlankArea();
    //alert(offset);
    //regenerate();
}

function localize(submission,really_stuck){
    var hs=0;
	var m_hs=0;
	var n_hs=0;
	var rc_hs="row";
	var ok=0;
	var score;
	var dummy;
	//rows
	//alert(submission+": "+submission.length);
	for(var midx=0;midx<rows.length;midx++){
		for(var nidx=0;nidx<=(cells[0].length-submission.length);nidx++){
			//m,n candidate row head
			ok=1;
			score=0;
			
			for(var sidx=0;sidx<submission.length;sidx++){
				if(cells[midx][nidx+sidx].str!="blank"){
				    if(cells[midx][nidx+sidx].str==submission.charAt(sidx)){
				        score=score+1;
				    } else{
				        ok=0;
				        score=0;
				        break;    
				    }
				}
				/*if(cells[midx][nidx+sidx].str=="blank")dummy=99;
				else{
					if(cells[midx][nidx+sidx].str==submission.charAt(sidx))score=score+1;
					else ok=0;
				}*/
			}
			if(ok==1 && score>hs){
			   hs=score;m_hs=midx;n_hs=nidx;rc_hs="row";
			}
			//print midx,nidx,ok,score
			/*if(ok==1 && score<submission.length){
				if(really_stuck>0){score++;}
				if(score>hs){
					if(check4conflict(submission,"row",midx,nidx)){
						hs=score;m_hs=midx;n_hs=nidx;rc_hs="row";
					}
				}
			}*/	
		}
	}
	//cols
	//alert(rows.length);
	for(var midx=0;midx<=rows.length-submission.length;midx++){
		//if(midx+submission.length>rows.length)continue;
		for(var nidx=0;nidx<cells[0].length;nidx++){
			ok=1
			score=0
			for(var sidx=0;sidx<submission.length;sidx++){
				if(cells[midx+sidx][nidx].str!="blank"){
				    if(cells[midx+sidx][nidx].str==submission.charAt(sidx)){
				        score=score+1;
				    } else{				        
				        ok=0;
				        score=0;
				        break;
				    }
				}
			}
			if(ok==1 && score>hs){
			   hs=score;m_hs=midx;n_hs=nidx;rc_hs="col";
			}
			//print midx,nidx,ok,score
			/*if(ok==1 && score<submission.length){
				if(really_stuck>0){score++;}
				if(score>hs){
					if(check4conflict(submission,"col",midx,nidx)){
						hs=score;m_hs=midx;n_hs=nidx;rc_hs="col";
					}
				}
			}*/	
		}
	}
	//Store the submission in the selected cell
	
	if(hs>0){
		//alert(rc_hs+" r="+m_hs+" c="+n_hs+", submission="+submission+" ok="+ok+" score="+score);
		for(var sidx=0;sidx<submission.length;sidx++){
			if(rc_hs=="row"){
				//if(submission.charAt(sidx)=="-"){cells[m_hs][n_hs+sidx].firstChild.src="images/dash.jpeg";}
				//else{
				    //cells[m_hs][n_hs+sidx].firstChild.src="images/"+submission.charAt(sidx)+".jpeg";
				    cells[m_hs][n_hs+sidx].innerHTML=submission.charAt(sidx);
				//}
				cells[m_hs][n_hs+sidx].str=submission.charAt(sidx);
				cells[m_hs][n_hs+sidx].AMROW=1;
				if(sidx==0){
					cells[m_hs][n_hs+sidx].AMHEAD=1;
					cells[m_hs][n_hs+sidx].AMROWHEAD=1;
					cells[m_hs][n_hs+sidx].ROW_WORD=submission;
					//alert("row "+m_hs+","+n_hs+" "+submission+" "+score);
				}
			}
			else{
				//if(submission.charAt(sidx)=="-"){cells[m_hs+sidx][n_hs].firstChild.src="images/dash.jpeg";}
				//else{
				    //cells[m_hs+sidx][n_hs].firstChild.src="images/"+submission.charAt(sidx)+".jpeg";
				    cells[m_hs+sidx][n_hs].innerHTML=submission.charAt(sidx);
				//}
				cells[m_hs+sidx][n_hs].str=submission.charAt(sidx);
				cells[m_hs+sidx][n_hs].AMCOL=1;
				if(sidx==0){
					cells[m_hs+sidx][n_hs].AMHEAD=1;
					cells[m_hs+sidx][n_hs].AMCOLHEAD=1;
					cells[m_hs+sidx][n_hs].COL_WORD=submission;
					//alert("col "+m_hs+","+n_hs+" "+submission+" "+score);
				}
			}
			//if(rc_hs=="row")board.set_row_head(m_hs,n_hs,len(submission));
			//else:board.set_col_head(m_hs,n_hs,len(submission));
		}
		return(1);
	}
	//if(really_stuck>0){
	//}
	return(submission);
}

function localize1(submission,really_stuck){
	//next: localize remaining...
	var hs=0;
	var m_hs=0;
	var n_hs=0;
	var rc_hs="row";
	var ok=0;
	var score;
	var dummy;
	//rows
	//alert(submission+": "+submission.length);
	for(var midx=0;midx<rows.length;midx++){
		for(var nidx=0;nidx<cells[0].length-submission.length;nidx++){
			//m,n candidate row head
			ok=1;
			score=0;
			
			if(nidx+submission.length>cells[0].length){ok=0;continue;}
			for(var sidx=0;sidx<submission.length;sidx++){
				if(cells[midx][nidx+sidx].str=="blank")dummy=99;
				else{
					if(cells[midx][nidx+sidx].str==submission.charAt(sidx))score=score+1;
					else ok=0;
				}
			}
			//print midx,nidx,ok,score
			if(ok==1 && score<submission.length){
				if(really_stuck>0){score++;}
				if(score>hs){
					if(check4conflict(submission,"row",midx,nidx)){
						hs=score;m_hs=midx;n_hs=nidx;rc_hs="row";
					}
				}
			}	
		}
	}
	//cols
	for(var midx=0;midx<rows.length-submission.length;midx++){
		if(midx+submission.length>rows.length)continue;
		for(var nidx=0;nidx<NCOL;nidx++){
			ok=1
			score=0
			for(var sidx=0;sidx<submission.length;sidx++){
				if(cells[midx+sidx][nidx].str=="blank")dummy=99;
				else{
					if(cells[midx+sidx][nidx].str==submission.charAt(sidx))score=score+1;
					else ok=0;
				}
			}
			//print midx,nidx,ok,score
			if(ok==1 && score<submission.length){
				if(really_stuck>0){score++;}
				if(score>hs){
					if(check4conflict(submission,"col",midx,nidx)){
						hs=score;m_hs=midx;n_hs=nidx;rc_hs="col";
					}
				}
			}	
		}
	}
	
	if(hs>0){
		//alert(rc_hs+" r="+m_hs+" c="+n_hs+", submission="+submission+" ok="+ok+" score="+score);
		for(var sidx=0;sidx<submission.length;sidx++){
			if(rc_hs=="row"){
				if(submission.charAt(sidx)=="-"){cells[m_hs][n_hs+sidx].firstChild.src="images/dash.jpeg";}
				else{
				    //cells[m_hs][n_hs+sidx].firstChild.src="images/"+submission.charAt(sidx)+".jpeg";
				    cells[m_hs][n_hs+sidx].innerHTML=submission.charAt(sidx);
				}
				cells[m_hs][n_hs+sidx].str=submission.charAt(sidx);
				cells[m_hs][n_hs+sidx].AMROW=1;
				if(sidx==0){
					cells[m_hs][n_hs+sidx].AMHEAD=1;
					cells[m_hs][n_hs+sidx].AMROWHEAD=1;
					cells[m_hs][n_hs+sidx].ROW_WORD=submission;
					//alert("row "+m_hs+","+n_hs+" "+submission+" "+score);
				}
			}
			else{
				if(submission.charAt(sidx)=="-"){cells[m_hs+sidx][n_hs].firstChild.src="images/dash.jpeg";}
				else{
				    //cells[m_hs+sidx][n_hs].firstChild.src="images/"+submission.charAt(sidx)+".jpeg";
				    cells[m_hs+sidx][n_hs].innerHTML=submission.charAt(sidx);
				}
				cells[m_hs+sidx][n_hs].str=submission.charAt(sidx);
				cells[m_hs+sidx][n_hs].AMCOL=1;
				if(sidx==0){
					cells[m_hs+sidx][n_hs].AMHEAD=1;
					cells[m_hs+sidx][n_hs].AMCOLHEAD=1;
					cells[m_hs+sidx][n_hs].COL_WORD=submission;
					//alert("col "+m_hs+","+n_hs+" "+submission+" "+score);
				}
			}
			//if(rc_hs=="row")board.set_row_head(m_hs,n_hs,len(submission));
			//else:board.set_col_head(m_hs,n_hs,len(submission));
		}
		return(1);
	}
	//if(really_stuck>0){
	//}
	return(submission);
}	
function check4conflict(submission,rcval,m,n){
	ok=1
	if(rcval=="row"){
		//check rows above and below;
		//above
		if(m>0){
			for(nidx=n;nidx<n+submission.length;nidx++){
				if(cells[m-1][nidx].str=="blank")dummy=99;
				else{
					if(cells[m][nidx].str==submission.charAt(nidx-n))dummy=99;
					else ok=0;
				}
			}
		}
		//below
		if(m<rows.length-1){
			for(nidx=n;nidx<n+submission.length;nidx++){
				if(cells[m+1][nidx].str=="blank")dummy=99;
				else{
					if(cells[m][nidx].str==submission.charAt(nidx-n))dummy=99;
					else ok=0;
				}
			}
		}
		//check row endpoints
		if(n>0){
			if(cells[m][n-1].str=="blank")dummy=99;
			else ok=0;
		}
		if(n+submission.length<cells[0].length){
			if(cells[m][n+submission.length].str=="blank")dummy=99;
			else ok=0;
		}
	}

	else if(rcval=="col"){
		//check cols left and right;
		//left	//left,up,down failed!
		if(n>0){
			for(midx=m;midx<m+submission.length;midx++){
				if(cells[midx][n-1].str=="blank")dummy=99;
				else{
					if(cells[midx][n].str==submission.charAt(midx-m))dummy=99;
					else ok=0;
				}
			}
		}
		//right
		if(n<cells[0].length-1){
			for(midx=m;midx<m+submission.length;midx++){
				if(cells[midx][n+1].str=="blank")dummy=99;
				else{
					if(cells[midx][n].str==submission.charAt(midx-m))dummy=99;
					else ok=0;
				}
			}
		}	
		//check col endpoints
		if(m>0){
			if(cells[m-1][n].str=="blank")dummy=99;
			else ok=0;
		}
		if(m+submission.length<rows.length){
			if(cells[m+submission.length][n].str=="blank")dummy=99;
			else ok=0;
		}
	}
	return(ok);
}
function clear_out_hints(){
	while(down_list.length>0){down_list.shift();}
	while(across_list.length>0){across_list.shift();}
	try{HintDiv.removeChild(HintTableDiv);}
	catch(ex){dummy=99;}
	delete(HintTableDiv);
}
function generate_puzzle(){
	
	down_counter=1;
	across_counter=1;
	//clear_out_hints();
	
	for(var midx=0;midx<rows.length;midx++){
		for(var nidx=0;nidx<cells[0].length;nidx++){
			
			if(cells[midx][nidx].AMHEAD==1){
				
				if((cells[midx][nidx].AMROWHEAD==1) && (cells[midx][nidx].AMCOLHEAD==1)){//corner_head
					if(across_counter>=down_counter){
						down_counter=across_counter;
						pcells[midx][nidx].firstChild.src="images/"+parseInt(across_counter)+".jpeg";
						//pcells[midx][nidx].innerHTML='<img src="images/'+parseInt(across_counter)+'.jpeg" /><input type="text" style="height:22px;width:25px;" size="1">';
						pcells[midx][nidx].innerHTML='<div style="height:30px;position:inherit;"><span class="cwSpan">' + across_counter + '</span><input id="' + midx + '_' + nidx + '" onkeypress="enableSubmit();" type="text" class="cwInput" border="0" maxlength="1" size="1"></div>';
						across_list.push(parseInt(across_counter)+". "+get_hint_by_key(cells[midx][nidx].ROW_WORD));
						down_list.push(parseInt(down_counter)+". "+get_hint_by_key(cells[midx][nidx].COL_WORD));
						across_counter++;
						down_counter++
						//alert("section 1:\n"+across_list+"\n"+down_list);
					}
					else{//corner_head
						across_counter=down_counter;
						//pcells[midx][nidx].firstChild.src="images/"+parseInt(down_counter)+".jpeg";
						pcells[midx][nidx].innerHTML='<div style="height:30px;position:inherit;"><span class="cwSpan">' + down_counter + '</span><input id="' + midx + '_' + nidx + '" onkeypress="enableSubmit();" type="text" class="cwInput" border="0" maxlength="1" size="1"></div>';
						across_list.push(parseInt(across_counter)+". "+get_hint_by_key(cells[midx][nidx].ROW_WORD));
						down_list.push(parseInt(down_counter)+". "+get_hint_by_key(cells[midx][nidx].COL_WORD));
						down_counter++;
						across_counter++;
						//alert("section 2:\n"+across_list+"\n"+down_list);
					}
				}
				else{
					if(cells[midx][nidx].AMROWHEAD){//just_row_head
						//pcells[midx][nidx].firstChild.src="images/"+parseInt(across_counter)+".jpeg";
						//pcells[midx][nidx].innerHTML='<img src="images/'+parseInt(across_counter)+'.jpeg" style="z-index:10;" /><input type="text" style="height:22px;width:25px;" size="1">';
						pcells[midx][nidx].innerHTML='<div style="height:30px;position:inherit;"><span class="cwSpan">' + across_counter + '</span><input id="' + midx + '_' + nidx + '" onkeypress="enableSubmit();" type="text" class="cwInput" border="0" maxlength="1" size="1"></div>';
						//pcells[midx][nidx].append("T");
						across_list.push(parseInt(across_counter)+". "+get_hint_by_key(cells[midx][nidx].ROW_WORD));
						across_counter++;
						//alert("section 3:\n"+across_list+"\n"+down_list);
					}
					else{//just_col_head
						//pcells[midx][nidx].firstChild.src="images/"+parseInt(down_counter)+".jpeg";
						//pcells[midx][nidx].innerHTML='<img src="images/'+parseInt(down_counter)+'.jpeg" style="z-index:10;" /><input type="text" style="height:22px;width:25px;" size="1">';;
						pcells[midx][nidx].innerHTML='<div style="height:30px;position:inherit;"><span class="cwSpan">' + down_counter + '</span><input id="' + midx + '_' + nidx + '" onkeypress="enableSubmit();" type="text" class="cwInput" border="0" maxlength="1" size="1"></div>';
						down_list.push(parseInt(down_counter)+". "+get_hint_by_key(cells[midx][nidx].COL_WORD));
						down_counter++;
						//alert("section 4:\n"+across_list+"\n"+down_list);
					}
				}
			}
			else{//AMHEAD==0
				if((cells[midx][nidx].AMROW==1) || (cells[midx][nidx].AMCOL)){
					//pcells[midx][nidx].firstChild.src="images/white.jpeg";
					//pcells[midx][nidx].innerHTML='<input type="text" style="height:22px;width:25px;top:0px;left:0px;" border="0" size="1">';
					//pcells[midx][nidx].innerHTML='<div style="position:relative;top:0px;left:0px;background-color:#FF0000;"><span class="cwSpan">1</span><input type="text" class="cwInput" border="0" size="1"></div>';
					//pcells[midx][nidx].innerHTML='<span class="cwSpan">1</span><input type="text" class="cwInput" border="0" size="1">';
					pcells[midx][nidx].innerHTML='<div style="height:30px;position:inherit;"><input id="' + midx + '_' + nidx + '" onkeypress="enableSubmit();" type="text" class="cwInput" border="0" maxlength="1" size="1"></div>';
					
				}
			}
		}
	}

//alert(across_list.length+","+down_list.length);
//alert(across_list);
//alert(down_list);
}

function enableSubmit(){
    $("#btnSubmit").attr("disabled",false); 
}
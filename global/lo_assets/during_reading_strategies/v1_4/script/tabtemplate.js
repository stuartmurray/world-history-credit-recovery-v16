//***************Dependent js*******************
// 1. Utility.js
// 2. 
//*********************************************

function CreateTabTemplate(contentNode, sHTML){

    GBL_itemID = 0;             
    //alert(sHTML);
    contentNode.children(0).each(function(index){
        if(index>0){
            GBL_levelID = parseInt(GBL_levelID)+ 1;
            GBL_itemID = 0;
            sHTML=ReprocessMarkup($(this),sHTML);
            //alert("Full:" + sHTML);
        }
        //alert(sHTML);
        //alert('index'+index);
        //alert($(GBL_Template_Multi).outerHTML());
        GBL_fieldID = 0;
        GBL_itemID +=1;
        
        var divMulti=document.createElement('div');
        $(divMulti).html(sHTML);
        /*if(sMultiAction=='innerhtml'){
            $(divMulti).find("[multiple='true']").find('.child').each(function(index){
                //$(this).parent().append($(oDynamicItem).children('div:eq(' + index + ')'));
                //$(this).parent().append($(oDynamicItem).find('div:nth-child(0)'));
                $(this).parent().append($(divTempMulti).children().get(0));
                
            });
        } else{
        
        }*/
        sHTML = sHTML.replace(GenerateDelimiter('pagetitle'), $(this).attr('Title'));
        
        GBL_fieldID += 1;
        var pageContent=GetSubTemplate($(this).attr('TemplateID'));
        //alert(pageContent);
        //$(this).find("Body Level").children(0).each(function(index){
        $(this).children(0).each(function(index){
            var sFullTabPageID = index;          
            var FieldID=index+1;
            switch (this.tagName){
                case "Text":                             
                    pageContent=pageContent.replace(GenerateDelimiterforTabContent(FieldID,"content"),RenderTabTemplateText($(this),sFullTabPageID));
                    break;
		        case "Media":       
		            pageContent=pageContent.replace(GenerateDelimiterforTabContent(FieldID,"filename") , RenderTabTemplateMedia($(this),sFullTabPageID) );
			        break;
            }
        });
        //sHTML = sHTML.replace(GenerateDelimiter('tabcontent'), pageContent);
        //alert(pageContent);
        sHTML = sHTML.replace(GenerateDelimiter('tabcontent'), pageContent);
       //alert(sHTML); 
    });
return sHTML;
}



function GenerateDelimiterforTabContent(fieldid,dbfieldname){   
    return '<!--{level' + 1 + '_group' + 1 + '_item' + 1 + '_field' + fieldid + '_' + dbfieldname + '}-->';
}

function RenderTabTemplateText(contentNode, sParentID){
	var eq='';
	
	eq = eq + '<div class="scrollParent">';
	eq = eq + '<div id="divScroll' + sParentID + '" >';
	eq = eq + formatText(contentNode);
	eq = eq + '</div>';
	eq = eq + '</div>';
	setiScroll('divScroll' + sParentID);

	return eq;
}

function RenderTabTemplateMedia(contentNode, sParentID){

	var media='';
	var fName = contentNode.attr("Src");
	if(fName != undefined){
	    var ext = fName.split('.').pop().toLowerCase();
	    if(currObject != ''){
		    fName = currObject + '/' + fName;
	    }
	    switch(ext){
		    case "mov":
		    case "mp4":
		    case "m4v":
			    if(ext=="mov" && isTouchDevice==true) fName=fName.substring(0,fName.lastIndexOf(".")+1) + "mp4";
			    
			    var wd = $("#" + sParentID).width() ;
			    var ht = $("#" + sParentID).height();
			    var vTag;
			    if(isTouchDevice){
			        vTag='<video width="' + wd + 'px" height="' + ht + 'px" controls src="' + fName + '" type="video/mp4"></video>'
			    } else{
			        vTag='<embed width="' + wd + 'px" height="' + ht + 'px" controls="controls" src="' + fName + '" type="video/' + ext + '"></embed>'
			    }
			    media = '<div><div class="imgBorder" style="height:' + ht + 'px; width:' + wd + 'px;">' + vTag + '</div></div>';
			    break;
			case "swf":
			    media='<object height="100%" width="100%" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">';
			    media = media + '<param value="' + fName + '" name="movie">';
			    media = media + '<param value="high" name="quality"><embed height="100%" width="100%" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" ';
			    media = media + ' src="' + fName + '"></object>';

			    break;
		    default:
			    media = RenderImage(contentNode);	
			    break;
	    }
	    
	    return media;
	}
}

    
function showTabContent(tabIndex,currPage){
    /*var parentContainer = $(thisElement).closest(".PageContainer");    
    var ElementIndex = $(thisElement).index()+1;
    //removing template selector
    $(parentContainer).find(".tab_templatecontent").remove();
    
    $(parentContainer).find(".taborange").removeClass("tabactive");    
    $(parentContainer).find(".taborange:nth-child("+ElementIndex+")").addClass("tabactive"); 
    $(parentContainer).find(".tab_content").hide();
    $(parentContainer).find(".tab_content:nth-child("+ElementIndex+")").show(); 
    
     var tabID;*/
        $(currPage).find(".tabheader").removeClass("TabActive");
        //$(currPage).find("#liTabMenu_"+tabIndex).addClass("tabactive"); 
        $(currPage).find(".tabheader").eq(tabIndex-1).addClass("TabActive"); 
        $(currPage).find(".tab_content").hide();
        $(currPage).find(".tab_content").eq(tabIndex-1).show();  
}

/*var glossary =[{text:"foo", definition: "The quick brown fox jumps over the lazy dog."},
			   {text:"bar", definition: "ABCDEFG"}];*/
var glossary='';
/*var glossary ={"gloss":[{text:"foo", definition: "The quick brown fox jumps over the lazy dog."},
			   {text:"bar", definition: "ABCDEFG"}]}; */
//var glossary;
currObject = '';

var sort_by = function(field, reverse, primer){ 
    reverse = (reverse) ? -1 : 1; 
    return function(a,b){ 
       a = a[field]; 
       b = b[field]; 
 
 		if (typeof(primer) != 'undefined'){ 
           a = primer(a); 
           b = primer(b); 
       } 
 
       if (a<b) return reverse * -1; 
       if (a>b) return reverse * 1; 
       return 0; 
 
   } 
}

$(document).ready(function(){
	//parseGlossaryXML();
	//alert(glossary.gloss[0].text);
	//alert(glossary.text)
	//$.extend(glossary, {text:"a", definition:" message"});
	//glossary.push({text:"a", definition:" message"});
	//glossary.push({text:"a", definition:" message"});
	//glossary=$.unique(glossary);
	//alert(a['new']);
	//parseGlossary('');
//	var b=a.sort();
	// Sort by price high to low 
	//glossary.sort(sort_by('price', true, parseInt)); 
 
	// Sort by city, case-insensitive, A-Z 
	//glossary.sort(sort_by('text', false, function(a){return a.toUpperCase()})); 
	//glossary.sort();
	//alert($.inArray("foo",glossary[].text))
	//alert($.grep(glossary, function(n) { return n.text == "bar"; })[0].definition);
	//alert($.each(glossary, function(n) { if (n.text == "bar") return n; }).definition);
	/*$.each(glossary, function(n){
				alert(this.text);
				if(this.text=='foo'){
					
					//return n;
				}
				return false ;
	});*/
	//alert($.inArray(glossary, function(n) { return n.text == "foo"; }));
	//alert(glossary[0].text);
});

function parseGlossaryXML(){
	if(glossary==''){
		if (currObject==''){		
	        parseGlossaryContentXML(GlossaryXMLContent);
	    }
	    else{
		    parseGlossary(currObject);
		}
	} else{
		DisplayGlossary('A');
	}
}

function parseGlossary(xmlFolder){
	xmlFolder='LO1';
	var	xmlFileName='';
	if(xmlFolder != ''){
		xmlFileName = xmlFolder + '/';
	}
	xmlFileName = xmlFileName + 'glossary.xml';
	$.ajax({
		async: false,
		type: "GET",
		url: xmlFileName,
		dataType: "xml",
		success: parseGlossaryContentXML
	  });
}

function parseGlossaryContentXML(xml){
	//$(xml).find('term').attr('text');
	//alert($(xml).find('term').length);
	//glossary.push({text:"a", definition:" message"});
	//alert($(xml).find('letter'))
	//alert($(xml).serializeArray());
	//alert($(xml).find('letter').length);
	//alert($(xml).find('letter').serialize());
	/*$(xml).find('letter').each(function(){
		
	});*/
	//letter = $(xml).find('letter');
	glossary = xml;
	DisplayGlossary('A');
}

function DisplayGlossary(alpha){
	//alert(alpha);
	//$('#divGlossary').find('#divContent').html(alpha);
	
	//$('#divContent').html('hi');
	//alert($(glossary).find('letter').length);
	//alert("inside glossary");
	var data='';
	var content=[{term:'',definition:''}];
	$(glossary).find('letter[text=' + alpha + ']').children(0).each(function(){
		content.push({term:$(this).attr('text'), definition:$(this).children(0).text()});
		//alert($(this).attr('text'));
	});
	//alert(content);
	content.splice(0,1);
	content.sort(sort_by('term', false, function(a){return a.toUpperCase()}));
	$(content).each(function(){
		data = data + '<b>' + this.term + '</b>' + '<br />' + this.definition + '<br /><br />';
	});
	if($(content).length==0){
		data = '<b>No term available</b>';
	}
	$('#divContent').html(data);
}
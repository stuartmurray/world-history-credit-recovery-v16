﻿/* =====================================================================================
Package Track data of Object21 - for SCORM maipulation
by Rajendran

dependency:
Jquery.js
======================================================================================== */

var Object21 = {};  //namespace

Object21.Utility = { // relavent utilitys
    ModelBox:{Type:"confirm",OnYesClick:function(){},OnNoClick:function(){},OnOkClick:function(){}}
};

Object21.Trace = {}; // for internal execution log


// ------------------------------------------------------------------------- //
// --- Object21.Package functions -------------------------------------------- //
// ------------------------------------------------------------------------- //

Object21.Package = { // all packeage needs
    ObjectCount:0,
    PageCount: 0,
    InteractionCount: 0,
    ObjectVisit:null,
    PageVisit:null,
    InteractionAttend: null,
    InteractionSuccess: null,
    Scorm: {isAvailable:false,Version:null},
    Time: {StartTime:null, EndTime:null, SessionTime:function(){}},
    Score: {Min:50, Max:100, Raw:function(){return 0;},Scaled:null},
    Current:{Object:null,PageNumber:0},
    Delimiter:";",
    Display:{SetTheme:function(){}, SetSkin:function(){}, SetResolution:function(){}},
    QueryString:{ObjectID:0,ObjectType:0,NewSkin:"",NewTheme:""},
    PreviewMode:{isCourse:false,isObject:false,isWebservice:false},
    Device:{IsTouch:false,IsWeb:false}
};


/* --------------------------------------------------------------------------------
   Object21.Package.Init
   
   All variable intialization

   Parameters: none
   Returns:    N/A
   Dependency: N/A
----------------------------------------------------------------------------------- */
Object21.Package.Init= function(){
    this.ObjectVisit = new Array(this.ObjectCount);
    this.PageVisit=new Array(this.PageCount);
    this.InteractionAttend=new Array(this.InteractionCount);
    this.InteractionSuccess=new Array();
};


/* --------------------------------------------------------------------------------
   Object21.Package.Evaluate
   This function is to evaluate Scorm.isAvailable, Scorm.Version, QueryString.ObjectID
   QueryString.ObjectType, Display.NewSkin, Display.NewTheme, PreviewMode.isCourse, 
   PreviewMode.isObject, PreviewMode.isWebservice
   
   Parameters: none
   Returns:    N/A
   Dependency: imsmanifest.xml, coursemap.xml, LO.xml, Query string, 
----------------------------------------------------------------------------------- */
Object21.Package.Evaluate=function(){
    var oManifest, sVersion;

    oManifest = Object21.Utility.GetXml("imsmanifest.xml");
    if(oManifest){
        this.Scorm.isAvailable=true;
        if ($(oManifest).find("manifest metadata schemaversion").length>0){
            sVersion = $(oManifest).find("manifest metadata schemaversion").text();
            if (sVersion.indexOf("2004")!=-1) this.Scorm.Version="2004";
            else this.Scorm.Version="1.2";
        }
        else{
            this.Scorm.Version="1.2";
        }
    }
    else{
         this.Scorm.isAvailable=false;
    }
 
    if ((navigator.platform == 'iPhone') || (navigator.platform == 'iPod') || (navigator.platform == 'iPad')) {
         this.Device.isTouch = true;   
    }
    else{
        this.Device.isWeb = true;   
    }
};

/* --------------------------------------------------------------------------------
   Object21.Package.isCompleted
   This function is to validate learner completion 
   
   Parameters: none
   Returns:    N/A
   Dependency: N/A
----------------------------------------------------------------------------------- */
Object21.Package.isCompleted = function() {
    if(Object21.Utility.IsFull(this.ObjectVisit)){ // object complete
        if (Object21.Utility.IsFull(this.PageVisit)){ //page complete
            if (this.InteractionCount==this.InteractionAttend.length){ // interaction with in page complete   
                return true;
            }
        }
    }
    return false;
}; 




Object21.Package.Display.SetResolution = function(){
    if(screen.width==800){
        $('body').addClass('Zoom800');
        if($.browser.msie){
            $('.topStrip').addClass('Zoom800');
            $('.titleStrip').addClass('Zoom800');
        }
    } else{
        $('body').removeClass('Zoom800');
        if($.browser.msie){
            $('.topStrip').removeClass('Zoom800');
            $('.titleStrip').removeClass('Zoom800');
        }
    }
};

Object21.Package.SetVisit=function(iIndex,value,filter){
    switch (filter){
        case "object":
            this.ObjectVisit[iIndex] = value;
            break;
        case "page":
            this.PageVisit[iIndex] = value;
            break;
        case "interaction":
            if ($.inArray(value,this.InteractionAttend)==-1){
                this.InteractionAttend.push(value);
            }
            break;
    }
};


Object21.Package.Time.SessionTime=function(){
    var oStartTime = Package.Time.StartTime;
    if (oStartTime!=null){
        Package.Time.EndTime = new Date();
        var oNowTime = Package.Time.EndTime;
        
        var StartH = oStartTime.getHours();
        var StartM = oStartTime.getMinutes();
        var StartS = oStartTime.getSeconds();
       
        var NowH = oNowTime.getHours();
        var NowM = oNowTime.getMinutes();
        var NowS = oNowTime.getSeconds();

        var ElapsedH = NowH - StartH;
        var ElapsedM = NowM - StartM;
        var ElapsedS = NowS - StartS;
          
        if (ElapsedH < 10) ElapsedH = "0" + ElapsedH; 
        if (ElapsedM < 10) ElapsedM = "0" + ElapsedM;
        if (ElapsedS < 10) ElapsedS = "0" + ElapsedS;
        
        var timeSpan = ElapsedH + ":" + ElapsedM + ":" + ElapsedS;
        
        return timeSpan;
    }
};

Object21.Package.Score.Raw=function(){
    /*var iEachScore, iTotalSuccess;
    
    iEachScore = 100/Package.InteractionCount;
    if (Package.InteractionSuccess==null) iTotalSuccess = 0;
    else iTotalSuccess=Package.InteractionSuccess.length;
    
    return parseInt(iTotalSuccess * iEachScore);*/
    return 0;
};

Object21.Package.SuspendData=function(){
    //return Package.Current.Object + Package.Delimiter + Package.Current.PageNumber;
    return location.href;
};

// --- Shortcut! ---------------------------------------------------------- //

var Package = Object21.Package;

// ---  Object21.Package fuctions end ---------------------------------------- //





// ------------------------------------------------------------------------- //
// --- Object21.Trace fuctions       --------------------------------------- //
// ------------------------------------------------------------------------- //

Object21.Trace.Msg=function(msg){
	var debugText = document.getElementById("debugText");
	if(debugText){
		debugText.innerHTML += msg + "<br/>";
	}
	//Can also show data using pipwerks.UTILS.trace
	pipwerks.UTILS.trace(msg);
};

Object21.Trace.Show=function(msg){
	$(".Trace").removeClass("hide");
};

Object21.Trace.Hide=function(msg){
	$(".Trace").addClass("hide");
};


// --- Shortcut! ---------------------------------------------------------- //

var Trace = Object21.Trace;

// ---  Object21.Trace fuctions end ---------------------------------------- //





// ------------------------------------------------------------------------- //
// --- Object21.Utility fuctions       --------------------------------------- //
// ------------------------------------------------------------------------- //

Object21.Utility.IsFull=function(oArray){
    var iCnt=0;
    if (oArray!=null){
        for (var iIndex in oArray){
            iCnt ++;
        }
        if (iCnt==oArray.length) return true;
        else return false;
    }
    else return false;
};

Object21.Utility.GetXml=function(sFilePath){
    var oData;
    $.ajax({
        async: false,
        type: "GET",
        url: sFilePath,
        dataType: (jQuery.browser.msie) ? 'text' : 'xml',
        success: function(data){
            oData=ConvertToSupportableFormat(data);
        },
        error: function(){
            oData=null;
        }
      });
      
      return oData;
};

Object21.Utility.GetHtml=function(sFilePath){
    var oData;
    $.ajax({
        async: false,
        type: "GET",
        url: sFilePath,
        dataType: 'html',
        success: function(data){
            oData=data;
        },
        error: function(){
            oData=null;
        }
      });
      
      return oData;
};

Object21.Utility.ModelBox.Init=function(){
    var sModelBox = Object21.Utility.GetHtml("skin/ModelBox.html");

    $("body").append(sModelBox);
    $(".ModelBack").hide();
    $(".ModelBox").hide();
     $("#ModelControlYes").click(function(){
        Object21.Utility.ModelBox.OnYesClick();
     });
     $("#ModelControlNo").click(function(){
        Object21.Utility.ModelBox.OnNoClick();
     });
     $("#ModelControlOk").click(function(){
        Object21.Utility.ModelBox.OnOkClick();
     });
     $(".ModelClose").click(function(){
        Object21.Utility.ModelBox.Hide();
     });
};

Object21.Utility.ModelBox.Show=function(Title,Content,Comments,Type){
    var iTop, iLeft;
    $(".ModelTitle").html(Title);
    $(".ModelContent").html(Content);
    $(".ModelComments").html(Comments);
    
    iTop =(($(window).height() -$(".ModelBox").height())/2) + "px";
    iLeft =(($(window).width() - $(".ModelBox").width())/2) + "px";
    
    switch (Type){
        case "alert":
            $("#ModelControlYes").hide();
            $("#ModelControlNo").hide();
            $("#ModelControlOk").show();
            break;
        case "confirm": 
            $("#ModelControlYes").show();
            $("#ModelControlNo").show();
            $("#ModelControlOk").hide();
            break;
    }
    $(".ModelBack").show();
    $(".ModelBox").show();
    $(".ModelBox").css({"top":iTop,"left":iLeft});
};

Object21.Utility.ModelBox.Hide=function(){
    $(".ModelBack").hide();
    $(".ModelBox").hide();
};

Object21.Utility.ModelBox.Dispose=function(){
    $(".ModelBack").remove();
    $(".ModelBox").remove();
};


Object21.Utility.IsExist=function(sFilePath){
     var iSuccess;
    $.ajax({
        async: false,
        type: "GET",
        url: sFilePath,
        dataType: (jQuery.browser.msie) ? 'text' : 'xml',
        success: function(data){
            iSuccess=true;
        },
        error: function(){
           iSuccess=false;
        }
      });
      
      return iSuccess;
};

// --- Shortcut! ---------------------------------------------------------- //

var Utility = Object21.Utility;

// ---  Object21.Utility fuctions end ---------------------------------------- //


 
 
 
// ------------------------------------------------------------------------- //
// --- window load/unload fuctions       --------------------------------------- //
// ------------------------------------------------------------------------- //
 

window.onunload = function(){
    if(Package.Scorm.isAvailable){
	    if(Package.isCompleted()){
            Score();
            SuccessStatus();
            Complete("completed");
        }
        else{
            Location();
            SuspendData();
            Complete("incomplete");
        }

        SessionTime();

	    Trace.Msg("Terminating connection.");
	    var callSucceeded = scorm.quit();
	    Trace.Msg("Call succeeded? " + callSucceeded);
	}
}


//function InitCoursePackage(){
//    // Package is Global valiable
//    Package.ObjectCount=GBLObjectSequence.length;
//    Package.PageCount=totalPageCount;
//    Package.InteractionCount=TotalInteractionCnt;
//    Package.Time.StartTime = new Date();
//    Package.Init();
//}

//function InitObjectPackage(){
//    // Package is Global valiable
//    Package.ObjectCount=1;
//    Package.PageCount=pgSize;
//    Package.InteractionCount=TotalInteractionCnt;
//    Package.Time.StartTime = new Date();
//    Package.Init();
//}

// ---  window fuctions end ---------------------------------------- //
var GBL_CurrMedia;

// JScript File

	function loadAccordin(CurrPage){	    
	    //alert(CurrPage);
        if ($(CurrPage).find(".AccordinTemplate").length>0){
            $(CurrPage).find('.AccordinTemplate').removeClass('selected').removeClass('ui-accordion').each(function(){
                    $(this).find('.selected').removeClass('selected');
                    $(this).find('.accord-tab_content').removeAttr('style');
                    //alert('inside control');
                    $(this).accordion({
                        header: '.headone',
                        collapsible: false,
	                    autoHeight: false
                    });
                 });   
        }
        
      
	}
/*Tab Page*/
function tabInitialize(CurrPage){
    if ($(CurrPage).find(".AccordinTemplate").length>0){
            /*$(CurrPage).find('.AccordinTemplate').removeClass('selected').removeClass('ui-accordion').each(function(){
                    $(this).find('.selected').removeClass('selected');
                    $(this).find('.accord-tab_content').removeAttr('style');
                    //alert('inside control');
                    $(this).accordion({
                        header: '.headone',
                        collapsible: false,
	                    autoHeight: false
                    });
                 });*/
                 
             $(CurrPage).find('.AccordinTemplate').removeClass('selected').removeClass('ui-accordion').each(function(){
                    $('.AccordinTemplate').find('.selected').removeClass('selected');
                    $('.AccordinTemplate').find('.accord-tab_content').removeAttr('style');
                    //alert('inside control');
                    $('.AccordinTemplate').accordion({
                        header: '.headone',
                        autoheight:false
                    });
                    
                    
              });       
    } else{
        $(CurrPage).find('.tabheader').each(function(index){
            $(this).unbind('click').bind('click',function(){
                showTabContent(index+1,CurrPage);
            });
        });
        showTabContent(1,CurrPage);
    }
    
    
}
/*function showTabContent(tabIndex){
        alert(tabIndex);
        var tabID;
        $(".taborange").removeClass("tabactive");
        $("#liTabMenu_"+tabID).addClass("tabactive"); 
        $(".tab_content").hide();
        $("#tab_"+tabID).show();  
}*/
/*Tab Page End*/	
/* Slideshow */

function IntializeSlideShow(currPage){
    var UploadDelay = 6500; //millisecond
    setSlideshowControls(currPage);
    setInterval( "slideSwitch('" + currPage + "')", UploadDelay );
}

function slideSwitch(currPage) {
    $(currPage).find('.slideshow').each(function(){
    var sStatus=$(this).parent().find('.FCSlideStatus').html();
        if(sStatus=='play'){
         
            var $active = $(this).find('img.active');
            if ( $active.length == 0 ) $active = $(this).find('img:last');
            
            var imgIndex = $(this).find('img').index($active);            
            var totalImgIndex = $(this).find('img').length;
            var PrevImgIndex = totalImgIndex -1;
            imgIndex =imgIndex+1;
            if (PrevImgIndex==imgIndex){
                $(this).parent().find('.FCSlideStatus').html('restart');
                $(this).parent().find('.FCRestartBtn').removeClass('hide');
                $(this).parent().find('.FCPauseBtn').addClass('hide');
                $(this).parent().find('.FCPlayBtn').addClass('hide');                              
            }
            if ((totalImgIndex==imgIndex) && ($(this).parent().find('.FCSlideStatus').html()== 'restart')  ){                
                return;
            }
            var $next =  $active.next().length ? $active.next() : $(this).find('img:first');
            $active.addClass('last-active');
            $next.css({opacity: 0.0})
                .addClass('active')
                .animate({opacity: 1.0}, 0, function() {
                    $active.removeClass('active last-active');
                }); 
        }
        else if ((sStatus=='previous') || (sStatus=='next')) {
              $(this).parent().find('.FCSlideStatus').html('play');          
        }
        
    });
}

function setSlideshowControls(currPage){
    var divContent = '<div class="FCSlideShowToolbar" style="left:340px; position:absolute; height:24px; width:140px; border:solid 0px red; top:275px; z-index:2000; cursor:pointer">';
     
    divContent += '<img onclick="NextSlideShow(this);" class="fr mgRight5 FCNextBtn" src="../../../genimage/theme/template_image/btn_next.png" title="Next" alt="Next" />';
   
    divContent += '<div class="hide FCSlideStatus">play</div>';
    divContent += '<img onclick="PlaySlideShow(this);" class="fr mgRight5 FCPlayBtn hide" src="../../../genimage/theme/template_image/btn_play.png" title="Play" alt="Play" />';
    divContent += '<img onclick="PauseSlideShow(this);" class="fr mgRight5 FCPauseBtn" src="../../../genimage/theme/template_image/btn_pause.png" title="Pause" alt="Pause" />';
    divContent += '<img onclick="ReplaySlideShow(this);" class="fr mgRight5 FCRestartBtn hide" src="../../../genimage/theme/template_image/btn_replay.png" title="Replay" alt="Replay" />';
     divContent += '<img onclick="PreviousSlideShow(this);" class="fr mgRight5 FCPreviousBtn" src="../../../genimage/theme/template_image/btn_previous.png" title="Previous" alt="Previous" />';
    divContent += '</div>';
    
    $(currPage).find('.slideshow').each(function(){
        $(this).parent().append(divContent);
    });
}

function PlaySlideShow(obj){
    $(obj).parent().find('.FCSlideStatus').html('play');
    $(obj).parent().find('.FCPauseBtn').removeClass('hide');
    $(obj).parent().find('.FCPlayBtn').addClass('hide');
}

function PauseSlideShow(obj){
    $(obj).parent().find('.FCSlideStatus').html('pause');
    $(obj).parent().find('.FCPlayBtn').removeClass('hide');
    $(obj).parent().find('.FCPauseBtn').addClass('hide');
}

function ReplaySlideShow(obj){
    $(obj).parent().parent().find('.slideshow').find('img.active').removeClass('active');
    $(obj).parent().parent().find('.slideshow').find('img:first').addClass('active');
    $(obj).parent().find('.FCSlideStatus').html('play');
    $(obj).parent().find('.FCPlayBtn').addClass('hide');
    $(obj).parent().find('.FCPauseBtn').removeClass('hide');
    $(obj).parent().find('.FCRestartBtn').addClass('hide');
}

function NextSlideShow(obj){
    var objSlideShow = $(obj).parent().parent().find('.slideshow');
    var $active = $(objSlideShow).find('img.active');
    if ( $active.length == 0 ) $active = $(objSlideShow).find('img:last');
    
    var imgIndex = $(objSlideShow).find('img').index($active);            
    var totalImgIndex = $(objSlideShow).find('img').length;
    var PrevImgIndex = totalImgIndex -1;
    imgIndex =imgIndex+1;
    if (PrevImgIndex==imgIndex){
        $(objSlideShow).parent().find('.FCSlideStatus').html('restart');
        $(objSlideShow).parent().find('.FCRestartBtn').removeClass('hide');
        $(objSlideShow).parent().find('.FCPauseBtn').addClass('hide');
        $(objSlideShow).parent().find('.FCPlayBtn').addClass('hide');                              
    }
    else{
        $(objSlideShow).parent().find('.FCSlideStatus').html('next');
        $(objSlideShow).parent().find('.FCRestartBtn').addClass('hide');
        $(objSlideShow).parent().find('.FCPauseBtn').removeClass('hide');
        $(objSlideShow).parent().find('.FCPlayBtn').addClass('hide'); 
    }
    var $next =  $active.next().length ? $active.next() : $(objSlideShow).find('img:first');
    $active.addClass('last-active');
    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 0, function() {
            $active.removeClass('active last-active');
        }); 
    
}

function PreviousSlideShow(obj){
    $(obj).parent().find('.FCSlideStatus').html('previous');
    
    var objSlideShow = $(obj).parent().parent().find('.slideshow');
    var $active = $(objSlideShow).find('img.active');
    if ( $active.length == 0 ) { $active = $(objSlideShow).find('img:first');}

    var imgIndex = $(objSlideShow).find('img').index($active);            
    var totalImgIndex = $(objSlideShow).find('img').length;
    var PrevImgIndex = totalImgIndex -1;
    //imgIndex =imgIndex+1;
    if (imgIndex==0){
        $(objSlideShow).parent().find('.FCSlideStatus').html('restart');
        $(objSlideShow).parent().find('.FCRestartBtn').removeClass('hide');
        $(objSlideShow).parent().find('.FCPauseBtn').addClass('hide');
        $(objSlideShow).parent().find('.FCPlayBtn').addClass('hide');                              
    }
    else{
        $(objSlideShow).parent().find('.FCSlideStatus').html('next');
        $(objSlideShow).parent().find('.FCRestartBtn').addClass('hide');
        $(objSlideShow).parent().find('.FCPauseBtn').removeClass('hide');
        $(objSlideShow).parent().find('.FCPlayBtn').addClass('hide'); 
    }

    var $previous =  $active.prev().length ? $active.prev() : $(objSlideShow).find('img:last');
    $active.addClass('last-active');
    $previous.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 0, function() {
            $active.removeClass('active last-active');
        }); 
}

var RandomUploadedImages=new Array();
var curindex=0;
function PopulateSlideShow(contentNode){
   var sFileNames, UploadDelay, sFilepath;
   var iCnt=0;  
    try{                 
        RandomUploadedImages=new Array();
        contentNode.find('Media').each(function(){
            var fName='';
            fName = $(this).attr('Src');
            if(currObject != ''){
                fName = currObject + '/' + fName;
            }
            RandomUploadedImages[iCnt] =  fName;
            iCnt = iCnt +1;
        });
        
        //alert(RandomUploadedImages[0])
        //setInterval("RotateSlideShowUploadedImage()",UploadDelay);
        
        /*if ($("#Field3_SlideShow_FileName_Path").val()!=undefined && $("#Field3_SlideShow_FileName_Path").val()!=''){
            sFilepath = $("#Field3_SlideShow_FileName_Path").val();
        }
        if ($("#Field3_SlideShow_FileName").val()!=undefined && $("#Field3_SlideShow_FileName").val()!= ''){
            sFileNames = $("#Field3_SlideShow_FileName").val().split(",")
        }
        if (sFileNames != undefined){
            UploadDelay = 5000; //millisecond
            RandomUploadedImages=new Array();
            iCnt=0; 
            for (var iCnt =0; iCnt<sFileNames.length; iCnt++){
                RandomUploadedImages[iCnt] =  "../" + sFilepath + sFileNames[iCnt]
            }
            setInterval("RotateSlideShowUploadedImage()",UploadDelay);
        }*/
         
    }
    catch(err){ }// this to catch ,if slide show template is not selected
}
  

function RotateSlideShowUploadedImage()
{
    var tempindex;
    if (curindex==(tempindex=Math.floor(Math.random()*(RandomUploadedImages.length)))){
        curindex=curindex==0? 1 : curindex-1;
    }
    else {
          curindex=tempindex;
    }
    //alert(RandomUploadedImages[curindex]);
    //$("#SlideShowPlaceHolderImg").attr("src",RandomUploadedImages[curindex]);
    $(".SlideShowPlaceHolder").attr("src",RandomUploadedImages[curindex]);
}
/* Slideshow End */

/*#### Click reveal start*/
function ToggleClickReveal(obj){
    if ($(obj).closest(".ClickReveal").children("div.ClickReveal-body").css("display") != "none"){
        $(obj).closest(".ClickReveal").children("div.ClickReveal-body").hide();
        $(obj).removeClass("ClickReveal-up").addClass("ClickReveal-down");
        $(obj).closest(".ClickReveal").find(".ClickRevealMarkActive").removeClass("ClickRevealMarkActive").addClass("ClickRevealMark");
         $(obj).closest(".ClickReveal").find(".ClickRevealLeftActive").removeClass("ClickRevealLeftActive").addClass("ClickRevealLeft");
        $(obj).closest(".ClickReveal").find(".ClickRevealRightActive").removeClass("ClickRevealRightActive").addClass("ClickRevealRight");

    }
    else{
        $(obj).closest(".ClickReveal").children("div.ClickReveal-body").fadeIn("slow", "linear", function(){
                $(this).show();
        });
        $(obj).addClass("ClickReveal-up").removeClass("ClickReveal-down");
        $(obj).closest(".ClickReveal").find(".ClickRevealMark").addClass("ClickRevealMarkActive").removeClass("ClickRevealMark");
       
        $(obj).closest(".ClickReveal").find(".ClickRevealLeft").removeClass("ClickRevealLeft").addClass("ClickRevealLeftActive");
        $(obj).closest(".ClickReveal").find(".ClickRevealRight").removeClass("ClickRevealRight").addClass("ClickRevealRightActive");
    }
}
        
function IntializeClickReveal(){
    try{
        var sDefaultSelect='';        
        $('.CRTemplateSeleted').each(function(){
            sDefaultSelect = $.trim($(this).text());
            $(this).closest('.ClickReveal').find(".CRText").hide();
            $(this).closest('.ClickReveal').find(".CRTextImage").hide();
            if (sDefaultSelect!=''){
                $(this).closest('.ClickReveal').find("." + sDefaultSelect).addClass("ClickReveal-body");
            }
            else{
                $(this).closest('.ClickReveal').find(".CRText").addClass("ClickReveal-body");
            }
        });
        
                
    }
    catch(err){ // this is to handle if above selected element are not present in the current selected template
    }
}
/*#### Click reveal end*/


/* Flash card General start */
function FlashCardFlip(objChild){
    var iWidth;
    var obj=$(objChild).parent().children(".FlashCardFlip");
    iWidth = $(obj).width();
    //sponsorFlip
    var front, back;
    front= $(obj).children("div:visible");
    back= $(obj).children("div:not(:visible)");
    $(front).css("position","absolute");
    $(front).css("z-index","1000");
    $(front).css("left","0px");
    $(back).css("position","absolute");
    $(back).css("z-index","999");
       $(front).animate( {left: (iWidth/2) + "px", width:"10px"}, 100, "linear", function(){
            $(this).css("z-index","999");
            $(this).hide();
            $(back).css("left", (iWidth/2) + "px");
            $(back).css("width","10px");
            $(back).show();
            $(back).animate( {left:"0px", width: iWidth + "px"}, 100, "linear", function(){
                    $(this).css("left","0px");
                    $(this).css("width", iWidth + "px");
                    $(this).css("z-index","1000");
                    $(front).css("left","0px");
                    $(front).css("width", iWidth + "px");
                });
        });
} 

function IntializeFlashCard(){
    $(".FlashCardFlip").click(function(){
        FlashCardFlip($(this));
    });
}
/* Flash card General start */


/* Flash card type2 start */
function FlipFlashCard(objChild){
    var iWidth;
    var obj=$(objChild).parent().children(".sponsor");
    iWidth = $(obj).width();
    //sponsorFlip
    var front, back;
    front= $(obj).children("div:visible");
    back= $(obj).children("div:not(:visible)");
    $(front).css("position","absolute");
    $(front).css("z-index","1000");
    $(front).css("left","0px");
    $(back).css("position","absolute");
    $(back).css("z-index","999");
       $(front).animate( {left: (iWidth/2) + "px", width:"10px"}, 100, "linear", function(){
            $(this).css("z-index","999");
            $(this).hide();
            $(back).css("left", (iWidth/2) + "px");
            $(back).css("width","10px");
            $(back).show();
            $(back).animate( {left:"0px", width: iWidth + "px"}, 100, "linear", function(){
                    $(this).css("left","0px");
                    $(this).css("width", iWidth + "px");
                    $(this).css("z-index","1000");
                    $(front).css("left","0px");
                    $(front).css("width", iWidth + "px");
                });
        });
} 


function IntializeFlashCardType2(){
//    for(i=1;i<=4;i++){
//        if (i>=$('.ddlFlashCardCount').html()){                    
//            $('.sponsorListHolder').children(".fl:nth("+i+")").hide();                    
//        }
//    }
    $(".sponsor").click(function(){
        FlipFlashCard($(this));
    });
}
/* Flash card type2 end */



/* Flash card type1 start */

 function IntializeFlashCardType1(){ 
    hideFlashCard1data();
    var FlashCard1Index;
    try{
    $(".cardSmallBg").click(function(){
        var iHeight=$(".CardHolderBg").height() - 20;
       $("#FlashCard1overlay").css("height",iHeight + "px");
       $("#FlashCard1overlay").show();
	        index=$(".cardSmallBg").index(this)
	        show=$(".displayPanel");
		         $.each(show,function(i){
			        if(index==i){
			          $(".displayPanel:eq("+i+")").show();
			           FlashCard1Index=i;
			           audPlay(i)
			        }
	         })
    });
    $(".closebtn").click(function(){
     hideFlashCard1data();
     audStop(FlashCard1Index)
    })
    }
    catch(err){
    }
 }
 
function hideFlashCard1data(){
  $(".displayPanel,#FlashCard1overlay").hide();
}  
           
    
function audStop(FlashCard1Index){

if(isTouchDevice){
obj=document.getElementsByTagName("audio");
}else{
obj=document.getElementsByTagName("embed");
}

var domClass=obj[FlashCard1Index];
//domClass.pause()
domClass.autostart = false;
domClass.src = domClass.src;
}

function audPlay(i)
{
var domClass;
if(isTouchDevice){
obj=document.getElementsByTagName("audio");
}else{
obj=document.getElementsByTagName("embed");
}

var domClass=obj[i];   
domClass.autostart = true;
domClass.src = domClass.src;
//domClass.load();
//domClass.play();
}
/* Flash card type1 end */


/* slider start */

function IntializeSlider(){
    //alert('IntializeSlider');
    var iMaxSlider;
    $(".sliderParent").each(function(){
        iMaxSlider = $(this).find(".slider").length;
        $(this).find(".slider").hide();
        if (iMaxSlider>1)
            $(this).find(".slider").find(".MaxCount").text(iMaxSlider);
        else
            $(this).find(".slider").find(".intInfo").addClass("hide");
            
        $(this).find(".slider").hide();
        $(this).find(".slider:first").show();
        
        //alert($(this).find(".slider").find('audio').length);
        /*if ($(this).find(".slider").find('audio').length>0){
            $(this).find(".slider").find('audio').unbind('ended')
            $(this).find(".slider").find('audio').bind('ended',function(){
                $(this).closest('.sliderParent').find('.audiooverlay').hide();
            });
        }*/     
    });
    
}


function SliderGo(obj,sNavigation){
   var oSliderParent, iMaxSlider, iCurIndex;
   
   oSliderParent = $(obj).closest(".sliderParent");
   iMaxSlider = $(oSliderParent).find(".slider").length;
   iCurIndex = $(oSliderParent).find(".slider").index($(oSliderParent).find(".slider:visible"));
    
   if (sNavigation=="next"){
        iCurIndex +=1;
   }
   else{
        iCurIndex -=1;
   }
   
   $(oSliderParent).find(".sliderNext").show();
   $(oSliderParent).find(".sliderPrev").show();
   if (iCurIndex==(iMaxSlider-1)){
       $(oSliderParent).find(".sliderNext").hide();
       $(oSliderParent).find(".sliderPrev").show();
   }
   if (iCurIndex<=0){
       $(oSliderParent).find(".sliderNext").show();
       $(oSliderParent).find(".sliderPrev").hide();
   }
   $(oSliderParent).find(".slider:visible").hide();
   $(oSliderParent).find(".slider:eq(" + iCurIndex + ")").show();
 
}

function CheckMyWork_SliderGo(PageID,sNavigation){
   var oSliderParent, iMaxSlider, iCurIndex;
   
   HideCheckMyWorkModalPopUp(PageID);
   //HideCheckMyWorkModalPopUp(PageID);
   
   oSliderParent = $('#page_' + PageID).find(".sliderParent");
   
   iMaxSlider = $(oSliderParent).find(".slider").length;
   iCurIndex = $(oSliderParent).find(".slider").index($(oSliderParent).find(".slider:visible"));

   if (sNavigation=="next"){
        iCurIndex +=1;
   }
   else{
        iCurIndex -=1;
   }
   
   if (iCurIndex==(iMaxSlider)){
     iCurIndex=0;
   }
   if (iCurIndex<=0){
    
   }
   $(oSliderParent).find(".slider:visible").hide();
   $(oSliderParent).find(".slider:eq(" + iCurIndex + ")").show();
   $(oSliderParent).find(".slider:eq(" + iCurIndex + ")").find(".ItemNumber").text(iCurIndex+1);
   
   if($(oSliderParent).find(".slider:visible").find("audio").length>0)
        $(oSliderParent).find(".slider:visible").find("audio").get(0).play();

   //if($(oSliderParent).find(".slider:visible").find("video").length>0)
        //$(oSliderParent).find(".slider:visible").find("video").get(0).play();
   
   if(iCurIndex>0){
        $(oSliderParent).find(".slider:visible").find(".PreviousQuestion").show();
        $(oSliderParent).find(".slider:visible").find(".PreviousQuestionDummy").hide();
   }
   if (iCurIndex==(iMaxSlider-1)){ //For End Of the slide
     //$(oSliderParent).find(".NextQuestionDummy").hide();
     //$(oSliderParent).find(".NextQuestion").hide();
     if($(oSliderParent).find(".slider:visible").find(".NextQuestion").length>0){
         $(oSliderParent).find(".slider:visible").find(".NextQuestioncontainer").hide();
     }
   }
}

function CheckMyWork_ShowRestart(PageID){
    $('#page_' + PageID).find('.restart').bind('click',function(){ 
        //CheckMyWork_SliderGotoFirst(PageID,this);
        SequenceRestart($(this));
        CheckMyWork_SliderGotoFirst(PageID,this);
        $('#page_' + PageID).find('.restart').unbind("click");
    });
}

function CheckMyWork_SliderGotoFirst(PageID,restartBtn){
   HideCheckMyWorkModalPopUp(PageID);   
    
   var oSliderParent, iMaxSlider, iCurIndex;
   oSliderParent = $('#page_' + PageID).find(".sliderParent");
   
   //$(restartBtn).hide();
    
   iCurIndex=0;
   
   //$(oSliderParent).find(".slider:visible").hide();
   //$(oSliderParent).find(".slider:eq(" + iCurIndex + ")").show();
   $(oSliderParent).find(".slider").each(function(){
        $(this).find(".ItemNumber").text(iCurIndex + 1);        
        $(this).find(".NextQuestionDummy").show();
        $(this).find(".NextQuestion").hide();
        $(this).find(".CheckMyWork").show();
        $(this).find("input:text").val('');
        
        $(this).find('.fcImgState').each(function(){
            $(this).attr('src','../../../genimage/theme/template_image/spacer.gif');         
        });    
        
        
        $(this).find(".CheckMyWorkDummy").show();
        $(this).find(".CheckMyWork").hide();
        
        $(this).find('.ChoiceBtnActive').each(function(){
            $(this).find('input').attr('checked',false);    
            $(this).addClass('ChoiceBtn').removeClass('ChoiceBtnActive');              
        });        
        
        $(this).find('.BtnActive').each(function(){
            $(this).removeClass('BtnActive').addClass('Btn');      
        });
        //tryAgainDefault(PageID);
   });
   
   CheckMyWork_SliderGo(PageID,'next');
   tryAgainDefault(PageID);
   $('#page_' + PageID).find('#divValidation_Counter_'+PageID).html('0');
}

function CheckMyWork_SliderGotoFirst_old(PageID,restartBtn){
   var oSliderParent, iMaxSlider, iCurIndex;
   oSliderParent = $('#page_' + PageID).find(".sliderParent");
   
   $(restartBtn).hide();
    
   iCurIndex=0;
   
   $(oSliderParent).find(".slider:visible").hide();
   $(oSliderParent).find(".slider:eq(" + iCurIndex + ")").show();
   $(oSliderParent).find(".slider").each(function(){
        $(this).find(".ItemNumber").text(iCurIndex + 1);
        $(this).find(".NextQuestion").hide();
        $(this).find(".CheckMyWork").show();
        $(this).find("input:text").val('');
        
        $(this).find('.ChoiceBtnActive').each(function(){
            $(this).find('input').attr('checked',false);    
            $(this).addClass('ChoiceBtn').removeClass('ChoiceBtnActive');        
        });        
        
            tryAgainDefault(PageID);
   });
   $('#page_' + PageID).find('#divValidation_Counter_'+PageID).html('0');
}

function OnNewSlideAdd(obj){
    var oSliderParent;
    oSliderParent = $(obj).closest(".sliderParent");
    $(oSliderParent).find(".slider").hide();
    $(oSliderParent).find(".slider:first").show();
    $(oSliderParent).find(".sliderPrev").hide();
    $(oSliderParent).find(".sliderNext").show();
}

/* slider end */

/* Rollover start */
function IntializeRollover(){
    //$(".img-rollover-container").hide();    
    //$(".img-rollover-container:first").show();
}
function showRolloverContentDiv(obj){
    var index=$(obj).parent().parent().children().index($(obj).parent());
    $(obj).closest('.img-rollover-outercontainer').find('.img-rollover-container').children().hide().eq(index).show();
    //$(".img-rollover-container").hide();
    //$(obj).closest(".img-rollover-outercontainer").find(".img-rollover-container").show();
}
/* Rollover 84 end */

/* Slider Slide show start */

function IntializeSliderSlideShow(isRestart){
    $(".sliderSlideShowParent").each(function(){
        $(this).find(".sliderSlideShow").hide();
        $(this).find(".sliderSlideShow:first").show();
        $(this).find(".sliderSlideShow:first").find(".slideShowImage").hide();
        $(this).find(".sliderSlideShow:first").find(".slideShowImage:first").show();
        if(!isRestart){
            $(this).find(".sliderSlideShow").find("audio").each(function(){
               LoadAudio($(this));
            });
        }
        $(this).append('<div style="opacity:0.8;" class="SliderSlideShowStart"><div class="imgStart" onclick="StartSliderSlideShow(this);"></div><div class="text">Start</div></div>');
    });
    
}

function StartSliderSlideShow(obj){
    $(obj).parent().hide();
    if($(obj).closest(".sliderSlideShowParent").find(".SliderSlideShowStart").find(".imgRestart").length>0){ // restart
        IntializeSliderSlideShow(true);
        $(obj).closest(".sliderSlideShowParent").find(".SliderSlideShowStart").hide();
        $(obj).closest(".sliderSlideShowParent").find(".sliderSlideShowPrevDisable").hide();
        $(obj).closest(".sliderSlideShowParent").find(".sliderSlideShowNextDisable").hide();
        $(obj).closest(".sliderSlideShowParent").find(".sliderSlideShowNext").show();
        $(obj).closest(".sliderSlideShowParent").find(".sliderSlideShowPrev").show();
    }
    PlayAudioNow($(obj).closest(".sliderSlideShowParent").find(".sliderSlideShow:first").find(".slideShowImage:first").find("audio"));
    $(obj).closest(".sliderSlideShowParent").find(".sliderSlideShowPrev").hide();
    $(obj).closest(".sliderSlideShowParent").find(".sliderSlideShowPrevDisable").show();
}

function SliderSlideShowGo(obj,sNavigation){
    var oSliderParent, iMaxSlider, iCurIndex, sSrc;

    oSliderParent = $(obj).closest(".sliderSlideShowParent");
    iMaxSlider = $(oSliderParent).find(".sliderSlideShow").length;
    iCurIndex = $(oSliderParent).find(".sliderSlideShow").index($(oSliderParent).find(".sliderSlideShow:visible"));

    if (sNavigation=="next"){
        iCurIndex +=1;
    }
    else{
        iCurIndex -=1;
    }

    $(oSliderParent).find(".sliderSlideShowNext").show();
    $(oSliderParent).find(".sliderSlideShowPrev").show();
    $(oSliderParent).find(".sliderSlideShowNextDisable").hide();
    $(oSliderParent).find(".sliderSlideShowPrevDisable").hide();
    
    if (iCurIndex==(iMaxSlider-1)){
        $(oSliderParent).find(".sliderSlideShowNext").hide();
        $(oSliderParent).find(".sliderSlideShowNextDisable").show();
        $(oSliderParent).find(".sliderSlideShowPrev").show();
    }
    if (iCurIndex<=0){
        $(oSliderParent).find(".sliderSlideShowNext").show();
        $(oSliderParent).find(".sliderSlideShowPrev").hide();
        $(oSliderParent).find(".sliderSlideShowPrevDisable").show();
    }
    $(oSliderParent).find(".sliderSlideShow:visible").hide();
    $(oSliderParent).find(".sliderSlideShow:eq(" + iCurIndex + ")").show();
    PlayAudioNow($(oSliderParent).find(".sliderSlideShow:eq(" + iCurIndex + ")").find(".slideShowImage:first").find("audio"));
    
    if($(oSliderParent).find(".sliderSlideShowPlay").length>0){ // to reset the existing play/pause
        $(oSliderParent).find(".sliderSlideShowPlay").addClass("sliderSlideShowPause").removeClass("sliderSlideShowPlay")
        $(oSliderParent).find(".sliderSlideShowPlay").attr("title","Pause");
    }
    
}

function SliderSlideShowPlay(obj){
    var sAudioID;
    sAudioID = $(obj).closest(".sliderSlideShowParent") .find(".slideShowImage:visible").find("audio").attr("id");
    if($(obj).attr("class").indexOf("sliderSlideShowPlay")!=-1){
        $(obj).addClass("sliderSlideShowPause").removeClass("sliderSlideShowPlay");
        $(obj).attr("title","Pause");
        document.getElementById(sAudioID).play();
    }
    else{
        $(obj).removeClass("sliderSlideShowPause").addClass("sliderSlideShowPlay");
        $(obj).attr("title","Play");
        document.getElementById(sAudioID).pause();
    }
}


function SliderSlideShowAudio(obj){
    var sAudioID;
    sAudioID = $(obj).closest(".sliderSlideShowParent") .find(".slideShowImage:visible").find("audio").attr("id");

    if($(obj).attr("class").indexOf("sliderSlideShowAudioMute")!=-1){
        $(obj).removeClass("sliderSlideShowAudioMute").addClass("sliderSlideShowAudio");
        document.getElementById(sAudioID).muted = false;
        //document.getElementById(sAudioID).volume = 1; 
        //$("#" + sAudioID).attr("muted","false");
        //alert("muted false");
    }
    else{
        $(obj).addClass("sliderSlideShowAudioMute").removeClass("sliderSlideShowAudio");
        document.getElementById(sAudioID).muted = true;
        $(obj).attr("title","Play");
        //document.getElementById(sAudioID).volume = 0; 
        //$("#" + sAudioID).attr("muted","true");
        //alert("muted true");
    }
}

function LoadAudio(obj,isRestart) {
	var oAudio = document.getElementById($(obj).attr("id"));
    if(oAudio){
        oAudio.addEventListener("ended", UnloadAudio, false);
        //oAudio.addEventListener("error", ErrorAudio, false); 
        //oAudio.addEventListener("suspend", SuspendAudio, false); 
        //oAudio.addEventListener("playing", PlayingAudio, false); 
        //oAudio.addEventListener("loadeddata", LoadeddataAudio, false); 
        //oAudio.addEventListener("canplay", canplayAudio, false);
        //oAudio.addEventListener("load", loadAudio1, false);
        oAudio.load();
    }
}		

function RemoveAudio(obj) {
	var oAudio = document.getElementById($(obj).attr("id"));
    if(oAudio){
        oAudio.removeEventListener("ended", UnloadAudio, false);
    }
}		

function PlayAudioNow(obj){
    var oAudio = document.getElementById($(obj).attr("id"));
    oAudio.play();
}

function UnloadAudio() {
    var iCurIndex,oAudio, oNextAudio, oSliderParent, iMaxSlider, iCurParentIndex;
	oAudio = document.getElementById(this.id);
	
	oSliderParent = $(oAudio).closest(".sliderSlideShow");
	iMaxSlider = $(oSliderParent).closest(".sliderSlideShowParent").find(".sliderSlideShow").length;
	iCurParentIndex =$(oSliderParent).closest(".sliderSlideShowParent").find(".sliderSlideShow").index($(oSliderParent).closest(".sliderSlideShowParent").find(".sliderSlideShow:visible"));
	iCurIndex = $(oSliderParent).find(".slideShowImage").index($(oAudio).closest(".slideShowImage"));
	iCurIndex +=1; 
    oNextAudio = $(oSliderParent).find(".slideShowImage:eq(" + iCurIndex + ")").find("audio");
   
	if($(oNextAudio).attr("id")!=undefined){
	    PlayAudioNow(oNextAudio);
	    $(oAudio).closest(".slideShowImage").hide();
	    $(oSliderParent).find(".slideShowImage:eq(" + iCurIndex + ")").show();
	}
	else{
	    if(iCurParentIndex==(iMaxSlider-1)){
	        $(".SliderSlideShowStart").find(".imgStart").removeClass("imgStart").addClass("imgRestart");
	        $(".SliderSlideShowStart").find(".text").html("Restart");
	        $(".SliderSlideShowStart").show();
	    }
	}
	return false;
}


/*
function ErrorAudio(){
    alert("error");
}
function SuspendAudio(){
    alert("SuspendAudio");
}
	
function PlayingAudio(){
    alert("PlayingAudio");
}

function LoadeddataAudio(){
    alert("LoadeddataAudio");
}

function canplayAudio(){
    alert(canplayAudio);
}

function loadAudio1(){
    alert("loadAudio1");
}
*/	

/* Slider Slide show end */
/* ###### Sequence of interacion   ##########*/

function SequenceStart(obj){
    var sPageID = $(obj).closest(".PageContainer").attr("id");    
    SequenceFinish("#" +sPageID);
}

function SequenceRestart(obj){
    $(obj).closest(".Sequence").hide();
    $(obj).closest(".SequenceGroup").find(".Sequence:first").show();
    
    if($(obj).closest(".SequenceGroup").find(".Sequence:visible").find("video").length>0){
        $(obj).closest(".SequenceGroup").find(".Sequence:visible").find("video").get(0).play();
    }
    var sPageID = $(obj).closest(".PageContainer").attr("id").substring(5,$(obj).closest(".PageContainer").attr("id").length);
    resetStateImages(sPageID);
}

function SequenceFinishFromIframe(){
    var sPageID = $(".Sequence:visible").closest(".PageContainer").attr("id");
    SequenceFinish("#" +sPageID);
}

function SequenceFinish(sCurrentPage){
    //alert('SequenceFinish');
    var oNext;
    oNext = $(sCurrentPage).find(".Sequence:visible").next();
    //alert($(oNext).html());
    if($(oNext).html()!='' && $(oNext).html()!=null){
        $(sCurrentPage).find(".Sequence:visible").hide();
        $(oNext).show();
        if ($(oNext).find("video").length>0)
            $(oNext).find("video").get(0).play();
            
        if ($(oNext).find("audio").length>0)
            $(oNext).find("audio").get(0).play();

        if ($(oNext).find(".VoiceInteraction").length>0){
            InitVoiceInteraction(sCurrentPage);
        }
        if ($(oNext).find(".sliderParent").length>0){
            IntializeSlider();
        }
        if ($(oNext).find(".interecationBox").find('.restart').length>0){
            if($(oNext).find(".interecationBox").find('audio').length>0){
               var sPageID = sCurrentPage.substring(6,sCurrentPage.length);
               $(oNext).find(".interecationBox").find('audio').attr("onended","CheckMyWork_ShowRestart(" + sPageID + ");"); 
            }
            else{
                $(oNext).find(".interecationBox").find('.restart').click(function(){
                    SequenceRestart($(this));
                    var sPageID = $(this).closest(".PageContainer").attr("id").substring(5,$(this).closest(".PageContainer").attr("id").length)
                    CheckMyWork_SliderGotoFirst(sPageID,this);
                });
            }
        }
    }
    else{
        if($(sCurrentPage).find(".SequenceRestart").length>0){
            SequenceRestart($(sCurrentPage).find(".SequenceRestart"));
        }else{
            var currSeq;
            /*$(sCurrentPage).find(".SequenceGroup").find("iframe").each(function(){
                $(this).get(0).contentWindow.location.reload();
            });*/ 
            $(sCurrentPage).find(".Sequence").hide();
            currSeq=$(sCurrentPage).find(".SequenceGroup").find(".Sequence:first");
            currSeq.show()
            //InitSequenceNode(currSeq);
            /*if ($(currSeq).find("video").length>0){
                $(currSeq).find("video").get(0).currentTime=0;
                //$(currSeq).find("video").get(0).load();
            }*/
            
                  
        }
    }
}

function InitSequenceNode(sequenceNode){
        if ($(sequenceNode).find("video").length>0)
            $(sequenceNode).find("video").get(0).play();
            
        if ($(sequenceNode).find("audio").length>0)
            $(sequenceNode).find("audio").get(0).play();

        if ($(sequenceNode).find(".VoiceInteraction").length>0){
            InitVoiceInteraction(sCurrentPage);
        }
        if ($(sequenceNode).find(".sliderParent").length>0){
            IntializeSlider();
        }
        if ($(sequenceNode).find(".interecationBox").find('.restart').length>0){
            if($(sequenceNode).find(".interecationBox").find('audio').length>0){
               var sPageID = sCurrentPage.substring(6,sCurrentPage.length);
               $(sequenceNode).find(".interecationBox").find('audio').attr("onended","CheckMyWork_ShowRestart(" + sPageID + ");"); 
            }
            else{
                $(sequenceNode).find(".interecationBox").find('.restart').click(function(){
                    SequenceRestart($(this));
                    var sPageID = $(this).closest(".PageContainer").attr("id").substring(5,$(this).closest(".PageContainer").attr("id").length)
                    CheckMyWork_SliderGotoFirst(sPageID,this);
                });
            }
        }
}

function SingleSequenceBind(sequenceNode){
    //alert('SingleSequenceBind');
    if($(sequenceNode).html()!='' && $(sequenceNode).html()!=null){
        $(sequenceNode).show();
        //alert($(sequenceNode).find("audio").length);
        if ($(sequenceNode).find(".sliderParent").length>0){
            IntializeSlider();
        }
        if ($(sequenceNode).find("video").length>0){
            $(sequenceNode).find("video").get(0).play();
            GBL_CurrMedia=$(sequenceNode).find("video").get(0);
        }
        if ($(sequenceNode).find("audio").length>0){
            //alert($(sequenceNode).find("audio").length);
            //alert($(sequenceNode).find("audio").outerHTML());
           
           if ($(sequenceNode).find(".slider").find('audio').length>0){
                $(sequenceNode).find(".slider").find('audio:first').unbind('ended')
                $(sequenceNode).find(".slider").find('audio:first').bind('ended',function(){
                    //alert('audio ended');
                    $(sequenceNode).find(".slider").closest('.sliderParent').find('.audiooverlay').hide();
                });
            }
            
            GBL_CurrMedia=$(sequenceNode).find("audio:first").get(0);
            $(sequenceNode).find("audio:first").get(0).play();
            $(sequenceNode).find('.audiooverlay').removeClass('hide');
            
            
        }
        if ($(sequenceNode).find(".VoiceInteraction").length>0){
            InitVoiceInteraction(sCurrentPage);
        }
        
        if ($(sequenceNode).find(".interecationBox").find('.restart').length>0){
            $(sequenceNode).find(".interecationBox").find('.restart').click(function(){
                SequenceRestart($(this));
                var sPageID = $(this).closest(".PageContainer").attr("id").substring(5,$(this).closest(".PageContainer").attr("id").length)
                CheckMyWork_SliderGotoFirst(sPageID,this);
            });
        }
        
    }
}

function InitVoiceInteraction(sCurrentPage){
    var iMaxQues;
    $(sCurrentPage).find(".VoiceInteraction").find(".QuestionSet").hide();
    $(sCurrentPage).find(".VoiceInteraction").find(".QuestionSet:first").show();
    iMaxQues=$(sCurrentPage).find(".VoiceInteraction").find(".QuestionSet").length;
    $(sCurrentPage).find(".VoiceInteraction").find(".MaxCount").html(iMaxQues);
    $(sCurrentPage).find(".VoiceInteraction").find(".QuestionSet").each(function(index){
        $(this).find(".ItemNumber").html(index+1);
    });
    
     $(sCurrentPage).find("audio").each(function(){
            $(this).unbind("ended");
            $(this).bind("ended",function(){
               $(sCurrentPage).find(".VoiceInteraction").find(".manAnimation").addClass("manNormal").removeClass("manAnimation");
            });
            $(this).bind("playing",function(){
                $(sCurrentPage).find(".VoiceInteraction").find(".manNormal").addClass("manAnimation").removeClass("manNormal");
            });
            
     });
}
/* ###### Sequence of interacion end ##########*/


function AudioPlayerEnded(obj){
    $(obj).closest(".AudioPlayerControl").find(".replay").show();
    $(obj).closest(".AudioPlayerControl").find(".play").hide();
    $(obj).closest(".AudioPlayerControl").find(".pause").hide();
}

function InitClickRevealExternal(){
    $('.clickreveal').each(function(){
 	    $('.clickreveal').find('.clickrevealimg').find('img').each(function(index){
 	        $(this).click(function(){
 	            var pd;
 	            pd=index*110-10;
 	            $(this).closest('.clickreveal').find('.popupcontainer').show();
 	            $(this).closest('.clickreveal').find('.leftarrow').css('margin-top',pd);
 	            $(this).closest('.clickrevealimg').find('img').each(function(){
 	                $(this).attr('src',$(this).attr('src').replace('_lo.png','.png'));
 	            });
 	            $(this).attr('src',$(this).attr('src').replace('.png','_lo.png'));
 	            
 	            ShowClickRevealContainer(this,index);
 	        }); 	        
 	    });
 	    //$(this).find('.clickrevealimgcontainer').hide().find('video').attr("onended","InfoSliderNext(this)");
 	    $(this).find('.clickrevealimgcontainer').hide()
 	    //alert($(this).find('.clickrevealimgcontainer').find('video').length);
 	    $(this).find('.clickrevealimgcontainer').find('video').attr("onended","InfoSliderNext(this)");
 	});
}

var prevContainer;
function ShowClickRevealContainer(obj,index){
    $(obj).closest('.clickreveal').find('.clickrevealimgcontainer').hide();
    var currContainer=$(obj).closest('.clickreveal').find('.clickrevealimgcontainer').eq(index);
    
    /*$('video').each(function(){
        if(!$(this).get(0).paused){
            $(this).get(0).pause();
        }
    });*/
    
    //alert(currContainer.find('.slider').length);
    currContainer.find('.slider').hide();
    currContainer.find('.slider:first').show();
    //alert(currContainer.find('.slider').length);
    //currContainer.find("video").attr("onended","SequenceFinish('#"+ sPageID +"')");
    currContainer.show();
    
    currContainer.find('.slider:first').find('video').get(0).play();
   
    /*currContainer.find('iframe').each(function(){
        //alert('inside iframe');
        var sSrc=$(this).attr('src');
        //$(this).attr('src')=sSrc;
        $(this).get(0).contentWindow.location.reload();
        //$(this).attr('src')=sSrc;
        //alert(sSrc);
    });*/
    if(prevContainer!=null){
        prevContainer.find("iframe").each(function(){
                src = $(this).attr('src').split('?')[0];
                src = src + '?' + Number(new Date());
                $(this).attr('src',src);
        });
    }
    prevContainer=currContainer;
}
var GBL_refreshIntervalId;
function InfoSliderNext(obj){
    //alert('InfoSliderNext');
    /*$('video').each(function(){
        if(!$(this).get(0).paused){
            $(this).get(0).pause();            
        }
        //$(this).hide();
    });*/
    objNext=$(obj).closest('.slider').next();
    if($(objNext).html()!='' && $(objNext).html()!=null){
        $(obj).closest('.slider').hide();
    
        objNext.show();
    }
    //alert(objNext.find('audio').length);
    //GBL_refreshIntervalId=setInterval(function(){RenderInfoSlider(objNext);}, 101);
    //RenderInfoSlider(objNext);
}

function RenderInfoSlider(oSlider){
    //alert('RenderInfoSlider');
    //clearInterval(GBL_refreshIntervalId);
    if (oSlider.find('audio').length>0){
        //alert('audio play');
        //objNext.find('audio').get(0).load();
        //alert(objNext.find('audio').attr('src'));
        oSlider.find('audio').get(0).play();
    }
}

function InitClickRevealExternalType2(){
var marginTop = 158;
var CurrmarginTop=0;

    if($('.clickRevealExternalType2:first').find('audio').length>0){
        $('.clickRevealExternalType2:first').find('audio:first').load();
        $('.clickRevealExternalType2:first').find('audio:first').get(0).play();
    }
        
    $('.clickRevealExternalType2').each(function(){
        $(this).find('.ExternalVideoPlayer').remove();
        var sPlayer ='';
        sPlayer +='<div class="ExternalVideoPlayer hide">';
        sPlayer +='  <div class="ExternalVideoPlayerclosebtn" onclick="closeVideoDiv(this);"></div>';
        sPlayer +='<div class="clr"></div>';
        sPlayer +='<div class="video"></div>';
        sPlayer +='</div>';
        sPlayer +='';
        
        $(this).append(sPlayer);
    
    
    
        $('.clickRevealExternalType2').find('.clickrevealimg').find('.controlParent').css('z-index',100);
 	    $('.clickRevealExternalType2').find('.clickrevealimg').find('.controlParent').each(function(index){
 	        $(this).find('.visitStatus').remove();
 	        $(this).append('<div class="visitStatus hide">0</div>');

 	        $(this).unbind("click");
 	        $(this).click(function(){
                $(this).closest('.clickRevealExternalType2').find('.visitStatus').each(function(){                   
                    if ($(this).html()=='1'){
                        $(this).parent().find('img').addClass('boxHighlight');
                        $(this).css('opacity','0.8');
                    }
                    else{
                        $(this).parent().parent().removeClass('boxHighlight');
                        $(this).css('opacity','1');                
                    }
                });
 	        
 	            $(this).closest('.clickRevealExternalType2').find('.clickrevealimgcontainer').hide();
 	            $(this).closest('.clickRevealExternalType2').find('.clickrevealimgcontainerRight').hide();
 	            
 	            var currContainer;
 	            currContainer=$(this).closest('.clickRevealExternalType2').find('.clickrevealimgcontainer').eq(index);  
                
                $(currContainer).show();
                $(currContainer).find(".sliderParent").each(function(){
                    $(this).find(".slider").hide();
                    $(this).find(".slider:first").show();
                    $(this).find(".slider:first").find("video").get(0).play();                    
                    $(this).find(".slider").each(function(){
                        if ($(this).find("iframe").length>0){
                            src = $(this).find("iframe").attr('src').split('?')[0];
                            src = src + '?' + Number(new Date());
                            $(this).find("iframe").attr('src',src);
                        }                        
                    });                    
                    $(this).find(".sliderPrev").hide();
                    if ($(this).find(".slider").length==1){
                        $(this).find(".sliderNext").hide();
                    }
                });
                
                CurrmarginTop = ((index-1) * marginTop) + marginTop;                 
                $('.rightcontainer3').find('.rightarrow').removeClass('rightarrow').addClass('leftarrow');
                $('.rightcontainer3').find('.leftarrow').css('margin-top',CurrmarginTop+'px');
                $('.rightcontainer3').find('.box5').removeClass('box5').addClass('box4'); 
                
                $(this).find('.visitStatus').html('1');
                                
 	            return false;
 	        }); 	        
 	    });
 	    
 	    $('.clickRevealExternalType2').find('.clickrevealimgright').find('.controlParent').css('z-index',100);
 	    $('.clickRevealExternalType2').find('.clickrevealimgright').find('.controlParent').each(function(index){
 	        $(this).find('.visitStatus').remove();
 	        $(this).append('<div class="visitStatus hide">0</div>');
 	        $(this).unbind("click");
 	        $(this).click(function(){
                $(this).closest('.clickRevealExternalType2').find('.visitStatus').each(function(){
                    if ($(this).html()=='1'){
                        $(this).parent().find('img').addClass('boxHighlight');
                        $(this).css('opacity','0.8');
                    }
                    else{
                        $(this).parent().parent().removeClass('boxHighlight');
                        $(this).css('opacity','1');                
                    }
                });
 	            $(this).closest('.clickRevealExternalType2').find('.clickrevealimgcontainer').hide();
 	            $(this).closest('.clickRevealExternalType2').find('.clickrevealimgcontainerRight').hide();
 	            
 	            var currContainer;

                currContainer=$(this).closest('.clickRevealExternalType2').find('.clickrevealimgcontainerRight').eq(index);                    
              
                $(currContainer).show();
                $(currContainer).find(".sliderParent").each(function(){
                    $(this).find(".slider").hide();
                    $(this).find(".slider:first").show();
                    $(this).find(".slider:first").find("video").get(0).play();                   
                    $(this).find(".slider").each(function(){
                        if ($(this).find("iframe").length>0){
                            src = $(this).find("iframe").attr('src').split('?')[0];
                            src = src + '?' + Number(new Date());
                            $(this).find("iframe").attr('src',src);
                        }                        
                    });                    
                    $(this).find(".sliderPrev").hide();
                    if ($(this).find(".slider").length==1){
                        $(this).find(".sliderNext").hide();
                    }
                });
                
                CurrmarginTop = ((index-1) * marginTop) + marginTop;                 
                $('.rightcontainer3').find('.leftarrow').removeClass('leftarrow').addClass('rightarrow');
                $('.rightcontainer3').find('.rightarrow').css('margin-top',CurrmarginTop+'px');                
                $('.rightcontainer3').find('.box4').removeClass('box4').addClass('box5');                            
                
                $(this).find('.visitStatus').html('1');
                
 	            return false;
 	        }); 	        
 	    });
 	     	    
 	    IntializeSlider();
 	    
 	    $('.clickRevealExternalType2').find('.clickrevealimgcontainer').each(function(){
 	        $(this).hide();
 	    });
 	    $('.clickRevealExternalType2').find('.clickrevealimgcontainerRight').each(function(){
 	        $(this).hide();
 	    });
 	    
 	    //$('.clickRevealExternalType2').find('.clickrevealimgcontainer:first').show();
 	    
 	    
 	});
}

function closeVideoDiv(obj){
    $(obj).parent().find('video').get(0).pause();
    $(obj).parent().hide();
}

function ClickRevealExternalType2playVideo(videofile){
    var imagefile = videofile.replace('.mp4','.jpg');
    var sPlayer ='';
    sPlayer +='<video controls="controls" preload="metadata" ';
    sPlayer +=' poster="assets/'+imagefile+'" ';
    sPlayer +=' class="pdTop10 mgLeft40" width="712px" height="310px" src="assets/'+videofile+'" type="video/mp4"></video>';    
   
    $('.clickRevealExternalType2:visible').find('.ExternalVideoPlayer').find('.video').html(sPlayer);
    $('.clickRevealExternalType2:visible').find('.ExternalVideoPlayer').show();
    $('.clickRevealExternalType2:visible').find('.ExternalVideoPlayer').find('video').get(0).play();
        
}

function SequenceFinishIFrame(obj){
    var oNext = $('.SequenceDivider:visible').find(".Sequence:visible").next();
    if($(oNext).html()!='' && $(oNext).html()!=null){
        var sPageID = $('.SequenceDivider:visible').closest('.PageContainer').attr('id');
        SequenceFinish("#" +sPageID);
    }
    else{        
        if( $('.Sequence:visible').closest('.SequenceDivider').length>0 ){
            var oCurr = $('.Sequence:visible').closest('.SequenceDivider:visible');
            var oNext = oCurr.next();
            $(oNext).show();        
            $(oNext).find('.Sequence').each(function(){
                $(this).hide();
            });        
            $(oNext).find('.Sequence:first').show();
            oCurr.hide();
        }
    }
}
    
function ClickRevealExternalType3playVideo(videofile){
    $('.PageContainer:visible').remove('.ExternalVideoPlayer');

    var imagefile = videofile.replace('.mp4','.jpg');
    var sPlayer ='';    
    sPlayer +='        <div class="ExternalVideoPlayer" style="display: block;">';
    sPlayer +='            <div class="ExternalVideoPlayerclosebtn" onclick="$(this).parent().hide();">';
    sPlayer +='            </div>';
    //sPlayer +='            <div class="clr"></div>';
    //sPlayer +='            <div class="video">';
    sPlayer +='<video controls="controls" preload="metadata" ';
    sPlayer +=' poster="assets/'+imagefile+'" ';
    sPlayer +=' class="pdTop10 mgLeft40" width="712px" height="310px" src="assets/'+videofile+'" type="video/mp4"></video>';       
    //sPlayer +='            </div>';
    sPlayer +='        </div>';
    
    $('.PageContainer:visible').prepend(sPlayer);   
        
}

function ShowPassage(iSrcIndex){
    //$('.questionsummary').eq(iDestIndex).find('.editcontent').html($('.questionsummary').eq(iSrcIndex).find('.editcontent').html());
    //$('.rightcontainer1').eq(iDestIndex).find('img:first').attr('src',$('.rightcontainer1').eq(iSrcIndex).find('img:first').attr('src'));
    $('.questionsummary:visible').find('.editcontent').html($('.questionsummary').eq(iSrcIndex-1).find('.editcontent').html());
    InitToolTip();
}

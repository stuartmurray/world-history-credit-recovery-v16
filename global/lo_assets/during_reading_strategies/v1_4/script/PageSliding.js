﻿var GBLisNavButton;    //Detect the button/swipe action, since the "after" event is also triggered after page load
var GBLbTouchWipe=true; //To prevent continuous wipe or button action
var GBLisMoveNext=true; //Detect move next or previous
var GBLpreventWipe=false; //Maran:To prevent swipe on drag items
var GBLdivPageHeight;
  function swipeInit(startSlide){   
    GBLisNavButton=false;
    var iSlide=0;
    if (startSlide != undefined){
        iSlide = startSlide;
    }
    if (GBL_CTRL_ModeFlag!=1) GBLdivPageHeight=583;
    else GBLdivPageHeight=540;

    if (isTouchDevice){
        $("#divPage").touchwipe({
            wipeLeft: function() {
                if(!GBLpreventWipe){
                    if(currPageNumber < totalPageCount){
                        GBLpreventWipe=true;
                        SlideItTo($("#divXML"),"next");
                         FixScroll();
                    }
                   
                }
            },
            wipeRight: function() {
                if(!GBLpreventWipe){
                   if(currPageNumber > 1){
                        GBLpreventWipe=true;
                        SlideItTo($("#divXML"),"previous");
                         FixScroll();
                   }
                }
            },
            preventDefaultEvents:true
        });
//      $('#divPage').cycle({
//        timeout: 0,
//        fx: 'scrollHorz',
//        next: '#iNext',
//        prev: '#iPrevious',
//        prevNextClick: prevnext,
//        startingSlide:iSlide,
//        width: '965px',
//        height: GBLdivPageHeight + 'px',   
//        nowrap: true,
//        after: endslide
//        });
        $("#iPrevious").click(function(){
            //SlideItTo($("#divXML"),"previous");
            movePrevious();
             FixScroll();
        });
        
        $("#iNext").click(function(){
                //SlideItTo($("#divXML"),"next");
                 moveNext();
                FixScroll();
        });
    }
    else{
        $("#iPrevious").click(function(){
            movePrevious();
            FixScroll();
            //SlideItTo($("#divXML"),"previous");
        });
        
        $("#iNext").click(function(){
            moveNext();
            FixScroll();
           //$("#divXML").css("overflow","auto");
           // $("#divXML").scrollTop=0;
            //SlideItTo($("#divXML"),"next");
        });
    }
    
}  

function FixScroll(){
    window.scrollTo(0,0);
}
    
/* ALREADY INTALIZED 
if (!$.browser.msie){
    $("#divPage").touchwipe({
        wipeLeft: function() {
            //alert("Left");   
	        if(GBLbTouchWipe){
	            if(currPageNumber < totalPageCount){
	                $("#divPage").cycle("next");
	            }
	        }
	        //moveNext();
	        //swipeDestory();
        },
        wipeRight: function() {
            //alert("Right");
	        if(GBLbTouchWipe){
	            if(currPageNumber > 1){
	                $("#divPage").cycle("prev");
	            }
	        }
	        //movePrevious();
            //swipeDestory();
        },
        preventDefaultEvents:true
    });
}*/
    
 
   
/*
  function swipeDestory(){
    $('#divPage').cycle('destroy');
  }
    
    function endslide(a,b,c,isMoveNext) {
        if(GBLisNavButton==true && GBLbTouchWipe==false){
            if(GBLisMoveNext==true){            
                $("#ulPreviousContent li").html('');
                moveNext();
            }
            else{
                $("#divPage").html('<ul id="ulPreviousContent"><li></li></ul>' + $("#divPage").html());
                $("#ulNextContent li").html('');
                movePrevious();
            }
            //excludeWipe();
            swipeDestory();
            swipeInit(1);
            GBLbTouchWipe=true;
            //$("#test").val($("#divPage").html());
        }
        //alert('end');    
    }
    
    
    function prevnext(a,b,c) {
		
		if(GBLbTouchWipe){
		    //alert(GBLpreventWipe);
		    GBLpreventWipe=false;
		    GBLbTouchWipe =false;
		    GBLisMoveNext = a;
		    GBLisNavButton=true;
		    $("div[id^='divScroll']").each(function(){
	            $(this).css('webkitTransitionDuration','0');
	            $(this).css('webkitTransform','');
	        });
	        $("div[style^='-webkit-mask']").each(function(){
	            $(this).remove();
	        });
	        $("video").remove();
	        //alert("hi");
	        //$("#test").val($('#ulContent').html());
		    
            if (a==true) //move next
            {
                $("#ulPreviousContent").remove();   //remove previouscontent
                $("#ulContent").attr("id","ulPreviousContent");          //rename ulcontent to previouscontent            
                $("#ulNextContent").attr("id","ulContent");        //rename ulNextContent to ulcontent 
                
                $("#divPage").append('<ul id="ulNextContent"><li></li></ul>');       //add ulNextContent       
                
            }
            else{   //move previous
                $("#ulNextContent").remove();   //remove ulNextContent            
                $("#ulContent").attr("id","ulNextContent");          //rename ulcontent to ulNextContent
                $("#ulPreviousContent").attr("id","ulContent");        //rename ulPreviousContent to ulcontent 
                
                //$("#divPage").html('<ul id="ulPreviousContent"><li></li></ul>' + $("#divPage").html());//add ulPreviousContent       
            }
            $("#ulContent").attr('class','pageContainerUL');
            $("#ulNextContent").attr('class','pageContainerNxtPrevUL');
            $("#ulPreviousContent").attr('class','pageContainerNxtPrevUL');
            $("#ulContent li").html('<div class="contentDiv" id="divXML"><div class="LoadingTitle">Loading...</div></div>');
        }
    }
*/

function excludeWipe(){
    //This function is called in the parseContentXml object.js
//    $(".preventWipe").each(function(){
//        //alert("inside exclude wipe");
//        $(this).bind("touchstart", function(event, ui) {
//		    //alert("Inside prevent wipe");
//		    GBLpreventWipe=true;
//        });
//        //this.addEventListener('touchend', onTouchEnd, false); 
//        $(this).bind("touchend", function(event, ui) {
//		    //alert("Inside prevent wipe end");
//		    GBLpreventWipe=false;
//        });
//    });
}


function SlideItTo(obj,sDirection){
    var iTopToMove, iMoveHeight, sTopToMove, sFunctionName;
    
    iTopToMove = $(obj).width();
    iMoveHeight = $(obj).height();
    
    if (sDirection=="previous"){
        sTopToMove =  (iTopToMove + 150);
        sFunctionName = "movePrevious()";
    }
    else{
        sTopToMove = "-" + (iTopToMove + 150);
        sFunctionName = "moveNext()";
    }
    
    $(obj).parent().css({"overflow":"hidden","position":"relative","height": iMoveHeight + "px","width": iTopToMove +"px"});
    $(obj).css({"position":"absolute","width":iTopToMove +"px"});
    
    $(obj).animate({left: sTopToMove + "px"}, 800, "linear", function(){
        $(this).hide();
        if (eval(sFunctionName)){
            $(this).css("left","0px").css("position","static");
            $(this).parent().removeAttr("style");
            $(this).show();
            $(this).css("opacity","0.5");
            $(this).animate({opacity:"1"}, 300, "linear", function(){
                $(this).removeAttr("style");
                 GBLpreventWipe=false;
            });
        }
    });
}
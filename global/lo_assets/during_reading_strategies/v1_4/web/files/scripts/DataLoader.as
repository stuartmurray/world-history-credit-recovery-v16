﻿package scripts{

	import flash.display.*;

	import flash.events.*;

	import flash.net.*;

	import flash.text.*;

	import fl.transitions.*;

	import fl.transitions.easing.*;

	import flash.text.TextFieldAutoSize;

	import flash.display.Bitmap;

	import flash.net.navigateToURL;

	import flash.external.ExternalInterface;

	import flash.system.ApplicationDomain;

	import flash.geom.Rectangle

	import flash.media.*;

	import flash.media.SoundChannel

	import fl.controls.UIScrollBar;

	import flash.net.SharedObject;

	import flash.net.SharedObject;

	import flash.utils.setTimeout

	public class DataLoader extends Sprite {

		public var novel:XML;

		private var soundFlag:Boolean=true

		private var myTweenX:Tween;

		private var leftIncr:Number=0;

		private var rightIncr:Number=0;

		private var sharedObj:SharedObject;

		private var myTweenY:Tween;

		private var urlLoader:URLLoader;

		private var disObj:Object;

		private var data_fill_target_arr:Array= new Array();

		private var caller:Object;

		public var tempswfname:String;

		private var reveal_mc:Sprite;

		private var images_array:Array;

		private var temp_loop:Number=0;

		public var imageCapt:String=''



		private var number,totalButton,totalNumberOfText,enterCount,totalMc:Number;

		public var removeNumber:Number;

		public var currentPopStr:String=new String();

		public var currentPopTitle:String = new String();

		//-------------preload------------

		//private var imageLoader:URLLoader;

		private var imageLoader:Loader;

		private var numberOfNodes:Number;

		private var numberOfImage:Number;

		private var totalImgaes:Number;

		//------------------------------------

		//---------------------------

		private var playerHeight,playerWidth:Number;

		private var videoArr:Array=[];

		private var id=null;

		private var remainingPercent:uint = 0;

		private var urlRequest:URLRequest = null;

		private var waitForInit:Boolean =false;

		private var sound:Sound=new Sound();

		public var soundChannel:SoundChannel;

		

		public var __path:String;

		public var StageOBJ:Object

		private var target:MovieClip;

		var fontCol="#262626"

		var fontFace="Arial"

		var fontSize:Number=1.5;

		var glossaryId:Number=0

		private var XMLObj:Object

		private var __Target:Object

		private var swfpath:String

		public var myScrollBar:UIScrollBar;

		var link;

		//---------------------------

		public function DataLoader(obj:Object,caller):void {

			numberOfImage=-1;

			totalImgaes=0;

			images_array=[];

			this.disObj=obj;

			try{

				

				var swfurl:String=(disObj.loaderInfo.url);

				tempswfname=(disObj.loaderInfo.url);

				var swfname:String=(swfurl.substring(swfurl.lastIndexOf("/")+1));

				swfname=swfname.substring(0,swfname.lastIndexOf("."));

				__path=(swfurl.substring(0,swfurl.lastIndexOf("/")))

				swfpath=(swfurl.substring(0,swfurl.lastIndexOf("/")))+"/xml/"+disObj.parent.parent.parent.parent.xmlName

				urlRequest=new URLRequest(swfpath);

				

				



			init()

			}catch(Err:Error){

			}

			

		}

		public function alreayExisit(){

			for(var i=disObj.numChildren-1;i>=0;i--){

				if(disObj.getChildAt(i).name=='glossary_mc'){

					disObj.removeChild(disObj.getChildAt(i))

				}

			}

		}

		private function CheckExisit(){

			var flag:Boolean=true

			for(var i=disObj.numChildren-1;i>=0;i--){

				if(disObj.getChildAt(i).name=='glossary_mc'){

					//loadGlossaryText()

					//disObj.removeChild(disObj.getChildAt(i))

					flag=false

					break;

				}

			}

			return flag

		}

		private function jquery(Obj:Object,id:Number){

			trace('clicked.........')

			var fileName=String(Obj.glossary.child(id).attribute('jqryImage'));

			var titleName:String=Obj.glossary.child(id).attribute('jqryTitle');

			var type:String=Obj.glossary.child(id).attribute('jqryType');

			

			var __width:Number=Number(Obj.glossary.child(id).attribute('width'))

			var __height:Number=Number(Obj.glossary.child(id).attribute('height'))

			try{

				var printOpt:String=String(Obj.glossary.child(id).attribute('print_btn'))

			}catch(Err:Error){

			}

			

			var keyItem:String=Obj.glossary.child(id)

			var filNameArr:Array=[];

			var titleArr:Array=[];

			filNameArr=fileName.split(',');

			

			

			var sharedObj:SharedObject=SharedObject.getLocal("player","/");

			sharedObj.clear()

			sharedObj.data.fileName=fileName

			sharedObj.flush()

			

			

			sharedObj=SharedObject.getLocal("keyTerms","/");

			sharedObj.clear()

		  	sharedObj.data.keyTerms=fileName

			sharedObj.data.Width=__width

			sharedObj.data.Height=__height

			try{

				sharedObj.data.print_btn=printOpt

			}catch(Error){

			}

			sharedObj.flush()

			

			

			sharedObj.flush()

			titleArr=titleName.split(',');

			var url:String=(__path.substring(0,__path.lastIndexOf("/")))

			url=url+'/assets/jqry/keyterm.swf'

			

			if(type=='imageSwf'){

				var link:String=''

				link=(__path.substring(0,__path.lastIndexOf("/")))

				link=link+'/assets/jqry/swf_file.swf'

				ExternalInterface.call('keyTerms',link,titleArr,__width,__height);

				

			}else{

				

				if(keyItem=='KeyTerms' || titleName=='Key Terms'){

					if(type=='swf'){

						ExternalInterface.call('keyTerms',url,titleArr,__width,__height);

					}else{

						ExternalInterface.call('openImage',filNameArr,titleArr,type);

					}

				}else if(fileName.indexOf('.swf')!=-1){



					var urlArr:String='assets/jqry/online_swf.swf'

					__width=Number(String(titleName).split('**')[1].split('-')[0])

					__height=Number(String(titleName).split('**')[1].split('-')[1])

					if(__width==0){

						__width=870

					}else if(__width==0){

						__height=560

					}else {

						__width=__width

						__height=__height

					}

					sharedObj.data.Width=__width

					sharedObj.data.Height=__height

					sharedObj.flush()

	



					ExternalInterface.call('onlineActivity',urlArr,String(titleName).split('**')[0],__width,__height);

				}else{				

				

					ExternalInterface.call('openImage',filNameArr,titleArr,type);

				}

			}

			

			

			

			

			

			



		}

		private function online(Obj:Object,id:Number){

			

			ExternalInterface.call('online',String(Obj.glossary.child(id)));

			

			

		}

		private function flashPopup(Obj:Object,param:Number){

			

		}

		private function soundComplete(evnt:Event){

			evnt.target.play()

		}

		private function popupJqry(xmlObj:Object,param:Number){

			sharedObj=SharedObject.getLocal("xmlName","/");

			sharedObj.data.param=(param+1)

			sharedObj.flush()

		

			var popupName:String=xmlObj.glossary.child(param).attribute('popupType')

			trace(popupName+'___________popupName')

			var url:String=(__path.substring(0,__path.lastIndexOf("/")))

			if(popupName.split('_')[0]=='jump'){

				var popName:String=String(popupName.split('_')[0]).toLowerCase()

				url=url+'/assets/jqry/'+popName

			}else{

				popupName=String(popupName.split('_')[0])

				url=url+'/assets/jqry/'+popupName.toLowerCase()

			}

			

			var Title:String=xmlObj.glossary.child(param).attribute('jqryTitle')

			ExternalInterface.call('openSwf',url,750,400,Title);

			

		}

		public function loadGlossaryItem(__target:Object,param:Number,xmlObj:Object){

			

			XMLObj=xmlObj

			if(String(xmlObj.glossary.child((param-1)).attribute('type'))=='audio'){

				var channel:SoundChannel = new SoundChannel();

				var urlStr:String=String(xmlObj.glossary.child((param-1)))

				var Paths:String=__path.substring(0,__path.lastIndexOf("/"));

				Paths=Paths+"/swf"+urlStr;

				var URLREQ:URLRequest=new URLRequest(Paths)

				var snd:Sound=new Sound()

				snd.addEventListener(Event.COMPLETE, soundComplete);

				snd.load(URLREQ);

				snd.play();

				

			}else if(String(xmlObj.glossary.child((param-1)).attribute('type'))=='popupJquery'){

				popupJqry(xmlObj,(param-1))

				

			}else if(String(xmlObj.glossary.child((param-1)).attribute('type'))=='popup'){

				var popupName:String=xmlObj.glossary.child((param-1)).attribute('popupType')

				var popupObj:popupDisplay=new popupDisplay(swfpath)

				popupObj.showPopup(popupName,disObj,xmlObj)

			}else if(String(xmlObj.glossary.child((param-1)).attribute('type'))=='jquery'){

				jquery(xmlObj,(param-1))

			}else if(String(xmlObj.glossary.child((param-1)).attribute('type'))=='online'){

				online(xmlObj,(param-1))

			}else if(String(xmlObj.glossary.child((param-1)).attribute('type'))=='glossary'){

				glossaryId=param

				var flag:Boolean=true;

				flag=CheckExisit();

				if(!flag){

				loadGlossaryText()

				}else{

				

					var glos_mc:MovieClip=new MovieClip()

					glos_mc.name='glossary_mc'

					var url:String='assets/popup/glossary.swf'

					var Path:String=__path.substring(0,__path.lastIndexOf("/"));

					

					Path=Path+"/"+url;

					

					var loaderInf:Loader=new Loader()

					var urlReq:URLRequest=new URLRequest(Path);

					loaderInf.load(urlReq);

					loaderInf.contentLoaderInfo.addEventListener(Event.COMPLETE,ImageLoadComplete);

					loaderInf.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);

					glos_mc.addChild(loaderInf)

					glos_mc.x=0

					glos_mc.y=0

					disObj.addChild(glos_mc)

					disObj.setChildIndex(glos_mc,disObj.numChildren-1)

				}

			}

				

			

		}

		private function SoundCompleted(evnt:Event){

			soundFlag=true

			

			__Target.description_mc.aud_btn.alpha=1

			__Target.description_mc.aud_btn.btn.mouseEnabled=true

		}

		private function playAudio(evnt:MouseEvent){

			if(soundFlag){

				soundChannel = sound.play();

				soundFlag=false

				__Target.description_mc.aud_btn.alpha=0.5

				__Target.description_mc.aud_btn.btn.mouseEnabled=false

				

			}

			soundChannel.addEventListener(Event.SOUND_COMPLETE,SoundCompleted)

		}

		private function loadSound() {

			__Target.description_mc.aud_btn.alpha=1

			__Target.description_mc.aud_btn.btn.mouseEnabled=true

			soundFlag=true

			SoundMixer.stopAll();

			var Path:String=__path.substring(0,__path.lastIndexOf("/"));

			var url:String=Path+'/swf/'+disObj.audioName;



			if (disObj.audioName!='') {

				

				var req:URLRequest = new URLRequest(url);

				soundChannel = new SoundChannel();

				sound = new Sound(new URLRequest(url));

						

				__Target.description_mc.aud_btn.visible=true

			}else{

				

				__Target.description_mc.aud_btn.visible=false

			}

			//__Target.description_mc.aud_btn.buttonMode=true

			__Target.description_mc.aud_btn.btn.addEventListener(MouseEvent.CLICK,playAudio)

			__Target.description_mc.aud_btn.btn.addEventListener(MouseEvent.ROLL_OVER,audBtnOver)

			__Target.description_mc.aud_btn.btn.addEventListener(MouseEvent.ROLL_OUT,audBtnOut)

		}

		private function audBtnOver(evnt:MouseEvent){

			evnt.currentTarget.parent.gotoAndStop(2)

		}

		private function audBtnOut(evnt:MouseEvent){

			evnt.currentTarget.parent.gotoAndStop(1)

		}

		private function loadGlossaryText(){

			

			if(myScrollBar!=null){

				myScrollBar.visible=false;

				}

			//try{

				__Target.description_mc.tit_text.htmlText=''

				

				

				__Target.description_mc.body_txt.htmlText=''

				//checkForScroll(__Target.description_mc.body_txt);

				__Target.description_mc.tit_text.htmlText="<b>"+String(XMLObj.glossary.child((glossaryId-1)).attribute('title'))+"<b>"

				__Target.description_mc.body_txt.htmlText=XMLObj.glossary.child((glossaryId-1));

				

				

				

				

				

				disObj.audioName=String(XMLObj.glossary.child((glossaryId-1)).attribute('audio'))

				if (disObj.audioName!='') {

					__Target.description_mc.aud_btn.visible=true

				}else{

				

				 __Target.description_mc.aud_btn.visible=false

			 }

				

				

				__Target.description_mc.body_txt.y=(__Target.description_mc.tit_text.y+__Target.description_mc.tit_text.height)

				__Target.description_mc.body_txt.height=__Target.description_mc._target_mcc.y-(__Target.description_mc.tit_text.y+__Target.description_mc.tit_text.height)

				

				if(__Target.description_mc.tit_text.numLines==1){

					__Target.description_mc.body_txt.y+=10

				}

				

				checkForScroll(__Target.description_mc.body_txt);

			

			loadSound()

			

		}

		private function checkForScroll(txt:TextField) {

						

			myScrollBar = new UIScrollBar();

			myScrollBar.move(txt.x + txt.width, txt.y);

			myScrollBar.height = txt.height

			myScrollBar.scrollTarget = txt;

			__Target.description_mc.addChild(myScrollBar);

			myScrollBar.update();

			

			if(txt.height<txt.textHeight){

				trace('false....')

				myScrollBar.visible = true

			}else{

				

				myScrollBar.visible = false

			}

			myScrollBar.update();

			//myScrollBar.visible = txt.maxScrollV > 1;

			//txt.addEventListener(Event.CHANGE, function():void{   

			//myScrollBar.visible = txt.maxScrollV > 1;}); 

		}

		private function closeWindow(evnt:MouseEvent){

			alreayExisit()

			SoundMixer.stopAll()

			

		}

		private function MouseOver(evnt:MouseEvent){

			evnt.currentTarget.gotoAndStop(2)

		}

		private function MouseOut(evnt:MouseEvent){

			evnt.currentTarget.gotoAndStop(1)

		}

		private function ImageLoadComplete(evnt:Event){

			

			 __Target=(evnt.target.content)

			__Target.parent.parent.x=(disObj.stage.stageWidth/2)-(__Target.parent.parent.width/2)

			__Target.parent.parent.y=(disObj.stage.stageHeight/2)-(__Target.parent.parent.height)

			__Target.description_mc.close_btn.buttonMode=true

			__Target.description_mc.close_btn.addEventListener(MouseEvent.CLICK,closeWindow)

			__Target.description_mc.close_btn.addEventListener(MouseEvent.ROLL_OVER,MouseOver)

			__Target.description_mc.close_btn.addEventListener(MouseEvent.ROLL_OUT,MouseOut)

			__Target.description_mc.drg_btn.useHandCursor=false

			__Target.description_mc.drg_btn.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown)

			__Target.description_mc.drg_btn.addEventListener(MouseEvent.MOUSE_UP,MouseUp)

			disObj.stage.addEventListener(MouseEvent.MOUSE_UP,stageClicked)

			

			loadGlossaryText()

			

			

		}

		private function stageClicked(evnt:MouseEvent){

			__Target.description_mc.stopDrag()

		}

		private function MouseDown(evnt:MouseEvent){

			evnt.currentTarget.parent.startDrag(false,new Rectangle(-310,-75,610,470-evnt.currentTarget.parent.height))

		}

		private function MouseUp(evnt:MouseEvent){

			__Target.description_mc.stopDrag()

		}

		private function ioErrorHandler(evnt:IOErrorEvent){

		}

		private function thumnailLoaded(evnt:Event){

			

			target=MovieClip(evnt.target.content)

			target._holder_mc.buttonMode=true

			target._holder_mc.rollOver_mc.mouseChildren=false

			/*target._holder_mc.addEventListener(MouseEvent.CLICK, clicked);

			target._holder_mc.addEventListener(MouseEvent.ROLL_OVER, imageOver);

			target._holder_mc.addEventListener(MouseEvent.ROLL_OUT, imageOut);*/

			var str:String = parseData(StageOBJ.imageId)

			var url:String=__path+str

			

			var urlReq:URLRequest=new URLRequest(url)

			var loadSwf:Loader=new Loader()

			loadSwf.load(urlReq)

			target._holder_mc.image.addChild(loadSwf)

			target._holder_mc.preloader_mc.visible=true

			loadSwf.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS,imageProgress)

			try {

				disObj.traceTitle(disObj.imageId)

			} catch(err:Error){

			

			}

		}

		public function imageProgress(evnt:Event){

			

			if(evnt.target.bytesLoaded==evnt.target.bytesTotal){

				target._holder_mc.preloader_mc.visible=false

				

					traceLink(disObj.imageId)

				

			}

		}

		public function parseData(num:Number) {

			var results:XMLList =novel.xml.node.(attribute('id') == num.toString());

			var str=results.text();

			return str;

		}

		private function traceLink(id) {

			var idNum;			

			idNum = id;		

			var results:XMLList =disObj.dataobj.novel.xml.node.(attribute('id') == idNum.toString());

			var str:String=results.text();

			link = results.@link;

			if (link != "") {

				if (link =='video') {

					trace('video '+target.videoIcon)

					target.videoIcon.alpha = 0

					target.slideShowIcon.visible = false

					target.activityIcon.visible = false

					target.videoIcon.buttonMode = true

					target.videoIcon.addEventListener(MouseEvent.ROLL_OVER, iconRollOver);

					target.videoIcon.addEventListener(MouseEvent.ROLL_OUT, iconRollOut);

					target.videoIcon.addEventListener(MouseEvent.CLICK, callJqueryWindow);

				} else if (link =='slideShow') {

					trace('slideShow')

					target.videoIcon.visible = false

					target.slideShowIcon.alpha = 0

					target.activityIcon.visible = false

					target.slideShowIcon.buttonMode = true

					target.slideShowIcon.addEventListener(MouseEvent.ROLL_OVER, iconRollOver);

					target.slideShowIcon.addEventListener(MouseEvent.ROLL_OUT, iconRollOut);

					target.slideShowIcon.addEventListener(MouseEvent.CLICK, callJqueryWindow);

				} else if (link =='activity') {

					trace('activity')

					target.videoIcon.visible = false

					target.slideShowIcon.visible = false

					target.activityIcon.alpha = 0

					target.activityIcon.buttonMode = true

					target.activityIcon.addEventListener(MouseEvent.ROLL_OVER, iconRollOver);

					target.activityIcon.addEventListener(MouseEvent.ROLL_OUT, iconRollOut);

					target.activityIcon.addEventListener(MouseEvent.CLICK, callJqueryWindow);

				}

			} else {

				target.videoIcon.visible = false

				target.slideShowIcon.visible = false

				target.activityIcon.visible = false

				target._holder_mc.addEventListener(MouseEvent.CLICK, clicked);

				target._holder_mc.addEventListener(MouseEvent.ROLL_OVER, imageOver);

				target._holder_mc.addEventListener(MouseEvent.ROLL_OUT, imageOut);

			}

		}

		private function callJqueryWindow(event:MouseEvent) {

			var temp:Array=[]

			if (link =='video') {

				ExternalInterface.call("openSwf",disObj.swfName,disObj._wid,disObj._hei);

			} else if (link =='slideShow') {

				trace('cming  hereeeee')

				//var str:String=String(disObj.titleArr)

				//ExternalInterface.call("openSlideShow",disObj.imageArr,'<font size=\"'+2+'\"color=\"'+String(fontCol)+'\"'+'face=\"'+String(fontFace)+'\">'+str+'</font>');	

				ExternalInterface.call("openSlideShow",disObj.imageArr,disObj.titleArr);

//				ExternalInterface.call("openSlideShow",disObj.imageArr,'<font size=\"'+fontSize+'\"color=\"'+String(fontCol)+'\"'+'face=\"'+String(fontFace)+'\">'+disObj.titleArr+'</font>');

			} else if (link =='activity') {

				trace(disObj.swfName+' , '+disObj._wid+' , '+disObj._hei)

				//ExternalInterface.call("openSwf",disObj.swfName,disObj._wid,disObj._hei);

				ExternalInterface.call("openSwfNew",disObj.swfName,disObj._wid,disObj._hei);

				

			}

		}

		private function imageRollOvered(event:MouseEvent){

			event.currentTarget.alpha = 1;

		}

		private function imageRollOut(event:MouseEvent){

				event.currentTarget.alpha = 0;

			}

		public function addImageHolder(__this:Object){

			StageOBJ=__this

			var _holder:MovieClip=new MovieClip()

			

			var url:String=__path+'/images/graphics.swf'

			var urlReq:URLRequest=new URLRequest(url)

			var loadSwf:Loader=new Loader()

			_holder.x=__this.xpos

			_holder.y=__this.ypos

			loadSwf.load(urlReq)

			loadSwf.contentLoaderInfo.addEventListener(Event.COMPLETE,thumnailLoaded)

			_holder.addChild(loadSwf)

			__this.addChild(_holder)

		}

		private function delay(_txtObj:Object){

			

		}

		public function loadCaption(_txtObj:Object){

	

			_txtObj.htmlText=_txtObj.parent.parent.imageCapt

			var id=setTimeout(delay,2,_txtObj)

			if(_txtObj.height>54.9){

				_txtObj.y=(_txtObj.parent.plusSymbol_mc.y-_txtObj.height)-Number(5)

				_txtObj.parent.gray_mc.y=_txtObj.y

				if(_txtObj.parent.parent.imageType=='imageWitoutZoom'){

					_txtObj.parent.plusSymbol_mc.visible=false

				}

				_txtObj.parent.gray_mc.height=(_txtObj.height)+Number(5)

				

			}



		}

		public function iconRollOver(evnt:MouseEvent) {

			evnt.currentTarget.alpha = 1;

		}

		public function iconRollOut(evnt:MouseEvent) {

			evnt.currentTarget.alpha = 0;

		}

		public function imageOver(evnt:MouseEvent) {

			evnt.currentTarget.rollOver_mc.gotoAndStop(3);

		}

		public function imageOut(evnt:MouseEvent) {

			evnt.currentTarget.rollOver_mc.gotoAndStop(1);

		}

		public function clicked(evnt:MouseEvent) {

			trace('thum clicked-------------- '+disObj.titleArr)

			//trace('<font size=\"'+8+'\"color=\"'+String(fontCol)+'\"'+'face=\"'+String(fontFace)+'\">'+'<b>'+disObj.titleArr+'</b>'+'</font>')

			//ExternalInterface.call("openImage",disObj.imageArr,disObj.titleArr)

			ExternalInterface.call("openImage",disObj.imageArr,'<font size=\"'+fontSize+'\"color=\"'+String(fontCol)+'\"'+'face=\"'+String(fontFace)+'\">'+'<b>'+disObj.titleArr+'</b>'+'</font>');



		}

		public function init():void{

			

			urlLoader = new URLLoader();

			urlLoader.addEventListener(Event.COMPLETE, completeListener);

			urlLoader.load(urlRequest);

			}

		private function completeListener(e:Event):void {

			novel=new XML(e.target.data);

			

			numberOfNodes=novel.xml.node.length();

			disObj.play();

			/*var count=0;

			for (var i=0; i<=numberOfNodes; i++) {

				var imgStr=String(novel.xml.child(i));

				if (imgStr.split("/")[1]=="images") {

					count++;

				}

			}

			

			if (count==0) {

				trace('event.complete')

				dispatchEvent(new Event(Event.COMPLETE));

				disObj.play();

			} else {

				disObj.play();

				//calcImages_Node();

				//preloadImages();

			}*/

		}

		private function imageLoadComplete(e:Event) {

			var a:MovieClip= new MovieClip();

			var ll:MovieClip = new MovieClip()

			ll.addChild(e.target.content);

			var temp:MovieClip=ll;

			temp.myId="_content";

			a.addChild(temp);

			images_array.push({no:id,cont:a});

			if (numberOfImage==Number(totalImgaes-1)) {

   			     trace('event.complete')

				dispatchEvent(new Event(Event.COMPLETE));

				disObj.play();

			} else {

				preloadImages();

			}

		}

		private function imageLoadComplete1(ev:ProgressEvent) {

			var percentageForEachComponent=uint(100/Number(totalImgaes));

			var tempVal= ev.bytesLoaded/ev.bytesTotal;

			tempVal=Math.floor(tempVal*(percentageForEachComponent));

			remainingPercent = ((Number(numberOfImage)*(percentageForEachComponent)+tempVal));

			//trace(remainingPercent+'--per')

			dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS,false,false,Number(Math.floor(remainingPercent)),100));

			//disObj.preloader_mc.gotoAndStop((Number(numberOfImage))*(percentageForEachComponent)+tempVal);

		}

		

		private function calcImages_Node() {

			for (var i=0; i<=numberOfNodes; i++) {

				var imgStr=String(novel.xml.child(i));

				if (imgStr.split("/")[1]=="images") {

					totalImgaes++;

				}

			}

		}

		private function preloadImages() {

			try {

			} catch(err:Error) {

				

			}

			var swfpath1:String=tempswfname.substring(0,tempswfname.lastIndexOf("/"));

			for (var i=temp_loop; i<=numberOfNodes; i++) {

				var imgStr=String(novel.xml.child(i));

				id=novel.xml.child(i).@id;



				if (imgStr.split("/")[1]=="images") {

					var swfpath=swfpath1+imgStr;

					var urlRequest:URLRequest=new URLRequest(swfpath);

					imageLoader = new Loader();

					imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);

					imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoadComplete);

					imageLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, imageLoadComplete1);

					imageLoader.load(urlRequest);

					numberOfImage++;

					temp_loop=i+1;

					break;





				}

			}

		}

		//-----------------------------------------------------



		public function getXML(xmlString:String,obj:Object) {

			var str=xmlString;

			var patterns:RegExp=/<img/;

			var swfpath1:String=tempswfname.substring(0,tempswfname.lastIndexOf("/"));

			if (obj.name=="image") {

				var url:String=swfpath1+str;

				var loader=new Loader();

				var URLReq=new URLRequest(url);



				loader.load(URLReq);

				obj.addChild(loader);



			} else {

				if (str.search(patterns)==-1) {

					obj.htmlText=str;

				} else {

					var swfpath2:String=" <img src='"+swfpath1;

					var pattern:RegExp=/<img src='/g;

					str=String(str);

					str=str.replace(pattern,swfpath2);

					obj.htmlText=str;



				}

				str=str;

			}

		}

		public function parseXMLData(num:Number,obj:Object) {

			var results:XMLList =novel.xml.node.(attribute('id') == num.toString());

			var str=results.text();

			getXML(str,obj);

			//obj.selectable=false;

			//obj.mouseEnabled = true;

			//obj.mouseWheelEnabled=false;

			//obj.addEventListener(TextEvent.LINK, Disp_popup);

			//obj.addEventListener(MouseEvent.ROLL_OVER, onRollOverText);

		}

		private function Disp_popup(e:Event) {

           

            /* if(disObj.description_mc!=undefined){

			if (disObj.description_mc.currentFrame==2) {

				disObj.description_mc.myScrollBar.visible=false;

				disObj.description_mc.gotoAndStop(1);

			}}

			

			var temp=e.text.split("_");

			

			

			if (temp[0]=="imagePopUp") {

				imagePopUpId=Number(temp[1]);

				imagepopup();

				

			}else if(String(temp[0])=='pdf'){

				var pdf=e.text.split("_")[1]

				var path=absolutepath+'/pdf/'+pdf

				trace("pdf path"+path);

				ExternalInterface.call("window.open",path,"_blank")

				//var urlRequest:URLRequest=new URLRequest(path);

				//navigateToURL(urlRequest,"_blank");

								

			}else if(String(temp[0])=='audio'){

				audioPlay();

				}

			else {		

				

			    disObj.description_mc.visible=true;

				disObj.description_mc.alpha=1

				disObj.description_mc.gotoAndStop(2);

				var num:Number=Number(e.text);

				var results:XMLList =novel.xml.popup.descr.(attribute('id') == num.toString());

				var str=results.text();

				currentPopStr=str;

				currentPopTitle = results.@title

				if(disObj.test_txt != null)

				{

				   disObj.test_txt.text = disObj;//currentPopStr;

				}

			}*/

			//disObj.description_mc.txt.htmlText=currentPopStr;

		}

		public function imagepopup(){

			/*if (disObj.img_mc!=undefined){

			if (disObj.img_mc.currentFrame>=2) {

				disObj.img_mc.gotoAndStop(1);

			}

			}

			disObj.img_mc.gotoAndStop((imagePopUpId+1));*/

			}

		public function getData(num:String):String{

			var results:XMLList =novel.xml.node.(attribute('id') == num);

			return String(results);

			}

		public function dataPullText(num:Number,obj:MovieClip) {

			

			for (var i=0; i<images_array.length; i++) {

				if ( Number(images_array[i].no) == num) {

					obj.addChild(images_array[i].cont);

					getContentRef(images_array[i].cont).gotoAndPlay(1);

					break;

				}

			}

		}

		function getContentRef(mc:MovieClip):MovieClip {

			for (var i=mc.numChildren-1; i>=0; i--) {

				if (MovieClip(mc.getChildAt(i)).myId =='_content') {

					return(MovieClip(mc.getChildAt(i)));

				}

			}

		return null;	

		}

	}

}


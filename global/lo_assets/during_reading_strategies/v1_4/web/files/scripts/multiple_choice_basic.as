﻿package scripts{

	import flash.events.MouseEvent;

	import flash.text.TextField;

	import flash.text.TextFieldAutoSize;

	import flash.text.TextFormat;

	import flash.events.Event;

	import flash.events.IOErrorEvent;

	import flash.events.ProgressEvent;

	import flash.net.URLLoader;

	import flash.display.Loader;

	import flash.net.URLRequest;

	import flash.external.ExternalInterface;

	import fl.controls.RadioButton;

	import fl.controls.RadioButtonGroup;

	import flash.utils.getDefinitionByName;

	import flash.geom.Rectangle;

	import flash.media.*;

	import scripts.customeEvent;

	public class multiple_choice_basic extends xmlLoader {

		private var currentSound:Sound =null;

		private var soundChannel:SoundChannel;

		private var xmlObj:Object;

		private var stageRef:Object;

		private var numberOf:Number;

		private var answer:Number;

		private var optArr:Array=[];

		private var feedBack:TextField;

		private var _path:String;

		private var temArr:Array=[];

		private var textArr:Array=[];

		//----------------------added new------------------------------------------------

		private var starty:Number;

		private var userSelect:Number;

		private var endy:Number;

		private var clickNum:Number=0;

		private var herlineArray:Array=[];

		private var verlineArray:Array=[];

		private var feedBackArray:Array=[];



		private var arrowArray:Array=[];

		private var optArray:Array=[];

		private var tickArr:Array=[];

		private var yposArr:Array=[];

		private var numberOfAttempt:Number;

		private var tryCount:Number=0;







		private var numberQuestions:Number=1;

		//-------------------------------------------------------------------------------



		private var xmlId:Number;

		private var xmlVariables:XML;

		private var numberOfNodes:Number;

		private var questionCount:Number=0;

		//---------------------------------------------------

		public function multiple_choice_basic(StageRef:Object) {

			stageRef=StageRef;

			var pathObj:getRelativePath=new getRelativePath();

			var swfurl:String=String(stageRef.loaderInfo.url);

			var swfname:String=(swfurl.substring(swfurl.lastIndexOf("/")+1));



			var xmlFileName:String=swfname.split('.')[0];

			_path=pathObj.getPath(stageRef,xmlFileName);

			parseXML(_path,stageRef);

			stageRef.addEventListener(customeEvent.dispatch,assign);



			stageRef.popup_mc.orginX=stageRef.popup_mc.x;

			stageRef.popup_mc.orginY=stageRef.popup_mc.y;

			stageRef.stage.addEventListener(Event.MOUSE_LEAVE,MouseLeave);

			stageRef.nxt_btn.addEventListener(MouseEvent.CLICK,loadQuestion);

		}

		private function MouseLeave(evnt:Event) {

			stageRef.popup_mc.stopDrag();

		}

		private function attachAudio(linkageName) {

			SoundMixer.stopAll();

			var classReference:Class = getDefinitionByName(linkageName) as Class;

			currentSound= new classReference();

			soundChannel=currentSound.play();

		}

		private function loadQuestion(evnt:MouseEvent) {

			SoundMixer.stopAll();

			tryCount=0;

			stageRef.popup_mc.visible=false;

			nxtDisabled();

			questionCount++;

			if (stageRef.txt_mc) {

				stageRef.txt_mc.gotoAndStop('step_'+questionCount);

			}

			numberOfAttempt=Number(xmlObj.questions['question_'+questionCount].option.attribute('numberOfTry'));



			answer=Number(xmlObj.questions['question_'+questionCount].option.attribute('answer'));

			stageRef.title_txt.htmlText=xmlObj.questions['question_'+questionCount].ques;

			stageRef.subTitle_txt.htmlText=xmlObj.questions['question_'+questionCount].subTitle_txt;

			stageRef.title_txt.autoSize=TextFieldAutoSize.LEFT;

			stageRef.title_txt.mouseWheelEnabled=false;

			stageRef.title_txt.htmlText=xmlObj.questions['question_'+questionCount].ques;





			stageRef.title_txt.x=stageRef.subTitle_txt.x;

			stageRef.title_txt.y=stageRef.subTitle_txt.y+stageRef.subTitle_txt.height+Number(10);



			stageRef.back_mc.y=stageRef.title_txt.y+stageRef.title_txt.height;

			numberOf=xmlObj.questions['question_'+questionCount].option.children().length();

			attachOptToStage(xmlObj);

			LoadThumbNail();





		}

		private function nxtEnabled() {

			stageRef.nxt_btn.alpha=1;

			stageRef.nxt_btn.buttonMode=true;

			stageRef.nxt_btn.mouseEnabled=true

			;

		}

		private function nxtDisabled() {

			stageRef.nxt_btn.alpha=0.5;

			stageRef.nxt_btn.buttonMode=false;

			stageRef.nxt_btn.mouseEnabled=false;

		}



		public function assign(evnt:customeEvent) {

			xmlObj=evnt.xmlObj.xml;

			numberQuestions=xmlObj.questions.children().length();

			if (numberQuestions==1) {

				stageRef.nxt_btn.visible=false;

			}

			loadQuestion(null);





		}

		public function EnableCaption(Obj:Object) {

			Obj._txt.htmlText=xmlObj.questions['question_'+questionCount].media.child(0).attribute('caption');

		}

		public function LoadThumbNail() {

			for (var i=stageRef.imageClip.image.numChildren-1; i>=0; i--) {

				stageRef.imageClip.image.removeChild(stageRef.imageClip.image.getChildAt(i));

			}

			try {

				var url:String=xmlObj.questions['question_'+questionCount].media.child(0).attribute('thumNail');

				var Path:String=_path.substring(0,_path.lastIndexOf("/"));

				Path=Path.substring(0,Path.lastIndexOf("/"))+url;



				var urlReq:URLRequest=new URLRequest(Path);

				var loader:Loader=new Loader();

				loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS,progressImage);

				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);



				loader.load(urlReq);

				stageRef.imageClip.image.addChild(loader);

			} catch (Err:Error) {

			}

		}

		private function ioErrorHandler(evnt:IOErrorEvent) {

		}

		private function splitPath() {

			try {

				var Path:String=_path.substring(0,_path.lastIndexOf("/"));

				Path=Path.substring(0,Path.lastIndexOf("/"));

				return Path;

			} catch (Err:Error) {

			}

		}

		private function ThumNailClicked(evnt:MouseEvent) {

			try {

				var path:String=splitPath();

				var fileName:String=String(xmlObj.media.child(0));

				var titleName:String=xmlObj.media.child(0).attribute('jqueryTitle');

				var type:String=xmlObj.media.child(0).attribute('type');

				var filNameArr:Array=[];

				var titleArr:Array=[];

				filNameArr=fileName.split(',');

				titleArr=titleName.split(',');



				ExternalInterface.call('openImage',filNameArr,titleArr,type);

			} catch (Err:Error) {

			}

		}

		private function showCaption(evnt:MouseEvent) {

			try {

				var tpe:String=String(xmlObj.media.child(0).attribute('type'));

				if (tpe=='video') {

					evnt.currentTarget.parent.gotoAndStop('video');

				} else if (tpe=='image') {

					evnt.currentTarget.parent.gotoAndStop('image');

				} else if (tpe=='slideShow') {

					evnt.currentTarget.parent.gotoAndStop('slideShow');

				} else {

					evnt.currentTarget.parent.gotoAndStop('swf');

				}

			} catch (Err:Error) {

			}

		}

		private function hideCaption(evnt:MouseEvent) {

			evnt.currentTarget.parent.gotoAndStop(1);

		}

		private function progressImage(evnt:ProgressEvent) {

			try {

				stageRef.imageClip.preloader_mc.visible=true;

				if (evnt.target.bytesLoaded==evnt.target.bytesTotal) {

					stageRef.imageClip.preloader_mc.visible=false;

				}

			} catch (Err:Error) {

			}

		}

		private function optDisabled() {

			try {

				for (var i=1; i<optArr.length; i++) {

					optArr[i].mouseEnabled=false;



				}

			} catch (Err:Error) {

			}

		}

		private function optEnabled() {

			try {

				for (var i=1; i<optArr.length; i++) {

					optArr[i].mouseEnabled=true;

					if (optArr[i].selected) {

						optArr[i].selected=false;

					}



				}

			} catch (Err:Error) {

			}

		}

		private function CloseWindow(evnt:MouseEvent) {

			SoundMixer.stopAll();

			evnt.currentTarget.parent.visible=false;

			if (tryCount!=numberOfAttempt) {

				if (answer!=userSelect) {

					attachOptToStage(xmlObj);

				}

			}

		}

		private function SetDrag(evnt:MouseEvent) {

			evnt.currentTarget.parent.startDrag(false,new Rectangle(stageRef.Stage_mc.x,stageRef.Stage_mc.y,stageRef.Stage_mc.width-evnt.currentTarget.parent.width,stageRef.Stage_mc.height-evnt.currentTarget.parent.height));

		}

		private function StopDrag(evnt:MouseEvent) {

			evnt.currentTarget.parent.stopDrag();

		}

		private function selectOpt(evnt:MouseEvent) {



			userSelect=evnt.currentTarget.name.split('_')[1];

			clickNum=userSelect;

			stageRef.setChildIndex(stageRef.popup_mc,stageRef.numChildren-1);

			stageRef.popup_mc.visible=true;



			stageRef.popup_mc.x=stageRef.popup_mc.orginX;

			stageRef.popup_mc.y=stageRef.popup_mc.orginY;

			stageRef.popup_mc.exit_btn.buttonMode=true;

			stageRef.popup_mc.exit_btn.addEventListener(MouseEvent.CLICK,CloseWindow);

			stageRef.popup_mc.drag_btn.addEventListener(MouseEvent.MOUSE_DOWN,SetDrag);

			stageRef.popup_mc.drag_btn.addEventListener(MouseEvent.MOUSE_UP,StopDrag);

			optDisabled();





			if (answer==userSelect) {

				tickArr[userSelect].gotoAndStop('right');

				attachAudio('q'+questionCount+'_c');

				stageRef.popup_mc._txt.htmlText=xmlObj.questions['question_'+questionCount].feedback.correct;

				if (numberQuestions>questionCount) {

					nxtEnabled();

				}

			} else {



				tryCount++;

				if (tryCount!=numberOfAttempt) {

					attachAudio('q'+questionCount+'_t');

					tickArr[userSelect].gotoAndStop('wrong');

					stageRef.popup_mc._txt.htmlText=xmlObj.questions['question_'+questionCount].feedback.tryagain;

				} else {

					attachAudio('q'+questionCount+'_i');

					tickArr[userSelect].gotoAndStop('wrong');

					tickArr[answer].gotoAndStop('right');

					if (stageRef.txt_mc) {

						stageRef.txt_mc.mc.gotoAndStop(2);

					}

					stageRef.popup_mc._txt.htmlText=xmlObj.questions['question_'+questionCount].feedback.incorrect;

					if (numberQuestions>questionCount) {

						nxtEnabled();

					}

				}



			}

			stageRef.popup_mc.uiScroll.visible=stageRef.popup_mc._txt.maxScrollV>1;

		}

		private function scrollUpdate() {

			for (var i=1; i<=100; i++) {

				stageRef.scrollPane_mc.update();

			}

		}

		private function reset(param:Number,Obj:Object) {

			var prevObj:Object;



			var distance:Number=30;

			for (var i=(param+1); i<yposArr.length; i++) {

				if (i==(param+1)) {

					optArr[i].y=(Obj.y+Obj.height);

					textArr[i].y=optArr[i].y+Number(3);

					prevObj=optArr[i];

				} else {



					optArr[i].y=(prevObj.y)+distance;

					textArr[i].y=optArr[i].y+Number(3);

					prevObj=optArr[i];

				}

			}

			scrollUpdate();

		}

		private function setTextFORMAT(Obj:Object,ypos:Number,str:String) {

			var backObj:Object;

			for (var i=stageRef.scrollPane_mc.content.numChildren-1; i>=0; i--) {

				if (stageRef.scrollPane_mc.content.getChildAt(i).name=='back_mc') {

					backObj=stageRef.scrollPane_mc.content.getChildAt(i);

				}

				if (stageRef.scrollPane_mc.content.getChildAt(i).name=='feedBack') {

					stageRef.scrollPane_mc.content.removeChild(stageRef.scrollPane_mc.content.getChildAt(i));

				}

			}

			var feedBack:feedback = new feedback();

			var id:Number=Number(Obj.name.split('_')[1]);

			var num:Number=Number(textArr[Obj.name.split('_')[1]]);

			feedBack.x=(textArr[id].x)+(textArr[id].width)+Number(5);

			feedBack.txt.htmlText=str;

			feedBack.txt.autoSize=TextFieldAutoSize.LEFT;



			feedBack.txt.selectable=false;

			feedBack.name="feedBack";

			feedBack.txt.mouseEnabled=false;

			feedBack.txt.mouseWheelEnabled=false;

			stageRef.addChild(feedBack);

			feedBackArray[id-1]=feedBack;

			feedBack.y=(Obj.y)+Number(3);

			stageRef.scrollAction();

			if ((backObj.x+backObj.width)<(feedBack.x+feedBack.width)) {

				feedBack.txt.width=(backObj.x+backObj.width)-feedBack.x;

				feedBack.txt.width-=20;

				feedBack.txt.height=feedBack.txt.textHeight;

			}

			if (feedBack.txt.numLines>2) {

				reset(id,feedBack);

			}

		}

		function resetAlignment() {



			//------------------------------------------newly added-------------------------------------------------

			var startPoint:Number=clickNum-1;

			var optstand:Number=0;

			var fNum:Number=0;



			var nNum:Number=feedBackArray[startPoint].height+ Number(13);

			var sNum:Number=herlineArray[startPoint].height+ Number(13);

			if (sNum>nNum) {

				fNum=sNum;

				stageRef.scrollAction();

			} else {

				fNum=nNum;

				resetSubAlignment(startPoint,fNum);

			}

			//------------------------------------------------------------------------------------------------------



		}

		function resetSubAlignment(startPoint,fNum) {

			var alignFlag:Boolean=true;

			//------------------------------------------------------------------------------------------------------

			for (var i=startPoint; i<numberOf; i++) {



				try {

					if (feedBackArray[i]!=undefined) {

						feedBackArray[i].y=optArray[i].y;

						feedBackArray[i].x=optArray[i].x +  herlineArray[i]._txt.textWidth+50;

						if (feedBackArray[i].y+feedBackArray[i].height>herlineArray[i+1].y) {

							alignFlag=false;

							herlineArray[i+1].y=optArray[i].y +  fNum + Number(2);

							optArray[i+1].y=optArray[i].y +  fNum;

						}

					} else {

						if (!alignFlag) {

							herlineArray[i+1].y=optArray[i].y+herlineArray[i].height+ Number(15);

							optArray[i+1].y=optArray[i].y+herlineArray[i].height+ Number(12);

						}

					}

				} catch (e:Error) {

				}

			}

			stageRef.back_mc.height=stageRef.scrollPane_mc.content.height-stageRef.ques_bg.height;

			stageRef.scrollAction();



			var nGreat:Number=Number(385);

			if (stageRef.back_mc.height>nGreat) {

				trace("---------stageRef.back_mc.height-222--"+stageRef.back_mc.height);

				stageRef.scrollPane_mc.verticalScrollPolicy='on';//added new

			} else {

				stageRef.scrollPane_mc.verticalScrollPolicy='off';//added new

			}

			//------------------------------------------------------------------------------------------------------

		}

		private function attachArrow(Obj:Object) {

			var arw_mc:arrow_mc=new arrow_mc();

			arw_mc.x=Obj.x+Number(2);

			arw_mc.y=Obj.y+(Obj.height/2)-(arw_mc.height/2)-Number(3);

			arw_mc.name="arrowMc";

			stageRef.addChild(arw_mc);

		}

		public function attachOptToStage(Obj:Object) {

			temArr=[];

			var prevAdded:Object;

			for (var i=stageRef.numChildren-1; i>=0; i--) {

				if (stageRef.getChildAt(i).name.split('_')[0]=='opt' || stageRef.getChildAt(i).name.split('_')[0]=='txtMc' || stageRef.getChildAt(i).name.split('_')[0]=='tick' ) {

					stageRef.removeChild(stageRef.getChildAt(i));

				}

			}

			for (i=1; i<=numberOf; i++) {

				var opt:RadioButton = new RadioButton();

				var txtMc:ques_txt=new ques_txt();

				opt.name='opt_'+i;



				if (prevAdded!=null) {

					opt.y=(prevAdded.y+prevAdded.height)+Number(10);

				} else {

					opt.y=stageRef.back_mc.y+Number(15);

				}

				yposArr[i]=opt.y;

				txtMc.name='txtMc_'+i;

				txtMc.y=opt.y+Number(2);

				txtMc._txt.autoSize=TextFieldAutoSize.LEFT;

				txtMc._txt.htmlText=Obj.questions['question_'+questionCount].option.child((i-1));

				txtMc._txt.mouseWheelEnabled=false;

				opt.label = ''

				;

				stageRef.addChild(txtMc);

				stageRef.addChild(opt);

				optArray.push(opt);

				herlineArray.push(txtMc);

				optArr[i]=opt;

				textArr[i]=txtMc;

				prevAdded=txtMc;

				txtMc.y=opt.y+Number(3);





				opt.addEventListener(MouseEvent.CLICK,selectOpt);

				opt.x=stageRef.back_mc.x+Number(10);

				txtMc.x=opt.x+Number(30);

				txtMc.oldY=txtMc.y;



				//------Add tick_mark-----

				var tick:tick_mark=new tick_mark();

				tick.x=txtMc.x-Number(50);

				tick.y=txtMc.y-Number(5);

				tick.gotoAndStop(1);

				tick.name='tick_'+i;

				tickArr[i]=tick;

				stageRef.addChild(tick);

				//--------------------------



			}

			if (stageRef.intro_audio) {

				if (stageRef.intro_audio.visible) {

					stageRef.setChildIndex(stageRef.intro_audio,stageRef.numChildren-1);

				}

			}

		}

	}

}
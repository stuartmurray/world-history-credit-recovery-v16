﻿package scripts{

	import flash.display.*;

	import flash.events.*;

	import flash.net.*;

	import flash.text.*;

	import fl.transitions.*;

	import fl.transitions.easing.*;

	import flash.text.TextFieldAutoSize;

	import flash.net.navigateToURL;

	import flash.external.ExternalInterface;

	public class contentLoader extends Sprite {

		public var novel:XML;

		private var urlLoader:URLLoader;

		private var disObj:Object;

		private var caller:Object;

		public var tempswfname:String;

		private var i;

		private var numberOfNodes;

		private var file_load_obj:Object;

		public function contentLoader(obj:Object,caller:Object):void {

			this.disObj=obj;

			this.caller=caller;

			var swfurl:String=(disObj.loaderInfo.url);

			tempswfname=(disObj.loaderInfo.url);

			var swfname:String=(swfurl.substring(swfurl.lastIndexOf("/")+1));

			swfname=swfname.substring(0,swfname.lastIndexOf("."));

			//var swfpath:String=(swfurl.substring(0,swfurl.lastIndexOf("/")))+"/xml/"+swfname+".xml";

			

			// ______________________________________________________Newly added 06/06/09

			var absolutepath = (swfurl.substring(0, swfurl.lastIndexOf("/")));

			var swfpath:String="";

			if (swfurl.indexOf("file:")==-1){				

				swfpath = absolutepath+"/xml/"+swfname+".xml?vers="+new Date().getTime()+"";

			} else {

				swfpath = absolutepath+"/xml/"+swfname+".xml";

			}

			//trace(">> "+swfpath)

			// ______________________________________________________Newly added 06/06/09

			var urlRequest:URLRequest=new URLRequest(swfpath);

			var urlLoader = new URLLoader();

			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);

			urlLoader.addEventListener(Event.COMPLETE, completeListener);

			urlLoader.load(urlRequest);

		}

		public function loadTitle(num:Number,obj:Object) {

			obj.autoSize=TextFieldAutoSize.LEFT;

			parseXMLData(num,obj);

			var maxLen=85

			if (obj.length>maxLen) {

				var str=obj.text;

				obj.htmlText="";

				for (i=0; i<=maxLen; i++) {

					obj.htmlText+=str.split("")[i];

				}

				obj.htmlText+="...";

				obj.parent.scale_mc.width=obj.width+Number(15)

			}else{

				obj.parent.scale_mc.width=obj.width+Number(15)

			}

			



		}

		private function completeListener(e:Event):void {

			novel=new XML(e.target.data);

			numberOfNodes=novel.xml.node.length();

			var count=0;

			for (i=0; i<=numberOfNodes; i++) {

				var imgStr=String(novel.xml.child(i));				

				if(isExtension(imgStr)){

					if (imgStr.split("/")[1]=="images") {

						count++;

					}

				}

			}

			disObj.play();

		}

		

		public function ioErrorHandler(evnt:IOErrorEvent) {

			

		}

		private function isExtension(str:String):Boolean {

			var flag:Boolean = false;

			var arr:Array = new Array(".swf",".SWF", ".jpg", ".JPG", "jpeg", ".JPEG", ".png", ".PNG", ".gif", ".GIF");			

			for(var i:Number = 0; i < arr.length; i++) {

				/*if (str.endsWith(arr[i]) ) {

					flag = true;

				}*/

			}

			return flag;

		}

		String.prototype.startsWith = function(str){

			return !this.indexOf(str);

		}

		String.prototype.endsWith = function(str){

			return this.lastIndexOf(str) == this.length-str.length;

		}

		public function callImage(nodeNum) {

			try {

			var results:XMLList =novel.xml.node.(attribute('id') == nodeNum.toString());

			var str=results.text();

			var swfpath1:String=tempswfname.substring(0,tempswfname.lastIndexOf("/"));

			

				var url:String=swfpath1+str;

				//trace(url+' ---urlo--')

				var loader=new Loader();

				var URLReq=new URLRequest(url);

				loader.load(URLReq);

				disObj.image.mc.addChild(loader);

			} catch (Err:Error) {

			}

		}

		/*private function fileLoading(evnt:ProgressEvent) {

			try {

				var perc=(evnt.target.bytesLoaded/evnt.target.bytesTotal)*100;

				file_load_obj.parent.preloader_mc.visible=true;

			} catch (Err:Error) {

			}

		}

		private function fileLoadingCompleted(evnt:Event) {

			var target=evnt.target.content;

			file_load_obj.parent.preloader_mc.visible=false;

			file_load_obj.addChild(target);



		}*/

		//-----------------------------------------------------

		public function getXML(xmlString:String,obj:Object) {

			var str=xmlString;

			var patterns:RegExp=/<img/;

			var swfpath1:String=tempswfname.substring(0,tempswfname.lastIndexOf("/"));

			file_load_obj = obj;

			if (obj.name=="image") {

				var url:String=swfpath1+str;

				var loader=new Loader();

				var URLReq=new URLRequest(url);



				loader.load(URLReq);

				//loader.contentLoaderInfo.addEventListener(Event.COMPLETE,fileLoadingCompleted);

				//loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS,fileLoading);

				obj.addChild(loader)



			} else {

				if (str.search(patterns)==-1) {

					obj.htmlText=str;

				} else {



					var swfpath2:String=" <img src='"+swfpath1;

					var pattern:RegExp=/<img src='/g;

					str=String(str);

					str=str.replace(pattern,swfpath2);

					obj.htmlText=str;



				}

				str=str;

			}

		}

		

		public function parseXMLData(num:Number,obj:Object) {

			var results:XMLList =novel.xml.node.(attribute('id') == num.toString());

			var str=results.text();

			var daObj:Object=obj;

			getXML(str,daObj);

			

		}

		

		

	}

}
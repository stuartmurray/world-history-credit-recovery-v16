﻿package scripts{

	import flash.display.*;

	import flash.events.*;

	import flash.utils.*;

	import flash.geom.*;

	import fl.transitions.Tween;

	import fl.transitions.easing.*;

	public class draganddrop_10 {

		var _stage:Object;

		var dragNum:Number = 0;

		var dropNumber:Number = 0;

		var dropName:String;

		var closeString:String;

		var userArr:Array = new Array("0","0","0","0","0","0","0","0");

		var wrongArr:Array = new Array();

		var tryCount:Number = 0;

		var XYpos:Array = new Array();

		var myTweenX:Tween;

		var myTweenY:Tween;

		var myRectangle:Rectangle;

		public function draganddrop_10(mc:Object) {

			_stage = mc;

			init();

		}

		private function init() {

			for (var i=1; i<=_stage.dragCount; i++) {

				_stage["drag_" + i].addEventListener(MouseEvent.MOUSE_DOWN, downFun);

				_stage["drag_" + i].addEventListener(MouseEvent.MOUSE_UP, upFun);

				_stage["drag_"+i].buttonMode = true;

				XYpos.push({Xval:_stage["drag_"+i].x,Yval:_stage["drag_"+i].y});

			}

			checkSubmitStatus();

		}

		private function downFun(evnt:MouseEvent) {

			myRectangle = new Rectangle(_stage.bound_mc_1.x, _stage.bound_mc_1.y, _stage.bound_mc_1.width, _stage.bound_mc_1.height);

			evnt.currentTarget.startDrag(false, myRectangle);

			dragNum = evnt.currentTarget.name.split("_")[1];

			_stage.setChildIndex(_stage.getChildByName("drag_"+dragNum), _stage.numChildren-3);

			//_stage["drag_" + dragNum].addEventListener(MouseEvent.ROLL_OUT, upFun);

		}

		private function upFun(evnt:MouseEvent) {

			evnt.currentTarget.stopDrag();

			dropName = evnt.currentTarget.dropTarget.parent.parent.name.split("_")[0];

			dropNumber = evnt.currentTarget.dropTarget.parent.parent.name.split("_")[1];

			trace("dropName : : :"+evnt.currentTarget.dropTarget.parent.parent.name);

			if (dropName == "drop") {

				userArr[dragNum-1] = dropNumber;

				//---------------------------------------------------//

				evnt.currentTarget.x = _stage["drop_"+dropNumber].x;

				evnt.currentTarget.y = _stage["drop_"+dropNumber].y;

				evnt.currentTarget.visible = false;

				_stage["drop_" + dropNumber].visible = false;

				_stage["mc_"+dropNumber].gotoAndStop("st_"+dragNum);

				//---------------------------------------------------//

			} else {

				//------------------------------------//

				userArr[dragNum-1] = 0;

				//------------------------------------//

				//myTweenX = new Tween(_stage["drag_" + dragNum], "x", Strong.easeOut, _stage["drag_" + dragNum].x, XYpos[dragNum-1].Xval, 0.5, true);

				//myTweenY = new Tween(_stage["drag_" + dragNum], "y", Strong.easeOut, _stage["drag_" + dragNum].y, XYpos[dragNum-1].Yval, 0.5, true);

				//------------------------------------//

				_stage["drag_" + dragNum].x = XYpos[dragNum-1].Xval;

				_stage["drag_" + dragNum].y = XYpos[dragNum-1].Yval;

				//------------------------------------//

			}

			checkSubmitStatus();

			//_stage["drag_" + dragNum].removeEventListener(MouseEvent.ROLL_OUT, upFun);

		}

		private function checkSubmitStatus() {

			var subCount:Number = 0;

			for (var e=1; e<=userArr.length; e++) {

				if (userArr[e] != "0") {

					subCount++;

				}

			}

			if (subCount == _stage.dragCount) {

				_stage.check_btn.addEventListener(MouseEvent.CLICK, validateFun);

				_stage.check_btn.enabled = true;

				_stage.check_btn.alpha = 1;

			} else {

				_stage.check_btn.removeEventListener(MouseEvent.CLICK, validateFun);

				_stage.check_btn.enabled = false;

				_stage.check_btn.alpha = 0.5;

			}

		}

		private function validateFun(evnt:MouseEvent) {

			tryCount++;

			//----//

			var rightCount:Number = 0;

			wrongArr.splice(0, wrongArr.length);

			//----//

			for (var i=1; i<=_stage.dragCount; i++) {

				if (userArr[i-1] == i) {

					rightCount++;

				} else {

					wrongArr.push(i);

				}

				//------------------//

				_stage["drag_" + i].removeEventListener(MouseEvent.MOUSE_DOWN, downFun);

				_stage["drag_" + i].removeEventListener(MouseEvent.MOUSE_UP, upFun);

				_stage["drag_"+i].buttonMode = false;

				//------------------//

			}

			trace("userArr : : "+userArr)

			//-----//

			if (rightCount == _stage.dragCount) {

				_stage.feedback.gotoAndPlay("right");

			} else {

				if (tryCount == 2) {

					_stage.feedback.gotoAndPlay("showanswer");

				} else {

					_stage.feedback.gotoAndPlay("try");

				}

			}

			//-----//

			_stage.check_btn.removeEventListener(MouseEvent.CLICK, validateFun);

			_stage.check_btn.enabled = false;

			_stage.check_btn.alpha = 0.5;

			//-----//

		}

		public function tryAgainFun() {

			for (var k=1; k<=wrongArr.length; k++) {

				//---------------------------//

				_stage["drag_" + wrongArr[k-1]].x = XYpos[wrongArr[k-1]-1].Xval;

				_stage["drag_" + wrongArr[k-1]].y = XYpos[wrongArr[k-1]-1].Yval;

				_stage["drag_" + wrongArr[k-1]].visible = true;

				_stage["drop_" + wrongArr[k-1]].visible = true;

				_stage["mc_"+wrongArr[k-1]].gotoAndStop(1);

				//---------------------------//

				_stage["drag_" + wrongArr[k - 1]].addEventListener(MouseEvent.MOUSE_DOWN, downFun);

				_stage["drag_" + wrongArr[k - 1]].addEventListener(MouseEvent.MOUSE_UP, upFun);

				_stage["drag_"+wrongArr[k-1]].buttonMode = true;

				//---------------------------//

				userArr[wrongArr[k-1]-1] = 0;				

				//---------------------------//

				checkSubmitStatus();

				//---------------------------//

			}

			trace("try userArr"+userArr)

		}

		public function showAnswerFun() {

			for (var k=1; k<=_stage.dragCount; k++) {

				//---------------------------//

				_stage["drag_" + k].x = _stage["drop_" + k].x;

				_stage["drag_" + k].y = _stage["drop_" + k].y;				

				_stage["drag_" + k].visible = false;

				_stage["drop_" + k].visible = false;

				_stage["mc_"+k].gotoAndStop(k);

				

				//---------------------------//

				userArr[k-1] = k;

				//---------------------------//

				//checkSubmitStatus();

				//---------------------------//

			}

		}

		public function exitPopup(str:String) {

			closeString = str;

			_stage.feedback.exit_btn.addEventListener(MouseEvent.CLICK, exitFun);

			_stage.feedback.exit_btn.buttonMode = true;



		}

		public function exitFun(evnt:MouseEvent) {

			if (closeString == "right") {



			} else if (closeString == "try") {

				tryAgainFun();

			} else if (closeString == "showanswer") {

				showAnswerFun();

			}

			_stage.feedback.gotoAndStop(1);

		}

	}

}
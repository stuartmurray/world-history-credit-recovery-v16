﻿package scripts{

	import flash.display.*;

	import flash.events.*;

	import flash.text.*;

	import flash.geom.*;

	import flash.utils.setTimeout;

	public class DynTextLink extends Sprite {

		private var _fmt:TextFormat;

		private var _xml:XML;

		private var _tf:TextField;

		private var _s:String;

		private var _linkText:String;

		private var _output:TextField;

		private var Ref:Object;

		private var count=0;

		private var totalClip:Number;

		//private var answer=[5,18,21,25,31,38,48,56,63,68,71,78,95,103,112,128,130,143,151,160,175];

		private var answer:Array=new Array()//=[4,52,53,54,55,56,57,58,59,60,61,62]

		private var userAnswer:Array=[];

		private var correctCount:Number=0

		public function DynTextLink(Obj:Object,param:Number,arr:Array) {

			answer=arr

			totalClip=param;

			Ref=Obj;

			Ref.show_ans_mc.visible=false

			_fmt = new TextFormat("Tahoma", 11, 0x000000);

			createTextFields();

			disabledBtn(Ref.sumbit)

			//disabledBtn(Ref.show_answer)

			//Ref.show_answer.addEventListener(MouseEvent.CLICK,showAnswer)

			hideAll()

		}

		private function showAnswer(evnt:MouseEvent){

			for (var i=1; i<=totalClip; i++) {

				Ref['btn_' + i].gotoAndStop(1)

			}

			disabledBtn(evnt.currentTarget)

			Ref.ques_mc.visible=false

			tryCount++

			for(i=0;i<answer.length;i++){

				if(Ref['ans_mc_'+answer[i]]){

					Ref['ans_mc_'+answer[i]].visible=true

					Ref['ans_mc_'+answer[i]].play()

				}

			}

			Ref.show_ans_mc.visible=true

		}

		private function checking(){

			var count=0

			for (var i=1; i<=totalClip; i++) {

				if(Ref['btn_' + i].currentFrame==1){

					count++

				}

			}

			if(count==(totalClip-1)){

				disabledBtn(Ref.sumbit)

			}else{

				enabledBtn(Ref.sumbit)

			}

			

		}

		private function hideAll(){

			for(var i=0;i<answer.length;i++){

				if(Ref['ans_mc_'+answer[i]]){

					Ref['ans_mc_'+answer[i]].visible=false

				}

			}

		}

		private function addEvent(Obj:Object){

			Obj.addEventListener(MouseEvent.ROLL_OVER,SubmitOver);

			Obj.addEventListener(MouseEvent.ROLL_OUT,SubmitOut);

		}

		private function enabledBtn(Obj:Object){

			Obj.mouseEnabled=true

			Obj.alpha=1

			Obj.buttonMode=true

			

		}

		private function disabledBtn(Obj:Object){

			Obj.mouseEnabled=false

			Obj.alpha=0.5

			Obj.buttonMode=false

		}

		private function getIdVal(evnt:MouseEvent) {

			var id=evnt.currentTarget.name.split('_')[1];

			if (evnt.currentTarget.currentFrame==1) {

				evnt.currentTarget.gotoAndStop(2);

				userAnswer[id]=id;

			} else {

				userAnswer[id]=undefined;

				evnt.currentTarget.gotoAndStop(1);

			}

			checking()

		}



		private function validate(evnt:MouseEvent) {

			trace("Validate  function ")

			evnt.currentTarget.buttonMode=false

			evnt.currentTarget.mouseEnabled=false;

			evnt.currentTarget.alpha=0.5;

			var flag:Boolean=true;

			

			for (var i=1; i<=userAnswer.length-1; i++) {

				if (userAnswer[i]!=undefined) {

					flag=true;

					for (var j=0; j<answer.length; j++) {

						if (Number(userAnswer[i])==Number(answer[j])) {

							flag=false;

						}

					}

					if (!flag) {

						correctCount++

						Ref['btn_' + i].gotoAndStop('correct');

					} else {

						Ref['btn_' + i].gotoAndStop('wrong');

						Ref.popup_mc.gotoAndStop(3)

					}

				}

			}

			feedBack()

			for (i=1; i<=totalClip; i++) {

				Ref['btn_'+i].mouseEnabled=false;

				Ref['btn_'+i].buttonMode=false;

			}

		}

		private function feedBack(){

			if(correctCount==answer.length){

				Ref.popup_mc.gotoAndStop(1)

			}else{

				Ref.popup_mc.gotoAndStop(3)

			}

		}

		private function SubmitOver(evnt:MouseEvent){

			evnt.currentTarget.gotoAndStop(2)

		}

		private function SubmitOut(evnt:MouseEvent){

			evnt.currentTarget.gotoAndStop(1)

		}

		private function createTextFields():void {

			Ref.sumbit.buttonMode=true

			Ref.sumbit.addEventListener(MouseEvent.CLICK,validate);

			addEvent(Ref.sumbit)

			Ref.sumbit.buttonMode=true;

			for (var i=1; i<=totalClip; i++) {

				Ref['btn_' + i].buttonMode=true;

				Ref['btn_' + i].addEventListener(MouseEvent.CLICK,getIdVal);

			}

		}

		private function createButton(Str:String) {



		}

	}

}
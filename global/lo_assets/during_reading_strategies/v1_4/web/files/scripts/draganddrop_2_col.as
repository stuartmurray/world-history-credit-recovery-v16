﻿package scripts{

	import flash.display.*;

	import flash.events.*;

	import flash.utils.*;

	import flash.geom.*;

	import fl.transitions.Tween;

	import fl.transitions.easing.*;

	public class draganddrop_2_col {

		var _stage:Object;

		var dragNum:Number = 0;

		var dropNumber:Number = 0;

		var dropName:String;

		var closeString:String;

		var userArr:Array = new Array();

		var wrongArr:Array = new Array();

		var tryCount:Number = 0;

		var XYpos:Array = new Array();

		var myTweenX:Tween;

		var myTweenY:Tween;

		var myRectangle:Rectangle;

		

		var tempDragObj:Object

		var dropClip:Object;

		public function draganddrop_2_col(mc:Object) {

			_stage = mc;

			init();

		}

		private function init() {

			

			for (var i=1; i<=_stage.dragCount; i++) {

				userArr.push("0")

				_stage["drop_"+ _stage.correctAnsArr[i-1]+"_"+ i].drop=true

				_stage["drag_" + i].dropName="0"

				_stage["drag_" + i].bg.visible=false

				_stage["drag_" + i].addEventListener(MouseEvent.MOUSE_DOWN, downFun);

				_stage["drag_" + i].addEventListener(MouseEvent.MOUSE_UP, upFun);

				_stage["drag_"+i].buttonMode = true;

				XYpos.push({Xval:_stage["drag_"+i].x,Yval:_stage["drag_"+i].y,objDepth:_stage.getChildIndex(_stage["drag_"+i])});

				

			}

			

			checkSubmitStatus();

		}

		private function downFun(evnt:MouseEvent) {

			_stage.stage.addEventListener(MouseEvent.MOUSE_UP, upFun);

			_stage.stage.addEventListener(Event.MOUSE_LEAVE, upFun);

			//_stage.stage.addEventListener(MouseEvent.MOUSE_OUT, upFun);

			

			var _this=MovieClip(evnt.currentTarget)

			tempDragObj=MovieClip(evnt.currentTarget)

			if(tempDragObj.dropName!="0")

			{

				trace(_stage[tempDragObj.dropName]+"drop____")

				_stage[tempDragObj.dropName].drop = true;

				_stage[tempDragObj.dropName].visible=true

				tempDragObj.dropName="0"

			}

			

			

			trace(_this.name+"?????")

			myRectangle = new Rectangle(_stage.bound_mc_1.x, _stage.bound_mc_1.y, _stage.bound_mc_1.width-_this.width, _stage.bound_mc_1.height-_this.height);

			evnt.currentTarget.startDrag(false, myRectangle);

			dragNum = evnt.currentTarget.name.split("_")[1];

			_stage.setChildIndex(_stage.getChildByName("drag_"+dragNum), _stage.numChildren-1);

			

		}

		private function upFun(evnt:Event) {

			_stage.stage.removeEventListener(MouseEvent.MOUSE_UP, upFun);

			_stage.stage.removeEventListener(Event.MOUSE_LEAVE, upFun);

			//_stage.stage.removeEventListener(MouseEvent.MOUSE_OUT, upFun);

			

			tempDragObj.stopDrag();

			try{

				dropName = tempDragObj.dropTarget.parent.name.split("_")[0];

				dropNumber = tempDragObj.dropTarget.parent.name.split("_")[1];

				dropClip= tempDragObj.dropTarget.parent

				trace("dropName : : :"+dropName+"_"+dropNumber+"_"+dropClip);

			}catch(e:Error)

			{

			}

			if (dropName == "drop" && dropClip.drop==true) {

				

				if(tempDragObj.dropName!="0")

				{

					trace(_stage[tempDragObj.dropName]+"drop____")

					_stage[tempDragObj.dropName].drop = true;

					_stage[tempDragObj.dropName].visible=true

					tempDragObj.dropName="0"

				}

				

				

				

				userArr[dragNum-1] = dropNumber;

				//---------------------------------------------------//

				tempDragObj.x = dropClip.x;

				tempDragObj.y = dropClip.y;

				dropClip.drop = false;

				dropClip.visible=false

				

				tempDragObj.dropName=dropClip.name

				resetDragClip()

				//_stage["mc_"+dropNumber].gotoAndStop("st_"+dragNum);

				//---------------------------------------------------//

			} else {

				//------------------------------------//

				userArr[dragNum-1] = 0;

				dropClip.drop = true;

				if(tempDragObj.dropName!="0")

				{

					trace(_stage[tempDragObj.dropName]+"drop____")

					_stage[tempDragObj.dropName].drop = true;

					_stage[tempDragObj.dropName].visible=true

					tempDragObj.dropName="0"

				}

				dropClip.visible=true

				//------------------------------------//

				//myTweenX = new Tween(_stage["drag_" + dragNum], "x", Strong.easeOut, _stage["drag_" + dragNum].x, XYpos[dragNum-1].Xval, 0.5, true);

				//myTweenY = new Tween(_stage["drag_" + dragNum], "y", Strong.easeOut, _stage["drag_" + dragNum].y, XYpos[dragNum-1].Yval, 0.5, true);

				//------------------------------------//

				_stage["drag_" + dragNum].x = XYpos[dragNum-1].Xval;

				_stage["drag_" + dragNum].y = XYpos[dragNum-1].Yval;

				resetDragClip()

				

				//------------------------------------//

			}

			checkSubmitStatus();

			resetDragClip()

			//_stage["drag_" + dragNum].removeEventListener(MouseEvent.ROLL_OUT, upFun);

		}

		

		

		private function resetDragClip() {

			for (var i=1; i<=_stage.dragCount; i++) {

				_stage.setChildIndex(_stage.getChildByName("drag_"+i), XYpos[i-1].objDepth);

				

			}

			

		}

		

		private function checkSubmitStatus() {

			var subCount:Number = 0;

			for (var e=1; e<=userArr.length; e++) {

				if (userArr[e] != "0") {

					subCount++;

				}

			}

			if (subCount == _stage.dragCount) {

				_stage.check_btn.buttonMode=true

				_stage.check_btn.addEventListener(MouseEvent.CLICK, validateFun);

				_stage.check_btn.enabled = true;

				_stage.check_btn.alpha = 1;

			} else {

				_stage.check_btn.buttonMode=false

				_stage.check_btn.removeEventListener(MouseEvent.CLICK, validateFun);

				_stage.check_btn.enabled = false;

				_stage.check_btn.alpha = 0.5;

			}

		}

		private function validateFun(evnt:MouseEvent) {

			tryCount++;

			//----//

			var rightCount:Number = 0;

			wrongArr.splice(0, wrongArr.length);

			//----//

			for (var i=1; i<=_stage.dragCount; i++) {

				if (userArr[i-1] == _stage.correctAnsArr[i-1]) {

					rightCount++;

				} else {

					wrongArr.push(i);

				}

				//------------------//

				_stage["drag_" + i].removeEventListener(MouseEvent.MOUSE_DOWN, downFun);

				_stage["drag_" + i].removeEventListener(MouseEvent.MOUSE_UP, upFun);

				_stage["drag_"+i].buttonMode = false;

				//------------------//

			}

			trace("userArr : : "+userArr)

			//-----//

			_stage.setChildIndex(_stage.feedback, _stage.numChildren-1);

			if (rightCount == _stage.dragCount) {

				_stage.feedback.gotoAndPlay("right");

			} else {

				if (tryCount == 2) {

					_stage.feedback.gotoAndPlay("showanswer");

				} else {

					_stage.feedback.gotoAndPlay("try");

				}

			}

			//-----//

			_stage.check_btn.removeEventListener(MouseEvent.CLICK, validateFun);

			_stage.check_btn.enabled = false;

			_stage.check_btn.alpha = 0.5;

			//-----//

		}

		public function tryAgainFun() {

			trace(wrongArr+"::::::::::::::wrong")

			for (var k=1; k<=wrongArr.length; k++) {

				//---------------------------//

				_stage["drag_" + wrongArr[k-1]].x = XYpos[wrongArr[k-1]-1].Xval;

				_stage["drag_" + wrongArr[k-1]].y = XYpos[wrongArr[k-1]-1].Yval;

				_stage["drag_" + wrongArr[k-1]].visible = true;

				

				if(_stage["drag_" + wrongArr[k-1]].dropName!="0")

				{

					trace(_stage[_stage["drag_" + wrongArr[k-1]].dropName]+"drop____")

					_stage[_stage["drag_" + wrongArr[k-1]].dropName].drop = true;

					_stage[_stage["drag_" + wrongArr[k-1]].dropName].visible=true

					_stage["drag_" + wrongArr[k-1]].dropName="0"

				}

				

				_stage["drag_" + wrongArr[k-1]].dropName="0"

				

				//_stage["mc_"+wrongArr[k-1]].gotoAndStop(1);

				//---------------------------//

				_stage["drag_" + wrongArr[k - 1]].addEventListener(MouseEvent.MOUSE_DOWN, downFun);

				_stage["drag_" + wrongArr[k - 1]].addEventListener(MouseEvent.MOUSE_UP, upFun);

				_stage["drag_"+wrongArr[k-1]].buttonMode = true;

				//---------------------------//

				userArr[wrongArr[k-1]-1] = 0;				

				//---------------------------//

				

				//---------------------------//

			}

			checkSubmitStatus();

			resetDragClip();

			trace("try userArr"+userArr)

		}

		public function showAnswerFun() {

			

			for (var k=1; k<=_stage.dragCount; k++) {

				//---------------------------//

				 _stage["drop_"+ _stage.correctAnsArr[k-1]+"_"+ k].visible=true

				 //_stage["drag_" + k].bg.visible=true

				_stage["drag_" + k].x = _stage["drop_"+ _stage.correctAnsArr[k-1]+"_"+ k].x;

				_stage["drag_" + k].y = _stage["drop_" + _stage.correctAnsArr[k-1]+"_"+ k].y;				

				

				//---------------------------//

				userArr[k-1] = _stage.correctAnsArr[k-1];

				//---------------------------//

				//checkSubmitStatus();

				//---------------------------//

			}

			resetDragClip();

		}

		public function exitPopup(str:String) {

			closeString = str;

			_stage.feedback.exit_btn.addEventListener(MouseEvent.CLICK, exitFun);

			_stage.feedback.exit_btn.buttonMode = true;

			if(str=="showanswer"){

				for(var i:Number=0;i<wrongArr.length;i++){

				_stage["drag_" + wrongArr[i]].bg.visible=true

				}

			}

			

			

			



		}

		public function exitFun(evnt:MouseEvent) {

			if (closeString == "right") {

				_stage.finalClip.gotoAndPlay('ply')

			} else if (closeString == "try") {

				tryAgainFun();

			} else if (closeString == "showanswer") {

				_stage.finalClip.gotoAndPlay('ply')

				showAnswerFun();

			}

			_stage.feedback.gotoAndStop(1);

		}

	}

}
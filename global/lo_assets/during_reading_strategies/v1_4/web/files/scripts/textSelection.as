﻿package scripts{

	import flash.events.MouseEvent;

	import flash.text.TextField;

	import flash.text.TextFieldAutoSize;

	import flash.text.TextFormat;

	import flash.events.Event;

	import flash.events.IOErrorEvent;

	import flash.events.ProgressEvent;

	import flash.net.URLLoader;

	import flash.display.Loader;

	import flash.net.URLRequest;

	

	import flash.utils.getDefinitionByName



	import flash.utils.Timer;

	import flash.utils.setTimeout;

	import flash.events.TimerEvent;

	import flash.media.*;

	import scripts.customeEvent;

	public class textSelection extends xmlLoader {

		private var currentSound:Sound =null;

		private var soundChannel:SoundChannel;

		private var userStr:String='';

		private var numberOfTry:Number=2

		public var xmlObj:Object;

		private var stageRef:Object;

		private var _path:String;

		private var qNum:Number=0;

		private var answerArr:Array=[];

		private var feedArr:Array=[];

		

		private var tryCount:Number=0



		//private var 

		private var myTimer:Timer;

		private var flag:Boolean=true;



		public function textSelection(StageRef:Object,param:Number) {

			qNum=param;

			stageRef=StageRef;

			var pathObj:getRelativePath=new getRelativePath();

			var swfurl:String=String(stageRef.loaderInfo.url);

			var swfname:String=(swfurl.substring(swfurl.lastIndexOf("/")+1));



			var xmlFileName:String=swfname.split('.')[0];

			_path=pathObj.getPath(stageRef,xmlFileName);

			parseXML(_path,stageRef);

			stageRef.addEventListener(customeEvent.dispatch,assign);



			myTimer = new Timer(500);

			myTimer.addEventListener("timer", timerHandler);

			if (StageRef.Exit_btn) {

				StageRef.Exit_btn.addEventListener(MouseEvent.CLICK,Exit);

			}

			stageRef.answer_mc.visible=false

			stageRef.stage.addEventListener(Event.MOUSE_LEAVE,Stage_leave);



		}

		private function attachAudio(linkageName) {

			try{

				SoundMixer.stopAll()

				var classReference:Class = getDefinitionByName(linkageName) as Class;

				currentSound= new classReference();

				soundChannel=currentSound.play();

			}catch(Err:*){

			}

		}

		private function Stage_leave(evnt:Event) {

			stageRef.txt_mc._txt.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP));

		}

		

		private function Exit(evnt:MouseEvent) {

			stageRef.parent.gotoAndStop('Start');

		}

		private function timerHandler(event:TimerEvent):void {

			//trace('timer called..')

		}

		public function init(Obj:Object) {

			stageRef.popup_mc.visible=false;

			Obj._txt.addEventListener(MouseEvent.MOUSE_UP,MouseUp);

			Obj._txt.addEventListener(MouseEvent.MOUSE_DOWN,MouseDown);

			Obj._txt.alwaysShowSelection=true;

			numberOfTry=xmlObj.questions['question_'+qNum].child(0).attribute('numberOfTry')

			

			

			for (var i=0; i<xmlObj.questions['question_'+qNum].feedback.children().length(); i++) {

				answerArr[i+1]=xmlObj.questions['question_'+qNum].feedback.child(i).attribute('ans');

				feedArr[i+1]=xmlObj.questions['question_'+qNum].feedback.child(i);

			}



		}

		private function MouseDown(evnt:MouseEvent) {

			SoundMixer.stopAll();

			stageRef.popup_mc.visible=false;

			myTimer.reset();

			myTimer.start();

		}

		private function CloseWindow(evnt:MouseEvent){

			SoundMixer.stopAll()

			stageRef.popup_mc.visible=false

		}

		private function checkResult() {

			if (!stageRef.popup_mc.visible) {

				var answer:Number=0;

				stageRef.setChildIndex(stageRef.popup_mc,stageRef.numChildren-1);

				stageRef.popup_mc.visible=true;

				stageRef.popup_mc.exit_btn.buttonMode=true

				stageRef.popup_mc.exit_btn.addEventListener(MouseEvent.CLICK,CloseWindow)

				var flag:Boolean=true;

				for (var i=1; i<answerArr.length; i++) {

					if ((String(answerArr[i]).toLowerCase()).split(' ').join('').split('.').join('').split('!').join('')==(String(userStr).toLowerCase()).split(' ').join('').split('.').join('').split('!').join('')) {

						flag=false

						answer=i;

						break;



					}

				}

				

				if (answer==0) {

					

					tryCount++

					trace(tryCount+'___'+numberOfTry)

					if(tryCount==numberOfTry){

						attachAudio('q'+qNum+'_w');

						stageRef.popup_mc._txt.htmlText=xmlObj.questions['question_'+qNum].wrongFeedBack.wrong

						stageRef.answer_mc.visible=true

						stageRef.txt_mc._txt.selectable=false

						stageRef.txt_mc._txt.removeEventListener(MouseEvent.MOUSE_UP,MouseUp);

						stageRef.txt_mc._txt.removeEventListener(MouseEvent.MOUSE_DOWN,MouseDown);

						stageRef.stage.removeEventListener(Event.MOUSE_LEAVE,Stage_leave);

					}else{

						

						stageRef.popup_mc._txt.htmlText=xmlObj.questions['question_'+qNum].wrongFeedBack.try_again

						attachAudio('try_again');

					}

				} else {

					tryCount=0

					attachAudio('q'+qNum+'_'+answer);

					stageRef.popup_mc._txt.htmlText=xmlObj.questions['question_'+qNum].feedback.child(answer-1);

				}

				stageRef.popup_mc.uiScroll.visible=stageRef.popup_mc._txt.maxScrollV>1

				

			}

		}

		private function MouseUp(evnt:MouseEvent) {



			var temp:Array=[];

			userStr='';

			userStr=evnt.currentTarget.text.substring(evnt.currentTarget.selectionBeginIndex,evnt.currentTarget.selectionEndIndex);

			if (evnt.currentTarget.selectionBeginIndex!=evnt.currentTarget.selectionEndIndex) {

				var id=setTimeout(checkResult,300);

			}

		}

		public function assign(evnt:customeEvent) {

			xmlObj=evnt.xmlObj.xml;

			init(stageRef.txt_mc);

		}

	}

}
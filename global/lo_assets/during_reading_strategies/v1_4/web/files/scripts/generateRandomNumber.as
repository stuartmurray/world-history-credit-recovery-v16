﻿package scripts{

	public class generateRandomNumber {

		private function randRange(min:Number, max:Number):Number {

			var randomNum:Number = Math.floor(Math.random()*(max-min+1))+min;

			return randomNum;

		}

		public function randomNumber_generate(Start,End) {

			var num=End;

			var randomNumber=0;

			var Result=[];

			var temp=[];

			for (var i = 0; i<100; i++) {

				var n:Number=randRange(Start,num);

				temp.push(n);

			}

			for (i=0; i<temp.length; i++) {

				var flag=true;

				for (var j=i+Number(1); j<temp.length; j++) {

					if (temp[i]==temp[j]) {

						flag=false;

					}

				}

				if (flag) {

					Result.push(temp[i]);

				}

			}

			return Result;

		}

	}

}
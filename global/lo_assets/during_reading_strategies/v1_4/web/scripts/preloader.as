﻿package scripts{

	import flash.net.URLRequest;

	import flash.events.Event;

	import flash.events.ProgressEvent;

	import flash.display.Loader;

	import flash.display.LoaderInfo;

	import flash.display.MovieClip;

	public class preloader {

		private var dispList:Object;

		private var preloadObject:Object;

		public function loadFile(reference:Object,target:Object,fileName:String) {

		trace(fileName,"  Pre ")

			dispList=target;

			preloadObject=reference.preloader_mc

			dispList.stage.showDefaultContextMenu=false;

			dispList.stage.stageFocusRect=false;

			var urlReq:URLRequest=new URLRequest(fileName);

			var loader:Loader=new Loader();

			loader.load(urlReq);

			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,fileCompleted);

			loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS,fileLoading);

			dispList.addChild(loader);

		}

		public function fileCompleted(evnt:Event) {

			var target:MovieClip=MovieClip(evnt.target.content);

			preloadObject.visible=false;

			target.play();

		}

		public function fileLoading(evnt:ProgressEvent) {

			if (evnt.currentTarget!=null) {

				var percent:Number=(Math.floor(evnt.currentTarget.bytesLoaded)/Math.floor(evnt.currentTarget.bytesTotal))*100;

				preloadObject._percentage.text = String(Math.floor(percent))+' % Loaded...';

				preloadObject.gotoAndStop(Math.floor(percent));



			}

		}

	}

}
﻿package scripts{

	import flash.net.URLRequest;

	import flash.events.Event;

	import flash.events.MouseEvent;

	import flash.events.ProgressEvent;

	import flash.net.URLLoader;

	import flash.display.Loader;

	import flash.display.LoaderInfo;

	import flash.display.MovieClip;

	import flash.net.navigateToURL;

	import flash.media.SoundMixer;

	import flash.external.ExternalInterface;

	import flash.geom.Rectangle;

	import fl.data.DataProvider;

	import flash.media.*;

	import fl.transitions.*;

	import fl.transitions.easing.*;

	//--------------------//

	import flash.display.SimpleButton;

	import flash.text.*;

	//--------------------//



	public class main {

		private var dispList:Object;

		private var xmlObj:XML;

		private var xmlTranObj:XML;

		private var myTweenY:Tween;

		private var preloadObject:Object;

		private var fileName:Array=[];

		private var fileCount:Number=1;

		private var numberOf:Number;

		private var pdfname:String='';

		private var _tragetY:Number=0;

		private var _orginY:Number=0;

		public var zoomValue:Array=['1x','2x','3x'];

		private var soundTrans:SoundTransform;

		private var contentHolder:MovieClip=new MovieClip();

		private var minFrame:Number=10;

		private var tempContentHolder:MovieClip=new MovieClip();

		private var disableFlag:Boolean=false;

		public var clickedCombo:Boolean = false;

		public var selectedCombo:Number = 1;

		private var tempPrevContent;

		private var loader:Loader;

		public var PClip:Object;



		public function main(reference:Object) {

			dispList=reference;

			dispList.stage.showDefaultContextMenu=false;

			dispList.stage.stageFocusRect=false;

			var xmlPath:String='xml/main.xml';

			var urlReq:URLRequest=new URLRequest(xmlPath);

			var urlloader:URLLoader=new URLLoader();

			urlloader.load(urlReq);

			urlloader.addEventListener(Event.COMPLETE,xmlLoadingCompleted);

			//dispList.zoom_btn.addEventListener(MouseEvent.CLICK,zoomClicked)

			//loadZoomCombo();



			//-----------Temp Change-----------------//

			dispList.audio_btn.buttonMode=true;

			dispList.pause_btn.addEventListener(MouseEvent.CLICK,pauseClciked);

			dispList.audio_btn.addEventListener(MouseEvent.CLICK,audioClciked);

			setAudioBtn();

			//-----------------------------//

			var xmlTranPath:String='xml/transcript.xml';

			var urlTranReq:URLRequest=new URLRequest(xmlTranPath);

			var urlTranloader:URLLoader=new URLLoader();

			urlTranloader.load(urlTranReq);

			urlTranloader.addEventListener(Event.COMPLETE,xmlTranLoadingCompleted);

			//----------------------------//

			//----------Drop Down------------------//

			dispList.combo.btn.addEventListener(MouseEvent.MOUSE_OVER, overDrop);

			dispList.combo.btn.addEventListener(MouseEvent.MOUSE_OUT, outDrop);

			dispList.combo.btn.addEventListener(MouseEvent.CLICK, openDrop);

			dispList.combo.txt.text = zoomValue[0];

			//----------------------------//

		}

		//-------Temp Change---------//

		private function setAudioBtn() {

			dispList.audio_btn.gotoAndStop(1);

			soundTrans=new SoundTransform(1);

			SoundMixer.soundTransform=soundTrans;

		}

		private function audioClciked(evnt:MouseEvent) {

			if (evnt.currentTarget.currentFrame==2) {

				soundTrans=new SoundTransform(1);

				SoundMixer.soundTransform=soundTrans;

				evnt.currentTarget.gotoAndStop(1);

			} else {

				soundTrans=new SoundTransform(0);

				SoundMixer.soundTransform=soundTrans;

				evnt.currentTarget.gotoAndStop(2);

			}

		}

		private function pauseClciked(evnt:MouseEvent) {

			trace("i in the function");

			if (evnt.currentTarget.currentFrame==2) {

				evnt.currentTarget.gotoAndStop(1);

				if (PClip.currentFrame==PClip.totalFrames) {

					PClip.gotoAndPlay(2);

					draggerEnablee();

				} else {

					PClip.play();

				}

			} else {

				PClip.stop();

				evnt.currentTarget.gotoAndStop(2);

			}

		}

		//----------------//

		//--------------Drop Down -----------------------//

		private function overDrop(evnt:MouseEvent) {

			evnt.target.parent.over_mc.gotoAndStop("over");

		}

		private function outDrop(evnt:MouseEvent) {

			evnt.target.parent.over_mc.gotoAndStop("up");

		}

		private function openDrop(evnt:MouseEvent) {

			if (clickedCombo) {

				evnt.target.parent.gotoAndStop(1);

				clickedCombo = false;

			} else {

				clickedCombo = true;

				evnt.target.parent.gotoAndStop(2);

			}

			evnt.target.parent.over_mc.gotoAndStop("down");

		}

		public function menuClick(selNum:Number) {

			dispList.combo.gotoAndStop(1);

			selectedCombo = selNum+1;

			ExternalInterface.call('zoom', selNum);

		}

		//-----------------------------------------------//



		/*private function changeHandler(evnt:Event) {

		ExternalInterface.call('zoom',evnt.currentTarget.selectedIndex);

		}

		private function loadZoomCombo() {

		var dp:DataProvider=new DataProvider(zoomValue);

		dispList.comboBox.dataProvider=dp;

		dispList.comboBox.width = 55;

		dispList.comboBox.addEventListener(Event.CHANGE, changeHandler);

		

		}*/

		private function stageClicked(evnt:MouseEvent) {

			trace("+++++++++++++++++"+dispList.menu_item.currentFrame);

			if (dispList.menu_item.currentFrame==3) {

				dispList.menu_item.dispatchEvent(new MouseEvent(MouseEvent.CLICK));

			}

			//dispList.menu_item.gotoAndStop(1);

		}

		private function MouseLeave(evnt:Event) {

			if (dispList.menu_item.currentFrame==3) {

				dispList.menu_item.dispatchEvent(new MouseEvent(MouseEvent.CLICK));

			}

			//dispList.menu_item.gotoAndStop(1);

		}

		private function xmlLoadingCompleted(evnt:Event) {



			xmlObj=new XML(evnt.target.data);

			numberOf=xmlObj.module.children().length();



			pdfname=String(xmlObj.module.attribute('print'));

			for (var i=1; i<=xmlObj.module.children().length(); i++) {

				fileName[i]=xmlObj.module.child(i-1).attribute('fileName');

			}

			navigate();

			loadSwf(fileCount);



			init();

			//dispList.stage.addEventListener(Event.MOUSE_LEAVE,MouseLeave);

			//dispList.menu_mc.hand_btn.addEventListener(MouseEvent.CLICK,stageClicked);

			//dispList.menu_mc.hand_btn.useHandCursor = false;



			dispList.prev_btn.addEventListener(MouseEvent.CLICK,naviClciked);

			dispList.prev_btn.buttonMode = true;

			dispList.nxt_btn.addEventListener(MouseEvent.CLICK,naviClciked);

			dispList.nxt_btn.buttonMode = true;





			setProperties(dispList.prev_btn);

			setProperties(dispList.nxt_btn);

		}

		//----------Temp Change--------------//

		private function xmlTranLoadingCompleted(evnt:Event) {

			xmlTranObj = new XML(evnt.target.data);

		}

		public function loadTranscriptContent() {

			trace("PClip.TranscriptCount : : "+PClip.TranscriptCount+" : PClip : "+PClip.currentLabel.split("_")[1]);

			try {

				if(PClip.TranscriptCount == undefined){

					   PClip.TranscriptCount = 0

					}

					if(dispList.trans_mc.currentFrame == 2){

					dispList.trans_mc.t_text.htmlText = xmlTranObj.child(Number((fileCount)-1)).child(PClip.TranscriptCount);

					dispList.trans_mc.exit_trans.addEventListener(MouseEvent.CLICK, closeTransFun);

					dispList.trans_mc.exit_trans.buttonMode = true;

					if (dispList.trans_mc.t_text.height < dispList.trans_mc.t_text.textHeight) {

						dispList.trans_mc.span.visible = true;

					} else {

						dispList.trans_mc.span.visible = false;

					}

					dispList.trans_mc.span.update();

					}

				

			} catch (e:Error) {

			}

		}

		public function loadTranscriptContent1() {

			try {

				if(PClip.currentLabel.split("_")[1] == undefined){

					   PClip.TranscriptCount = 0

					}

					if(dispList.trans_mc.currentFrame == 2){

					dispList.trans_mc.t_text.htmlText = xmlTranObj.child(Number((fileCount)-1)).child(PClip.currentLabel.split("_")[1]);

					PClip.TranscriptCount = PClip.currentLabel.split("_")[1];

					trace(" 22 "+PClip.TranscriptCount);

					dispList.trans_mc.exit_trans.addEventListener(MouseEvent.CLICK, closeTransFun);

					dispList.trans_mc.exit_trans.buttonMode = true;

					if (dispList.trans_mc.t_text.height < dispList.trans_mc.t_text.textHeight) {

						dispList.trans_mc.span.visible = true;

					} else {

						dispList.trans_mc.span.visible = false;

					}

					dispList.trans_mc.span.update();

					}else{

						PClip.TranscriptCount = PClip.currentLabel.split("_")[1];

					}

				

			} catch (e:Error) {

			}

		}

		private function closeTransFun(evnt:MouseEvent) {

			dispList.trans_mc.gotoAndStop(1);

		}

		private function transcriptClicked(evnt:MouseEvent) {

			dispList.trans_mc.gotoAndStop(2);

		}

		//-----------------------------------//

		private function setProperties(Obj:Object) {

			try {

				Obj.buttonMode=true;

				Obj.clicked=true;

				Obj.addEventListener(MouseEvent.ROLL_OVER,overTheObject);

				Obj.addEventListener(MouseEvent.ROLL_OUT,outTheObject);

			} catch (Err:Error) {

			}

		}

		private function overTheObject(evnt:MouseEvent) {

			if (evnt.currentTarget.clicked) {

				evnt.currentTarget.gotoAndStop(2);

			}

		}

		private function outTheObject(evnt:MouseEvent) {

			if (evnt.currentTarget.clicked) {

				evnt.currentTarget.gotoAndStop(1);

			}

		}

		private function init() {



			dispList.menu_mc.orginYpos=dispList.menu_mc.y;

			trace("dispList.menu_mc.orginYpos : : "+dispList.menu_mc.orginYpos);

			_tragetY=(dispList.menu_item.y+dispList.menu_mc.height);

			_orginY=dispList.menu_mc.y;

			dispList.menu_mc.mask=dispList.mask_menu;

			enabledMenu();



			for (var i=1; i<=xmlObj.module.children().length(); i++) {

				setProperties(dispList.menu_mc['btn_'+i]);

				if (dispList.menu_mc['btn_'+i]) {

					dispList.menu_mc['btn_'+i]._txt.htmlText=xmlObj.module.child(i-1);

					dispList.menu_mc['btn_'+i].buttonMode=true;

					dispList.menu_mc['btn_'+i].mouseChildren=false;

					dispList.menu_mc['btn_' + i].addEventListener(MouseEvent.CLICK,MenuItem);

				}

			}

		}

		private function MenuItem(evnt:MouseEvent) {

			var id:Number=evnt.currentTarget.name.split('_')[1];

			fileCount=id;

			loadSwf(fileCount);

		}

		private function toggle_menu(param:Number) {

			try {

				for (var i=1; i<=xmlObj.module.children().length(); i++) {

					if (dispList.menu_mc['btn_'+i]) {

						dispList.menu_mc['btn_'+i].clicked=true;

						dispList.menu_mc['btn_'+i].buttonMode=true;

						dispList.menu_mc['btn_'+i].mouseEnabled=true;

						dispList.menu_mc['btn_' + i].gotoAndStop(1);

					}

				}

				//trace(param+'___')

				if (param == 2 || param == 3) {

					dispList.menu_mc['btn_' + 2].gotoAndStop(2);

					dispList.menu_mc['btn_'+2].clicked=false;

				} else if (param == 4) {

					dispList.menu_mc['btn_' + 5].gotoAndStop(2);

					dispList.menu_mc['btn_'+5].clicked=false;

				} else if (param == 5 || param == 6 || param == 7) {

					dispList.menu_mc['btn_' + 6].gotoAndStop(2);

					dispList.menu_mc['btn_'+6].clicked=false;

				} else if (param == 8 || param == 9 || param == 10) {

					dispList.menu_mc['btn_' + 8].gotoAndStop(2);

					dispList.menu_mc['btn_'+8].clicked=false;

				} else {

					dispList.menu_mc['btn_' + param].gotoAndStop(2);

					dispList.menu_mc['btn_'+param].clicked=false;

				}

				//dispList.menu_mc['btn_'+param].clicked=false;

				//dispList.menu_mc['btn_' + param].gotoAndStop(2);

				dispList.menu_mc['btn_'+param].buttonMode=false;

				dispList.menu_mc['btn_'+param].mouseEnabled=false;

			} catch (Err:*) {

			}

		}

		private function MenuItemClicked(evnt:MouseEvent) {

			dispList.setChildIndex(dispList.menu_mc,dispList.numChildren-1);

			//trace("currentFrame : :"+evnt.currentTarget.currentFrame+" : : _orginY : : "+_orginY+" : : _tragetY : : "+_tragetY+" : dispList.menu_mc.y : "+dispList.menu_mc.y)

			if (evnt.currentTarget.currentFrame==2 || evnt.currentTarget.currentFrame==1) {

				//dispList.rollOutMc.visible=true;

				dispList.menu_mc.hand_btn.addEventListener(MouseEvent.MOUSE_OVER,onMenufalse);

				dispList.menu_mc.hand_btn.useHandCursor = false;

				evnt.currentTarget.gotoAndStop(3);

				//dispList.menu_mc.hand_btn.y = -386.1

				dispList.menu_item.clicked=false;

				myTweenY = new Tween(dispList.menu_mc, "y", Strong.easeOut, dispList.menu_mc.y, _tragetY, 0.5, true);

			} else {

				dispList.menu_item.clicked=true;

				myTweenY = new Tween(dispList.menu_mc, "y", Strong.easeOut, dispList.menu_mc.y, _orginY, 0.5, true);

				//dispList.menu_mc.hand_btn.y = 23.9

				evnt.currentTarget.gotoAndStop(1);

			}

		}

		private function onMenufalse(ev:Event) {

			//dispList.rollOutMc.visible=false;

			dispList.menu_item.clicked=true;

			myTweenY = new Tween(dispList.menu_mc, "y", Strong.easeOut, dispList.menu_mc.y, _orginY, 0.5, true);

			dispList.menu_item.gotoAndStop(1);

		}

		private function printClicked(evnt:MouseEvent) {



			var request:URLRequest = new URLRequest(pdfname);

			navigateToURL(request,'_blank');

		}

		private function naviClciked(evnt:MouseEvent) {

			if (evnt.currentTarget.name=='prev_btn') {

				fileCount--;

			} else {

				fileCount++;

			}

			SoundMixer.stopAll();

			contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);



			dispList.dr.dragger.x=0;

			disableNavi();

			disableMenu();

			disableDragger();

			loadSwf(fileCount);

		}

		private function fileCompleted(evnt:Event) {

			enableNavi();

			drggerOnlyEnablee();

			draggerEnablee();

			dispList.stage_btn.visible = false;

			//

			try {

				var _mc:MovieClip=new MovieClip();

				_mc.addChild(loader);

				_mc.name='swfLoader';

				tempPrevContent=_mc;

				dispList.loadClip_mc.addChild(_mc);

				contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);

				contentHolder=new MovieClip();

				tempContentHolder=new MovieClip();

				contentHolder=MovieClip(evnt.currentTarget.content);

				tempContentHolder = contentHolder;

				if (contentHolder.totalFrames>2) {

					contentHolder.play();

				}

				dispList.dr.visible=false;

				//----------Temp Change--------------//

				dispList.pause_btn.visible = false;

				dispList.audio_btn.visible = false;

				//------------------------//

				if (contentHolder.totalFrames>minFrame) {

					if (fileCount!=1) {

						dispList.dr.visible=true;

						//---------Temp Change---------------//

						dispList.pause_btn.visible = true;

						dispList.audio_btn.visible = true;

						//------------------------//

					}

				}

				dispList.dr.dragger.x=0;

				//dispList.setChildIndex(dispList.dr,dispList.numChildren-1);

				contentHolder.addEventListener(Event.ENTER_FRAME,progressBar);

				enableDragger();

				enabledMenu();

				//---------Temp Change---------------//

				PClip = evnt.target.content;

				if (PClip.TranscriptCount != undefined) {

					dispList.transcript_btn.addEventListener(MouseEvent.CLICK, transcriptClicked);

					dispList.transcript_btn.buttonMode = true;

					dispList.transcript_btn.alpha = 1;

				} else {

					dispList.transcript_btn.removeEventListener(MouseEvent.CLICK, transcriptClicked);

					dispList.transcript_btn.buttonMode = false;

					dispList.transcript_btn.alpha = 0.5;

				}

				//------------------------//

			} catch (err:Error) {

			}

		}

		private function fileLoading(evnt:Event) {

			if (evnt.currentTarget!=null) {

				var percent:Number=(Math.floor(evnt.currentTarget.bytesLoaded)/Math.floor(evnt.currentTarget.bytesTotal))*100;

				dispList.preloader_mc._percentage.text = String(Math.floor(percent))+' % Loaded...';

				dispList.preloader_mc.gotoAndStop(Math.floor(percent));

			}

		}

		private function deactivate(Obj:Object) {

			Obj.buttonMode=true;

			Obj.mouseEnabled=false;

			Obj.mouseChildren=false;

			Obj.alpha=0.5;

		}

		private function activate(Obj:Object) {

			Obj.buttonMode=false;

			Obj.mouseEnabled=true;

			Obj.mouseChildren=true;

			Obj.alpha=1;

		}

		private function navigate() {

			try {

				if (fileCount==1) {

					dispList.dummy_prev_btn.visible=true;

					dispList.dummy_nxt_btn.visible=false;

					deactivate(dispList.prev_btn);

					activate(dispList.nxt_btn);

				} else if (fileCount==numberOf) {

					dispList.dummy_nxt_btn.visible=true;

					dispList.dummy_prev_btn.visible=false;

					deactivate(dispList.nxt_btn);

					activate(dispList.prev_btn);

				} else {

					dispList.dummy_prev_btn.visible=false;

					dispList.dummy_nxt_btn.visible=false;

					activate(dispList.prev_btn);

					activate(dispList.nxt_btn);

				}

			} catch (Err:Error) {

			}



		}



		private function loadSwf(param:Number) {

			try {

				disableDragger();

				removeScreen();

			} catch (err:Error) {

			}

			if (contentHolder != null) {

				contentHolder.gotoAndStop(1);

			}

			if(loader != null){

				loader.unloadAndStop() 

			}

			dispList.stage_btn.visible = true;

			//---------Temp Change---------------//

			dispList.trans_mc.gotoAndStop(1);

			dispList.pause_btn.gotoAndStop(1);

			//---------------------------------//

			SoundMixer.stopAll();

			trace("++++++++++++++++++++++++++++ "+dispList.menu_item.currentFrame);

			if (dispList.menu_item.currentFrame==3) {

				dispList.menu_mc.dispatchEvent(new MouseEvent(MouseEvent.CLICK));

			}

			dispList.menu_mc.y = dispList.menu_mc.orginYpos;

			dispList.dr.dragger.x=0;

			dispList.menu_item._txt.htmlText=xmlObj.module.child(param-1);

			dispList.menu_item.gotoAndStop(1);

			toggle_menu(param);

			dispList.preloader_mc.gotoAndStop(1);

			dispList.preloader_mc._percentage.text = String(Math.floor(0))+' % Loaded...';

			navigate();

			dispList.ind_txt.htmlText=fileCount+ ' of '+numberOf;

			for (var i=dispList.loadClip_mc.numChildren-1; i>=0; i--) {

				if (dispList.loadClip_mc.getChildAt(i).name=='swfLoader') {

					dispList.loadClip_mc.removeChild(dispList.loadClip_mc.getChildAt(i));

				}

			}

			var url=String(fileName[param]);

			loader=new Loader();

			var req:URLRequest=new URLRequest(url);

			loader.load(req);

			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,fileCompleted);

			loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS,fileLoading);

			dispList.loadClip_mc.x=dispList.mask_mc.x;

			dispList.loadClip_mc.y=dispList.mask_mc.y;

			dispList.loadClip_mc.mask=dispList.mask_mc;

		}

		//Drop Down Menu Listener Add

		private function enabledMenu() {

			dispList.menu_item.buttonMode=true;

			dispList.menu_item.addEventListener(MouseEvent.CLICK,MenuItemClicked);

			dispList.menu_item.clicked=true;

			dispList.menu_item.addEventListener(MouseEvent.ROLL_OVER,overTheObject);

			dispList.menu_item.addEventListener(MouseEvent.ROLL_OUT,outTheObject);

		}//enabledMenu



		//Drop Down Menu Listener Remove

		private function disableMenu() {

			dispList.menu_item.buttonMode=false;

			dispList.menu_item.removeEventListener(MouseEvent.CLICK,MenuItemClicked);

			dispList.menu_item.clicked=true;

			dispList.menu_item.removeEventListener(MouseEvent.ROLL_OVER,overTheObject);

			dispList.menu_item.removeEventListener(MouseEvent.ROLL_OUT,outTheObject);



		}//disableMenu



		//remove priv Swf remove form Stage

		private function removeScreen() {

			if (dispList.loadClip_mc.numChildren>0) {

				for (var i:Number=0; i<dispList.loadClip_mc.numChildren; i++) {

					var temp=dispList.loadClip_mc.getChildAt(i);

					dispList.loadClip_mc.removeChild(temp);

				}

			}

			contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);

		}//removeScreen



		//Disable Next Prev button Listener

		private function disableNavi() {

			dispList.prev_btn.removeEventListener(MouseEvent.CLICK,naviClciked);

			dispList.prev_btn.buttonMode = false;

			dispList.nxt_btn.removeEventListener(MouseEvent.CLICK,naviClciked);

			dispList.nxt_btn.buttonMode = false;

			//dispList.stage_btn.visible = true;

		}//disableNavi



		//Enable Next Prev button Listener

		private function enableNavi() {

			dispList.prev_btn.addEventListener(MouseEvent.CLICK,naviClciked);

			dispList.prev_btn.buttonMode = true;

			dispList.nxt_btn.addEventListener(MouseEvent.CLICK,naviClciked);

			dispList.nxt_btn.buttonMode = true;

			//dispList.stage_btn.visible = false;

		}//enableNavi



		//Progress of Draqgger 

		private function progressBar(ev:Event) {

			//trace(contentHolder.totalFrames);

			if (contentHolder.currentFrame!=contentHolder.totalFrames) {

				var drag=contentHolder.currentFrame/contentHolder.totalFrames*dispList.dr.bar.width;

				dispList.dr.dragger.x=drag;

				//dispList.setChildIndex(dispList.dr,dispList.numChildren-1);

			}

		}//progressBar



		//Loading time disable Dragger  function

		private function disableDragger() {

			dispList.dr.dragger.but_play.enabled=false;

			dispList.dr.dragger.flag=false;

			dispList.dr.dragger.removeEventListener(MouseEvent.MOUSE_DOWN,startDr);

			dispList.dr.dragger.removeEventListener(MouseEvent.MOUSE_OUT,startDrOut);

			dispList.stage.removeEventListener(MouseEvent.MOUSE_UP,stopDr);

		}//disableDragger



		//Loading Completed Enable function 

		private function enableDragger() {

			dispList.dr.dragger.but_play.enabled=true;

			dispList.dr.dragger.flag=false;

			dispList.dr.dragger.addEventListener(MouseEvent.MOUSE_DOWN,startDr);

			dispList.dr.dragger.addEventListener(MouseEvent.MOUSE_OUT,startDrOut);

			dispList.stage.addEventListener(MouseEvent.MOUSE_UP,stopDr);

		}//enableDragger



		private function startDrOut(ev:Event) {

			dispList.dr.dragger.stopDrag();

		}

		//Dragger start Drag funciton 

		private function startDr(ev:Event) {

			ev.currentTarget.flag = true;

			contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);

			ev.currentTarget.startDrag(true,new Rectangle(3,0,dispList.dr.bar.width-3,0));

			checkBoth();

		}//startDr



		//Dragger StopDrag function 

		private function stopDr(ev:Event) {

			if (dispList.dr.dragger.flag) {

				dispList.dr.dragger.flag = false;

				dispList.dr.dragger.stopDrag();				

				var tempNum=dispList.dr.dragger.x/dispList.dr.bar.width*contentHolder.totalFrames;

				if (Math.round(tempNum) != contentHolder.totalFrames) {

					contentHolder.gotoAndPlay(Math.round(tempNum));

					dispList.pause_btn.gotoAndStop(1);

				} else {

					contentHolder.gotoAndStop(contentHolder.totalFrames);

					dispList.pause_btn.gotoAndStop(2);

				}

				contentHolder.addEventListener(Event.ENTER_FRAME,progressBar);

				loadTranscriptContent1();

				if (contentHolder.pause_btn) {

					//contentHolder.pause_btn.gotoAndStop(1);

				}

			}

		}//



		private function checkBoth() {

			var tempNum=dispList.dr.dragger.x/dispList.dr.bar.width*contentHolder.totalFrames;

			contentHolder.gotoAndStop(Math.round(tempNum));



		}



		//popup Dragger Enabled

		public function sliderChange(mc:MovieClip) {

			contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);

			//tempContentHolder=contentHolder;

			dispList.dr.dragger.x=0;

			contentHolder=mc;

			contentHolder.addEventListener(Event.ENTER_FRAME,progressBar);

			//---------Temp Change---------------//

			PClip = mc;

			//------------------------//

		}//sliderChange



		//Enable Dragger in Popup Parent File 

		public function endSliderPopup() {

			contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);

			contentHolder=tempContentHolder;

			PClip = tempContentHolder;

			contentHolder.addEventListener(Event.ENTER_FRAME,progressBar);

			dispList.dr.dragger.x= dispList.dr.width;

			trace("tempContentHolder : : "+tempContentHolder.totalFrames);

		}//endSliderPopup



		public function drggerOnlyEnablee() {

			trace("i m in dragger enable");

			dispList.dr.dragger.but_play.enabled=true;

			dispList.dr.alpha=1;

			dispList.dr.dragger.addEventListener(MouseEvent.MOUSE_DOWN,startDr);

			dispList.stage.addEventListener(MouseEvent.MOUSE_UP,stopDr);

			dispList.pause_btn.addEventListener(MouseEvent.CLICK,pauseClciked);

			dispList.pause_btn.alpha = 1;

		}

		public function draggerOnlyDisablee() {

			trace("i m in dragger disable");

			dispList.dr.dragger.but_play.enabled=false;

			dispList.dr.alpha=0;

			dispList.dr.dragger.x = 0;

			dispList.dr.dragger.removeEventListener(MouseEvent.MOUSE_DOWN,startDr);

			dispList.stage.removeEventListener(MouseEvent.MOUSE_UP,stopDr);

			dispList.pause_btn.removeEventListener(MouseEvent.CLICK,pauseClciked);

			dispList.pause_btn.gotoAndStop(3);

			dispList.pause_btn.alpha = 0.5;

		}

		// this function call by Sub file(load SWF)  --  Disable Dragger

		public function disableDraggerr() {

			disableFlag=false;

			disable();

		}//disableDraggerr



		private function disable() {

			contentHolder.removeEventListener(Event.ENTER_FRAME,progressBar);

			dispList.dr.dragger.flag=false;

			dispList.dr.dragger.but_play.enabled=false;

			dispList.dr.dragger.removeEventListener(MouseEvent.MOUSE_DOWN,startDr);

			dispList.dr.dragger.removeEventListener(MouseEvent.MOUSE_OUT,startDrOut);

			dispList.stage.removeEventListener(MouseEvent.MOUSE_UP,stopDr);

			dispList.dr.alpha=0.5;

			dispList.pause_btn.removeEventListener(MouseEvent.CLICK,pauseClciked);

			dispList.pause_btn.gotoAndStop(3);

			dispList.pause_btn.alpha = 0.5;

		}//disable





		//this function call by sub file(load swf) --  Enable

		public function draggerEnablee() {

			disableFlag=true;

			enable();

		}//draggerEnablee



		private function enable() {

			contentHolder.addEventListener(Event.ENTER_FRAME,progressBar);

			dispList.dr.dragger.flag=true;

			dispList.dr.dragger.but_play.enabled=true;

			dispList.dr.dragger.addEventListener(MouseEvent.MOUSE_DOWN,startDr);

			dispList.dr.dragger.addEventListener(MouseEvent.MOUSE_OUT,startDrOut);

			dispList.stage.addEventListener(MouseEvent.MOUSE_UP,stopDr);

			dispList.dr.alpha=1;

		}//enable

		public function FileEndStatus() {

			trace("i m in file End");

			dispList.pause_btn.gotoAndStop(2);

		}

		public function FileStartStatus() {

			trace("i m in file End");

			dispList.pause_btn.gotoAndStop(1);

		}

	}

}
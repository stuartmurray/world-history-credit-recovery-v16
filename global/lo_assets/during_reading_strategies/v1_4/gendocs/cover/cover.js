 
$(document).ready(function(){
    if ((navigator.platform == 'iPhone') || (navigator.platform == 'iPod') || (navigator.platform == 'iPad')) {
        parseCourseMap();    
    }
    else{
        location.href="web/index.html";
    }
});

function parseCourseMap(){
    $.ajax({
        async: false,
        type: "GET",
        url: "coursemap.xml",
        dataType: (jQuery.browser.msie) ? 'text' : 'xml',
        success: function(data){
            parseCourseTitle(ConvertToSupportableFormat(data));
        }
      });
}

function parseCourseTitle(courseXML){
    sSrc=$(courseXML).find('content:first').attr('src');
    location.href=sSrc;
}

function ConvertToSupportableFormat(data){
    var xml; 
    if ( typeof data == 'string') { 
        xml = new ActiveXObject( 'Microsoft.XMLDOM'); 
        xml.async = false; 
        xml.loadXML( data); 
    } else { 
        xml = data; 
    } 
    return xml;
}
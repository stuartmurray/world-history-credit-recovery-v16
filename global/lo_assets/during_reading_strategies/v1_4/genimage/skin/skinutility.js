

function intializeSkin(){
	readCouseMap();    
}


function readCouseMap(){
    $.ajax({
        async: false,
        type: "GET",
        url: "../../../coursemap.xml",
        dataType: (jQuery.browser.msie) ? 'text' : 'xml',
        success: function(data){
            parseCourseMap(ConvertToSupportableFormat(data));
        }
      });
}

function parseCourseMap(xml){
    buildTOC(xml);
    getCurrentObject();
    bindPageMeta(xml);
    //$('.learnObj').find('a').eq(5).addClass('topicListSelected');
    $('.learnObj').find('a').eq($(xml).find('item').index($(xml).find("content[src='" + currPage + "']").parent())).addClass('topicListSelected');
    $('.topicSelect').find('a').html($('.learnObj').find('.topicListSelected').html());    
}

function buildTOC(xml){
    var topic='';
    //$(".learnObj").css('visibility','hidden');
    $(xml).find('item').each(function(index){
            //topic += '<li><a href="javascript:void(0)">'+ $(this).attr("title") +'</a></li>';
            var sURL='javascript:void(0)'
            if($(this).find('content').length!=0){
                sURL = '../../../' + $(this).find('content').eq(0).attr('src');
            }
            //topic = '<li><a href="' + sURL + '">'+ $(this).attr("title") +'</a></li>' + topic;
            topic += '<li><a href="' + sURL + '">'+ $(this).attr("title") +'</a></li>';
    });
    $(".learnObj").html(topic);
    $('.topicSelect').addClass('dropdown');
    
    $(".topicSelect").unbind('click');
    $(".topicSelect").click(function(){
        //To display the link in Fangs
        if($(".learnObj").css('visibility')=='visible'){
            $(".learnObj").show();
        } else{
            $(".learnObj").hide();
            $(".learnObj").css('visibility','visible');
        }
        $(".learnObj").animate({height:'toggle'},100,function(){
           if($(".learnObj").css('display')=='none'){
                $('.topicSelect').removeClass('dropdownSelected').addClass('dropdown');
                $('video').show();
                $('.videoposter').remove();
        
                $(".learnObj").css('visibility','hidden');
                $(".learnObj").show();
                //$('video').css('visibility','hidden');
           } else{
                $('.topicSelect').removeClass('dropdown').addClass('dropdownSelected');
                $(".learnObj").css('visibility','visible');
                //$('video').css('visibility','visible');
                GBL_PausedVideo=null; //Declared in lightbox.js
                $('video:visible').each(function(){
                    var sSrc=$(this).attr('src');
                    sSrc=sSrc.replace('.mp4','.jpg');
                    $(this).parent().append('<img class="videoposter" src="' + sSrc + '" alt="video" />');
                    if(!$(this).get(0).paused){
                        $(this).get(0).pause();
                        GBL_PausedVideo=$(this).get(0);
                    }
                    $(this).hide();
                });
                
                if(GBL_PausedVideo!=null){
                    GBL_PausedVideo.play();
                }
           }
        });
    });
}

function BuildTopic(lesson){
    var topic, sFilter;    
    topic = '';
    
    if(lesson!=undefined && lesson!=''){
        sFilter = "item[title='" + lesson + "']";
        $(GLBCourseMapXml).find('TOC').find(sFilter).children(0).each(function(index){
            topic += '<li><a href="javascript:void(0)">'+ $(this).attr("title") +'</a></li>';
        });
    }
    else{
        sFilter = "item";
        
    }    

    $(".learnObj").html(topic);
    /*$(".learnObj li").click(function(){
        //$(".learnObj").hide();
        $(".learnObj").css('visibility','hidden');
        buildBreadCrumb($(this).text()); 
    });*/
}
function getCurrentObject(){
    var sURL=location.href.toLowerCase();
    var aURL;
    aURL=sURL.split('/');
    
    currObject=aURL[aURL.length-4] + '/' + aURL[aURL.length-3] + '/' + aURL[aURL.length-2];
    currPage=aURL[aURL.length-4] + '/' + aURL[aURL.length-3] + '/' + aURL[aURL.length-2] + '/' + aURL[aURL.length-1]
}

function bindPageMeta(xml){
    //var subTitle;
    //var parentNode;
    var totalPageNumber;
    var currPageNumber;
    var totalItemPageNumber;
    var currItemPageNumber;
    var currNode;
    
    //parentNode=$(xml).find("content[src='" + currPage + "']").parent();
    //subTitle=$(parentNode).attr('title');
    $('#divTitle').html($(xml).find('Title').text());
    
    totalItemPageNumber=$(xml).find('content').length;
    currItemPageNumber=$(xml).find("content").index($(xml).find("content[src='" + currPage + "']")) + 1;
    //$('#tbPage').html(currItemPageNumber + ' of ' + totalItemPageNumber);
    
    totalPageNumber=$(xml).find('content').length;
    currPageNumber=$(xml).find("content").index($(xml).find("content[src='" + currPage + "']")) + 1;
    $('#tbPage').html(currPageNumber + ' of ' + totalPageNumber);
    if(currPageNumber==1){
        $('#iPreviousDisable').show();
        $('#iPrevious').hide();
    } else{
        $('#iPreviousDisable').hide();
        $('#iPrevious').show();
        $('#iPrevious').attr('href','../../../'+$(xml).find("content").eq(currPageNumber-2).attr('src'));
    }
    
    if(currPageNumber==totalPageNumber){
        $('#iNextDisable').show();
        $('#iNext').hide();
    } else{
        $('#iNextDisable').hide();
        $('#iNext').show();
        $('#iNext').attr('href','../../../' + $(xml).find("content").eq(currPageNumber).attr('src'));
    }
}

function MM_openBrWindow(winName,features) { //v2.0
	var theURL='../../../web/textonly_version/index/page_1.htm'
  	window.open(theURL,winName,features);
}
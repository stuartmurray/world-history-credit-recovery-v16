/* indexOf for older browsers */
if (!Array.indexOf) {
  Array.prototype.indexOf = function (obj, start) {
    for (var i = (start || 0); i < this.length; i++) {
      if (this[i] == obj) {
        return i;
      }
    }
    return -1;
  }
}
/* end indexOf */

$(document).ready(function(){
	//gives a width of 650 to discovery ed textversions
	$('div[data-icon="icon_discoveryedlogo"]').parents('.htmltext_version').addClass('sixfifty');
	
	//shrinks the size of the d-link container ir there's a textversion

	$('a[data-title="Show Text Version"]').addClass('twenty');

	
	
	
	//added 7/8/14 Michael Shur - stops all audio on a tab click
	$('.tab').on( "click", function() {
		$('.mejs-button').each(function() {
			if ($(this).hasClass('mejs-pause')){
				$(this).click();
			}
		});
	});
	
	$('.clickReveal').find('.title').on( "click", function() {
		$('.mejs-button').each(function() {
			if ($(this).hasClass('mejs-pause')){
				$(this).click();
			}
		});
	});
	
	$('.accordion').find('.title').on( "click", function() {
		$('.mejs-button').each(function() {
			if ($(this).hasClass('mejs-pause')){
				$(this).click();
			}
		});
	});

	if(typeof isexternal === 'undefined' && typeof islearningobject === 'undefined'){
		
		// Populate Breadcrumb
		$('.breadcrumbs_course').html(FLVS.settings.course_title);
		
		//$('.breadcrumbs_module').html("MODULE "+(Number(current_module) + 1));
		$('.breadcrumbs_module').html(FLVS.Sitemap.module[current_module].title);
		
		// Navigation Position
		var pos = $('#menu_inner').offset();
		$('#nav_menu').css('left',pos.left+'px');
		
		// Position Popup Menu
		$(window).bind('resize',function(){
			var pos = $('#menu_inner').offset();
			$('#nav_menu').css('left',pos.left+'px');	
		});
		
		// Create Popup Menu
		createMenu();
		
		// Event for Menu Button
		$('.menubtn, .menubtn_mobile').click(function(){
				$('.nav_menu_lessons').hide();
				
				
				if(!$('#nav_menu').is(':visible')){
					$('body').append('<div class="menu_backdrop">&nbsp;</div>');
					$('.menu_backdrop').click(function(){
							$('#nav_menu').fadeToggle('fast');
							$(this).remove();
					});
				} else {
							$('.menu_backdrop').remove();
				}
				
				$('#nav_menu').fadeToggle('fast');
				
		});
		
		// Event for Showing Menu Lessons
		$('.modlink').click(function(){
			var resolution = $(document).width();		// variable to get resolution
			
			// Menu for iphones
			//if (resolution < 640 && $(this).next().css('display') != "none"){			
			if (resolution < 760 && $(this).next().css('display') != "none"){			
				$('.nav_menu_lessons').hide();
			}
			else {						
				$('.nav_menu_lessons').hide();
				$(this).next().stop().fadeIn('fast');
			}
		});
		
	}
/*
	//loads background of video gallery - Michael Shur 5/21/2014
	else if (typeof isvideolibrary !== "undefined"){		
		$('body').removeAttr('style').fadeIn();
	}
*/	
	// Puts the image caption inside of a block below the image.
	$('.image-container').each(function(){
				var imgsize = $(this).find('img').width();
				$(this).find('span.copyright').css('max-width',(imgsize - 30) + 'px');
				$(this).find('span.caption').css('max-width', (imgsize - 30) + 'px');
	});


	/******** added James Palmer - resets forms in Firefox ***********/
	// added 12/6/2013 James Palmer - resets fill in the Blanks for Firefox
	$.fn.resetForm = function() {
		return this.each(function(){
		  this.reset();
		});
	}
	if (typeof isAssessment == 'undefined' || isAssessment != true) {
		$('a[title=Next]').click(function(){
			$('div').find('form').resetForm();
		});
		
		$('a[title=Prev]').click(function(){
			$('div').find('form').resetForm();
		});
		
		$('a[title=Restart]').click(function(){
			$('div').find('form').resetForm();
		});
	};
	/******** added James Palmer - resets forms in Firefox ***********/
});


function createMenu(){
	var menu = '<ul class="nav_menu_modules">';
	for(var i=0; i<FLVS.Sitemap.module.length; i++){
		if (FLVS.Sitemap.module[i].visible == 'true') { // JP added 10/15/2013
			menu += '<li>';
			menu += '<a href="javascript:void(0);" class="modlink">'+FLVS.Sitemap.module[i].title+'</a>';
			
			// Lessons
			menu += '<ul class="nav_menu_lessons mod'+(i + 1)+'">';
			var submenu = '';
			for(var j=0; j<FLVS.Sitemap.module[i].lesson.length; j++){
				var link = FLVS.Sitemap.module[i].lesson[j].section[0].page[0].href;
				
				
				submenu += '<li>';
				if(j == 0 || j== 2 || j==4 || j== 6 || j== 8 || j== 10 || j== 12){
					submenu += '<a href="'+link+'"><span class="lesson_num">'+FLVS.Sitemap.module[i].lesson[j].num+'</span>';
				} else {
					submenu += '<a href="'+link+'" class="odd"><span class="lesson_num">'+FLVS.Sitemap.module[i].lesson[j].num+'</span>';
				}
				
				var minutes = "mins";
				if(Number(FLVS.Sitemap.module[i].lesson[j].time) < 2){
					minutes = "min";
				}
				var points = "pts";
				if(Number(FLVS.Sitemap.module[i].lesson[j].points) < 2){
					points = "pt";
				}
				
				submenu += '<span class="lesson_title">'+FLVS.Sitemap.module[i].lesson[j].title+'</span><span class="lesson_nfo">'+FLVS.Sitemap.module[i].lesson[j].time+' '+minutes+' | '+FLVS.Sitemap.module[i].lesson[j].points+' '+points+'</span></a>';
  				submenu += '</li>';
			}
			menu += submenu;
			menu += '</ul>';
			
			menu += '</li>';
		}
	}
	menu += '</ul>';
	
	// Remove all modlinks from nav_menu_lessons
	$('#nav_menu').append(menu);
	$('.nav_menu_lessons .modlink').remove();
}

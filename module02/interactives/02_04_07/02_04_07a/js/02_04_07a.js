﻿$(document).ready(function () {
    $('.customPopDiv').hide();
    $('.customPopDiv .copyrights').html('Public Domain');
    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
    });

    $('map .area').unbind('click').bind('click', function () {
        $('.customPopDiv').show();

        var getId = $(this).attr('id');
        if (getId == 'area1') {
           $('.customPopDiv .copyrights').css("margin-top","244px");
            $('.customPopDiv .head1').html('The Church at Calke Abbey in Derbyshire, Great Britain');
            $('.customPopDiv .content1').html('<img src="interactives/02_04_07/02_04_07a/img/02_04_07_art.jpg"  class="left" alt=" " />This stained glass is located in the church at Calke Abbey in Derbyshire, Great Britain. This type of window is formed from cut pieces of colored glass that are assembled to form a picture. Stained glass was first used during the early Middle Ages and continues to be used to this day. It is found mostly in churches and often greatly enhances the beauty of these buildings. The pictures depicted in stained glass sparkle when light shines through them. Their striking beauty was a powerful force for encouraging devotion during the Medieval times.');
        } else if (getId == 'area2') {
            $('.customPopDiv .copyrights').css("margin-top", "142px");
            $('.customPopDiv .head1').html("Hunter's Picnic");
            $('.customPopDiv .content1').html('<img src="interactives/02_04_07/02_04_07a/img/02_04_07_tapestry.jpg"  class="left" alt=" " />Created during the late 1400s, this tapestry shows peasants having a picnic. A tapestry is made of different color threads woven together to form a picture. This art form dates back to around 1500 BCE. During the Middle Ages in Europe, tapestries were often produced by workshops. They depicted scenes from the Bible, history, mythology, or daily life. Besides being beautiful, tapestries also had a practical purpose. Since they were made of wool, they served as a type of insulation for castles and churches.');
            $('.pop_illuminated').show();
        } else if (getId == 'area3') {
            $('.customPopDiv .copyrights').css("margin-top", "244px");
            $('.customPopDiv .head1').html('Book of Hours');
            $('.customPopDiv .content1').html('<img src="interactives/02_04_07/02_04_07a/img/02_04_07_book of hours.jpg"class="left" alt=" " />This page from a French <i>Book of Hours</i> is a great example of an illuminated manuscript. The text is overwhelmed by an elaborate border, a detailed illustration, and an enormous initial letter.');
        } else if (getId == 'area4') {
            $('.customPopDiv .copyrights').css("margin-top", "244px");
            $('.customPopDiv .head1').html('First Page of the Divine Comedy');
            $('.customPopDiv .content1').html('<img src="interactives/02_04_07/02_04_07a/img/02_04_07_literature.jpg"  class="left" alt=" " />This image shows the first page of <i>The Divine Comedy</i> written by Dante Aligheri around 1308. The work is an epic poem that dealt with life after death and features Dante himself as the main character. The poem is divided into three parts: Inferno (Hell), Purgatorio (Purgatory), and Paradiso (Paradise). Dante called the entire work a comedy because it ends happily.');
        }

    });

});
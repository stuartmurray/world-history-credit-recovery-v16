﻿$(document).ready(function () {
    $('.customPopDiv').hide();


    $('.clickedArea').unbind('click').bind('click', function () {
        $('.customPopDiv').show();
        var getId = $(this).attr('id');
        if (getId == 'area1' || getId == 'area11') {
            $('.customPopDiv .head1').html('King');
            $('.customPopDiv .content1').html('Kings were at the top of the social hierarchical system. They had ultimate control over their land and could grant land to others.');
        } else if (getId == 'area2' || getId == 'area22') {
            $('.customPopDiv .head1').html('Nobles');
            $('.customPopDiv .content1').html('Nobles received grants of land from the kings. These nobles then use the land to generate income and provide money, in the form of taxes, to the king.');
        } else if (getId == 'area3' || getId == 'area33') {
            
            $('.customPopDiv .head1').html('Knights');
            $('.customPopDiv .content1').html('Knights were the warrior class. They fought the king’s wars and were sometimes rewarded with land of their own.');
        } else if (getId == 'area4' || getId == 'area44') {
            $('.customPopDiv .head1').html('Peasants');
            $('.customPopDiv .content1').html('Peasants and serfs were the toiling classes. They worked the land and performed the labor required to keep the system functioning.');
        }
   });




});


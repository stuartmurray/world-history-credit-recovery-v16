﻿var count = 1;
var currentSlide = 0;
$(document).ready(function () {
    preload(['interactives/02_00_01/02_00_01a/imgs/02_00_01_a.jpg',
        'interactives/02_00_01/02_00_01a/imgs/02_00_01_b.jpg',
        'interactives/02_00_01/02_00_01a/imgs/02_00_01_c.jpg', ]);
    countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click'); //start audio player mep_0
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden" ); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/02_00_01/02_00_01a/audio/02_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        $('<img/>')[0].src = this;
    });
}

function showBkgnd() {
    if (count < 5) {
        $('.ui_interactive').css('background', 'url("interactives/02_00_01/02_00_01a/imgs/02_00_01_a.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    }
    else if (count >= 5 && count < 8) {
        $('.ui_interactive').css('background', 'url("interactives/02_00_01/02_00_01a/imgs/02_00_01_b.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    } else {
        $('.ui_interactive').css('background', 'url("interactives/02_00_01/02_00_01a/imgs/02_00_01_c.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    }
};


function countmessage() {
	showBkgnd();
	switch (currentSlide){
	case 0:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! It´s good to see you again. Soo-jin is here, too.");
		break;
	case 1:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! It´s good to see you again. Soo-jin is here, too.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! Did you hear that we´re going to visit a real Medieval Festival?");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Today we are traveling to Medieval Europe. As you can see, medieval towns were small by today´s standards. But they were also busy bustling places.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That´s right. As you´ll learn in this module, Europe underwent major economic changes during this time. This was due to many things: the exposure to new peoples during the Crusades, the exposure to new goods overseas, and the establishment of guilds back home. The effects of these economic changes were on full display in every large town´s market.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("The markets of towns in Medieval Europe would have been the center of action for people living there. They would have been able to purchase or trade for vegetables, bread, eggs, and animals for meat.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("At this time, Europeans would have had access to more variety than they had before. But, by today´s standards, they would not have had much variety at all. Common items like tomatoes and potatoes were not available to Europeans at this time. These fruits and vegetables come from the Americas, not Europe.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 7:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("But it was at this time that Europeans were starting to see new foods and other goods. It was likely in the markets that people were first introduced to these products.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 8:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That´s right, Ali. But it wasn´t just new foods coming into Europe at this time; people were also being exposed to new peoples and cultures.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 9:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("There were people going to and from the battles in the Holy Land and the Crusades, peasants fleeing from the Black Death, and soldiers helping their lords conquer more territory and expand their kingdoms.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 10:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("But there were other people too: peddlers selling their goods, blacksmiths pounding iron, local priests and monks moving from village to village, and people making pilgrimages through Europe.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 11:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("The Middle Ages was actually a very busy time. Europe was finding its way after the collapse of the Roman Empire.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 12:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That´s right, Ali. But soon Europeans would start what´s called the Renaissance. But we´ll look into that later.");
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
    }
}
﻿$(document).ready(function () {
    $('.customPopDiv').hide();
    $('.customPopDiv .copyrights').html('Public Domain');
    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
    });
    $('#area11').hide();
    $('#area12').hide();
    $('#area31').hide();
    $('#area32').hide();

    $('.area1').unbind('click').bind('click', function () {
        $('.customPopDiv').show();

        var getId = $(this).attr('id');
        if (getId == 'area4') {
            $('#area11').hide();
            $('#area12').hide();
            $('#area31').hide();
            $('#area32').hide();
            $('.customPopDiv .headbuttom').html('');
            $('.customPopDiv .copyrights').css("margin-top", "215px");
            $('.customPopDiv .head1').html('Political Structure');
            $('.customPopDiv .imghead').html('Bushido').css("margin-left", "137px"); ;
            $('.customPopDiv .content1').html('The word Bushido means "Way of the Warrior-Knight." This uniquely Japanese code of conduct was an integral part of the samurai life. The code developed, with the influence of both Shinto and Buddhism, between the 9th and 12th centuries. The Bushido code is typified by the following seven virtues:<br/>Rectitude<br/>Courage<br/>Benevolence<br/>Respect<br/>Honesty<br/>Honor<br/>Loyalty<br/>');
            $('.customPopDiv .img').html('<img src="interactives/02_07_10/02_07_10a/img/02_07_10_06.jpg" alt=" " />')
        } else if (getId == 'area2') {
            $('#area11').hide();
            $('#area12').hide();
            $('#area31').hide();
            $('#area32').hide();
            $('.customPopDiv .headbuttom').html('');
            $('.customPopDiv .copyrights').css("margin-top", "212px");
            $('.customPopDiv .head1').html("Literature");
            $('.customPopDiv .imghead').html('A yamato-e painting from Tale of Genji').css("margin-left", "45px");
            $('.customPopDiv .content1').html('Long before the novel became popular in Europe, the Japanese were using this form of literature. The novel Tale of Genji recounts the story of high courtiers in Heian Japan. It tells the story of the good-looking son of the Japanese emperor. It describes both methods of romance and the customs of the aristocracy of this era in Japan. For this reason it continues to provide a unique insight into this era of Japanese courtly life. This yamato-e painting depicts a scene from Chapter Five of the novel. The work was painted in the yamato-e style in the 17th century by Japanese artist Tosa Mitsuoki (1617-1691 CE).');
            $('.customPopDiv .img').html('<img src="interactives/02_07_10/02_07_10a/img/02_07_10_03.jpg" alt=" " />')
        } else if (getId == 'area3' || getId == 'area31') {
            $('#area11').hide();
            $('#area12').hide();
            $('#area31').show().attr('disable',true).css('opacity','0.3').css('cursor','default');
            $('#area32').show().attr('disable', false).css('opacity', '1').css('cursor', 'pointer');

            $('.customPopDiv .headbuttom').html('<i>Select the <b>arrows</b> at the bottom right to advance the slides.</i>');
            $('.customPopDiv .copyrights').css("margin-top", "213px");
            $('.customPopDiv .head1').html('Religion');
            $('.customPopDiv .imghead').html('Zen Buddhism').css("margin-left", "127px");
            $('.customPopDiv .img').html('<img src="interactives/02_07_10/02_07_10a/img/02_07_10_04.jpg" alt=" " />')
            $('.customPopDiv .content1').html('Unlike the resistance some in Japan had toward traditional Buddhism, Zen Buddhism became very popular. Zen Buddhism plays down the role of a set of theoretical knowledge and encourages the practitioner to attain enlightenment through personal meditation. Zen Buddhism fit neatly with the customs and traditions of the samurai in Japan.');
        }
        else if (getId == 'area32') {
            $('#area11').hide();
            $('#area12').hide();
            $('#area31').show().attr('disable', false).css('opacity', '1').css('cursor', 'pointer');
            $('#area32').show().attr('disable', true).css('opacity', '0.3').css('cursor', 'default');
            $('.customPopDiv .headbuttom').html('<i>Select the <b>arrows</b> at the bottom right to advance the slides.</i>');
            $('.customPopDiv .imghead').html('Shinto').css("margin-left", "150px");
            $('.customPopDiv .copyrights').css("margin-top", "213px");
            $('.customPopDiv .head1').html('Religion');
            $('.customPopDiv .img').html('<img src="interactives/02_07_10/02_07_10a/img/02_07_10_05.jpg" alt=" " />')
            $('.customPopDiv .content1').html('The Shinto religion is indigenous to Japan. However, Shinto is less of a religion in the traditional sense than it is a collection of folklore, history, and mythology. Followers of Shinto hold that Japan was created by two gods: one male and one female. These gods were given a spear that they used to create the island of Japan. Because the islands were created directly by the gods, the link between the beauty of nature and the people of Japan is very strong in Shinto.');
        }
        else if (getId == 'area1' || getId == 'area11') {
            $('#area31').hide();
            $('#area32').hide();
            $('#area11').show().attr('disable', false).css('opacity', '0.3').css('cursor', 'default');
            $('#area12').show().attr('disable', true).css('opacity', '1').css('cursor', 'pointer');
            $('.customPopDiv .headbuttom').html('<i>Select the <b>arrows</b> at the bottom right to advance the slides.</i>');
            $('.customPopDiv .copyrights').css("margin-top", "213px");
            $('.customPopDiv .head1').html('Art');
            $('.customPopDiv .imghead').html('Zen Gardens').css("margin-left", "127px");
            $('.customPopDiv .content1').html("Zen Gardens are complex, highly-structured landscapes made up of elaborately combed sand, strategically placed rocks, and trimmed bushes and trees. The viewer is expected to meditate on the forms and the placement of the forms in the garden. Contemplation of their simple, perfect shape and placement allows the viewer's mind to enter a state of meditative peace.");
            $('.customPopDiv .img').html('<img src="interactives/02_07_10/02_07_10a/img/02_07_10_01.jpg" alt=" " />')
        } else if (getId == 'area12') {
            $('#area31').hide();
            $('#area32').hide();
            $('#area11').show().attr('disable', true).css('opacity', '1').css('cursor', 'pointer');
            $('#area12').show().attr('disable', false).css('opacity', '0.3').css('cursor', 'default');
            $('.customPopDiv .headbuttom').html('<i>Select the <b>arrows</b> at the bottom right to advance the slides.</i>') .css("margin-top","415px");
             $('.customPopDiv .img').html('<img src="interactives/02_07_10/02_07_10a/img/02_07_10_02.jpg" alt=" " />')
            $('.customPopDiv .copyrights').css("margin-top", "216px");
            $('.customPopDiv .head1').html('Art');
            $('.customPopDiv .imghead').html('Noh Theater').css("margin-left", "127px");
            $('.customPopDiv .content1').html('Noh theater was a major performance art during the Kamakura period. In fact, Noh is one of the oldest forms of theater in the world. The acting is very austere; they wear masks and their movements are prescribed and precise. The beauty of Noh was in its sparseness and its power as a cultural and historical metaphor. This is a modern performance of Noh. It still shows the stylized and austere nature of the performance. Notice the rigid mask worn by the actor. Musicians were also an integral part of the performance.');
        }

    });

});
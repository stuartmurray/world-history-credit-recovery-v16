var count = 1;
var currentSlide = 0;
$(document).ready(function () {

    countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click'); //start audio player mep_0
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden" ); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			// currentSlide = "0" + currentSlide; not using 01, 02, etc.
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/01_01_01/01_01_01a/audio/01_01_01a_aud"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function countmessage() {
	switch (currentSlide){
	case 0:
		break;
	case 1:
        $('#mep_'+(count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
		break;
    }
}


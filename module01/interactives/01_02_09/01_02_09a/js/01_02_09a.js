$(document).ready(function(e) {
	var dropCntAbraham = 0;
	var dropCntMoses = 0;
	var dropCntTemple = 0;
	var dropCntAfter = 0;
	var dropCntOne = 0;
	var dropCntSin = 0;
	var correctCounter = 0;
	var dropCounter = 0;
	dropCounter = $('.droppable').length;
	
	$('.droppable').bind('drop', function (event, ui) {
		if($(this).attr('data-label') == $(ui.draggable).attr('data-label')) {
		//alert(1);
			correctCounter++;
			if($.trim($(ui.draggable).text()) == "Abraham") {
					dropCntAbraham++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntAbraham--;
							correctCounter--;
						}
					});
					if(dropCntAbraham < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Abraham") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "Moses") {
					dropCntMoses++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntMoses--;
							correctCounter--;
						}
					});
					if(dropCntMoses < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Moses") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "Temple Mount") {
					dropCntTemple++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntTemple--;
							correctCounter--;
						}
					});
					if(dropCntTemple < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Temple Mount") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "Afterlife") {
					dropCntAfter++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntAfter--;
							correctCounter--;
						}
					});
					if(dropCntAfter < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Afterlife") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "One God") {
					dropCntOne++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntOne--;
							correctCounter--;
						}
					});
					if(dropCntOne < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "One God") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
		}
		else if($(this).attr('data-label2') == $(ui.draggable).attr('data-label')) {
			//alert(2);
			if($.trim($(ui.draggable).text()) == "Sin as an Act") {
				dropCntSin++;
				$(this).find('li').each(function() {
					if($(this).text() == $.trim($(ui.draggable).text())) {
						$(this).remove();
						dropCntSin--;
						correctCounter--;
					}
				});
				
				$(this).find('ul').append('<li>' + $.trim($(ui.draggable).text()) + '</li>');
				correctCounter++;
				$(this).addClass('ui-state-highlight');
				$(ui.draggable).draggable('disable').css('display', 'none');
				setTimeout(function() {
					$('.droppable').each(function(index, element) {
						$(this).removeClass('ui-state-highlight');
					});
				}, 750);
				if(dropCntSin < 2) {
					setTimeout(function() {
						$('.draggable').each(function() {
							if($.trim($(this).text()) == "Sin as an Act") {
								$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
								$(this).attr('aria-disabled', false);
								$(this).removeAttr('style');
								$(this).css('display', 'inline-block');
								$(this).draggable('enable');
							}
						}); 
					}, 500);
				}
			}
			else {
				$(this).find('ul').append('<li>' + $.trim($(ui.draggable).text()) + '</li>');
				correctCounter++;
				$(this).addClass('ui-state-highlight');
				$(ui.draggable).draggable('disable').css('display', 'none');
				setTimeout(function() {
					$('.droppable').each(function(index, element) {
						$(this).removeClass('ui-state-highlight');
					});
				}, 750);
				if($.trim($(ui.draggable).text()) == "Abraham") {
					dropCntAbraham++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntAbraham--;
							correctCounter--;
						}
					});
					if(dropCntAbraham < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Abraham") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "Moses") {
					dropCntMoses++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntMoses--;
							correctCounter--;
						}
					});
					if(dropCntMoses < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Moses") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "Temple Mount") {
					dropCntTemple++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntTemple--;
							correctCounter--;
						}
					});
					if(dropCntTemple < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Temple Mount") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "Afterlife") {
					dropCntAfter++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntAfter--;
							correctCounter--;
						}
					});
					if(dropCntAfter < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "Afterlife") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
				else if($.trim($(ui.draggable).text()) == "One God") {
					dropCntOne++;
					$(this).find('li').each(function() {
						if($(this).text() == $.trim($(ui.draggable).text())) {
							$(this).remove();
							dropCntOne--;
							correctCounter--;
						}
					});
					if(dropCntOne < 3) {
						setTimeout(function() {
							$('.draggable').each(function() {
								if($.trim($(this).text()) == "One God") {
									$(this).removeClass('ui-draggable-disabled').removeClass('ui-state-disabled');
									$(this).attr('aria-disabled', false);
									$(this).removeAttr('style');
									$(this).css('display', 'inline-block');
									$(this).draggable('enable');
								}
							}); 
						}, 500);
					}
				}
			}
		}
		
		if(correctCounter >= 37) {
			$('.ui-dialog').show().css({'margin-left': '140px', 'margin-top': '102px'});
		}
		else {
			$('.ui-dialog').hide();
		}
	});
});
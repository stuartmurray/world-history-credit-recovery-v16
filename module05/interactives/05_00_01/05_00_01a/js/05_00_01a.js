﻿var count = 0;
var currentSlide = 0;
$(document).ready(function () {
    preload(['interactives/05_00_01/05_00_01a/imgs/05_00_01_img_01.jpg',
        'interactives/05_00_01/05_00_01a/imgs/05_00_01_img_02.jpg',
        'interactives/05_00_01/05_00_01a/imgs/05_00_01_img_03.jpg', ]);

	countmessage();
    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click');
		currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
		$('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .next').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
		$('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
		setTimeout( function () {
			$('.ui-navigation .prev').css("visibility", "visible");
		}, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
		count = 0;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
	} else {
		slideNumber();
		countmessage();
		if (currentSlide < 10){
			currentSlide = "0" + currentSlide;
		}
		$('#mep_' + (count)).find('.mejs-mute').mouseover();
		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/05_00_01/05_00_01a/audio/05_00_01_"+currentSlide+".mp3");
		$('#mep_' + (count)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
		$('#mep_' + (count)).find('.mejs-play').find('button').trigger('click');
	}
}

function slideNumber() {
	var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
	var step2 = step1.indexOf(" of"); // 1
	var step3 = step1.substr(0,step2);
	currentSlide = parseInt(step3);
	count = currentSlide-1;	
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        $('<img/>')[0].src = this;
    });
}

function showBkgnd() {
    if (currentSlide < 4) {
        $('.ui_interactive').css('background', 'url("interactives/05_00_01/05_00_01a/imgs/05_00_01_img_01.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    }
    else if (currentSlide >= 4 && currentSlide < 10) {
        $('.ui_interactive').css('background', 'url("interactives/05_00_01/05_00_01a/imgs/05_00_01_img_02.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    } else {
        $('.ui_interactive').css('background', 'url("interactives/05_00_01/05_00_01a/imgs/05_00_01_img_03.jpg") no-repeat');
        $('.ui_interactive').attr("title", "History time travelers Ali and Soo-Jin");
    }
};

function countmessage() {
	showBkgnd();
	switch (currentSlide){
	case 0:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It's good to see you again. Ali is here too.");
		break;
	case 1:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hi, students! It's good to see you again. Ali is here too.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 2:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! Sorry, it's kind of loud in this coffee shop so I'll speak up!");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 3:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("We're here in Paris and as you can see, this cafe looks a lot like the coffee shops today. So many people and different conversations going on. I see a group of people at that table with a bunch of maps and tools, and another one next to them with what look like pamphlets about religion.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 4:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("Makes sense. We're right in the middle of the Age of Enlightenment here in France so I know there are a lot of new ideas circulating. How exciting to see it happening first-hand!");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 5:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("In this module you will be learning about the Age of Enlightenment and what events led to it.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 6:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("That's right, Soo-jin, the Age of Enlightenment was a huge turning point in history, and we have the great thinkers of this time to thank for a lot of the freedoms we have today.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 7:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Really? Like what?");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 8:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("Well, one of the most important innovations of this time was the idea of freedom of speech, freedom of religion, civil liberties, the social contract, and the right to have a government that represents you rather than ruling you. We take all of these things for granted, but the people of the Enlightenment did not.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 9:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("It's funny, Ali. Everyone seems so comfortable talking about whatever they want. It's so hard to believe that free speech is something that hasn't been acceptable forever.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 10:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("The Enlightenment got its start in France, but other countries really took it and ran with it too.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 11:
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("That's right, Ali. Almost all of Europe changed during this time, and the civilians started to demand power and respect rather than just following the rules of the King. However, there were still some places that couldn't give up absolute monarchy.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 12:
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");

        $('.messageBox > span').html("Even those countries started to embrace new cultures and ways of thinking, although not the way France, England, and Germany did. There were so many discoveries that shaped our lives today. I think I can hear the men next to us talking about astronomy now!");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
		break;
	case 13:
        $('.messageBox > div').addClass("calloutman");

        $('.messageBox > span').html("So now that Soo-jin and I have gotten you excited about learning more about The Enlightenment, let's go and take a closer look.");
        $('#mep_' + count).find('.mejs-play').find('button').trigger('click');
    }
}
var count = 0;
$(document).ready(function(e) {
    $('.ui-dialog-titlebar-close').on('click', function() {
		$('.tooltip').fadeOut('slow');
    });

    $('.hiddenglobal').hide();
    $(".aud").bind('click', function () {
        disableHide1();

        var audid = $(this).attr("id");
        audid = audid.slice(7, 8);
        $('.hiddentool' + audid).show();
    });
    $('.ui-dialog-titlebar-close').bind('click', function () {
        $('.hiddenglobal').hide();
        disableHide1();
    });
});

function disableHide1() {

    $('.hiddenglobal').hide();

    $('.mejs-button').each(function () {
        if ($(this).hasClass('mejs-pause')) {
            $(this).find('button').trigger('click');
        }
    });
}
﻿var count = 0;
var listNames = "Ptolemy";
var currentSlide = 1;

$(document).ready(function () {
	showSlide(currentSlide);

	$('.ui-dialog-titlebar-close').on('click', function () {
		//Go to dialog box - look for aria-labelledby
		var clicked = $(this).parent().parent().attr("aria-labelledby"); //ui-dialog-title-popUp1
		var cName = $("#"+clicked).html(); // Ptolemy
		
		if (listNames.indexOf(cName) < 0) { 		// If not in list
			currentSlide++;
			listNames = listNames + "," + cName;
		} else if (currentSlide == 1){ 				//If first item must move to second
			currentSlide++;
		}
		showSlide(currentSlide);
	});

});


function showSlide(n){
	if (n < 8){ 																
		bkgrnd = "interactives/05_03_03/05_03_03a/img/05_03_03a_0"+n+".png"; 	// For each item replace the image
	} else {
		bkgrnd = "interactives/05_03_03/05_03_03a/img/05_03_03a_07.png";		// Keep last image - finished
	}
	
	if (n == 1) {										
		$('#wheelMap').css("src", bkgrnd );
	} else {
		$('#wheelMap').attr("src", bkgrnd );
	}
	
	switch (n){
	case 2:
		$("#img2").html('<area alt="" title="" href="#" shape="poly" coords="184,123,266,83,270,89,272,101,276,112,277,123,278,128,277,137,277,145,277,153,277,163,275,172,274,179,272,187,269,188,269,199,181,159,186,145,187,134,185,154" />' )
		break;
	case 3:
		$("#img3").html('<area alt="" title="" href="#" shape="poly" coords="153,184,163,181,172,174,178,167,183,164,264,203,262,213,251,223,243,227,242,238,230,247,220,256,205,263,193,267,174,274" />');
		break;
	case 4:
		$("#img4").html('<area alt="" title="" href="#" shape="poly" coords="56,248,115,175,122,179,131,182,140,184,148,184,151,184,170,274,155,278,135,279,114,275,96,270,83,263,71,258" />');
		break;
	case 5:
		$("#img5").html('<area alt="" title="" href="#" shape="poly" coords="2,141,96,143,99,154,100,162,106,169,111,177,54,247,38,236,28,222,18,207,11,190,6,171,4,162" />');
		break;
	case 6:
		$("#img6").html('<area alt="" title="" href="#" shape="poly" coords="111,108,104,116,99,126,95,135,95,141,4,139,3,127,4,116,6,106,10,100,14,88,17,81,21,73,28,61,37,52,43,46,50,41,58,36" />');
		break;
	case 7:
		$("#img7").html('<area alt="" title="" href="#" shape="poly" coords="60,31,66,26,72,20,79,16,84,14,91,11,98,9,106,6,114,5,127,4,140,4,151,4,162,4,169,6,148,97,139,96,131,96,122,97,117,104,113,104" />');
		break;
	};
	
	hideAllAreas(n);
}

function hideAllAreas(n) {
	for (i=1; i<8; i++){
		if (i <= n){
			$("#img"+i).css("display", "block");
		} else {
			$("#img"+i).css("display", "none");
		}
	}
}

